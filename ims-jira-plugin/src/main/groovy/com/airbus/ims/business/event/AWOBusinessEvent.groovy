package com.airbus.ims.business.event


import com.airbus.ims.util.ConstantKeys;
import com.airbus.ims.util.Toolbox;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;

/**
 * Projects : AWO
 * Description : Creation/Edit on AWO
 * Events : Issue Updated, AWO updated
 * isDisabled : false
 */
public class AWOBusinessEvent {

    public static void editAWO(MutableIssue issue, ApplicationUser user, Logger log) {

        //Calculate effective costs;
        calculateEffectiveCostsEditMode(issue, user, log);

        //Set cost data on PWO-2;
        setCostDataOnPWO2EditMode(issue, user, log);

        //Set costs data on Trigger;
        setCostsDataOnTriggerEditMode(issue, user, log);
    }

    private static void setCostsDataOnTriggerEditMode(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_EFFECTIVECOST);
        CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_EFFECTIVECOST);
        CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_EFFECTIVECOST);
        CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DOMAINPWO1);
        CustomField IAEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_EFFECTIVECOST);

        CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_EFFECTIVECOST);
        CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_EFFECTIVECOST);
        CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_EFFECTIVECOST);
        CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TRIGGERTOTALEFFECTIVECOSTS);


        //Variables

        Double PartsDataEffectiveCost = Double.valueOf(0);
        Double MaintPlanningEffectiveCost = Double.valueOf(0);
        Double MaintEffectiveCost = Double.valueOf(0);
        Double IAEffectiveCost = Double.valueOf(0);

        Double AuthEffectiveCostOfTrigger = Double.valueOf(0);
        Double FormEffectiveCostOfTrigger = Double.valueOf(0);
        Double IntEffectiveCostOfTrigger = Double.valueOf(0);
        Double TriggerEffectiveCost = Double.valueOf(0);

        String DomainPWO1;
        String DomainPWO2;

        Issue issueTrigger;

        Collection<Issue> LinkedIssuesOfAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfAWO : LinkedIssuesOfAWO) {
            if (linkedIssueOfAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                Collection<Issue> LinkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfAWO, user).getAllIssues();
                for (Issue LinkedIssueOfCurrentPWO1 : LinkedIssuesOfCurrentPWO1) {
                    if (LinkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                        Collection<Issue> LinkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfCurrentPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO0) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                Collection<Issue> LinkedIssuesOfCurrentCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                for (Issue linkedIssueOfCurrentCluster : LinkedIssuesOfCurrentCluster) {
                                    if (linkedIssueOfCurrentCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                        issueTrigger = linkedIssueOfCurrentCluster;
                                        log.debug("trigger is : " + issueTrigger.getKey());
                                        Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(issueTrigger, user).getAllIssues();
                                        for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                                log.debug("Cluster is : " + linkedIssueOfTrigger.getKey());
                                                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                        if (linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostField) != null) {
                                                            PartsDataEffectiveCost = PartsDataEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostField);
                                                        }
                                                        if (linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostField) != null) {
                                                            MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostField);
                                                        }
                                                        if (linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostField) != null) {
                                                            MaintEffectiveCost = MaintEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostField);
                                                        }

                                                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                                        for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                                DomainPWO1 = String.valueOf(linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field));
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField) != null) {
                                                                    IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField);
                                                                } else {
                                                                    IAEffectiveCost = Double.valueOf(0);
                                                                }

                                                                if (DomainPWO1 == "Parts Data") {
                                                                    PartsDataEffectiveCost = PartsDataEffectiveCost + IAEffectiveCost;
                                                                }
                                                                if (DomainPWO1 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + IAEffectiveCost;
                                                                }
                                                                if (DomainPWO1 == "Maintenance") {
                                                                    MaintEffectiveCost = MaintEffectiveCost + IAEffectiveCost;
                                                                }

                                                                Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                        DomainPWO2 = String.valueOf(linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field));
                                                                        Collection<Issue> SubTasksOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                                        for (Issue subTaskOfPWO2 : SubTasksOfPWO2) {
                                                                            if (subTaskOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                                                                                if (subTaskOfPWO2.getCustomFieldValue(Auth_EffectiveCostField) != null) {
                                                                                    AuthEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Auth_EffectiveCostField);
                                                                                } else {
                                                                                    AuthEffectiveCostOfTrigger = Double.valueOf(0);
                                                                                }

                                                                                if (DomainPWO2 == "Parts Data") {
                                                                                    PartsDataEffectiveCost = PartsDataEffectiveCost + AuthEffectiveCostOfTrigger;
                                                                                }
                                                                                if (DomainPWO2 == "Maintenance Planning") {
                                                                                    MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + AuthEffectiveCostOfTrigger;
                                                                                }
                                                                                if (DomainPWO2 == "Maintenance") {
                                                                                    MaintEffectiveCost = MaintEffectiveCost + AuthEffectiveCostOfTrigger;
                                                                                }
                                                                            } else if (subTaskOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                                                                                if (subTaskOfPWO2.getCustomFieldValue(Form_EffectiveCostField) != null) {
                                                                                    FormEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Form_EffectiveCostField);
                                                                                } else {
                                                                                    FormEffectiveCostOfTrigger = Double.valueOf(0);
                                                                                }

                                                                                if (DomainPWO2 == "Parts Data") {
                                                                                    PartsDataEffectiveCost = PartsDataEffectiveCost + FormEffectiveCostOfTrigger;
                                                                                }
                                                                                if (DomainPWO2 == "Maintenance Planning") {
                                                                                    MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + FormEffectiveCostOfTrigger;
                                                                                }
                                                                                if (DomainPWO2 == "Maintenance") {
                                                                                    MaintEffectiveCost = MaintEffectiveCost + FormEffectiveCostOfTrigger;
                                                                                }
                                                                            } else if (subTaskOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                                                                                if (subTaskOfPWO2.getCustomFieldValue(Int_EffectiveCostField) != null) {
                                                                                    IntEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Int_EffectiveCostField);
                                                                                } else {
                                                                                    IntEffectiveCostOfTrigger = Double.valueOf(0);
                                                                                }

                                                                                if (DomainPWO2 == "Parts Data") {
                                                                                    PartsDataEffectiveCost = PartsDataEffectiveCost + IntEffectiveCostOfTrigger;
                                                                                }
                                                                                if (DomainPWO2 == "Maintenance Planning") {
                                                                                    MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + IntEffectiveCostOfTrigger;
                                                                                }
                                                                                if (DomainPWO2 == "Maintenance") {
                                                                                    MaintEffectiveCost = MaintEffectiveCost + IntEffectiveCostOfTrigger;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        TriggerEffectiveCost = PartsDataEffectiveCost + MaintPlanningEffectiveCost + MaintEffectiveCost;
                                        MutableIssue mutableIssue = issueManager.getIssueObject(issueTrigger.getKey());
                                        mutableIssue.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost);
                                        mutableIssue.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost);
                                        mutableIssue.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost);
                                        mutableIssue.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost);

                                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                        issueIndexingService.reIndex(mutableIssue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void setCostDataOnPWO2EditMode(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField AWO_AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHEFFECTIVECOST);
        CustomField AWO_AuthVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFEFFECTIVECOST);
        CustomField AWO_FormEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMEFFECTIVECOST);
        CustomField AWO_FormVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFEFFECTIVECOST);
        CustomField AWO_TransEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_EFFECTIVECOST);
        CustomField AWO_IntEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTEFFECTIVECOST);

        CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_EFFECTIVECOST);
        CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_EFFECTIVECOST);
        CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_EFFECTIVECOST);


        //Variables

        Double Auth_EffectiveCost = Double.valueOf(0);
        Double Form_EffectiveCost = Double.valueOf(0);
        Double Int_EffectiveCost = Double.valueOf(0);

        Collection<Issue> LinkedIssuesOfCurrentAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        MutableIssue mutableIssue;
        for (Issue linkedIssueOfCurrentAWO : LinkedIssuesOfCurrentAWO) {
            if (linkedIssueOfCurrentAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                Collection<Issue> linkedIssuesOfCurrentPWO21 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO21 : linkedIssuesOfCurrentPWO21) {
                    if (linkedIssueOfCurrentPWO21.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                        if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField) != null) {
                            Auth_EffectiveCost = Auth_EffectiveCost + (Double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField);
                        }
                        if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField) != null) {
                            Auth_EffectiveCost = Auth_EffectiveCost + (Double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField);
                        }
                    }
                }
                mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentAWO.getKey());
                mutableIssue.setCustomFieldValue(Auth_EffectiveCostField, Auth_EffectiveCost);
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            } else if (linkedIssueOfCurrentAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                Collection<Issue> linkedIssuesOfCurrentPWO22 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO22 : linkedIssuesOfCurrentPWO22) {
                    if (linkedIssueOfCurrentPWO22.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                        if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField) != null) {
                            Form_EffectiveCost = Form_EffectiveCost + (Double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField);
                        }
                        if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField) != null) {
                            Form_EffectiveCost = Form_EffectiveCost + (Double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField);
                        }
                        if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField) != null) {
                            Form_EffectiveCost = Form_EffectiveCost + (Double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField);
                        }
                    }
                }
                mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentAWO.getKey());
                mutableIssue.setCustomFieldValue(Form_EffectiveCostField, Form_EffectiveCost);
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            } else if (linkedIssueOfCurrentAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                Collection<Issue> linkedIssuesOfCurrentPWO23 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO23 : linkedIssuesOfCurrentPWO23) {
                    if (linkedIssueOfCurrentPWO23.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                        if (linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField) != null) {
                            Int_EffectiveCost = Int_EffectiveCost + (Double) linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField);
                        }
                    }
                }
                mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentAWO.getKey());
                mutableIssue.setCustomFieldValue(Int_EffectiveCostField, Int_EffectiveCost);
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            }
        }
    }

    private static void calculateEffectiveCostsEditMode(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField AWO_AuthWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHWU);
        CustomField AWOAuthNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHNBWU);
        CustomField AWO_AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHEFFECTIVECOST);
        CustomField AWO_AuthVerifWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFWU);
        CustomField AWO_AuthVerifNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFNBWU);
        CustomField AWO_AuthVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFEFFECTIVECOST);
        CustomField AWO_FormWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMWU);
        CustomField AWO_FormNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMNBWU);
        CustomField AWO_FormEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMEFFECTIVECOST);
        CustomField AWO_FormVerifWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFWU);
        CustomField AWO_FormVerifNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFNBWU);
        CustomField AWO_FormVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFEFFECTIVECOST);
        CustomField AWO_TransWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TRANSWU);
        CustomField AWO_TransNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TRANSNBWU);
        CustomField AWO_TransEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_EFFECTIVECOST);
        CustomField AWO_IntWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTWU);
        CustomField AWO_IntNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTNBWU);
        CustomField AWO_IntEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTEFFECTIVECOST);
        CustomField WUValueField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WUVALUE);

        CustomField Auth_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_ADDITIONALCOST);
        CustomField Auth_Verif_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIF_ADDITIONALCOST);
        CustomField Form_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_ADDITIONALCOST);
        CustomField Form_Verif_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIF_ADDITIONALCOST);
        CustomField Trans_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TRANS_ADDITIONALCOST);
        CustomField Int_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_ADDITIONALCOST);


        Issue issueAWOAuthWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_AuthWUField, issue, log);
        Issue issueAWOAuthVerifhWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_AuthVerifWUField, issue, log);
        Issue issueAWOFormWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_FormWUField, issue, log);
        Issue issueAWOFormVerifhWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_FormVerifWUField, issue, log);
        Issue issueAWOTransWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_TransWUField, issue, log);
        Issue issueAWOInthWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_IntWUField, issue, log);

        //Variables
        Double AWOAuthEffectiveCost = Double.valueOf(0);
        Double AWOAuthVerifEffectiveCost = Double.valueOf(0);
        Double AWOFormEffectiveCost = Double.valueOf(0);
        Double AWOFormVerifEffectiveCost = Double.valueOf(0);
        Double AWOTransEffectiveCost = Double.valueOf(0);
        Double AWOIntEffectiveCost = Double.valueOf(0);

        Double AWOAuthWUValue;
        Double AWOAuthVerifWUValue;
        Double AWOFormWUValue;
        Double AWOFormVerifWUValue;
        Double AWOTransWUValue;
        Double AWOIntWUValue;

        Double AWOAuthNbWu;
        Double AWOAuthVerifNbWu;
        Double AWOFormNbWu;
        Double AWOFormVerifNbWu;
        Double AWOTransNbWu;
        Double AWOIntNbWu;

        if (issueAWOAuthWU != null && issue.getCustomFieldValue(AWOAuthNbWuField) != null) {
            AWOAuthWUValue = (Double) issueAWOAuthWU.getCustomFieldValue(WUValueField);
            AWOAuthNbWu = (Double) issue.getCustomFieldValue(AWOAuthNbWuField);
            AWOAuthEffectiveCost = AWOAuthWUValue * AWOAuthNbWu;
        }
        if (issue.getCustomFieldValue(Auth_AdditionalCostField) != null) {
            AWOAuthEffectiveCost = AWOAuthEffectiveCost + (Double) issue.getCustomFieldValue(Auth_AdditionalCostField);
        }

        if (issueAWOAuthVerifhWU != null && issue.getCustomFieldValue(AWO_AuthVerifNbWuField) != null) {
            AWOAuthVerifWUValue = (Double) issueAWOAuthVerifhWU.getCustomFieldValue(WUValueField);
            AWOAuthVerifNbWu = (Double) issue.getCustomFieldValue(AWO_AuthVerifNbWuField);
            AWOAuthVerifEffectiveCost = AWOAuthVerifWUValue * AWOAuthVerifNbWu;
        }
        if (issue.getCustomFieldValue(Auth_Verif_AdditionalCostField) != null) {
            AWOAuthVerifEffectiveCost = AWOAuthVerifEffectiveCost + (Double) issue.getCustomFieldValue(Auth_Verif_AdditionalCostField);
        }

        if (issueAWOFormWU != null && issue.getCustomFieldValue(AWO_FormNbWuField) != null) {
            AWOFormWUValue = (Double) issueAWOFormWU.getCustomFieldValue(WUValueField);
            AWOFormNbWu = (Double) issue.getCustomFieldValue(AWO_FormNbWuField);
            AWOFormEffectiveCost = AWOFormWUValue * AWOFormNbWu;
        }
        if (issue.getCustomFieldValue(Form_AdditionalCostField) != null) {
            AWOFormEffectiveCost = AWOFormEffectiveCost + (Double) issue.getCustomFieldValue(Form_AdditionalCostField);
        }

        if (issueAWOFormVerifhWU != null && issue.getCustomFieldValue(AWO_FormVerifNbWuField) != null) {
            AWOFormVerifWUValue = (Double) issueAWOFormVerifhWU.getCustomFieldValue(WUValueField);
            AWOFormVerifNbWu = (Double) issue.getCustomFieldValue(AWO_FormVerifNbWuField);
            AWOFormVerifEffectiveCost = AWOFormVerifWUValue * AWOFormVerifNbWu;
        }
        if (issue.getCustomFieldValue(Form_Verif_AdditionalCostField) != null) {
            AWOFormVerifEffectiveCost = AWOFormVerifEffectiveCost + (Double) issue.getCustomFieldValue(Form_Verif_AdditionalCostField);
        }

        if (issueAWOTransWU != null && issue.getCustomFieldValue(AWO_TransNbWuField) != null) {
            AWOTransWUValue = (Double) issueAWOTransWU.getCustomFieldValue(WUValueField);
            AWOTransNbWu = (Double) issue.getCustomFieldValue(AWO_TransNbWuField);
            AWOTransEffectiveCost = AWOTransWUValue * AWOTransNbWu;
        }
        if (issue.getCustomFieldValue(Trans_AdditionalCostField) != null) {
            AWOTransEffectiveCost = AWOTransEffectiveCost + (Double) issue.getCustomFieldValue(Trans_AdditionalCostField);
        }

        if (issueAWOInthWU != null && issue.getCustomFieldValue(AWO_IntNbWuField) != null) {
            AWOIntWUValue = (Double) issueAWOInthWU.getCustomFieldValue(WUValueField);
            AWOIntNbWu = (Double) issue.getCustomFieldValue(AWO_IntNbWuField);
            AWOIntEffectiveCost = AWOIntWUValue * AWOIntNbWu;
        }
        if (issue.getCustomFieldValue(Int_AdditionalCostField) != null) {
            AWOIntEffectiveCost = AWOIntEffectiveCost + (Double) issue.getCustomFieldValue(Int_AdditionalCostField);
        }

        issue.setCustomFieldValue(AWO_AuthEffectiveCostField, AWOAuthEffectiveCost);
        issue.setCustomFieldValue(AWO_AuthVerifEffectiveCostField, AWOAuthVerifEffectiveCost);
        issue.setCustomFieldValue(AWO_FormEffectiveCostField, AWOFormEffectiveCost);
        issue.setCustomFieldValue(AWO_FormVerifEffectiveCostField, AWOFormVerifEffectiveCost);
        issue.setCustomFieldValue(AWO_TransEffectiveCostField, AWOTransEffectiveCost);
        issue.setCustomFieldValue(AWO_IntEffectiveCostField, AWOIntEffectiveCost);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    public static void createAWO(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField IACompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_COMPANY);
        CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_COMPANYALLOWED);


        //Variables

        List<Group> CompanyAllowedList = new ArrayList<Group>();

        //Calculate effective costs;
        calculateEffectiveCostsCreationMode(issue, user, log);

        //Set cost data on PWO-2;
        setCostDataOnPWO2CreationMode(issue, user, log);

        //Set costs data on Trigger;
        setCostDataOnTriggerCreationMode(issue, user, log);


        Collection<Issue> LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfAWOForCompany : LinkedIssuesOfAWOForCompany) {
            if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != ConstantKeys.AIRBUS) {
                    String IA_Company_Value = String.valueOf(linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField));
                    String IA_Company_Formatted = "Partner_" + IA_Company_Value;

                    Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted);

                    if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                        CompanyAllowedList.add(IA_Company_Management_Formatted_Group);
                    }
                }
            }
        }
        if (CompanyAllowedList != null) {
            issue.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false); //JIRA7;
            issueIndexingService.reIndex(issue); //JIRA7;
        }
    }

    private static void calculateEffectiveCostsCreationMode(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField AWO_AuthWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHWU);
        CustomField AWOAuthNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHNBWU);
        CustomField AWO_AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHEFFECTIVECOST);
        CustomField AWO_AuthVerifWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFWU);
        CustomField AWO_AuthVerifNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFNBWU);
        CustomField AWO_AuthVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFEFFECTIVECOST);
        CustomField AWO_FormWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMWU);
        CustomField AWO_FormNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMNBWU);
        CustomField AWO_FormEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMEFFECTIVECOST);
        CustomField AWO_FormVerifWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFWU);
        CustomField AWO_FormVerifNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFNBWU);
        CustomField AWO_FormVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFEFFECTIVECOST);
        CustomField AWO_TransWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TRANSWU);
        CustomField AWO_TransNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TRANSNBWU);
        CustomField AWO_TransEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_EFFECTIVECOST);
        CustomField AWO_IntWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTWU);
        CustomField AWO_IntNbWuField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTNBWU);
        CustomField AWO_IntEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTEFFECTIVECOST);
        CustomField WUValueField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WUVALUE);

        CustomField Auth_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_ADDITIONALCOST);
        CustomField Auth_Verif_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIF_ADDITIONALCOST);
        CustomField Form_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_ADDITIONALCOST);
        CustomField Form_Verif_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIF_ADDITIONALCOST);
        CustomField Trans_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TRANS_ADDITIONALCOST);
        CustomField Int_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_ADDITIONALCOST);

        Issue issueAWOAuthWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_AuthWUField, issue, log);
        Issue issueAWOAuthVerifhWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_AuthVerifWUField, issue, log);
        Issue issueAWOFormWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_FormWUField, issue, log);
        Issue issueAWOFormVerifhWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_FormVerifWUField, issue, log);
        Issue issueAWOTransWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_TransWUField, issue, log);
        Issue issueAWOInthWU = Toolbox.getIssueFromNFeedFieldSingle(AWO_IntWUField, issue, log);

        //Variables
        Double AWOAuthEffectiveCost = Double.valueOf(0);
        Double AWOAuthVerifEffectiveCost = Double.valueOf(0);
        Double AWOFormEffectiveCost = Double.valueOf(0);
        Double AWOFormVerifEffectiveCost = Double.valueOf(0);
        Double AWOTransEffectiveCost = Double.valueOf(0);
        Double AWOIntEffectiveCost = Double.valueOf(0);

        Double AWOAuthWUValue;
        Double AWOAuthVerifWUValue;
        Double AWOFormWUValue;
        Double AWOFormVerifWUValue;
        Double AWOTransWUValue;
        Double AWOIntWUValue;

        Double AWOAuthNbWu;
        Double AWOAuthVerifNbWu;
        Double AWOFormNbWu;
        Double AWOFormVerifNbWu;
        Double AWOTransNbWu;
        Double AWOIntNbWu;

        if (issueAWOAuthWU != null && issue.getCustomFieldValue(AWOAuthNbWuField) != null) {
            AWOAuthWUValue = (Double) issueAWOAuthWU.getCustomFieldValue(WUValueField);
            AWOAuthNbWu = (Double) issue.getCustomFieldValue(AWOAuthNbWuField);
            AWOAuthEffectiveCost = AWOAuthWUValue * AWOAuthNbWu;
        }
        if (issue.getCustomFieldValue(Auth_AdditionalCostField) != null) {
            AWOAuthEffectiveCost = AWOAuthEffectiveCost + (Double) issue.getCustomFieldValue(Auth_AdditionalCostField);
        }

        if (issueAWOAuthVerifhWU != null && issue.getCustomFieldValue(AWO_AuthVerifNbWuField) != null) {
            AWOAuthVerifWUValue = (Double) issueAWOAuthVerifhWU.getCustomFieldValue(WUValueField);
            AWOAuthVerifNbWu = (Double) issue.getCustomFieldValue(AWO_AuthVerifNbWuField);
            AWOAuthVerifEffectiveCost = AWOAuthVerifWUValue * AWOAuthVerifNbWu;
        }
        if (issue.getCustomFieldValue(Auth_Verif_AdditionalCostField) != null) {
            AWOAuthVerifEffectiveCost = AWOAuthVerifEffectiveCost + (Double) issue.getCustomFieldValue(Auth_Verif_AdditionalCostField);
        }

        if (issueAWOFormWU != null && issue.getCustomFieldValue(AWO_FormNbWuField) != null) {
            AWOFormWUValue = (Double) issueAWOFormWU.getCustomFieldValue(WUValueField);
            AWOFormNbWu = (Double) issue.getCustomFieldValue(AWO_FormNbWuField);
            AWOFormEffectiveCost = AWOFormWUValue * AWOFormNbWu;
        }
        if (issue.getCustomFieldValue(Auth_AdditionalCostField) != null) {
            AWOFormEffectiveCost = AWOFormEffectiveCost + (Double) issue.getCustomFieldValue(Form_AdditionalCostField);
        }

        if (issueAWOFormVerifhWU != null && issue.getCustomFieldValue(AWO_FormVerifNbWuField) != null) {
            AWOFormVerifWUValue = (Double) issueAWOFormVerifhWU.getCustomFieldValue(WUValueField);
            AWOFormVerifNbWu = (Double) issue.getCustomFieldValue(AWO_FormVerifNbWuField);
            AWOFormVerifEffectiveCost = AWOFormVerifWUValue * AWOFormVerifNbWu;
        }
        if (issue.getCustomFieldValue(Form_Verif_AdditionalCostField) != null) {
            AWOFormVerifEffectiveCost = AWOFormVerifEffectiveCost + (Double) issue.getCustomFieldValue(Form_Verif_AdditionalCostField);
        }

        if (issueAWOTransWU != null && issue.getCustomFieldValue(AWO_TransNbWuField) != null) {
            AWOTransWUValue = (Double) issueAWOTransWU.getCustomFieldValue(WUValueField);
            AWOTransNbWu = (Double) issue.getCustomFieldValue(AWO_TransNbWuField);
            AWOTransEffectiveCost = AWOTransWUValue * AWOTransNbWu;
        }
        if (issue.getCustomFieldValue(Trans_AdditionalCostField) != null) {
            AWOTransEffectiveCost = AWOTransEffectiveCost + (Double) issue.getCustomFieldValue(Trans_AdditionalCostField);
        }

        if (issueAWOInthWU != null && issue.getCustomFieldValue(AWO_IntNbWuField) != null) {
            AWOIntWUValue = (Double) issueAWOInthWU.getCustomFieldValue(WUValueField);
            AWOIntNbWu = (Double) issue.getCustomFieldValue(AWO_IntNbWuField);
            AWOIntEffectiveCost = AWOIntWUValue * AWOIntNbWu;
        }
        if (issue.getCustomFieldValue(Int_AdditionalCostField) != null) {
            AWOIntEffectiveCost = AWOIntEffectiveCost + (Double) issue.getCustomFieldValue(Int_AdditionalCostField);
        }

        issue.setCustomFieldValue(AWO_AuthEffectiveCostField, AWOAuthEffectiveCost);
        issue.setCustomFieldValue(AWO_AuthVerifEffectiveCostField, AWOAuthVerifEffectiveCost);
        issue.setCustomFieldValue(AWO_FormEffectiveCostField, AWOFormEffectiveCost);
        issue.setCustomFieldValue(AWO_FormVerifEffectiveCostField, AWOFormVerifEffectiveCost);
        issue.setCustomFieldValue(AWO_TransEffectiveCostField, AWOTransEffectiveCost);
        issue.setCustomFieldValue(AWO_IntEffectiveCostField, AWOIntEffectiveCost);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    private static void setCostDataOnTriggerCreationMode(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField

        CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_EFFECTIVECOST);
        CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_EFFECTIVECOST);
        CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_EFFECTIVECOST);
        CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DOMAINPWO1);
        CustomField IAEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_EFFECTIVECOST);

        CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_EFFECTIVECOST);
        CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_EFFECTIVECOST);
        CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_EFFECTIVECOST);
        CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TRIGGERTOTALEFFECTIVECOSTS);


        //Variables
        Issue issueTrigger = null;

        Double PartsDataEffectiveCost = Double.valueOf(0);
        Double MaintPlanningEffectiveCost = Double.valueOf(0);
        Double MaintEffectiveCost = Double.valueOf(0);
        Double IAEffectiveCost = Double.valueOf(0);

        Double AuthEffectiveCostOfTrigger = Double.valueOf(0);
        Double FormEffectiveCostOfTrigger = Double.valueOf(0);
        Double IntEffectiveCostOfTrigger = Double.valueOf(0);
        Double TriggerEffectiveCost = Double.valueOf(0);

        String DomainPWO1;
        String DomainPWO2;

        Collection<Issue> LinkedIssuesOfAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfAWO : LinkedIssuesOfAWO) {
            if (linkedIssueOfAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                Collection<Issue> LinkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfAWO, user).getAllIssues();
                for (Issue LinkedIssueOfCurrentPWO1 : LinkedIssuesOfCurrentPWO1) {
                    if (LinkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                        Collection<Issue> LinkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfCurrentPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO0) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                Collection<Issue> LinkedIssuesOfCurrentCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                for (Issue linkedIssueOfCurrentCluster : LinkedIssuesOfCurrentCluster) {
                                    if (linkedIssueOfCurrentCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                        issueTrigger = linkedIssueOfCurrentCluster;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (issueTrigger != null) {
            Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(issueTrigger, user).getAllIssues();
            for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                    log.debug("Cluster is : " + linkedIssueOfTrigger.getKey());
                    Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                    for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                        if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                            if (linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostField) != null) {
                                PartsDataEffectiveCost = PartsDataEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostField);
                            }
                            if (linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostField) != null) {
                                MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostField);
                            }
                            if (linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostField) != null) {
                                MaintEffectiveCost = MaintEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostField);
                            }

                            Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                            for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                    DomainPWO1 = String.valueOf(linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field));
                                    if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField) != null) {
                                        IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField);
                                    } else {
                                        IAEffectiveCost = Double.valueOf(0);
                                    }

                                    if (DomainPWO1 == "Parts Data") {
                                        PartsDataEffectiveCost = PartsDataEffectiveCost + IAEffectiveCost;
                                    }
                                    if (DomainPWO1 == "Maintenance Planning") {
                                        MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + IAEffectiveCost;
                                    }
                                    if (DomainPWO1 == "Maintenance") {
                                        MaintEffectiveCost = MaintEffectiveCost + IAEffectiveCost;
                                    }

                                    Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                    for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                            DomainPWO2 = String.valueOf(linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field));
                                            Collection<Issue> SubTasksOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                            for (Issue subTaskOfPWO2 : SubTasksOfPWO2) {
                                                if (subTaskOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                                                    if (subTaskOfPWO2.getCustomFieldValue(Auth_EffectiveCostField) != null) {
                                                        AuthEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Auth_EffectiveCostField);
                                                    } else {
                                                        AuthEffectiveCostOfTrigger = Double.valueOf(0);
                                                    }

                                                    if (DomainPWO2 == "Parts Data") {
                                                        PartsDataEffectiveCost = PartsDataEffectiveCost + AuthEffectiveCostOfTrigger;
                                                    }
                                                    if (DomainPWO2 == "Maintenance Planning") {
                                                        MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + AuthEffectiveCostOfTrigger;
                                                    }
                                                    if (DomainPWO2 == "Maintenance") {
                                                        MaintEffectiveCost = MaintEffectiveCost + AuthEffectiveCostOfTrigger;
                                                    }
                                                } else if (subTaskOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                                                    if (subTaskOfPWO2.getCustomFieldValue(Form_EffectiveCostField) != null) {
                                                        FormEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Form_EffectiveCostField);
                                                    } else {
                                                        FormEffectiveCostOfTrigger = Double.valueOf(0);
                                                    }

                                                    if (DomainPWO2 == "Parts Data") {
                                                        PartsDataEffectiveCost = PartsDataEffectiveCost + FormEffectiveCostOfTrigger;
                                                    }
                                                    if (DomainPWO2 == "Maintenance Planning") {
                                                        MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + FormEffectiveCostOfTrigger;
                                                    }
                                                    if (DomainPWO2 == "Maintenance") {
                                                        MaintEffectiveCost = MaintEffectiveCost + FormEffectiveCostOfTrigger;
                                                    }
                                                } else if (subTaskOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                                                    if (subTaskOfPWO2.getCustomFieldValue(Int_EffectiveCostField) != null) {
                                                        IntEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Int_EffectiveCostField);
                                                    } else {
                                                        IntEffectiveCostOfTrigger = Double.valueOf(0);
                                                    }

                                                    if (DomainPWO2 == "Parts Data") {
                                                        PartsDataEffectiveCost = PartsDataEffectiveCost + IntEffectiveCostOfTrigger;
                                                    }
                                                    if (DomainPWO2 == "Maintenance Planning") {
                                                        MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + IntEffectiveCostOfTrigger;
                                                    }
                                                    if (DomainPWO2 == "Maintenance") {
                                                        MaintEffectiveCost = MaintEffectiveCost + IntEffectiveCostOfTrigger;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            TriggerEffectiveCost = PartsDataEffectiveCost + MaintPlanningEffectiveCost + MaintEffectiveCost;
            MutableIssue mutableIssue = issueManager.getIssueObject(issue.getKey());
            mutableIssue.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost);
            mutableIssue.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost);
            mutableIssue.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost);
            mutableIssue.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        }
    }

    private static void setCostDataOnPWO2CreationMode(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField AWO_AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHEFFECTIVECOST);
        CustomField AWO_AuthVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFEFFECTIVECOST);
        CustomField AWO_FormEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMEFFECTIVECOST);
        CustomField AWO_FormVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFEFFECTIVECOST);
        CustomField AWO_TransEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_EFFECTIVECOST);
        CustomField AWO_IntEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTEFFECTIVECOST);

        CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_EFFECTIVECOST);
        CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_EFFECTIVECOST);
        CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_EFFECTIVECOST);


        //Variables
        Double Auth_EffectiveCost = Double.valueOf(0);
        Double Form_EffectiveCost = Double.valueOf(0);
        Double Int_EffectiveCost = Double.valueOf(0);

        MutableIssue mutableIssue;

        Collection<Issue> LinkedIssuesOfCurrentAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentAWO : LinkedIssuesOfCurrentAWO) {
            if (linkedIssueOfCurrentAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                Collection<Issue> linkedIssuesOfCurrentPWO21 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO21 : linkedIssuesOfCurrentPWO21) {
                    if (linkedIssueOfCurrentPWO21.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                        if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField) != null) {
                            Auth_EffectiveCost = Auth_EffectiveCost + (Double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField);
                        }
                        if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField) != null) {
                            Auth_EffectiveCost = Auth_EffectiveCost + (Double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField);
                        }
                    }
                }
                mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentAWO.getKey());
                mutableIssue.setCustomFieldValue(Auth_EffectiveCostField, Auth_EffectiveCost);
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            } else if (linkedIssueOfCurrentAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                Collection<Issue> linkedIssuesOfCurrentPWO22 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO22 : linkedIssuesOfCurrentPWO22) {
                    if (linkedIssueOfCurrentPWO22.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                        if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField) != null) {
                            Form_EffectiveCost = Form_EffectiveCost + (Double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField);
                        }
                        if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField) != null) {
                            Form_EffectiveCost = Form_EffectiveCost + (Double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField);
                        }
                        if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField) != null) {
                            Form_EffectiveCost = Form_EffectiveCost + (Double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField);
                        }
                    }
                }
                mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentAWO.getKey());
                mutableIssue.setCustomFieldValue(Form_EffectiveCostField, Form_EffectiveCost);
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            } else if (linkedIssueOfCurrentAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                Collection<Issue> linkedIssuesOfCurrentPWO23 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO23 : linkedIssuesOfCurrentPWO23) {
                    if (linkedIssueOfCurrentPWO23.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                        if (linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField) != null) {
                            Int_EffectiveCost = Int_EffectiveCost + (Double) linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField);
                        }
                    }
                }
                mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentAWO.getKey());
                mutableIssue.setCustomFieldValue(Int_EffectiveCostField, Int_EffectiveCost);
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            }
        }
    }

    public static void additionOrDeletionOfLinkBetweenAWOAndPWO(MutableIssue issue, MutableIssue issueSource, ApplicationUser user, Logger log) throws IndexException, CreateException, ClassNotFoundException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();


        log.debug("issue : " + issue.getKey());
        log.debug("source issue : " + issueSource.getKey());

        //CustomField;
        CustomField IACompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_COMPANY);
        CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_COMPANYALLOWED);
        CustomField AuthCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_COMPANY);
        CustomField AuthVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFCOMPANY);
        CustomField FormCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_COMPANY);
        CustomField FormVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFCOMPANY);
        CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_COMPANY);
        CustomField AuthVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTHVERIFCOMPANY);
        CustomField FormVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORMVERIFCOMPANY);
        CustomField IntCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INTCOMPANY);

        List<Group> CompanyAllowedList = new ArrayList<Group>();
        List<Group> AuthVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> FormVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> IntCompanyAllowedList = new ArrayList<Group>();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        if (issue.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
            if (PWOIT.contains(issueSource.getIssueTypeId())) {
                Collection<Issue> LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
                for (Issue linkedIssueOfAWOForCompany : LinkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                            String IA_Company_Value = String.valueOf(linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField));
                            String IA_Company_Formatted = "Partner_" + IA_Company_Value;

                            Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted);

                            if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(IA_Company_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != "AIRBUS") {
                            String Auth_Company_Value = String.valueOf(linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField));
                            String Auth_Company_Formatted = "Partner_" + Auth_Company_Value;

                            Group Auth_Company_Management_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted);

                            if (!CompanyAllowedList.contains(Auth_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != "AIRBUS") {
                            String Auth_VerifCompany_Value = String.valueOf(linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField));
                            String Auth_VerifCompany_Formatted = "Partner_" + Auth_VerifCompany_Value;

                            Group Auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Auth_VerifCompany_Formatted);

                            if (!CompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group);
                            }
                            if (!AuthVerifCompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                AuthVerifCompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != "AIRBUS") {
                            String Form_Company_Value = String.valueOf(linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField));
                            String Form_Company_Formatted = "Partner_" + Form_Company_Value;

                            Group Form_Company_Management_Formatted_Group = groupManager.getGroup(Form_Company_Formatted);

                            if (!CompanyAllowedList.contains(Form_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != "AIRBUS") {
                            String Form_VerifCompany_Value = String.valueOf(linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField));
                            String Form_VerifCompany_Formatted = "Partner_" + Form_VerifCompany_Value;

                            Group Form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Form_VerifCompany_Formatted);

                            if (!CompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group);
                            }
                            if (!FormVerifCompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                FormVerifCompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != "AIRBUS") {
                            String Int_Company_Value = String.valueOf(linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField));
                            String Int_Company_Formatted = "Partner_" + Int_Company_Value;

                            Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted);

                            if (!CompanyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                                IntCompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                            }
                        }
                    }
                }
                if (CompanyAllowedList != null) {
                    issue.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList);
                    if (AuthVerifCompanyAllowedList != null) {
                        issue.setCustomFieldValue(AuthVerifCompanyAllowedField, AuthVerifCompanyAllowedList);
                    }
                    if (FormVerifCompanyAllowedList != null) {
                        issue.setCustomFieldValue(FormVerifCompanyAllowedField, FormVerifCompanyAllowedList);
                    }
                    if (IntCompanyAllowedList != null) {
                        issue.setCustomFieldValue(IntCompanyAllowedField, IntCompanyAllowedList);
                    }
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false); //JIRA7;
                    issueIndexingService.reIndex(issue); //JIRA7;
                    CompanyAllowedList.clear();
                    AuthVerifCompanyAllowedList.clear();
                    FormVerifCompanyAllowedList.clear();
                    IntCompanyAllowedList.clear();
                }
            }
        }
    }


    public static void setCostDataOnPWO2x(MutableIssue issue, MutableIssue issueSource, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        log.debug("issue : " + issue.getKey());
        log.debug("issueSource : " + issueSource.getKey());

        //CustomField
        CustomField AWO_AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHEFFECTIVECOST);
        CustomField AWO_AuthVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_AUTHVERIFEFFECTIVECOST);
        CustomField AWO_FormEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMEFFECTIVECOST);
        CustomField AWO_FormVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFEFFECTIVECOST);
        CustomField AWO_TransEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_EFFECTIVECOST);
        CustomField AWO_IntEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_INTEFFECTIVECOST);


        CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_EFFECTIVECOST);
        CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_EFFECTIVECOST);
        CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_EFFECTIVECOST);



        double Auth_EffectiveCost = 0;
        double Form_EffectiveCost = 0;
        double Int_EffectiveCost = 0;

        //Business;

        //Set cost data on PWO-2;
        log.debug("issue : " + issue.getKey());
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO21 && issueSource.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
            Collection<Issue> LinkedIssuesOfCurrentPWO21 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO21 : LinkedIssuesOfCurrentPWO21) {
                if (linkedIssueOfCurrentPWO21.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                    if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField) != null) {
                        Auth_EffectiveCost = Auth_EffectiveCost + (double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField);
                    }
                    if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField) != null) {
                        Auth_EffectiveCost = Auth_EffectiveCost + (double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField);
                    }
                }
            }
            issue.setCustomFieldValue(Auth_EffectiveCostField, Auth_EffectiveCost);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO22 && issueSource.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
            Collection<Issue> LinkedIssuesOfCurrentPWO22 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO22 : LinkedIssuesOfCurrentPWO22) {
                if (linkedIssueOfCurrentPWO22.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField) != null) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField);
                    }
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField) != null) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField);
                    }
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField) != null) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField);
                    }
                }
            }
            issue.setCustomFieldValue(Form_EffectiveCostField, Form_EffectiveCost);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO23 && issueSource.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
            Collection<Issue> LinkedIssuesOfCurrentPWO23 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO23 : LinkedIssuesOfCurrentPWO23) {
                if (linkedIssueOfCurrentPWO23.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                    if (linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField) != null) {
                        Int_EffectiveCost = Int_EffectiveCost + (double) linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField);
                    }
                }
            }
            issue.setCustomFieldValue(Int_EffectiveCostField, Int_EffectiveCost);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    public static void createLinkBetweenNewAWOAndDU(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException, CreateException {

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSNAPPLI);

        Issue DUissue = null;

        log.debug("issue  =  " + issue.getKey());

        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_AWO_DUDM_ATB || issue.getIssueTypeId() == ConstantKeys.ID_IT_AWO_DUDM_ASD ){
            Collection<Issue> LinkedIssuesOfCurrentAWO  =  issueLinkManager.getLinkCollection(issue,user).getAllIssues();
            for (Issue linkedIssueOfCurrentAWO : LinkedIssuesOfCurrentAWO){
                //If linked issue is Cluster;
                if (linkedIssueOfCurrentAWO.getProjectId()  ==  ConstantKeys.ID_PROJECT_AWO){
                    log.debug("AWO linked  =  " + linkedIssueOfCurrentAWO.getKey());
                    Collection<Issue> LinkedIssuesOfLinkedAWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO,user).getAllIssues();
                    for (Issue linkedIssueOfLinkedAWO : LinkedIssuesOfLinkedAWO){
                        if (linkedIssueOfLinkedAWO.getProjectId() ==  ConstantKeys.ID_PROJECT_DU){
                            log.debug("DU linked : " + linkedIssueOfLinkedAWO.getKey());
                            Collection<Issue> LinkedIssuesOfDU  =  issueLinkManager.getLinkCollection(linkedIssueOfLinkedAWO,user).getInwardIssues("Copies");
                            for (Issue linkedIssueOfDU : LinkedIssuesOfDU){
                                log.debug("Outward link of DU  =  " + linkedIssueOfDU.getKey());
                                if (linkedIssueOfDU.getProjectId() ==  ConstantKeys.ID_PROJECT_DU){
                                    log.debug("DU copied and �to link : " + linkedIssueOfDU.getKey());
                                    DUissue = linkedIssueOfDU;
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            Collection<Issue> LinkedIssuesOfCurrentAWO  =  issueLinkManager.getLinkCollection(issue,user).getAllIssues();
            for (Issue linkedIssueOfCurrentAWO : LinkedIssuesOfCurrentAWO){
                //If linked issue is Cluster;
                if (linkedIssueOfCurrentAWO.getProjectId()  ==  ConstantKeys.ID_PROJECT_AWO){
                    log.debug("AWO linked  =  " + linkedIssueOfCurrentAWO.getKey());
                    Collection<Issue> LinkedIssuesOfLinkedAWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO,user).getAllIssues();
                    for (Issue linkedIssueOfLinkedAWO : LinkedIssuesOfLinkedAWO){
                        if (linkedIssueOfLinkedAWO.getProjectId() ==  ConstantKeys.ID_PROJECT_DU){
                            log.debug("DU copied and to link : " + linkedIssueOfLinkedAWO.getKey());
                            DUissue = linkedIssueOfLinkedAWO;
                        }
                    }
                }
            }
        }
        if (DUissue != null){
            issueLinkManager.createIssueLink(issue.getId(),DUissue.getId(),ConstantKeys.ID_LINK_RELATES, 1L, user);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            MutableIssue DUIssueMutable  =  issueManager.getIssueObject(DUissue.getKey());
            DUIssueMutable.setCustomFieldValue(FirstSNAppliField, ConstantKeys.FIRSTSNAPPLI);
            issueManager.updateIssue(user, DUIssueMutable, EventDispatchOption.ISSUE_UPDATED, false);
            issueIndexingService.reIndex(DUIssueMutable);
        }


        log.debug("issue : " + issue.getKey());
        Collection<Issue> LinkedIssuesOfCurrentAWO2  =  issueLinkManager.getLinkCollection(issue,user).getAllIssues();
        for (Issue linkedIssueOfCurrentAWO2 : LinkedIssuesOfCurrentAWO2){
            if (linkedIssueOfCurrentAWO2.getProjectId()  ==  ConstantKeys.ID_PROJECT_AWO){
                Collection<Issue> LinkedIssuesOfLinkedAWO2  =  issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO2,user).getAllIssues();
                for (Issue linkedIssueOfLinkedAWO2 : LinkedIssuesOfLinkedAWO2){
                    if (linkedIssueOfLinkedAWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO1){
                        issueLinkManager.createIssueLink(linkedIssueOfLinkedAWO2.getId(), issue.getId(),ConstantKeys.ID_LINK_CONCERN, 1L, user);
                        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(issue);
                    }
                }
            }
        }
    }

    public static void synchronizeCWXQuicksearch(MutableIssue mutableIssue, ApplicationUser user, Logger log) throws IndexException {
        //Managers
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();


        CustomField cwxField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(ConstantKeys.ID_CF_CWX_QUICKSEARCH);
        List<String> items = Arrays.asList(mutableIssue.getCustomFieldValue(cwxField).toString().trim().split(","));

        List<String> common = new ArrayList<String>();
        issueLinkManager.getOutwardLinks(mutableIssue.getId()).each{issueLink ->
            if(items.contains(issueLink.getDestinationObject().getKey())){
                common.add(issueLink.getDestinationObject().getKey());
            }
        }

        mutableIssue.setCustomFieldValue(cwxField, common.join(","));
        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(mutableIssue);
    }

    public static void copyTitleToSummary(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager;
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField ReferenceField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REFERENCE);

        //Business;
        log.debug("Creation/Edit of a WU/Obj. Freeze/AWO, copy reference to summary");
        log.debug("issue : " + issue.getKey());

        String Reference = String.valueOf(issue.getCustomFieldValue(ReferenceField));

        issue.setSummary(Reference);
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }
}
