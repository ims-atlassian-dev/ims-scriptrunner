package com.airbus.ims.business.event

import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.exception.CreateException
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.jql.parser.JqlParseException
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger
import org.ofbiz.core.entity.GenericValue

class DUBusinessEvent {

    static boolean setDUToActive(IssueEvent event, ApplicationUser user, Logger log) throws IndexException, CreateException, JqlParseException, SearchException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue)event.getIssue();


        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSNAPPLI);
        CustomField TypeOfChangeField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TYPEOFCHANGE);
        CustomField GoIfField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_GOIF);

        boolean result = false;

        log.debug("issue : "+issue);
        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());

        boolean isCreation = Toolbox.workloadIsUpdated(changeLog, itemsChangedAtCreation);
        log.debug("is creation : "+isCreation);
        log.debug("First SN applied : "+issue.getCustomFieldValue(FirstSNAppliField));
        if(isCreation && issue.getCustomFieldValue(FirstSNAppliField).toString()== ConstantKeys.FIRSTSNAPPLI){//Creation
            log.debug("creation of DU");
            Collection<Issue> LinkedIssuesOfCurrentDU = issueLinkManager.getLinkCollection(issue,user).getAllIssues();
            for (Issue linkedIssueOfCurrentDU : LinkedIssuesOfCurrentDU){
                if (linkedIssueOfCurrentDU.getProjectId() == ConstantKeys.ID_PROJECT_AWO){
                    log.debug("AWO found");
                    result = linkedIssueOfCurrentDU.getCustomFieldValue(TypeOfChangeField).toString() != ConstantKeys.DELETED || linkedIssueOfCurrentDU.getCustomFieldValue(GoIfField).toString().equalsIgnoreCase(ConstantKeys.YES)
                }
            }
        }
        else{
            result = false;
        }

        return result;
    }

    static void linkNewlyCreatedDUToAWO(IssueEvent event, ApplicationUser user, Logger log) throws IndexException, CreateException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue) event.getIssue();

        //CustomField;
        CustomField AWOReleasedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_RELEASED);
        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSNAPPLI);

        //Variables;
        Issue issuesAWOtoLink;
        String issueAWOId;
        String FirstSnAppli = "Calculation of status";

        //Business;
        log.debug("issue : " + issue.getKey());
        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());

        boolean isCreation = Toolbox.workloadIsUpdated(changeLog, itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation && issue.getCustomFieldValue(FirstSNAppliField).toString() == "New issue") {//Creation;
            issueAWOId = String.valueOf(issue.getCustomFieldValue(AWOReleasedField));
            if (issueAWOId != null) {
                log.debug("content of field AWO release : " + issueAWOId);
                issuesAWOtoLink = issueManager.getIssueObject(Long.parseLong(issueAWOId));
                log.debug("issue AWO to link : " + issuesAWOtoLink);
                //if (!issuesAWOtoLink.getCustomFieldValue(GoIfField)){
                issueLinkManager.createIssueLink(issuesAWOtoLink.getId(), issue.getId(), ConstantKeys.ID_LINK_RELATES, 1L, user);
                issue.setCustomFieldValue(FirstSNAppliField, FirstSnAppli);
                issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false);
                issueIndexingService.reIndex(issue);
            }
        }
    }
}
