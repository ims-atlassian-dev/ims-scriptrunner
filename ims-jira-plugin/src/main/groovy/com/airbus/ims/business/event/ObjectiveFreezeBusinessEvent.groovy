package com.airbus.ims.business.event


import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.exception.CreateException
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger

import java.sql.Timestamp

/**
 * Projects : FRZ
 * Description : Edit of an Obj. Freeze
 * Events : FRZ updated
 * isDisabled : false
 */
class ObjectiveFreezeBusinessEvent {

    static void edit(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException, CreateException, ClassNotFoundException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField PlannedQG2Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PLANNEDQG2);
        CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);

        CustomField PreIA_PartsData_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_PARTSDATA_REQUESTEDDATE);
        CustomField PreIA_PartsData_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_PARTSDATA_VALIDATION_REQUESTEDDATE);
        CustomField PartsData_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_PREIA_1STCOMMITEDDATE);
        CustomField PartsData_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_PREIA_LASTCOMMITEDDATE);
        CustomField PartsData_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_PREIA_VALIDATION_1STCOMMITEDDATE);
        CustomField PartsData_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_PREIA_VALIDATION_LASTCOMMITEDDATE);

        CustomField PreIA_MaintPlanning_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINTPLANNING_REQUESTEDDATE);
        CustomField PreIA_MaintPlanning_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINTPLANNING_VALIDATION_REQUESTEDDATE);
        CustomField MaintPlanning_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_PREIA_1STCOMMITEDDATE);
        CustomField MaintPlanning_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_PREIA_LASTCOMMITEDDATE);
        CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_VALIDATION_PREIA_1STCOMMITEDDATE);
        CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_VALIDATION_PREIA_LASTCOMMITEDDATE);

        CustomField PreIA_Maint_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINT_REQUESTEDDATE);
        CustomField PreIA_Maint_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINT_VALIDATION_REQUESTEDDATE);
        CustomField Maint_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_PREIA_1STCOMMITEDDATE);
        CustomField Maint_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_PREIA_LASTCOMMITEDDATE);
        CustomField Maint_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_PREIA_VALIDATION_1STCOMMITEDDATE);
        CustomField Maint_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_PREIA_VALIDATION_LASTCOMMITEDDATE);

        CustomField IA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_REQUESTEDDATE);
        CustomField IA_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_REQUESTEDDATE);
        CustomField IA_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_1STCOMMITTEDDATE);
        CustomField IA_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_LASTCOMMITTEDDATE);
        CustomField IA_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_1STCOMMITTEDDATE);
        CustomField IA_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE);

        CustomField Auth_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_REQUESTEDDATE);
        CustomField Auth_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_REQUESTEDDATE);
        CustomField Auth_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_REQUESTEDDATE);

        CustomField Auth_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_FIRSTCOMMITTEDDATE);
        CustomField Auth_Verification_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_FIRSTCOMMITTEDDATE);
        CustomField Auth_Validation_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE);

        CustomField Auth_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_LASTCOMMITTEDDATE);
        CustomField Auth_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField Auth_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);

        CustomField Form_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_REQUESTEDDATE);
        CustomField Form_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_REQUESTEDDATE);
        CustomField Form_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_REQUESTEDDATE);

        CustomField Form_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_1STCOMMITTEDDATE);
        CustomField Form_Verification_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_1STCOMMITTEDDATE);
        CustomField Form_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE);

        CustomField Form_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_LASTCOMMITTEDDATE);
        CustomField Form_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField Form_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE);

        CustomField Int_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_REQUESTEDDATE);
        CustomField Int_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_1STCOMMITTEDDATE);
        CustomField Int_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_LASTCOMMITTEDDATE);

        //Variables;
        MutableIssue TriggerIssue;
        Timestamp PlannedQG2;
        Date RequestedDateTrigger = null;

        Date RequestedDatePWO0;
        Date RequestedDatePWO1;
        Date PreIA_PartsData_RequestedDate;
        Date PreIA_PartsData_Validation_RequestedDate;
        Date PreIA_MaintPlanning_RequestedDate;
        Date PreIA_MaintPlanning_Validation_RequestedDate;
        Date PreIA_Maint_RequestedDate;
        Date PreIA_Maint_Validation_RequestedDate;
        Date IA_RequestedDate;
        Date IA_Validation_RequestedDate;

        Date Auth_RequestedDate;
        Date Auth_Verification_RequestedDate;
        Date Auth_Validation_RequestedDate;
        Date Form_RequestedDate;
        Date Form_Verification_RequestedDate;
        Date Form_Validation_RequestedDate;
        Date Int_RequestedDate;

        //Business;
        log.debug("issue : " + issue.getKey());

        //Get value to propagate;
        PlannedQG2 = (Timestamp) issue.getCustomFieldValue(PlannedQG2Field);

        //Get all link;
        Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssue : LinkedIssues) {
            if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                TriggerIssue = issueManager.getIssueObject(linkedIssue.getId());
                TriggerIssue.setCustomFieldValue(RequestedDateField, PlannedQG2);

                //issueManager.updateIssue(user, TriggerIssue, EventDispatchOption.ISSUE_UPDATED, false);
                issueManager.updateIssue(user, TriggerIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
                ;
                //Get linked issue of Trigger;
                Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(TriggerIssue, user).getAllIssues();
                for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                    if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                        //Get linked issue of Cluster;
                        Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                        for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                            if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                if (linkedIssueOfCluster.getCustomFieldValue(RequestedDateField) != null) {
                                    if (RequestedDateTrigger == null || RequestedDateTrigger.after((Date) linkedIssueOfCluster.getCustomFieldValue(RequestedDateField))) {
                                        RequestedDateTrigger = (Date) linkedIssueOfCluster.getCustomFieldValue(RequestedDateField);
                                        log.debug("Requested date trigger : " + RequestedDateTrigger);
                                    }
                                }
                            }
                        }
                    }
                }
                if (RequestedDateTrigger != null) {
                    MutableIssue mutableIssue;
                    Collection<Issue> LinkedIssuesOfTrigger2 = issueLinkManager.getLinkCollection(TriggerIssue, user).getAllIssues();
                    for (Issue linkedIssueOfTrigger2 : LinkedIssuesOfTrigger2) {
                        if (linkedIssueOfTrigger2.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                            //Get linked issue of Cluster;
                            Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger2, user).getAllIssues();
                            for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                                if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                    if (linkedIssueOfCluster.getStatus().getName() == "Not started") {
                                        RequestedDatePWO0 = Toolbox.incrementTimestamp(RequestedDateTrigger, -119);

                                        PreIA_PartsData_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -126);
                                        PreIA_PartsData_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -119);
                                        Date PartsData_PreIA_1stCommitedDate = PreIA_PartsData_RequestedDate;
                                        Date PartsData_PreIA_LastCommitedDate = PartsData_PreIA_1stCommitedDate;
                                        Date PartsData_PreIA_Validation_1stCommitedDate = PreIA_PartsData_Validation_RequestedDate;
                                        Date PartsData_PreIA_Validation_LastCommitedDate = PartsData_PreIA_Validation_1stCommitedDate;

                                        PreIA_MaintPlanning_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -126);
                                        PreIA_MaintPlanning_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -119);
                                        Date MaintPlanning_PreIA_1stCommitedDate = PreIA_MaintPlanning_RequestedDate;
                                        Date MaintPlanning_PreIA_LastCommitedDate = MaintPlanning_PreIA_1stCommitedDate;
                                        Date MaintPlanning_PreIA_Validation_1stCommitedDate = PreIA_MaintPlanning_Validation_RequestedDate;
                                        Date MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_PreIA_Validation_1stCommitedDate;

                                        PreIA_Maint_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -126);
                                        PreIA_Maint_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -119);
                                        Date Maint_PreIA_1stCommitedDate = PreIA_Maint_RequestedDate;
                                        Date Maint_PreIA_LastCommitedDate = Maint_PreIA_1stCommitedDate;
                                        Date Maint_PreIA_Validation_1stCommitedDate = PreIA_Maint_Validation_RequestedDate;
                                        Date Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_1stCommitedDate;


                                        mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                        mutableIssue.setCustomFieldValue(RequestedDateField, RequestedDatePWO0);

                                        mutableIssue.setCustomFieldValue(PreIA_PartsData_RequestedDateField, PreIA_PartsData_RequestedDate);
                                        mutableIssue.setCustomFieldValue(PreIA_PartsData_Validation_RequestedDateField, PreIA_PartsData_Validation_RequestedDate);
                                        mutableIssue.setCustomFieldValue(PartsData_PreIA_1stCommitedDateField, PartsData_PreIA_1stCommitedDate);
                                        mutableIssue.setCustomFieldValue(PartsData_PreIA_LastCommitedDateField, PartsData_PreIA_LastCommitedDate);
                                        mutableIssue.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate);
                                        mutableIssue.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate);

                                        mutableIssue.setCustomFieldValue(PreIA_MaintPlanning_RequestedDateField, PreIA_MaintPlanning_RequestedDate);
                                        mutableIssue.setCustomFieldValue(PreIA_MaintPlanning_Validation_RequestedDateField, PreIA_MaintPlanning_Validation_RequestedDate);
                                        mutableIssue.setCustomFieldValue(MaintPlanning_PreIA_1stCommitedDateField, MaintPlanning_PreIA_1stCommitedDate);
                                        mutableIssue.setCustomFieldValue(MaintPlanning_PreIA_LastCommitedDateField, MaintPlanning_PreIA_LastCommitedDate);
                                        mutableIssue.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate);
                                        mutableIssue.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate);

                                        mutableIssue.setCustomFieldValue(PreIA_Maint_RequestedDateField, PreIA_Maint_RequestedDate);
                                        mutableIssue.setCustomFieldValue(PreIA_Maint_Validation_RequestedDateField, PreIA_Maint_Validation_RequestedDate);
                                        mutableIssue.setCustomFieldValue(Maint_PreIA_1stCommitedDateField, Maint_PreIA_1stCommitedDate);
                                        mutableIssue.setCustomFieldValue(Maint_PreIA_LastCommitedDateField, Maint_PreIA_LastCommitedDate);
                                        mutableIssue.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate);
                                        mutableIssue.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate);

                                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false); ;
                                        issueIndexingService.reIndex(linkedIssueOfCluster); ;
                                    }
                                    Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                    for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                        if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                            if (linkedIssueOfPWO0.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                                RequestedDatePWO1 = Toolbox.incrementTimestamp(RequestedDateTrigger, -98);
                                                IA_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -105);
                                                IA_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -98);
                                                Date IA_1stCommittedDate = IA_RequestedDate;
                                                Date IA_LastCommittedDate = IA_1stCommittedDate;
                                                Date IA_Validation_1stCommittedDate = IA_Validation_RequestedDate;
                                                Date IA_Validation_LastCommittedDate = IA_Validation_1stCommittedDate;

                                                mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO0.getKey());
                                                mutableIssue.setCustomFieldValue(RequestedDateField, RequestedDatePWO1);
                                                mutableIssue.setCustomFieldValue(IA_RequestedDateField, IA_RequestedDate);
                                                mutableIssue.setCustomFieldValue(IA_Validation_RequestedDateField, IA_Validation_RequestedDate);
                                                mutableIssue.setCustomFieldValue(IA_1stCommittedDateField, IA_1stCommittedDate);
                                                mutableIssue.setCustomFieldValue(IA_LastCommittedDateField, IA_LastCommittedDate);
                                                mutableIssue.setCustomFieldValue(IA_Validation_1stCommittedDateField, IA_Validation_1stCommittedDate);
                                                mutableIssue.setCustomFieldValue(IA_Validation_LastCommittedDateField, IA_Validation_LastCommittedDate);
                                                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false); ;
                                                issueIndexingService.reIndex(linkedIssueOfPWO0); ;
                                            }

                                            Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                            for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                    //To remove if update not needed;
                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2 && linkedIssueOfPWO1.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                                        Date RequestedDatePWO2 = Toolbox.incrementTimestamp(RequestedDateTrigger, -28);

                                                        mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO1.getKey());
                                                        mutableIssue.setCustomFieldValue(RequestedDateField, RequestedDatePWO2);
                                                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false); ;
                                                        issueIndexingService.reIndex(linkedIssueOfPWO1); ;
                                                    }

                                                    Collection<Issue> LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                    for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
                                                        if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                                                            //To remove if update not needed;
                                                            if (linkedIssueOfPWO2.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                                                Auth_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -84);
                                                                Auth_Verification_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -70);
                                                                Auth_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -56);

                                                                Date Auth_FirstCommittedDate = Auth_RequestedDate;
                                                                Date Auth_Verification_FirstCommittedDate = Auth_Verification_RequestedDate;
                                                                Date Auth_Validation_FirstCommittedDate = Auth_Validation_RequestedDate;

                                                                Date Auth_LastCommittedDate = Auth_FirstCommittedDate;
                                                                Date Auth_Verification_LastCommittedDate = Auth_Verification_FirstCommittedDate;
                                                                Date Auth_Validation_LastCommittedDate = Auth_Validation_FirstCommittedDate;

                                                                mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO2.getKey());
                                                                mutableIssue.setCustomFieldValue(Auth_RequestedDateField, Auth_RequestedDate);
                                                                mutableIssue.setCustomFieldValue(Auth_Verification_RequestedDateField, Auth_Verification_RequestedDate);
                                                                mutableIssue.setCustomFieldValue(Auth_Validation_RequestedDateField, Auth_Validation_RequestedDate);

                                                                mutableIssue.setCustomFieldValue(Auth_FirstCommittedDateField, Auth_FirstCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Auth_Verification_FirstCommittedDateField, Auth_Verification_FirstCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Auth_Validation_FirstCommittedDateField, Auth_Validation_FirstCommittedDate);

                                                                mutableIssue.setCustomFieldValue(Auth_LastCommittedDateField, Auth_LastCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Auth_Verification_LastCommittedDateField, Auth_Verification_LastCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Auth_Validation_LastCommittedDateField, Auth_Validation_LastCommittedDate);

                                                                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false); ;
                                                                issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                            }
                                                        } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                                                            //To remove if update not needed;
                                                            if (linkedIssueOfPWO2.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                                                Form_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -49);
                                                                Form_Verification_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -42);
                                                                Form_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -35);

                                                                Date Form_FirstCommittedDate = Form_RequestedDate;
                                                                Date Form_Verification_FirstCommittedDate = Form_Verification_RequestedDate;
                                                                Date Form_Validation_FirstCommittedDate = Form_Validation_RequestedDate;

                                                                Date Form_LastCommittedDate = Form_FirstCommittedDate;
                                                                Date Form_Verification_LastCommittedDate = Form_Verification_FirstCommittedDate;
                                                                Date Form_Validation_LastCommittedDate = Form_Validation_FirstCommittedDate;

                                                                mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO2.getKey());
                                                                mutableIssue.setCustomFieldValue(Form_RequestedDateField, Form_RequestedDate);
                                                                mutableIssue.setCustomFieldValue(Form_Verification_RequestedDateField, Form_Verification_RequestedDate);
                                                                mutableIssue.setCustomFieldValue(Form_Validation_RequestedDateField, Form_Validation_RequestedDate);

                                                                mutableIssue.setCustomFieldValue(Form_1stCommittedDateField, Form_FirstCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Form_Verification_1stCommittedDateField, Form_Verification_FirstCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Form_Validation_1stCommittedDateField, Form_Validation_FirstCommittedDate);

                                                                mutableIssue.setCustomFieldValue(Form_LastCommittedDateField, Form_LastCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Form_Verification_LastCommittedDateField, Form_Verification_LastCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Form_Validation_LastCommittedDateField, Form_Validation_LastCommittedDate);

                                                                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false); ;
                                                                issueIndexingService.reIndex(linkedIssueOfPWO2); ;
                                                            }
                                                        } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                                                            //To remove if update not needed;
                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO23 && linkedIssueOfPWO2.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                                                Int_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -28);
                                                                Date Int_FirstCommittedDate = Int_RequestedDate;
                                                                Date Int_LastCommittedDate = Int_FirstCommittedDate;

                                                                mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO2.getKey());
                                                                mutableIssue.setCustomFieldValue(Int_RequestedDateField, Int_RequestedDate);
                                                                mutableIssue.setCustomFieldValue(Int_1stCommittedDateField, Int_FirstCommittedDate);
                                                                mutableIssue.setCustomFieldValue(Int_LastCommittedDateField, Int_LastCommittedDate);
                                                                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false); ;
                                                                issueIndexingService.reIndex(linkedIssueOfPWO2); ;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
