package com.airbus.ims.business.event


import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger
import org.ofbiz.core.entity.GenericValue;

class POBusinessEvent {

    static void createLinkBetweenPOAndWUCreationMode(MutableIssue issue, ApplicationUser user, Logger log) throws CreateException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField WorkUnitField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WORKUNIT);
        CustomField WorkUnitAdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WORKUNITADDITIONALCOST);

        //Create link with WU
        if (WorkUnitField != null) {
            //Collection toLink = fieldValueService.getFieldValues(issue.getKey(), ID_CF_WorkUnitString)
            Collection<Issue> toLink = Toolbox.getIssueFromNFeedFieldMultiple(WorkUnitField, issue, log);
            //toLink.each {
            if (toLink != null) {
                for (Issue issueToLink : toLink) {
                    Long toLink_id = issueToLink.getId();
                    issueLinkManager.createIssueLink(issue.getId(), toLink_id, ConstantKeys.ID_LINK_RELATES, 1L, user);
                }
            }
        }
        if (WorkUnitAdditionalCostField != null) {
            //Collection toLink = fieldValueService.getFieldValues(issue.getKey(), ID_CF_WorkUnitAdditionalCostString)
            Collection<Issue> toLink = Toolbox.getIssueFromNFeedFieldMultiple(WorkUnitAdditionalCostField, issue, log);
            //toLink.each {
            if (toLink != null) {
                for (Issue issueToLink : toLink) {
                    Long toLink_id = issueToLink.getId();
                    issueLinkManager.createIssueLink(issue.getId(), toLink_id, ConstantKeys.ID_LINK_RELATES, 1L, user);
                }
            }
        }
    }

    static void createLinkBetweenPOAndWUEditionMode(IssueEvent event, ApplicationUser user, Logger log) throws CreateException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue) event.getIssue();

        //CustomField
        CustomField WorkUnitField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WORKUNIT);
        CustomField WorkUnitAdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WORKUNITADDITIONALCOST);


        List<String> itemsChanged = new ArrayList<String>();
        itemsChanged.add(WorkUnitField.getFieldName());
        //If WU has been updated, change link
        boolean isWorlogUpdated = Toolbox.workloadIsUpdated(changeLog, itemsChanged);
        log.debug("worklog updated : " + isWorlogUpdated);
        if (isWorlogUpdated) {
            //Delete old link
            Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssue : LinkedIssues) {
                if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_WU) {
                    IssueLink linkToRemove = issueLinkManager.getIssueLink(issue.getId(), linkedIssue.getId(), ConstantKeys.ID_LINK_RELATES);
                    issueLinkManager.removeIssueLink(linkToRemove, user);
                }
            }
            //Create new link
            if (WorkUnitField != null) {
                log.debug("Adding link");
                //Collection toLink = fieldValueService.getFieldValues(issue.getKey(), ID_CF_WorkUnitString)
                Collection<Issue> toLink = Toolbox.getIssueFromNFeedFieldMultiple(WorkUnitField, issue, log);
                //toLink.each {
                if (!toLink.isEmpty()) {
                    for (Issue issueToLink : toLink) {
                        Long toLink_id = issueToLink.getId();
                        issueLinkManager.createIssueLink(issue.getId(), toLink_id, ConstantKeys.ID_LINK_RELATES, 1L, user);
                    }
                }
            }
        }

        List<String> itemsChangedAdditionalCost = new ArrayList<String>();
        itemsChangedAdditionalCost.add(WorkUnitAdditionalCostField.getFieldName());
        //If WU has been updated, change link
        boolean isWorlogAdditionalCostUpdated = Toolbox.workloadIsUpdated(changeLog, itemsChangedAdditionalCost);
        if (isWorlogAdditionalCostUpdated) {
            //Delete old link
            Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssue : LinkedIssues) {
                if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_WU_ADDITIONALCOST) {
                    IssueLink linkToRemove = issueLinkManager.getIssueLink(issue.getId(), linkedIssue.getId(), ConstantKeys.ID_LINK_RELATES);
                    issueLinkManager.removeIssueLink(linkToRemove, user);
                }
            }
            //Create new link
            if (WorkUnitAdditionalCostField != null) {
                log.debug("Adding link");
                //Collection toLink = fieldValueService.getFieldValues(issue.getKey(), ID_CF_WorkUnitAdditionalCostString)
                Collection<Issue> toLink = Toolbox.getIssueFromNFeedFieldMultiple(WorkUnitAdditionalCostField, issue, log);
                //toLink.each {
                if (toLink != null) {
                    for (Issue issueToLink : toLink) {
                        Long toLink_id = issueToLink.getId();
                        issueLinkManager.createIssueLink(issue.getId(), toLink_id, ConstantKeys.ID_LINK_RELATES, 1L, user);
                    }
                }
            }
        }
    }

    static void fillCompanyAllowedFieldForPO(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //Get CustomFields;
        CustomField CompanyAllowed = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_COMPANY_ALLOWED);
        CustomField PO_Company = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PO_COMPANY);

        List<Group> CompanyAllowedList = new ArrayList<Group>();

        log.debug("PO");
        //PO;
        String PO_Company_Value = String.valueOf(issue.getCustomFieldValue(PO_Company));

        String PO_Company_Formatted = "Partner_" + PO_Company_Value;

        Group PO_Company_Formatted_Group = groupManager.getGroup(PO_Company_Formatted);

        CompanyAllowedList.add(PO_Company_Formatted_Group);

        issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false); //JIRA7;
        issueIndexingService.reIndex(issue); //JIRA7;
    }
}
