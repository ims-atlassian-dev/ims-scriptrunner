package com.airbus.ims.business.event


import com.airbus.ims.util.ConstantKeys;
import com.atlassian.crowd.embedded.api.Group;
import com.airbus.ims.util.Toolbox;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.exception.CreateException
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem
import com.atlassian.jira.issue.customfields.manager.OptionsManager
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.security.groups.GroupManager;
import org.apache.log4j.Logger;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.user.ApplicationUser;
import org.joda.time.LocalDate
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp

/**
 * Projects : PWO
 * Description : Creation / Edit PWO-1
 * Events : Admin re-open, PWO-1 updated, Issue Created
 * isDisabled : false
 */
class PWO1BusinessEvent {

    static void createPWO1(MutableIssue issue , ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField restrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);
        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);
        CustomField iaRequestdDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_REQUESTEDDATE);
        CustomField iaValidationRequestdDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_REQUESTEDDATE);
        CustomField ia1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_1STCOMMITTEDDATE);
        CustomField iaLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_LASTCOMMITTEDDATE);
        CustomField iaValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_1STCOMMITTEDDATE);
        CustomField iaValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE);
        CustomField iaWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_WU);
        CustomField iaNbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_NBWU);
        CustomField iaEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_EFFECTIVECOST);
        CustomField wuValueField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WUVALUE);
        CustomField iaAdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_ADDITIONALCOST);
        
        //Variables
        Date requestedDateTrigger;
        String restrictedDataResult;
        Date requestedDate;
        Date iaRequestedDate;
        Date iaValidationRequestedDate;
        Date ia1stCommittedDate;
        Date iaLastCommittedDate;
        Date iaValidation1stCommittedDate;
        Date iaValidationLastCommittedDate;
        double iaNbWu;
        double iaEffectiveCost;
        double iaWuValue;

        Issue issueIAWU = Toolbox.getIssueFromNFeedFieldSingle(iaWUField, issue, log);

        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
            //Get linked issues
            Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssue : LinkedIssues) {
                //If linked issue is PWO0
                if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                    restrictedDataResult = linkedIssue.getCustomFieldValue(restrictedDataField).toString();
                    Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                    for (Issue LinkedIssueOfPWO : LinkedIssuesOfPWO0) {
                        if (LinkedIssueOfPWO.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                            Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPWO, user).getAllIssues();
                            for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                                if (LinkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                    if(requestedDateTrigger == null  || requestedDateTrigger.after((Date)LinkedIssueOfCluster.getCustomFieldValue(requestedDateField))){
                                        requestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(requestedDateField);
                                        log.debug("Requested date trigger : " + requestedDateTrigger);
                                     }
                                }
                            }
                        }
                    }
                }
            }
            if (requestedDateTrigger != null) {//Calculate date
                requestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 98);
                iaRequestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 105);
                iaValidationRequestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, - 98);
                ia1stCommittedDate = iaRequestedDate;
                iaLastCommittedDate = ia1stCommittedDate;
                iaValidation1stCommittedDate = iaValidationRequestedDate;
                iaValidationLastCommittedDate = iaValidation1stCommittedDate;
            }

            //Calculate effective costs
            if (issueIAWU != null && issue.getCustomFieldValue(iaNbWUField) != null) {
                iaWuValue = (Double) issueIAWU.getCustomFieldValue(wuValueField);
                iaNbWu = (Double) issue.getCustomFieldValue(iaNbWUField);
                iaEffectiveCost = iaWuValue * iaNbWu;
            }
            if (issue.getCustomFieldValue(iaAdditionalCostField) != null) {
                iaEffectiveCost = iaEffectiveCost + (Double) issue.getCustomFieldValue(iaAdditionalCostField);
            }

            //Set Data on PWO-1
            issue.setCustomFieldValue(requestedDateField, requestedDate);

            issue.setCustomFieldValue(iaRequestdDateField, iaRequestedDate);
            issue.setCustomFieldValue(ia1stCommittedDateField, ia1stCommittedDate);
            issue.setCustomFieldValue(iaLastCommittedDateField, iaLastCommittedDate);
            issue.setCustomFieldValue(iaValidationRequestdDateField, iaValidationRequestedDate);
            issue.setCustomFieldValue(iaValidation1stCommittedDateField, iaValidation1stCommittedDate);
            issue.setCustomFieldValue(iaValidationLastCommittedDateField, iaValidationLastCommittedDate);

            issue.setCustomFieldValue(iaEffectiveCostField, iaEffectiveCost);

            FieldConfig fieldConfigRestrictedData = restrictedDataField.getRelevantConfig(issue);
            Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
                it.toString() == restrictedDataResult;
            }
            issue.setCustomFieldValue(restrictedDataField, valueRestrictedData);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            //Set costs data on Trigger
            setCostsDataOnTrigger(issue, user, log, true);
        }
    }

    /**
     * Set costs data on Trigger
     * @param issue
     * @param user
     * @param log
     */
    static void setCostsDataOnTrigger(MutableIssue issue, ApplicationUser user, Logger log, boolean isCreationMode) throws IndexException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField;
        CustomField partsDataEstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_ESTIMATEDCOSTS);
        CustomField partsDataEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_EFFECTIVECOSTS);

        CustomField maintPlanningEstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_ESTIMATEDCOSTS);
        CustomField maintPlanningEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_EFFECTIVECOSTS);

        CustomField maintEstimatedCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_ESTIMATEDCOSTS);
        CustomField maintEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_EFFECTIVECOSTS);

        CustomField IA_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_EFFECTIVECOST);
        CustomField iaEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_EFFECTIVECOST);
        CustomField authEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_EFFECTIVECOST);
        CustomField formEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_EFFECTIVECOST);
        CustomField intEffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_EFFECTIVECOST);
        CustomField triggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TRIGGERTOTALEFFECTIVECOSTS);

        CustomField partsDataTimeSpentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_TIMESPENT);
        CustomField maintPlanningTimeSpentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_TIMESPENT);
        CustomField maintTimeSpentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_TIMESPENT);
        CustomField iaTimeSpentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_TIMESPENT);
        CustomField domainPWO1Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DOMAINPWO1);
        CustomField authTimeSpentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_TIMESPENT);
        CustomField formTimeSpentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_TIMESPENT);
        CustomField intTimeSpentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_TIMESPENT);

        //Variables
        String domainOfPWO2;
        double iaEffectiveCost;
        double partsDataEstimatedCosts = 0;
        double maintPlanningEstimatedCosts = 0;
        double maintEstimatedCosts = 0;
        long partsDataTimeSpent = 0;
        long maintPlanningTimeSpent = 0;
        long maintTimeSpent = 0;
        double partsDataEffectiveCostsOfTrigger = 0;
        double maintPlanningEffectiveCostsOfTrigger = 0;
        double maintEffectiveCostsOfTrigger = 0;
        String partsDataTimeSpentString;
        String maintPlanningTimeSpentString;
        String maintTimeSpenString;
        long iaTimeSpentCurrent;
        long authTimeSpentCurrent;
        long formTimeSpentCurrent;
        long intTimeSpentCurrent;
        double authEffectiveCost = 0;
        double formEffectiveCost = 0;
        double intEffectiveCost = 0;
        double triggerEffectiveCost;


        Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for(Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
            if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                for(Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                        Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                        for(Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                            if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                //Reset variable;
                                partsDataTimeSpent = 0;
                                maintPlanningTimeSpent=0;
                                maintTimeSpent=0;
                                partsDataEstimatedCosts=0;
                                maintPlanningEstimatedCosts=0;
                                maintEstimatedCosts=0;
                                //Trigger;
                                Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                for(Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                    if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                        Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                        for(Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(partsDataEstimatedCostsField) != null) {
                                                    partsDataEstimatedCosts = partsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(partsDataEstimatedCostsField);
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanningEstimatedCostsField) != null) {
                                                    maintPlanningEstimatedCosts = maintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanningEstimatedCostsField);
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(maintEstimatedCostsField) != null) {
                                                    maintEstimatedCosts = maintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(maintEstimatedCostsField);
                                                }

                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(partsDataTimeSpentField) != null) {
                                                    Long PartsDataTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(partsDataTimeSpentField).toString());
                                                    partsDataTimeSpent = partsDataTimeSpent + PartsDataTimeSpentCurrent;
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanningTimeSpentField) != null) {
                                                    Long MaintPlanningTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanningTimeSpentField).toString());
                                                    maintPlanningTimeSpent = maintPlanningTimeSpent + MaintPlanningTimeSpentCurrent;
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(maintTimeSpentField) != null) {
                                                    Long maintTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(maintTimeSpentField).toString());
                                                    maintTimeSpent = maintTimeSpent + maintTimeSpentCurrent;
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(partsDataEffectiveCostsField) != null) {
                                                    partsDataEffectiveCostsOfTrigger = partsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(partsDataEffectiveCostsField);
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanningEffectiveCostsField) != null) {
                                                    maintPlanningEffectiveCostsOfTrigger = maintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(maintPlanningEffectiveCostsField);
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(maintEffectiveCostsField) != null) {
                                                    maintEffectiveCostsOfTrigger = maintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(maintEffectiveCostsField);
                                                }

                                                Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                for(Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                        log.debug("PWO1 : " + linkedIssueOfPWO0.getKey());
                                                        log.debug("domain of PWO-1 : " + linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field));
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                            iaTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField).toString());
                                                        } else {
                                                            iaTimeSpentCurrent = 0;
                                                        }
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(iaEffectiveCostField) != null) {
                                                            iaEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField);
                                                        } else {
                                                            iaEffectiveCost = 0;
                                                        }

                                                        if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Parts Data") {
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                partsDataTimeSpent = partsDataTimeSpent + iaTimeSpentCurrent;
                                                            }
                                                            partsDataEffectiveCostsOfTrigger = partsDataEffectiveCostsOfTrigger + iaEffectiveCost;
                                                        } else if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Maintenance Planning") {
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                maintPlanningTimeSpent = maintPlanningTimeSpent + iaTimeSpentCurrent;
                                                            }
                                                            maintPlanningEffectiveCostsOfTrigger = maintPlanningEffectiveCostsOfTrigger + iaEffectiveCost;
                                                        } else if (linkedIssueOfPWO0.getCustomFieldValue(domainPWO1Field).toString() == "Maintenance") {
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(iaTimeSpentField) != null) {
                                                                maintTimeSpent = maintTimeSpent + iaTimeSpentCurrent;
                                                            }
                                                            maintEffectiveCostsOfTrigger = maintEffectiveCostsOfTrigger + iaEffectiveCost;
                                                        }

                                                        Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                        for(Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                domainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(domainPWO1Field).toString();
                                                                //Get PWO-2's subtask;
                                                                Collection<Issue> linkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                                for(Issue linkedIssueOfPWO2 : linkedIssuesOfPWO2) {
                                                                    if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField) != null) {
                                                                            authTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField).toString());
                                                                            //AuthTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(authTimeSpentField);
                                                                        } else {
                                                                            authTimeSpentCurrent = 0;
                                                                        }
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(authEffectiveCostField) != null) {
                                                                            authEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(authEffectiveCostField);
                                                                        }
                                                                        if (domainOfPWO2 == "Parts Data"){
                                                                            if(authEffectiveCost != 0) {
                                                                                partsDataEffectiveCostsOfTrigger = partsDataEffectiveCostsOfTrigger + authEffectiveCost;
                                                                            }
                                                                            partsDataTimeSpent = partsDataTimeSpent + authTimeSpentCurrent;
                                                                        } else if (domainOfPWO2 == "Maintenance Planning"){
                                                                            if (authEffectiveCost != 0){
                                                                                maintPlanningEffectiveCostsOfTrigger = maintPlanningEffectiveCostsOfTrigger + authEffectiveCost;
                                                                            }
                                                                            maintPlanningTimeSpent = maintPlanningTimeSpent + authTimeSpentCurrent;
                                                                        } else if (domainOfPWO2 == "Maintenance"){
                                                                            if(authEffectiveCost != 0) {
                                                                                maintEffectiveCostsOfTrigger = maintEffectiveCostsOfTrigger + authEffectiveCost;
                                                                            }
                                                                            maintTimeSpent = maintTimeSpent + authTimeSpentCurrent;
                                                                        }
                                                                    } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField) != null) {
                                                                            formTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField).toString());
                                                                            //FormTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(formTimeSpentField);
                                                                        } else {
                                                                            formTimeSpentCurrent = 0;
                                                                        }
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(formEffectiveCostField) != null) {
                                                                            formEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(formEffectiveCostField);
                                                                        }
                                                                        if (domainOfPWO2 == "Parts Data"){
                                                                            if(formEffectiveCost != 0) {
                                                                                partsDataEffectiveCostsOfTrigger = partsDataEffectiveCostsOfTrigger + formEffectiveCost;
                                                                            }
                                                                            partsDataTimeSpent = partsDataTimeSpent + formTimeSpentCurrent;
                                                                        } else if (domainOfPWO2 == "Maintenance Planning"){
                                                                            if(formEffectiveCost != 0) {
                                                                                maintPlanningEffectiveCostsOfTrigger = maintPlanningEffectiveCostsOfTrigger + formEffectiveCost;
                                                                            }
                                                                            maintPlanningTimeSpent = maintPlanningTimeSpent + formTimeSpentCurrent;
                                                                        } else if (domainOfPWO2 == "Maintenance"){
                                                                            if(formEffectiveCost != 0) {
                                                                                maintEffectiveCostsOfTrigger = maintEffectiveCostsOfTrigger + formEffectiveCost;
                                                                            }
                                                                            maintTimeSpent = maintTimeSpent + formTimeSpentCurrent;
                                                                        }
                                                                    } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(intTimeSpentField) != null) {
                                                                            intTimeSpentCurrent = Toolbox.formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(intTimeSpentField).toString());
                                                                            //IntTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(intTimeSpentField);
                                                                        } else {
                                                                            intTimeSpentCurrent = 0;
                                                                        }
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(intEffectiveCostField) != null) {
                                                                            intEffectiveCost = (Double)linkedIssueOfPWO2.getCustomFieldValue(intEffectiveCostField);
                                                                        }
                                                                        if (domainOfPWO2 == "Parts Data"){
                                                                            if(intEffectiveCost != 0) {
                                                                                partsDataEffectiveCostsOfTrigger = partsDataEffectiveCostsOfTrigger + intEffectiveCost;
                                                                            }
                                                                            partsDataTimeSpent = partsDataTimeSpent + intTimeSpentCurrent;
                                                                        } else if (domainOfPWO2 == "Maintenance Planning"){
                                                                            if(intEffectiveCost != 0) {
                                                                                maintPlanningEffectiveCostsOfTrigger = maintPlanningEffectiveCostsOfTrigger + intEffectiveCost;
                                                                            }
                                                                            maintPlanningTimeSpent = maintPlanningTimeSpent + intTimeSpentCurrent;
                                                                        } else if (domainOfPWO2 == "Maintenance"){
                                                                            if(intEffectiveCost != 0) {
                                                                                maintEffectiveCostsOfTrigger = maintEffectiveCostsOfTrigger + intEffectiveCost;
                                                                            }
                                                                            maintTimeSpent = maintTimeSpent + intTimeSpentCurrent;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //Set data on trigger;
                                triggerEffectiveCost = partsDataEffectiveCostsOfTrigger + maintPlanningEffectiveCostsOfTrigger + maintEffectiveCostsOfTrigger;
                                MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                mutableIssue.setCustomFieldValue(triggerTotalEffectiveCostsField, triggerEffectiveCost);
                                mutableIssue.setCustomFieldValue(partsDataEstimatedCostsField, partsDataEstimatedCosts);
                                mutableIssue.setCustomFieldValue(maintPlanningEstimatedCostsField, maintPlanningEstimatedCosts);
                                mutableIssue.setCustomFieldValue(maintEstimatedCostsField, maintEstimatedCosts);
                                mutableIssue.setCustomFieldValue(partsDataEffectiveCostsField, partsDataEffectiveCostsOfTrigger);
                                mutableIssue.setCustomFieldValue(maintPlanningEffectiveCostsField, maintPlanningEffectiveCostsOfTrigger);
                                mutableIssue.setCustomFieldValue(maintEffectiveCostsField, maintEffectiveCostsOfTrigger);
                                if(!isCreationMode) {
                                    partsDataTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(partsDataTimeSpent);
                                    mutableIssue.setCustomFieldValue(partsDataTimeSpentField, partsDataTimeSpentString);
                                    maintPlanningTimeSpentString = Toolbox.formatSecondesValueInHoursMinutes(maintPlanningTimeSpent);
                                    mutableIssue.setCustomFieldValue(maintPlanningTimeSpentField, maintPlanningTimeSpentString);
                                    maintTimeSpenString = Toolbox.formatSecondesValueInHoursMinutes(maintTimeSpent);
                                    mutableIssue.setCustomFieldValue(maintTimeSpentField, maintTimeSpenString);
                                }
                                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                issueIndexingService.reIndex(mutableIssue);
                            }
                        }
                    }
                }
            }
        }
    }

    static void linkPWO0ToPWO1(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);
        CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);
        CustomField WUValueField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WUVALUE);
        CustomField IA_RequestdDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_REQUESTEDDATE);
        CustomField IA_Validation_RequestdDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_REQUESTEDDATE);
        CustomField IA_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_1STCOMMITTEDDATE);
        CustomField IA_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_LASTCOMMITTEDDATE);
        CustomField IA_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_1STCOMMITTEDDATE);
        CustomField IA_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE);
        CustomField IA_WUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_WU);
        CustomField IA_NbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_NBWU);
        CustomField IA_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_EFFECTIVECOST);
        CustomField IA_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_ADDITIONALCOST);


        //Variables;
        String RestrictedDataResult;
        Date RequestedDateTrigger;
        Date RequestedDate;
        Date IA_RequestdDate;
        Date IA_Validation_RequestdDate;
        Date IA_1stCommittedDate;
        Date IA_LastCommittedDate;
        Date IA_Validation_1stCommittedDate;
        Date IA_Validation_LastCommittedDate;
        Issue issueIAWU = Toolbox.getIssueFromNFeedFieldSingle(IA_WUField, issue, log);
        Double IANbWu;
        Double IAEffectiveCost;
        Double IAWuValue;

        log.debug("Edit of PWO-1 : " + issue.getKey());
        //Get linked issues;
        Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssue : LinkedIssues) {
            //If linked issue is PWO0;
            if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                RestrictedDataResult = linkedIssue.getCustomFieldValue(RestrictedDataField).toString();
                Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue LinkedIssueOfPWO : LinkedIssuesOfPWO0) {
                    if (LinkedIssueOfPWO.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                        Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPWO, user).getAllIssues();
                        for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                            if (LinkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                RequestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField);
                            }
                        }
                    }
                }
            }
        }
        if (RequestedDateTrigger != null) {//Calculate date;
            RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 98);
            IA_RequestdDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 105);
            IA_Validation_RequestdDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 98);
            IA_1stCommittedDate = IA_RequestdDate;
            IA_LastCommittedDate = IA_1stCommittedDate;
            IA_Validation_1stCommittedDate = IA_Validation_RequestdDate;
            IA_Validation_LastCommittedDate = IA_Validation_1stCommittedDate;
        }

        //Calculate effective costs;
        if (issueIAWU != null && issue.getCustomFieldValue(IA_NbWUField) != null) {
            IAWuValue = (Double) issueIAWU.getCustomFieldValue(WUValueField);
            IANbWu = (Double) issue.getCustomFieldValue(IA_NbWUField);
            IAEffectiveCost = IAWuValue * IANbWu;
        }
        if (issue.getCustomFieldValue(IA_AdditionalCostField) != null) {
            IAEffectiveCost = IAEffectiveCost + (Double) issue.getCustomFieldValue(IA_AdditionalCostField);
        }

        //Set Data on PWO-1;
        issue.setCustomFieldValue(RequestedDateField, RequestedDate);

        issue.setCustomFieldValue(IA_RequestdDateField, IA_RequestdDate);
        issue.setCustomFieldValue(IA_Validation_RequestdDateField, IA_Validation_RequestdDate);
        issue.setCustomFieldValue(IA_1stCommittedDateField, IA_1stCommittedDate);
        issue.setCustomFieldValue(IA_LastCommittedDateField, IA_LastCommittedDate);
        issue.setCustomFieldValue(IA_Validation_1stCommittedDateField, IA_Validation_1stCommittedDate);
        issue.setCustomFieldValue(IA_Validation_LastCommittedDateField, IA_Validation_LastCommittedDate);

        issue.setCustomFieldValue(IA_EffectiveCostField, IAEffectiveCost);

        FieldConfig fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue);
        OptionsManager valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
            it.toString() == RestrictedDataResult;
        }
        issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);

        //Set costs data on Trigger;
        setCostsDataOnTriggerOnPWO0ToPWO1LinkCreated(issue, user, log);
    }

    static void setCostsDataOnTriggerOnPWO0ToPWO1LinkCreated(Issue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_EFFECTIVECOST);
        CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_EFFECTIVECOST);
        CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_EFFECTIVECOST);
        CustomField IA_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_EFFECTIVECOST);
        CustomField PartsData_EstimatedCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATAESTIMATEDCOST);
        CustomField MaintPlanning_EstimatedCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNINGESTIMATEDCOST);
        CustomField Maint_EstimatedCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTESTIMATEDCOST);
        CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DOMAINPWO1);
        CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TRIGGERTOTALEFFECTIVECOST);
        CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTHEFFECTIVECOST);
        CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORMEFFECTIVECOST);
        CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INTEFFECTIVECOST);


        //Variables;
        Double IAEffectiveCost = Double.valueOf(0);
        Double PartsDataEffectiveCostsOfTrigger = Double.valueOf(0);
        Double MaintPlanningEffectiveCostsOfTrigger = Double.valueOf(0);
        Double MaintEffectiveCostsOfTrigger = Double.valueOf(0);
        Double PartsDataEstimatedCosts = Double.valueOf(0);
        Double MaintPlanningEstimatedCosts = Double.valueOf(0);
        Double MaintEstimatedCosts = Double.valueOf(0);
        Double TriggerEffectiveCost = Double.valueOf(0);

        Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO0 : LinkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                    if (LinkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                        //Trigger;
                        Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues();
                        for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostField) != null) {
                                            PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostField) != null) {
                                            MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostField) != null) {
                                            MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostField);
                                        }

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostField) != null) {
                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostField) != null) {
                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostField);
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostField) != null) {
                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostField);
                                        }

                                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                        for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1 ) {
                                                if (linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField) != null) {
                                                    IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField);
                                                } else {
                                                    IAEffectiveCost = Double.valueOf(0);
                                                }

                                                if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost;
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost;
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost;
                                                }

                                                Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                        //Get PWO-2's subtask;
                                                        String DomainOfPWO2 = (String) linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field);
                                                        Double AuthEffectiveCost = Double.valueOf(0);
                                                        Double FormEffectiveCost = Double.valueOf(0);
                                                        Double IntEffectiveCost = Double.valueOf(0);
                                                        Collection<Issue> LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                                        for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(Auth_EffectiveCostField) != null) {
                                                                    AuthEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(Auth_EffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost;
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(Form_EffectiveCostField) != null) {
                                                                    FormEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(Form_EffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost;
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(Int_EffectiveCostField) != null) {
                                                                    IntEffectiveCost = (Double) linkedIssueOfPWO2.getCustomFieldValue(Int_EffectiveCostField);
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger;

                        //Set data on trigger;
                        MutableIssue mutableIssue = issueManager.getIssueObject(LinkedIssueOfCluster.getKey());
                        mutableIssue.setCustomFieldValue(PartsData_EstimatedCostField, PartsDataEstimatedCosts);
                        mutableIssue.setCustomFieldValue(MaintPlanning_EstimatedCostField, MaintPlanningEstimatedCosts);
                        mutableIssue.setCustomFieldValue(Maint_EstimatedCostField, MaintEstimatedCosts);
                        mutableIssue.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCostsOfTrigger);
                        mutableIssue.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCostsOfTrigger);
                        mutableIssue.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCostsOfTrigger);
                        mutableIssue.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost);
                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(LinkedIssueOfCluster);
                    }
                }
            }
        }
    }


    static void fireRestrictedDataChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField restrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);

        //Variable
        String RestrictedDataTrigger = "No";

        Collection<Issue> linkedIssuesOfCurrentPW01 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPW01) {
            if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                    if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                        Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                        for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                            if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                if (linkedIssueOfCluster.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                    RestrictedDataTrigger = "Yes";
                                } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfCluster.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                    RestrictedDataTrigger = "Unknown";
                                }
                                Collection<Issue> LinkedIssuesOfLinkedPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                for (Issue linkedIssueOfLinkedPWO0 : LinkedIssuesOfLinkedPWO0) {
                                    if (linkedIssueOfLinkedPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                        if (linkedIssueOfLinkedPWO0.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                            RestrictedDataTrigger = "Yes";
                                        } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfLinkedPWO0.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                            RestrictedDataTrigger = "Unknown";
                                        }
                                        Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfLinkedPWO0, user).getAllIssues();
                                        for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                            if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                if (linkedIssueOfPWO1.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                                    RestrictedDataTrigger = "Yes";
                                                } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfPWO1.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                                    RestrictedDataTrigger = "Unknown";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        FieldConfig fieldConfigRestrictedData = restrictedDataField.getRelevantConfig(linkedIssueOfPWO0);
                        Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
                            it.toString() == RestrictedDataTrigger;
                        }

                        //Set Restricted data on Cluster
                        MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO0.getKey());
                        mutableIssue.setCustomFieldValue(restrictedDataField, valueRestrictedData);
                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(mutableIssue);

                        //Set Restricted data on Triggers
                        Collection<Issue> LinkedTriggersOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                        for (Issue linkedTriggerOfCluster : LinkedTriggersOfCluster) {
                            if (linkedTriggerOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                FieldConfig fieldConfigRestrictedDataTrigger = restrictedDataField.getRelevantConfig(linkedTriggerOfCluster);
                                Option valueRestrictedDataTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedDataTrigger) ?.find {
                                    it.toString() == RestrictedDataTrigger;
                                }
                                mutableIssue = issueManager.getIssueObject(linkedTriggerOfCluster.getKey());
                                mutableIssue.setCustomFieldValue(restrictedDataField, valueRestrictedDataTrigger);
                                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                issueIndexingService.reIndex(mutableIssue);
                            }
                        }
                    }
                }
            }
        }
    }

    static void fireImpactOnTDChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField impactConfirmedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IMPACTCONFIRMED);
        CustomField impactOnTDField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IMPACTONTD);

        //Variables
        String impactOnTDResult;

        Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
            if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                        Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                        for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                            if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                //Trigger
                                Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                    if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                        Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                        for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                        impactOnTDResult = "No";
                                                    }
                                                }
                                                for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(impactConfirmedField).toString() == "Yes") {
                                                            impactOnTDResult = "Yes";
                                                        } else if (impactOnTDResult != "Yes" && linkedIssueOfPWO0.getCustomFieldValue(impactConfirmedField).toString() == "Analysis not completed") {
                                                            impactOnTDResult = "Analysis not completed";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //Set Trigger Impact on TD
                                if (impactOnTDResult != null) {
                                    FieldConfig fieldConfigImpactOnTD = impactOnTDField.getRelevantConfig(linkedIssueOfCluster);
                                    Option valueImpactOnTD = ComponentAccessor.getOptionsManager().getOptions(fieldConfigImpactOnTD)?.find {
                                        it.toString() == impactOnTDResult;
                                    }
                                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                    mutableIssue.setCustomFieldValue(impactOnTDField, valueImpactOnTD);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(mutableIssue);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    static void fireIAValidatedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField iaValidatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATED);
        CustomField iaReworkDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_REWORKDATE);

        //Variables
        Date iaReworkDate;
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp todaysDateCompare = new Timestamp(todayDate.getTime());

        if (issue.getCustomFieldValue(iaValidatedField).toString() == "Rejected") {
            iaReworkDate = Toolbox.incrementTimestamp(todaysDateCompare, 7);

            issue.setCustomFieldValue(iaReworkDateField, iaReworkDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    static void fireIAValidatedImpactConfirmedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField IA_Validation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_1STDELIVERYDATE);
        CustomField IA_Validation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_LASTDELIVERYDATE);
        CustomField TechnicalClosureDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TECHNICALCLOSURE);
        CustomField ImpactConfirmedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IMPACTCONFIRMED);
        CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TECHNICALLYCLOSED);
        CustomField IA_ValidatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATED);


        //Variables
        Date IA_Validation_LastDelivery;
        Date IA_Validation_1stDelivery;
        String TechnicallyClosed = "Yes";
        Date TechnicalClosure;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());

        if (issue.getCustomFieldValue(IA_ValidatedField).toString() == "Validated" && issue.getCustomFieldValue(ImpactConfirmedField).toString() != "Analysis not completed") {
            TechnicalClosure = TodaysDateCompare;
            TechnicallyClosed = "Yes";
            FieldConfig fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue);
            Option valueTechnicallyClosed = ComponentAccessor.getOptionsManager().getOptions(fieldConfigTechnicallyClosed)?.find {
                it.toString() == TechnicallyClosed;
            }
            issue.setCustomFieldValue(TechnicallyClosedField, valueTechnicallyClosed);
            issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosure);
        } else {
            TechnicalClosure = null;
            TechnicallyClosed = "No";
            FieldConfig fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue);
            Option valueTechnicallyClosed = ComponentAccessor.getOptionsManager().getOptions(fieldConfigTechnicallyClosed)?.find {
                it.toString() == TechnicallyClosed;
            }
            issue.setCustomFieldValue(TechnicallyClosedField, valueTechnicallyClosed);
            issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosure);
        }

        if (issue.getCustomFieldValue(IA_ValidatedField).toString() == "Validated" || issue.getCustomFieldValue(IA_ValidatedField).toString() == "Rejected") {
            if (issue.getCustomFieldValue(IA_Validation_1stDeliveryDateField) == null) {
                IA_Validation_1stDelivery = TodaysDateCompare;
                issue.setCustomFieldValue(IA_Validation_1stDeliveryDateField, IA_Validation_1stDelivery);
            }
            IA_Validation_LastDelivery = TodaysDateCompare;
            issue.setCustomFieldValue(IA_Validation_LastDeliveryDateField, IA_Validation_LastDelivery);
        }

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    static void fireIABlockedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField IA_BlockedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_BLOCKED);
        CustomField BlockedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKED);
        CustomField OldBlockStartDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OLDBLOCKSTARTDATE);
        CustomField OldBlockEndDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OLDBLOCKENDDATE);
        CustomField BlockStartDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKSTARTDATE);
        CustomField BlockEndDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKENDDATE);
        CustomField TotalBlockingDurationField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TOTALBLOCKINGDURATION);

        //Variables
        String TotalBlockingDurationString;
        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());

        Date BlockStartDate;
        Date BlockEndDate;
        double TotalBlockingDuration;

        String TotalBlockingDurationTriggerString;
        double TotalBlockingDurationOld;
        double TotalBlockingDurationTrigger;

        String BlockedTrigger;

        if (issue.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
            BlockEndDate = null;
            issue.setCustomFieldValue(BlockEndDateField, BlockEndDate);
            //if (!issue.getCustomFieldValue(BlockStartDateField)){
            BlockStartDate = TodaysDateCompare;
            issue.setCustomFieldValue(BlockStartDateField, BlockStartDate);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            if (issue.getCustomFieldValue(OldBlockStartDateField) == null || issue.getCustomFieldValue(OldBlockStartDateField) != TodaysDateCompare) {
                log.debug("Old start date to update");
                issue.setCustomFieldValue(OldBlockStartDateField, BlockStartDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }
            //}
            BlockedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                    Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                    for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                            Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                            for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                                if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                    //Trigger
                                    Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                    for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                        if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                            Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                            for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                                if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                    log.debug("PWO-0 : " + linkedIssueOfClusterTrigger.getKey());
                                                    //Get total blocking duration of PWO0
                                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockedField) != null) {
                                                        log.debug("PWO-0 " + linkedIssueOfClusterTrigger.getKey() + " is blocked");
                                                        BlockedTrigger = "Yes";
                                                    }
                                                    Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                    for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                        if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                            //Get total blocking duration of PWO1
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                BlockedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                            for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                                if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                    if (linkedIssueOfPWO1.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                        BlockedTrigger = "Yes";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //Set data on trigger
                                    FieldConfig fieldConfigBlockedTrigger = IA_BlockedField.getRelevantConfig(linkedIssueOfCluster);
                                    Option valueBlockedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigBlockedTrigger)?.find {
                                        it.toString() == BlockedTrigger;
                                    }
                                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                    mutableIssue.setCustomFieldValue(IA_BlockedField, valueBlockedTrigger);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(mutableIssue);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            BlockEndDate = TodaysDateCompare;
            issue.setCustomFieldValue(BlockEndDateField, BlockEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            Date OldBlockDate = (Date) issue.getCustomFieldValue(OldBlockEndDateField);

            if (issue.getCustomFieldValue(OldBlockEndDateField) == null || issue.getCustomFieldValue(OldBlockEndDateField) != TodaysDateCompare) {
                log.debug("Old end date to update");
                issue.setCustomFieldValue(OldBlockEndDateField, BlockEndDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

                BlockStartDate = (Date) issue.getCustomFieldValue(BlockStartDateField);
                if (issue.getCustomFieldValue(TotalBlockingDurationField) != null) {
                    TotalBlockingDurationOld = Double.valueOf(issue.getCustomFieldValue(TotalBlockingDurationField).toString().replace(" d", ""));
                    if (issue.getCustomFieldValue(OldBlockStartDateField) == TodaysDateCompare || ((Date)issue.getCustomFieldValue(OldBlockStartDateField)).after(OldBlockDate)) {
                        TotalBlockingDuration = Toolbox.daysBetween(BlockEndDate, BlockStartDate) + 1;
                    } else {
                        TotalBlockingDuration = Toolbox.daysBetween(BlockEndDate, BlockStartDate);
                    }
                    TotalBlockingDuration = TotalBlockingDurationOld + TotalBlockingDuration;
                } else {
                    TotalBlockingDuration = Toolbox.daysBetween(BlockEndDate, BlockStartDate) + 1;
                }
                TotalBlockingDuration = TotalBlockingDuration.trunc();
                TotalBlockingDurationString = TotalBlockingDuration.trunc() + " d";

                //issue.setCustomFieldValue(BlockEndDateField,BlockEndDate)
                issue.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationString);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }


            TotalBlockingDurationTrigger = 0;
            BlockedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                    Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                    for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                            Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                            for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                                if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                    //Trigger
                                    Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                    for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                        if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                            Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                            for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                                if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                    //Get total blocking duration of PWO0
                                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField) != null) {
                                                        TotalBlockingDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField).toString();
                                                        TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "");
                                                        TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + Double.valueOf(TotalBlockingDurationTriggerString);
                                                    }
                                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockedField) != null) {
                                                        BlockedTrigger = "Yes";
                                                    }
                                                    Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                    for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                        if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                            //Get total blocking duration of PWO1
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField) != null) {
                                                                TotalBlockingDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField).toString();
                                                                TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "");
                                                                TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + Double.valueOf(TotalBlockingDurationTriggerString);
                                                            }
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                BlockedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                            for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                                if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                    if (linkedIssueOfPWO1.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                        BlockedTrigger = "Yes";
                                                                    }
                                                                    if (linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField) != null) {
                                                                        TotalBlockingDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField).toString();
                                                                        TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "");
                                                                        TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + Double.valueOf(TotalBlockingDurationTriggerString);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //Set data on trigger
                                    FieldConfig fieldConfigBlockedTrigger = IA_BlockedField.getRelevantConfig(linkedIssueOfCluster);
                                    Option valueBlockedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigBlockedTrigger)?.find {
                                        it.toString() == BlockedTrigger;
                                    }
                                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                    mutableIssue.setCustomFieldValue(IA_BlockedField, valueBlockedTrigger);
                                    TotalBlockingDurationTrigger = TotalBlockingDurationTrigger.trunc();
                                    TotalBlockingDurationTriggerString = TotalBlockingDurationTrigger.trunc() + " d";
                                    mutableIssue.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationTriggerString);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(linkedIssueOfCluster);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    static void editPWO1(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        GenericValue changeLog = event.getChangeLog();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField restrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);
        CustomField IA_EffectiveCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_EFFECTIVECOST);
        CustomField IA_BlockedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_BLOCKED);
        CustomField WUValueField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WUVALUE);
        CustomField ImpactConfirmedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IMPACTCONFIRMED);
        CustomField IA_AdditionalCostField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_ADDITIONALCOST);
        CustomField EscalatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_ESCALATED);
        CustomField IA_ValidatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATED);
        CustomField IA_WUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_WU);
        CustomField IACompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_COMPANY);
        CustomField IA_NbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_NBWU);


        //Variable
        MutableIssue issue = issueManager.getIssueObject(event.getIssue().getKey());
        ApplicationUser user = event.getUser();
        Issue issueIAWU = Toolbox.getIssueFromNFeedFieldSingle(IA_WUField, issue, log);
        double IANbWu;
        double IAEffectiveCost = 0;
        double IAWuValue;


        log.debug("edit");
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
            List<String> itemsChangedRestrictedData = new ArrayList<String>();
            itemsChangedRestrictedData.add(restrictedDataField.getFieldName());

            boolean isRestrictedDataChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedRestrictedData);
            if (isRestrictedDataChanged) {
                fireRestrictedDataChanged(issue, user, log);
            }

            List<String> itemsChangedImpactOnTD = new ArrayList<String>();
            itemsChangedImpactOnTD.add(ImpactConfirmedField.getFieldName());
            boolean isImpactOnTDChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedImpactOnTD);
            if (isImpactOnTDChanged) {
                fireImpactOnTDChanged(issue, user, log);
            }


            //Calculate Technical Closure date
            List<String> itemsChangedIAValidatedImpactConfirmed = new ArrayList<String>();
            itemsChangedIAValidatedImpactConfirmed.add(ImpactConfirmedField.getFieldName());
            itemsChangedIAValidatedImpactConfirmed.add(IA_ValidatedField.getFieldName());
            boolean isIAValidatedImpactConfirmedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedIAValidatedImpactConfirmed);
            if (isIAValidatedImpactConfirmedChanged) {
                fireIAValidatedImpactConfirmedChanged(issue, user, log);
            }

            List<String> itemsChangedIAValidated = new ArrayList<String>();
            itemsChangedIAValidated.add(IA_ValidatedField.getFieldName());
            boolean isIAValidatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedIAValidated);
            if (isIAValidatedChanged) {
                fireIAValidatedChanged(issue, user, log);
            }


            //Calculate effective costs
            if (issueIAWU != null && issue.getCustomFieldValue(IA_NbWUField) != null) {
                IAWuValue = (Double) issueIAWU.getCustomFieldValue(WUValueField);
                IANbWu = (Double) issue.getCustomFieldValue(IA_NbWUField);
                IAEffectiveCost = IAWuValue * IANbWu;
            }
            if (issue.getCustomFieldValue(IA_AdditionalCostField) != null) {
                IAEffectiveCost = IAEffectiveCost + (Double) issue.getCustomFieldValue(IA_AdditionalCostField);
            }

            //Calculate value when Blocked is edited
            List<String> itemsChangedIABlocked = new ArrayList<String>();
            itemsChangedIABlocked.add(IA_BlockedField.getFieldName());

            boolean isIABlockedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedIABlocked);
            if (isIABlockedChanged) {
                fireIABlockedChanged(issue, user, log);
            }

            //Calculate value when Esclated is edited
            List<String> itemsChangedEscalated = new ArrayList<String>();
            itemsChangedEscalated.add(EscalatedField.getFieldName());

            boolean isEscalatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedEscalated);
            if (isEscalatedChanged) {
                fireEscalatedChanged(issue, user, log);
            }

            issue.setCustomFieldValue(IA_EffectiveCostField, IAEffectiveCost);


            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);


            //Set costs data on Trigger
            setCostsDataOnTrigger(issue, user, log, false);


            List<String> itemsChangedIACompany = new ArrayList<String>();
            itemsChangedRestrictedData.add(IACompanyField.getFieldName());

            boolean isIACompanyChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedRestrictedData);
            log.debug("IA Company is changed : " + isIACompanyChanged);
            if (isIACompanyChanged) {
                fireIACompanyChanged(issue, user, log);
            }
        }
    }

    private static void fireIACompanyChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException{
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //CustomField
        CustomField IACompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_COMPANY);
        CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_COMPANYALLOWED);
        CustomField AuthVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFCOMPANY);
        CustomField AuthCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_COMPANY);
        CustomField FormCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_COMPANY);
        CustomField FormVerifCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFCOMPANY);
        CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_COMPANY);
        CustomField IntCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INTCOMPANY);
        CustomField AuthVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTHVERIFCOMPANY);
        CustomField FormVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORMVERIFCOMPANY);

        //Variables
        List<Group> CompanyAllowedList = new ArrayList<Group>();
        List<Group> IntCompanyAllowedList = new ArrayList<Group>();
        List<Group> FormVerifCompanyAllowedList = new ArrayList<Group>();
        List<Group> AuthVerifCompanyAllowedList = new ArrayList<Group>();

        Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
            if (linkedIssueOfPWO1.getProjectId() == ConstantKeys.ID_PR_AWO) {
                log.debug("AWO : " + linkedIssueOfPWO1);
                Collection<Issue> LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO1, user).getAllIssues();
                for (Issue linkedIssueOfAWOForCompany : LinkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                        log.debug("PWO1 found : " + linkedIssueOfAWOForCompany);
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                            String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField).toString();
                            String IA_Company_Formatted = "Partner_" + IA_Company_Value;

                            Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted);
                            log.debug("Adding group : " + IA_Company_Management_Formatted_Group);

                            if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(IA_Company_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != "AIRBUS") {
                            String Auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField).toString();
                            String Auth_Company_Formatted = "Partner_" + Auth_Company_Value;

                            Group Auth_Company_Management_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted);

                            if (!CompanyAllowedList.contains(Auth_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != "AIRBUS") {
                            String Auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField).toString();
                            String Auth_VerifCompany_Formatted = "Partner_" + Auth_VerifCompany_Value;

                            Group Auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Auth_VerifCompany_Formatted);

                            if (!CompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group);
                            }
                            if (!AuthVerifCompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                AuthVerifCompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != "AIRBUS") {
                            String Form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField).toString();
                            String Form_Company_Formatted = "Partner_" + Form_Company_Value;

                            Group Form_Company_Management_Formatted_Group = groupManager.getGroup(Form_Company_Formatted);

                            if (!CompanyAllowedList.contains(Form_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_Company_Management_Formatted_Group);
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != "AIRBUS") {
                            String Form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField).toString();
                            String Form_VerifCompany_Formatted = "Partner_" + Form_VerifCompany_Value;

                            Group Form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Form_VerifCompany_Formatted);

                            if (!CompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group);
                            }
                            if (!FormVerifCompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                FormVerifCompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group);
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != null && linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != "AIRBUS") {
                            String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField).toString();
                            String Int_Company_Formatted = "Partner_" + Int_Company_Value;

                            Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted);

                            if (!CompanyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                                IntCompanyAllowedList.add(Int_Company_Management_Formatted_Group);
                            }
                        }
                    }
                }
                if (CompanyAllowedList != null) {
                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO1.getKey());
                    mutableIssue.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList);
                    if (AuthVerifCompanyAllowedList != null) {
                        issue.setCustomFieldValue(AuthVerifCompanyAllowedField, AuthVerifCompanyAllowedList);
                    }
                    if (FormVerifCompanyAllowedList != null) {
                        issue.setCustomFieldValue(FormVerifCompanyAllowedField, FormVerifCompanyAllowedList);
                    }
                    if (IntCompanyAllowedList != null) {
                        issue.setCustomFieldValue(IntCompanyAllowedField, IntCompanyAllowedList);
                    }
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
//JIRA7
                    issueIndexingService.reIndex(mutableIssue);//JIRA7
                    CompanyAllowedList.clear();
                    AuthVerifCompanyAllowedList.clear();
                    FormVerifCompanyAllowedList.clear();
                    IntCompanyAllowedList.clear();
                }
            }
        }
    }

    private static void fireEscalatedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField EscalatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_ESCALATED);
        CustomField EscalateStartDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_ESCALATESTARTDATE);
        CustomField OldEscalateStartDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OLDESCALATESTARTDATE);
        CustomField EscalateEndDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_ESCALATEENDDATE);
        CustomField OldEscalateEndDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OLDESCALATEENDDATE);
        CustomField TotalEscalatedDurationField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TOTALESCALATEDDURATION);

        //Variables
        String EscalatedTrigger;
        Date EscalateStartDate;
        Date EscalateEndDate;
        String TotalEscalatedDurationTriggerString;
        double TotalEscalatedDurationTrigger;
        double TotalEscalatedDurationOld;
        double TotalEscalatedDuration;
        String TotalEscalatedDurationString;



        Date TodayDate = new LocalDate(new Date()).toDate();
        Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime());

        if (issue.getCustomFieldValue(EscalatedField).toString() == "Yes") {
            EscalateEndDate = null;
            issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            if (issue.getCustomFieldValue(OldEscalateStartDateField) == null || issue.getCustomFieldValue(OldEscalateStartDateField) != TodaysDateCompare) {
                log.debug("Start date to update");
                EscalateStartDate = TodaysDateCompare;
                issue.setCustomFieldValue(EscalateStartDateField, EscalateStartDate);
                issue.setCustomFieldValue(OldEscalateStartDateField, EscalateStartDate);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }

            EscalatedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                    Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                    for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                            Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                            for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                                if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                    //Trigger
                                    Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                    for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                        if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                            Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                            for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                                if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                    log.debug("PWO-0 : " + linkedIssueOfClusterTrigger.getKey());
                                                    //Get total blocking duration of PWO0
                                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField) != null) {
                                                        EscalatedTrigger = "Yes";
                                                    }
                                                    Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                    for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                        if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                            //Get total blocking duration of PWO1
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                EscalatedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                            for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                    if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                        EscalatedTrigger = "Yes";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    FieldConfig fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(linkedIssueOfCluster);
                                    Option valueEscalatedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigEscalatedTrigger)?.find {
                                        it.toString() == EscalatedTrigger;
                                    }
                                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                    mutableIssue.setCustomFieldValue(EscalatedField, valueEscalatedTrigger);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(mutableIssue);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            EscalateEndDate = TodaysDateCompare;
            issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            EscalateStartDate = (Date) issue.getCustomFieldValue(EscalateStartDateField);
            Date OldEscalatedDate = (Date) issue.getCustomFieldValue(OldEscalateEndDateField);
            log.debug("old escalated date : " + OldEscalatedDate);
            log.debug("todaysdate : " + TodaysDateCompare);

            if (issue.getCustomFieldValue(OldEscalateEndDateField) == null || issue.getCustomFieldValue(OldEscalateEndDateField) != TodaysDateCompare) {
                log.debug("set old date");
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issue.setCustomFieldValue(OldEscalateEndDateField, EscalateEndDate);
                issueIndexingService.reIndex(issue);

                if (issue.getCustomFieldValue(TotalEscalatedDurationField) != null) {
                    TotalEscalatedDurationOld = Double.valueOf(issue.getCustomFieldValue(TotalEscalatedDurationField).toString().replace(" d", ""));
                    if (issue.getCustomFieldValue(OldEscalateStartDateField) == TodaysDateCompare || ((Date)issue.getCustomFieldValue(OldEscalateStartDateField)).after(OldEscalatedDate)) {
                        TotalEscalatedDuration = EscalateEndDate - EscalateStartDate + 1;
                    } else {
                        TotalEscalatedDuration = EscalateEndDate - EscalateStartDate;
                    }
                    TotalEscalatedDuration = TotalEscalatedDurationOld + TotalEscalatedDuration;
                } else {
                    TotalEscalatedDuration = EscalateEndDate - EscalateStartDate + 1;
                }
                TotalEscalatedDuration = TotalEscalatedDuration.trunc();
                TotalEscalatedDurationString = TotalEscalatedDuration.trunc() + " d";

                issue.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationString);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

            }

            TotalEscalatedDurationTrigger = 0;
            EscalatedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                    Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                    for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                            Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                            for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                                if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                    //Trigger
                                    Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                    for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
                                        if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                            Collection<Issue> LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                            for (Issue linkedIssueOfClusterTrigger : LinkedIssuesOfClusterTrigger) {
                                                if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                    //Get total blocking duration of PWO0
                                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField) != null) {
                                                        TotalEscalatedDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField).toString();
                                                        TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                        TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + Double.valueOf(TotalEscalatedDurationTriggerString);
                                                    }
                                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                        EscalatedTrigger = "Yes";
                                                    }
                                                    Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                    for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                                        if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                            //Get total blocking duration of PWO1
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField) != null) {
                                                                TotalEscalatedDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField).toString();
                                                                TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + Double.valueOf(TotalEscalatedDurationTriggerString);
                                                            }
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                EscalatedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                            for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                                                if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                    if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                        EscalatedTrigger = "Yes";
                                                                    }
                                                                    if (linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField) != null) {
                                                                        TotalEscalatedDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField).toString();
                                                                        TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                        TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + Double.valueOf(TotalEscalatedDurationTriggerString);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //Set data on trigger

                                    FieldConfig fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(linkedIssueOfCluster);
                                    Option valueEscalatedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigEscalatedTrigger) ?.find {
                                        it.toString() == EscalatedTrigger;
                                    }
                                    MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                    mutableIssue.setCustomFieldValue(EscalatedField, valueEscalatedTrigger);
                                    TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger.trunc();
                                    TotalEscalatedDurationTriggerString = TotalEscalatedDurationTrigger.trunc() + " d";
                                    mutableIssue.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationTriggerString);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(mutableIssue);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    static void resolveCreationOrEditionOnPWO1(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField firstSNAppliField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSNAPPLI);

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(firstSNAppliField.getFieldName());
        Issue issue = event.getIssue();
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        log.debug("issue = " + issue.getKey());

        boolean isCreation = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation || event.getEventTypeId() == ConstantKeys.ID_EVENT_ADMINREOPEN) {//CREATION
            createPWO1(mutableIssue, event.getUser(), log);
        } else {//EDIT
            editPWO1(event, log);
        }
    }


    static void calculationForPWO1(IssueEvent event, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue) event.getIssue();
        List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(issue);

        //CustomField;
        CustomField IA_Validation_ManuallyEditedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_MANUALLYEDITED);
        CustomField IA_ValidatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATED);
        CustomField IA_Validation_NbReworkField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_NBREWORK);


        ///PWO-1;
        //Valeur;
        String IA_Validation_ManuallyEdited = String.valueOf(issue.getCustomFieldValue(IA_Validation_ManuallyEditedField));
        double NbRework = 0;
        if (issue.getCustomFieldValue(IA_Validation_NbReworkField) != null) {
            NbRework = Double.parseDouble(issue.getCustomFieldValue(IA_Validation_NbReworkField).toString());
        }

        //Business;
        double ValeurToReturn;
        double Compteur = 0;

        List<String> itemsChangedIAValidatedStatus = new ArrayList<String>();
        itemsChangedIAValidatedStatus.add(IA_ValidatedField.getFieldName());

        boolean isIaValidated = Toolbox.workloadIsUpdated(changeLog, itemsChangedIAValidatedStatus);
        if (isIaValidated) {
            if (issue.getCustomFieldValue(IA_ValidatedField).toString() == ConstantKeys.VALIDATED || issue.getCustomFieldValue(IA_ValidatedField).toString() == "Rejected") {
                if (IA_Validation_ManuallyEdited == "Yes") {
                    ValeurToReturn = NbRework + 1;
                } else {
                    for (ChangeHistoryItem item : changeItems) {
                        if (item.getField() == ConstantKeys.IA_VALIDATED && item.getTos().values().contains(ConstantKeys.VALIDATED) || item.getField() == ConstantKeys.IA_VALIDATED && item.getTos().values().contains(ConstantKeys.REJECTED)) {
                            Compteur = Compteur + 1;
                        }
                    }
                    ValeurToReturn = Compteur - 1;
                    Compteur = 0;
                }
                issue.setCustomFieldValue(IA_Validation_NbReworkField, ValeurToReturn);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }
        }
    }

    static void fillCompanyAllowedFieldForPWO1(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //Get CustomFields;
        CustomField CompanyAllowed = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_COMPANY_ALLOWED);
        CustomField IA_Company = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_COMPANY);

        List<Group> CompanyAllowedList = new ArrayList<Group>();
        log.debug("issue type : " + issue.getIssueTypeId());

        //PWO-1;
        String IA_Company_Value = String.valueOf(issue.getCustomFieldValue(IA_Company));

        String IA_Company_Formatted = "Partner_" + IA_Company_Value;

        Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted);

        CompanyAllowedList.add(IA_Company_Management_Formatted_Group);


        issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);//JIRA7;
        issueIndexingService.reIndex(issue); //JIRA7;
    }

    static void fillManuallyEditedFieldForPWO1(IssueEvent event, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        MutableIssue issue = (MutableIssue) event.getIssue();
        GenericValue changeLog = event.getChangeLog();

        //CustomField
        CustomField IA_Validation_ManuallyEdited = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_MANUALLYEDITED);
        CustomField IA_ManuallyEdited = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_MANUALLYEDITED);

        List<String> IA_Validated_NbRework_Changed = new ArrayList<String>();
        IA_Validated_NbRework_Changed.add(ConstantKeys.ID_CF_IA_VALIDATION_NBREWORK_STRING);

        List<String> IA_NbRework_Changed = new ArrayList<String>();
        IA_NbRework_Changed.add(ConstantKeys.ID_CF_IA_NBREWORK_STRING);


        //PWO-1;
        boolean IAValidatedUpdated = Toolbox.workloadIsUpdated(changeLog, IA_Validated_NbRework_Changed);
        boolean IAUpdated = Toolbox.workloadIsUpdated(changeLog, IA_NbRework_Changed);

        if (IAValidatedUpdated) {
            issue.setCustomFieldValue(IA_Validation_ManuallyEdited, ConstantKeys.YES);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
        if (IAUpdated) {
            issue.setCustomFieldValue(IA_ManuallyEdited, ConstantKeys.YES);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    static void updateFieldOfAWO(MutableIssue issue, MutableIssue issueSource, ApplicationUser user, Logger log) throws IndexException, CreateException, JqlParseException, SearchException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField ScenarioField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_SCENARIO);
        CustomField TypeOfVerifField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TYPEOFVERIF);
        CustomField DomainField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DOMAIN);

        List<String> Scenario;
        List<Option> valuesToSet = new ArrayList<Option>();
        String Domain;

        double compteur = 0;

        //Business;
        Collection<Issue> LinkedIssuesOfAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfAWO : LinkedIssuesOfAWO) {
            log.debug("linked issue of AWO : " + linkedIssueOfAWO.getKey());
            if (linkedIssueOfAWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                compteur = compteur + 1;
                log.debug("compteur : " + compteur);
            }
        }

        log.debug("compteur final : " + compteur);
        if (issueSource.getIssueTypeId() == ConstantKeys.ID_IT_PWO1 && issue.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
            if (compteur < 2) {
                Scenario = (List<String>)issueSource.getCustomFieldValue(ScenarioField);
                log.debug("Scneario : " + Scenario);

                Option valueTypeOfChange;
                for (String scenari : Scenario) {
                    log.debug("Scenari : " + scenari);
                    FieldConfig fieldConfigTypeOfVerif = TypeOfVerifField.getRelevantConfig(issue);
                    valueTypeOfChange = ComponentAccessor.getOptionsManager().getOptions(fieldConfigTypeOfVerif)?.find {
                        it.toString() == scenari.trim();
                    }
                    log.debug("valueTypeOfChange : " + valueTypeOfChange);
                    if (valuesToSet != null) {
                        valuesToSet.add(valueTypeOfChange);
                    }
                    log.debug("valuesToSet" + valuesToSet);
                }

                if (!valuesToSet.isEmpty()) {
                    issue.setCustomFieldValue(TypeOfVerifField, valuesToSet);
                }

                Domain = String.valueOf(issueSource.getCustomFieldValue(DomainField));
                if (Domain != null) {
                    FieldConfig fieldConfigDomain = DomainField.getRelevantConfig(issue);
                    Option valueDomain = ComponentAccessor.getOptionsManager().getOptions(fieldConfigDomain)?.find {
                        it.toString() == Domain.toString().trim();
                    }
                    issue.setCustomFieldValue(DomainField, valueDomain);
                }

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
                log.debug("index");
            }
        }
    }

    static boolean transitionPWO1ToCompleted(IssueEvent event, ApplicationUser user, Logger log) {
        //Manager;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue)event.getIssue();

        //CustomField;
        CustomField ValidatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_VALIDATED);
        CustomField DNField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DN);


        //Variables;
        boolean result = false;

        //Business;
        List<String> itemsChanged = new ArrayList<String>();
        itemsChanged.add(ValidatedField.getFieldName());
        itemsChanged.add(DNField.getFieldName());

        boolean isWorlogUpdated = Toolbox.workloadIsUpdated(changeLog, itemsChanged);
        if(isWorlogUpdated){
            result = issue.getCustomFieldValue(ValidatedField).toString() == ConstantKeys.VALIDATED && issue.getCustomFieldValue(DNField) != null
        }
        return result;
    }

    static boolean transitionPWO1ToRejected(IssueEvent event, ApplicationUser user, Logger log) {
        //Manager;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue)event.getIssue();

        //CustomField;
        CustomField ValidatedField=customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_VALIDATED);
        CustomField DNField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DN);

        //Variables;
        boolean result = false;

        //Business;
        List<String> itemsChanged = new ArrayList<String>();
        itemsChanged.add(ValidatedField.getFieldName());

        boolean isWorlogUpdated = Toolbox.workloadIsUpdated(changeLog, itemsChanged);
        if(isWorlogUpdated){
            result = issue.getCustomFieldValue(ValidatedField).toString() == ConstantKeys.REJECTED
        }
        return result;
    }

}