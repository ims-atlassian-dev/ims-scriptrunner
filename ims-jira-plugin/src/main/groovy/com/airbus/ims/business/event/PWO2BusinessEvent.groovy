package com.airbus.ims.business.event


import com.airbus.ims.util.ConstantKeys;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import com.airbus.ims.util.Toolbox
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp

/**
 * Projects : PWO
 * Description : Creation/Edit of PWO-2 / PWO-2.1
 * Events : Admin re-open, PWO-2 updated, PWO-2.1 updated, Issue Created
 * isDisabled : false
 */
public class PWO2BusinessEvent {

    public static void createPWO2(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);


        //Variables
        Date requestedDateTrigger;
        Date requestedDate;

        Collection<Issue> linkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssue : linkedIssues) {
            if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                log.debug("PWO-1 : " + linkedIssue.getKey());
                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                        log.debug("PWO-0 : " + linkedIssueOfPWO1.getKey());
                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfPW0 : linkedIssuesOfPWO0) {
                            if (linkedIssueOfPW0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                log.debug("Cluster : " + linkedIssueOfPW0.getKey());
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfPW0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                        log.debug("Trigger : " + linkedIssueOfCluster.getKey());
                                        if (requestedDateTrigger == null || requestedDateTrigger > (Date) linkedIssueOfCluster.getCustomFieldValue(requestedDateField)) {
                                            requestedDateTrigger = (Date) linkedIssueOfCluster.getCustomFieldValue(requestedDateField)
                                            log.debug("Requested date trigger : " + requestedDateTrigger);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (requestedDateTrigger != null) {
            requestedDate = Toolbox.incrementTimestamp(requestedDateTrigger, -28);

            issue.setCustomFieldValue(requestedDateField, requestedDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    public static void fireRestrictedDataChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField restrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);

        //Variable
        String restrictedDataTrigger = "No";

        Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
            if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                        Collection<Issue> linkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO0) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                log.debug("Cluster is : " + linkedIssueOfCurrentPWO0.getKey());
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                        if (linkedIssueOfCluster.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                            restrictedDataTrigger = "Yes";
                                        } else if (restrictedDataTrigger != "Yes" && linkedIssueOfCluster.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                            restrictedDataTrigger = "Unknown";
                                        }
                                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                        for (Issue linkedIssueOfPWO : linkedIssuesOfPWO0) {
                                            if (linkedIssueOfPWO.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                if (linkedIssueOfPWO.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                                    restrictedDataTrigger = "Yes";
                                                } else if (restrictedDataTrigger != "Yes" && linkedIssueOfPWO.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                                    restrictedDataTrigger = "Unknown";
                                                }
                                                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO, user).getAllIssues();
                                                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                        if (linkedIssueOfPWO1.getCustomFieldValue(restrictedDataField).toString() == "Yes") {
                                                            restrictedDataTrigger = "Yes";
                                                        } else if (restrictedDataTrigger != "Yes" && linkedIssueOfPWO1.getCustomFieldValue(restrictedDataField).toString() == "Unknown") {
                                                            restrictedDataTrigger = "Unknown";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            FieldConfig fieldConfigRestrictedData = restrictedDataField.getRelevantConfig(linkedIssueOfCurrentPWO0);
                            Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
                                it.toString() == restrictedDataTrigger;
                            }

                            //Set Restricted data on Cluster
                            MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCurrentPWO0.getKey());
                            mutableIssue.setCustomFieldValue(restrictedDataField, valueRestrictedData);
                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                            issueIndexingService.reIndex(mutableIssue);

                            //Set Restricted data on Triggers
                            Collection<Issue> linkedTriggersOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                            for (Issue linkedTriggerOfCluster : linkedTriggersOfCluster) {
                                if (linkedTriggerOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                    FieldConfig fieldConfigRestrictedDataTrigger = restrictedDataField.getRelevantConfig(linkedTriggerOfCluster);
                                    Option valueRestrictedDataTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedDataTrigger)?.find {
                                        it.toString() == restrictedDataTrigger;
                                    }
                                    mutableIssue = issueManager.getIssueObject(linkedTriggerOfCluster.getKey());
                                    mutableIssue.setCustomFieldValue(restrictedDataField, valueRestrictedDataTrigger);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(mutableIssue);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void fireDomainChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField domainField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DOMAIN);

        String domain = issue.getCustomFieldValue(domainField).toString();
        Collection<Issue> subTasks = issue.getSubTaskObjects();
        MutableIssue mutableIssue;
        for (Issue subTask : subTasks) {
            FieldConfig fieldConfigDomain = domainField.getRelevantConfig(subTask);
            Option valueDomain = ComponentAccessor.getOptionsManager().getOptions(fieldConfigDomain)?.find {
                it.toString() == domain;
            }
            mutableIssue = issueManager.getIssueObject(subTask.getKey());
            mutableIssue.setCustomFieldValue(domainField, valueDomain);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        }
    }

    public static void fireBlockedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField blockedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKED);
        CustomField blockedDomainField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKEDDOMAIN);
        CustomField blockEndDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKENDDATE);
        CustomField oldBlockStartDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OLDBLOCKSTARTDATE);
        CustomField oldBlockEndDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OLDBLOCKENDDATE);
        CustomField blockStartDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKSTARTDATE);
        CustomField totalBlockingDurationField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TOTALBLOCKINGDURATION);

        //Variables
        String totalBlockingDurationString;
        Date blockStartDate;
        Date blockEndDate;

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp todaysDate = new Timestamp(todayDate.getTime());

        double totalBlockingDuration;
        String totalBlockingDurationTriggerString;
        double totalBlockingDurationTrigger;
        double totalBlockingDurationOld;
        String blockedTrigger;

        if (issue.getCustomFieldValue(blockedField).toString() == "Yes") {
            blockEndDate = null;
            issue.setCustomFieldValue(blockEndDateField, blockEndDate);
            blockStartDate = todaysDate;
            issue.setCustomFieldValue(blockStartDateField, blockStartDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            if (issue.getCustomFieldValue(oldBlockStartDateField) == null || issue.getCustomFieldValue(oldBlockStartDateField).toString().substring(0, 10) != todaysDate.toString().substring(0, 10)) {
                log.debug("Old start date to update");
                issue.setCustomFieldValue(oldBlockStartDateField, blockStartDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }
            blockedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
                if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                    Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues();
                    for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                        if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                            Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                            for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                    Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                    for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                        if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                            //Trigger
                                            Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                            for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                                    Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                    for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                            //Get total blocking duration of PWO0
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(blockedDomainField) != null) {
                                                                blockedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                            for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                                    //Get total blocking duration of PWO1
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                        blockedTrigger = "Yes";
                                                                    }
                                                                    Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                    for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                                blockedTrigger = "Yes";
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            //Set data on trigger
                                            MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                            FieldConfig fieldConfigBlockedTrigger = blockedField.getRelevantConfig(linkedIssueOfCluster);
                                            Option valueBlockedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigBlockedTrigger)?.find {
                                                it.toString() == blockedTrigger;
                                            }
                                            mutableIssue.setCustomFieldValue(blockedField, valueBlockedTrigger);
                                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(mutableIssue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            blockEndDate = todaysDate;
            issue.setCustomFieldValue(blockEndDateField, blockEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            Date oldBlockDate = (Date) issue.getCustomFieldValue(oldBlockEndDateField);

            if (issue.getCustomFieldValue(oldBlockEndDateField) == null || issue.getCustomFieldValue(oldBlockEndDateField) != todaysDate) {
                log.debug("Old end date to update");
                issue.setCustomFieldValue(oldBlockEndDateField, blockEndDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

                blockStartDate = (Date) issue.getCustomFieldValue(blockStartDateField);
                if (issue.getCustomFieldValue(totalBlockingDurationField) != null) {
                    totalBlockingDurationOld = Integer.valueOf(issue.getCustomFieldValue(totalBlockingDurationField).toString().replace(" d", ""));
                    if (issue.getCustomFieldValue(oldBlockStartDateField) == todaysDate || ((Date) issue.getCustomFieldValue(oldBlockStartDateField)).after(oldBlockDate)) {
                        totalBlockingDuration = Toolbox.daysBetween(blockEndDate, blockStartDate) + 1;
                    } else {
                        totalBlockingDuration = Toolbox.daysBetween(blockEndDate, blockStartDate);
                    }
                    totalBlockingDuration = totalBlockingDurationOld + totalBlockingDuration;
                } else {
                    totalBlockingDuration = Toolbox.daysBetween(blockEndDate, blockStartDate) + 1;
                }
                totalBlockingDuration = totalBlockingDuration.trunc();
                totalBlockingDurationString = totalBlockingDuration.trunc() + " d";
                issue.setCustomFieldValue(totalBlockingDurationField, totalBlockingDurationString);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }


            totalBlockingDurationTrigger = 0;
            blockedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
                Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues();
                for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                        Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                        for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                        Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                        for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                                Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(totalBlockingDurationField) != null) {
                                                            totalBlockingDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(totalBlockingDurationField).toString()
                                                            totalBlockingDurationTriggerString = totalBlockingDurationTriggerString.replaceAll(" d", "");
                                                            totalBlockingDurationTrigger = Double.parseDouble(totalBlockingDurationTrigger + totalBlockingDurationTriggerString);
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(blockedDomainField) != null) {
                                                            blockedTrigger = "Yes";
                                                        }
                                                        Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                        for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(totalBlockingDurationField) != null) {
                                                                    totalBlockingDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(totalBlockingDurationField).toString();
                                                                    totalBlockingDurationTriggerString = totalBlockingDurationTriggerString.replaceAll(" d", "");
                                                                    totalBlockingDurationTrigger = Double.parseDouble(totalBlockingDurationTrigger + totalBlockingDurationTriggerString);
                                                                }
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                    blockedTrigger = "Yes";
                                                                }
                                                                Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                            blockedTrigger = "Yes";
                                                                        }
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(totalBlockingDurationField) != null) {
                                                                            totalBlockingDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(totalBlockingDurationField).toString();
                                                                            totalBlockingDurationTriggerString = totalBlockingDurationTriggerString.replaceAll(" d", "");
                                                                            totalBlockingDurationTrigger = Double.parseDouble(totalBlockingDurationTrigger + totalBlockingDurationTriggerString);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                        FieldConfig fieldConfigBlockedTrigger = blockedField.getRelevantConfig(linkedIssueOfCluster);
                                        Option valueBlockedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigBlockedTrigger)?.find {
                                            it.toString() == blockedTrigger;
                                        }
                                        mutableIssue.setCustomFieldValue(blockedField, valueBlockedTrigger);
                                        totalBlockingDurationTrigger = totalBlockingDurationTrigger.trunc();
                                        totalBlockingDurationTriggerString = totalBlockingDurationTrigger.trunc() + " d";
                                        mutableIssue.setCustomFieldValue(totalBlockingDurationField, totalBlockingDurationTriggerString);
                                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                        issueIndexingService.reIndex(mutableIssue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void fireEscalatedChanged(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField blockedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKED);
        CustomField escalatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_ESCALATED);
        CustomField escalateStartDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_ESCALATESTARTDATE);
        CustomField oldEscalateStartDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OLDESCALATESTARTDATE);
        CustomField escalateEndDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_ESCALATEENDDATE);
        CustomField oldEscalateEndDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OLDESCALATEENDDATE);
        CustomField totalEscalatedDurationField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TOTALESCALATEDDURATION);

        //Variables
        String totalEscalatedDurationString;
        Date escalateStartDate;
        Date escalateEndDate;

        Integer totalEscalatedDuration;
        String totalEscalatedDurationTriggerString;
        Integer totalEscalatedDurationTrigger;
        Integer totalEscalatedDurationOld;
        String escalatedTrigger;

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp todaysDate = new Timestamp(todayDate.getTime());

        if (issue.getCustomFieldValue(escalatedField).toString() == "Yes") {
            escalateEndDate = null;
            issue.setCustomFieldValue(escalateEndDateField, escalateEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            if (issue.getCustomFieldValue(oldEscalateStartDateField) == null || issue.getCustomFieldValue(oldEscalateStartDateField) != todaysDate) {
                log.debug("Start date to update");
                escalateStartDate = todaysDate;
                issue.setCustomFieldValue(escalateStartDateField, escalateStartDate);
                issue.setCustomFieldValue(oldEscalateStartDateField, escalateStartDate);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);
            }


            escalatedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
                if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                    Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues()
                    for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                        if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                            Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                            for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                    Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                    for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                        if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                            //Trigger
                                            Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                                            for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                                    Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                    for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                            //Get total blocking duration of PWO0
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(escalatedField) != null) {
                                                                escalatedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                            for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                                    //Get total blocking duration of PWO1
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(escalatedField).toString() == "Yes") {
                                                                        escalatedTrigger = "Yes";
                                                                    }
                                                                    Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                    for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(escalatedField).toString() == "Yes") {
                                                                                escalatedTrigger = "Yes";
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                            FieldConfig fieldConfigEscalatedTrigger = escalatedField.getRelevantConfig(linkedIssueOfCluster);
                                            Option valueEscalatedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigEscalatedTrigger)?.find {
                                                it.toString() == escalatedTrigger;
                                            }
                                            mutableIssue.setCustomFieldValue(escalatedField, valueEscalatedTrigger);
                                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(mutableIssue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            escalateEndDate = todaysDate;
            issue.setCustomFieldValue(escalateEndDateField, escalateEndDate);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

            escalateStartDate = (Date) issue.getCustomFieldValue(escalateStartDateField);
            Date oldEscalatedDate = (Date) issue.getCustomFieldValue(oldEscalateEndDateField);

            if (issue.getCustomFieldValue(oldEscalateEndDateField) == null || issue.getCustomFieldValue(oldEscalateEndDateField) != todaysDate) {
                log.debug("set old date");
                issue.setCustomFieldValue(oldEscalateEndDateField, escalateEndDate);
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

                if (issue.getCustomFieldValue(totalEscalatedDurationField) != null) {
                    totalEscalatedDurationOld = Integer.valueOf(issue.getCustomFieldValue(totalEscalatedDurationField).toString().replace(" d", ""));
                    if (issue.getCustomFieldValue(oldEscalateStartDateField) == todaysDate || ((Date) issue.getCustomFieldValue(oldEscalateStartDateField)).after(oldEscalatedDate)) {
                        totalEscalatedDuration = Toolbox.daysBetween(escalateEndDate, escalateStartDate) + 1;
                    } else {
                        totalEscalatedDuration = Toolbox.daysBetween(escalateEndDate, escalateStartDate);
                    }
                    totalEscalatedDuration = totalEscalatedDurationOld + totalEscalatedDuration;
                } else {
                    totalEscalatedDuration = Toolbox.daysBetween(escalateEndDate, escalateStartDate) + 1;
                }
                totalEscalatedDuration = totalEscalatedDuration.trunc();
                totalEscalatedDurationString = totalEscalatedDuration.trunc() + " d";

                issue.setCustomFieldValue(totalEscalatedDurationField, totalEscalatedDurationString);

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(issue);

            }

            totalEscalatedDurationTrigger = 0;
            escalatedTrigger = "No";
            Collection<Issue> linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssueOfCurrentPWO2 : linkedIssuesOfCurrentPWO2) {
                if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                    Collection<Issue> linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues();
                    for (Issue linkedIssueOfCurrentPWO1 : linkedIssuesOfCurrentPWO1) {
                        if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                            Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues();
                            for (Issue linkedIssueOfCurrentPWO0 : linkedIssuesOfCurrentPWO) {
                                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                    Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues();
                                    for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
                                        if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                            //Trigger
                                            Collection<Issue> linkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                                            for (Issue linkedIssueOfTrigger : linkedIssuesOfTrigger) {
                                                if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                                    Collection<Issue> linkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                                                    for (Issue linkedIssueOfClusterTrigger : linkedIssuesOfClusterTrigger) {
                                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                                                            //Get total blocking duration of PWO0
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(totalEscalatedDurationField) != null) {
                                                                totalEscalatedDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(totalEscalatedDurationField).toString();
                                                                totalEscalatedDurationTriggerString = totalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                totalEscalatedDurationTrigger = Integer.valueOf(totalEscalatedDurationTrigger + totalEscalatedDurationTriggerString);
                                                            }
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(escalatedField).toString() == "Yes") {
                                                                escalatedTrigger = "Yes";
                                                            }
                                                            Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues();
                                                            for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                                                                if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                                                    //Get total blocking duration of PWO1
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(totalEscalatedDurationField) != null) {
                                                                        totalEscalatedDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(totalEscalatedDurationField).toString();
                                                                        totalEscalatedDurationTriggerString = totalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                        totalEscalatedDurationTrigger = Integer.valueOf(totalEscalatedDurationTrigger + totalEscalatedDurationTriggerString);
                                                                    }
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(escalatedField).toString() == "Yes") {
                                                                        escalatedTrigger = "Yes";
                                                                    }
                                                                    Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                                                    for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(blockedField).toString() == "Yes") {
                                                                                escalatedTrigger = "Yes";
                                                                            }
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(totalEscalatedDurationField) != null) {
                                                                                totalEscalatedDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(totalEscalatedDurationField).toString();
                                                                                totalEscalatedDurationTriggerString = totalEscalatedDurationTriggerString.replaceAll(" d", "");
                                                                                totalEscalatedDurationTrigger = Integer.valueOf(totalEscalatedDurationTrigger + totalEscalatedDurationTriggerString);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            //Set data on trigger
                                            FieldConfig fieldConfigEscalatedTrigger = escalatedField.getRelevantConfig(linkedIssueOfCluster);
                                            Option valueEscalatedTrigger = ComponentAccessor.getOptionsManager().getOptions(fieldConfigEscalatedTrigger)?.find {
                                                it.toString() == escalatedTrigger;
                                            }
                                            MutableIssue mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                            mutableIssue.setCustomFieldValue(escalatedField, valueEscalatedTrigger);
                                            totalEscalatedDurationTrigger = totalEscalatedDurationTrigger.trunc();
                                            totalEscalatedDurationTriggerString = totalEscalatedDurationTrigger.trunc() + " d";
                                            mutableIssue.setCustomFieldValue(totalEscalatedDurationField, totalEscalatedDurationTriggerString);
                                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(mutableIssue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void editPWO2(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        org.ofbiz.core.entity.GenericValue changeLog = event.getChangeLog();

        //CustomField
        CustomField restrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);
        CustomField domainField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DOMAIN);
        CustomField BlockedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_BLOCKEDDOMAIN);
        CustomField EscalatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_ESCALATED);

        //Variable
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        ApplicationUser user = event.getUser();

        List<String> itemsChangedRestrictedData = new ArrayList<String>();
        itemsChangedRestrictedData.add(restrictedDataField.getFieldName());
        boolean isRestrictedDataChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedRestrictedData);
        if (isRestrictedDataChanged) {
            fireRestrictedDataChanged(mutableIssue, user, log);
        }

        List<String> itemsChangedDomain = new ArrayList<String>();
        itemsChangedDomain.add(domainField.getFieldName());
        boolean isDomainChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedDomain);
        if (isDomainChanged) {
            fireDomainChanged(mutableIssue, user, log);
        }

        List<String> itemsChangedBlocked = new ArrayList<String>();
        itemsChangedBlocked.add(BlockedField.getFieldName());
        boolean isBlockedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedBlocked);
        if (isBlockedChanged) {
            fireBlockedChanged(mutableIssue, user, log);
        }


        List<String> itemsChangedEscalated = new ArrayList<String>();
        itemsChangedEscalated.add(EscalatedField.getFieldName());
        boolean isEscalatedChanged = Toolbox.workloadIsUpdated(changeLog, itemsChangedEscalated);
        if (isEscalatedChanged) {
            fireEscalatedChanged(mutableIssue, user, log);
        }
    }

    public static void resolveCreationOrEditionOnPWO2(IssueEvent event, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomField
        CustomField firstSNAppliField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSNAPPLI);

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(firstSNAppliField.getFieldName());
        Issue issue = event.getIssue();
        MutableIssue mutableIssue = issueManager.getIssueObject(event.getIssue().getKey());
        log.debug("issue = " + issue.getKey());

        boolean isCreation = Toolbox.workloadIsUpdated(event.getChangeLog(), itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation || event.getEventTypeId() == ConstantKeys.ID_EVENT_ADMINREOPEN) {//CREATION
            createPWO2(mutableIssue, event.getUser(), log);
        } else {//EDIT
            editPWO2(event, log);
        }
    }

    public static void linkSelectedAWOUponCreationForPWO2(IssueEvent event, ApplicationUser user, Logger log) throws CreateException, IndexException {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        GenericValue changeLog = event.getChangeLog()
        MutableIssue issue = (MutableIssue) event.getIssue();

        //CustomField;
        CustomField AWOToLinkField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TO_LINK);
        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTAPPLI);
        CustomField CreatePWO21Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_CREATEPWO21);

        //Variables;
        List<Issue> issuesAWOtoLink;

        //Business;
        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());
        log.debug("issue : " + issue.getKey());

        long isCreationTime = 0l;
        long getLinkCollectionTime = 0;
        long endLinkCollectionTme = 0;
        long getPWO1Time = 0;
        long endAWOCollectionTime = 0;
        long updateIssuePWO1Time = 0;
        long indexPWO1Time = 0;
        long updateIssueTime = 0;
        long indexIssueTime = 0;
        long transitionIssueTime = 0;
        long getAWOCollectionTime = 0;
        long begin = System.currentTimeMillis();

        log.debug("creation of PWO2");
        boolean isCreation = Toolbox.workloadIsUpdated(changeLog, itemsChangedAtCreation);
        log.debug("isCreation check: " + isCreation);
        log.debug("FirstSNAppliField value check " + issue.getCustomFieldValue(FirstSNAppliField));
        isCreationTime = System.currentTimeMillis();
        log.debug("checked if creation in ${isCreationTime - begin}");
        if (isCreation) { //Creation;
            //Get parent PWO-1;
            Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            getLinkCollectionTime = System.currentTimeMillis();
            log.debug("got Link Collection in ${getLinkCollectionTime - isCreationTime}");
            for (Issue linkedIssue : LinkedIssues) {
                //If linked issue is PWO-1;
                if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                    getPWO1Time = System.currentTimeMillis();
                    log.debug("PWO-1 linked : ${linkedIssue.key} in ${getPWO1Time - getLinkCollectionTime}");
                    log.debug("awo to link field : " + linkedIssue.getCustomFieldValue(AWOToLinkField));
                    issuesAWOtoLink = Toolbox.getIssueFromNFeedFieldMultiple(AWOToLinkField, linkedIssue, log);
                    getAWOCollectionTime = System.currentTimeMillis();
                    log.debug("Got ${issuesAWOtoLink.size()} AWOs in ${getAWOCollectionTime - getPWO1Time}");
                    for (Issue issueAWOtoLink : issuesAWOtoLink) {
                        // long beginAWO = System.currentTimeMillis();
                        issueLinkManager.createIssueLink(issue.getId(), issueAWOtoLink.getId(), ConstantKeys.ID_LINK_CONCERN, Long.valueOf(1), user);
                        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(issue);
                        // long endAWO = System.currentTimeMillis();
                        // log.debug("AWO evaluated : ${issueAWOtoLink.getKey()} in ${endAWO - beginAWO}";
                    }
                    endAWOCollectionTime = System.currentTimeMillis();
                    log.debug("Ended AWO Link in ${endAWOCollectionTime - getAWOCollectionTime}");
                    log.debug("after link");
                    MutableIssue mutbleIssue = issueManager.getIssueObject(linkedIssue.getKey());
                    mutbleIssue.setCustomFieldValue(AWOToLinkField, null);
                    issueManager.updateIssue(user, mutbleIssue, EventDispatchOption.DO_NOT_DISPATCH, false);

                    updateIssuePWO1Time = System.currentTimeMillis();
                    log.debug("Updated PWO-1 in ${updateIssuePWO1Time - endAWOCollectionTime}");
                    issueIndexingService.reIndex(linkedIssue);

                    indexPWO1Time = System.currentTimeMillis();
                    log.debug("Indexed PWO-1 in ${indexPWO1Time - updateIssuePWO1Time}");
                }
            }
            endLinkCollectionTme = System.currentTimeMillis();
            log.debug("Ended link collection time in  ${endLinkCollectionTme - getLinkCollectionTime}");

            issue.setCustomFieldValue(CreatePWO21Field, "Yes");
            //issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);

            updateIssueTime = System.currentTimeMillis();
            log.debug("Updated issue in  ${updateIssueTime - endLinkCollectionTme}");

            issueIndexingService.reIndex(issue);
            indexIssueTime = System.currentTimeMillis();
            log.debug("Indexed issue in  ${indexIssueTime - updateIssueTime}");

            long end = System.currentTimeMillis();
            log.debug("Ended script in " + (end - begin));
        }
    }

    public static void updateRequestedDateFieldAndRestrictedDataFieldOnPWO2(MutableIssue issue, MutableIssue issueSource, ApplicationUser user, Logger log) throws IndexException, CreateException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);

        //Variables;
        Date RequestedDateTrigger;
        Date RequestedDate;
        String RestrictedDataResult;
        long CompteurPWO1 = 0;

        //Business;
        if (issueSource.getIssueTypeId() == ConstantKeys.ID_IT_PWO1 && issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
            //Get linked issues;
            Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssue : LinkedIssues) {
                //If linked issue is Cluster;
                if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                    CompteurPWO1 = CompteurPWO1 + 1;
                    log.debug("PWO-1 : " + linkedIssue.getKey());
                    RestrictedDataResult = linkedIssue.getCustomFieldValue(RestrictedDataField).toString();
                    Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                    for (Issue LinkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                        if (LinkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                            log.debug("PWO-0 : " + LinkedIssueOfPWO1.getKey());
                            Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfPWO1, user).getAllIssues();
                            for (Issue LinkedIssueOfPW0 : LinkedIssuesOfPWO0) {
                                if (LinkedIssueOfPW0.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                                    log.debug("Cluster : " + LinkedIssueOfPW0.getKey());
                                    Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPW0, user).getAllIssues();
                                    for (Issue LinkedIssueOfCluster : LinkedIssuesOfCluster) {
                                        if (LinkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                                            log.debug("Trigger : " + LinkedIssueOfCluster.getKey());
                                            RequestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (issue.getCustomFieldValue(RequestedDateField) == null) {//Creation;
                if (RequestedDateTrigger != null) {
                    RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, -28);

                    issue.setCustomFieldValue(RequestedDateField, RequestedDate);
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                    issueIndexingService.reIndex(issue);
                }
            }
            if (CompteurPWO1 == 1) {
                if (RestrictedDataResult != null) {
                    FieldConfig fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue);
                    Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
                        it.toString() == RestrictedDataResult;
                    }
                    issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData);

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                    issueIndexingService.reIndex(issue);
                }
            }
        }
    }

    public static void updateRestrictedDataOfPWO2(MutableIssue issue, MutableIssue issueSource, ApplicationUser user, Logger log) throws IndexException, CreateException, JqlParseException, SearchException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        log.debug("issue : " + issue.getKey());
        log.debug("source issue : " + issueSource.getKey());

        //CustomField;
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);

        //Variable
        String RestrictedData;
        double compteur = 0;

        //Business;
        Collection<Issue> LinkedIssuesOfPWO2 = issueLinkManager.getLinkCollection(issueSource, user).getAllIssues();
        for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
            log.debug("linked issue of PWO2 : " + linkedIssueOfPWO2.getKey());
            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                compteur = compteur + 1;
                log.debug("compteur : " + compteur);
            }
        }

        log.debug("compteur final : " + compteur);
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1 && issueSource.getIssueTypeId() == ConstantKeys.ID_IT_PWO2 && compteur < 2) {
            RestrictedData = String.valueOf(issue.getCustomFieldValue(RestrictedDataField));
            log.debug("Restricted data : " + RestrictedData);

            if (RestrictedData != null) {
                FieldConfig fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issueSource);
                Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
                    it.toString() == RestrictedData.trim();
                }
                issueSource.setCustomFieldValue(RestrictedDataField, valueRestrictedData);
            }

            issueManager.updateIssue(user, issueSource, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issueSource);
            log.debug("index");
        }
    }

    public static boolean createPWO21(IssueEvent event, ApplicationUser user, Logger log) {
        //Manager;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue) event.getIssue();

        //CustomField;
        CustomField FirstSNField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSN);
        CustomField CreatePWO21Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_CREATEPWO21);


        //Variables
        boolean result = false;

        log.debug("issue : " + issue.getKey());

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        //itemsChangedAtCreation.add(FirstSNField.getFieldName());
        itemsChangedAtCreation.add(CreatePWO21Field.getFieldName());

        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
            log.debug("issue is PWO2");
            log.debug("CreatePWO21Field value : " + issue.getCustomFieldValue(CreatePWO21Field));
            boolean isCreation = Toolbox.workloadIsUpdated(changeLog, itemsChangedAtCreation);
            log.debug("is creation : " + isCreation);
            if (isCreation) {//Creation;
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }
}