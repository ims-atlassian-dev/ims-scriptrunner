package com.airbus.ims.business.event

import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.exception.CreateException
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.issue.security.IssueSecurityLevel
import com.atlassian.jira.issue.security.IssueSecurityLevelManager
import com.atlassian.jira.issue.worklog.Worklog
import com.atlassian.jira.issue.worklog.WorklogManager
import com.atlassian.jira.jql.parser.JqlParseException
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.query.Query
import org.apache.log4j.Logger

class PWOBusinessEvent {

    static void manageTimeTrackingInPWO(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {

        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        WorklogManager worklogManager = ComponentAccessor.getWorklogManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomFields;
        CustomField domainCF = customFieldManager.getCustomFieldObject(ConstantKeys.CF_DOMAIN);
        CustomField partsDataCF = customFieldManager.getCustomFieldObject(ConstantKeys.CF_TS_PARTS_DATA);
        CustomField maintPlanningCF = customFieldManager.getCustomFieldObject(ConstantKeys.CF_TS_MAINT_PLANNING);
        CustomField maintCF = customFieldManager.getCustomFieldObject(ConstantKeys.CF_TS_MAINT);
        CustomField IACF = customFieldManager.getCustomFieldObject(ConstantKeys.CF_TS_IA);
        CustomField AuthCF = customFieldManager.getCustomFieldObject(ConstantKeys.CF_TS_AUTH);
        CustomField FormCF = customFieldManager.getCustomFieldObject(ConstantKeys.CF_TS_FORM);
        CustomField IntCF = customFieldManager.getCustomFieldObject(ConstantKeys.CF_TS_INT);


        String PARTS_DATA = "Parts Data";
        String MAINT_PLANNING = "Maintenance Planning";
        String MAINT = "Maintenance";

        //get domain selected during transition;
        String domainSelected = issue.getCustomFieldValue(domainCF).toString();

        //Get Time Spent logged during transition;
        List<Worklog> worklogs = worklogManager.getByIssue(issue);
        Long lastTimeSpent = worklogs.get(worklogs.size() - 1).getTimeSpent();

        log.debug("lastTimeSpent " + lastTimeSpent);

        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
            if (PARTS_DATA.equals(domainSelected)) {
                String newTimeSpent = Toolbox.sumTimeSpentForDomain(issue, partsDataCF, lastTimeSpent);
                issue.setCustomFieldValue(partsDataCF, newTimeSpent);
            } else if (MAINT_PLANNING.equals(domainSelected)) {
                String newTimeSpent = Toolbox.sumTimeSpentForDomain(issue, maintPlanningCF, lastTimeSpent);
                issue.setCustomFieldValue(maintPlanningCF, newTimeSpent);
            } else if (MAINT.equals(domainSelected)) {
                String newTimeSpent = Toolbox.sumTimeSpentForDomain(issue, maintCF, lastTimeSpent);
                issue.setCustomFieldValue(maintCF, newTimeSpent);
            }

            //Clear domain;
            issue.setCustomFieldValue(domainCF, null);

            //Update Issue;
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);

        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
            log.debug("PWO-1");
            String newTimeSpent = Toolbox.sumTimeSpentForDomain(issue, IACF, lastTimeSpent);
            log.debug("newTimeSpent : " + newTimeSpent);
            issue.setCustomFieldValue(IACF, newTimeSpent);
            //Update Issue;
            issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
            log.debug("PWO-2.1");
            String newTimeSpent = Toolbox.sumTimeSpentForDomain(issue, AuthCF, lastTimeSpent);
            log.debug("newTimeSpent : " + newTimeSpent);
            issue.setCustomFieldValue(AuthCF, newTimeSpent);
            //Update Issue;
            issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
            log.debug("PWO-2.2");
            String newTimeSpent = Toolbox.sumTimeSpentForDomain(issue, FormCF, lastTimeSpent);
            log.debug("newTimeSpent : " + newTimeSpent);
            issue.setCustomFieldValue(FormCF, newTimeSpent);
            //Update Issue;
            issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
            log.debug("PWO-2.3");
            String newTimeSpent = Toolbox.sumTimeSpentForDomain(issue, IntCF, lastTimeSpent);
            log.debug("newTimeSpent : " + newTimeSpent);
            issue.setCustomFieldValue(IntCF, newTimeSpent);
            //Update Issue;
            issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false);
        }
    }


    static void updateTitleFieldForPWOAndCopyItToSummary(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException, CreateException, JqlParseException, SearchException {
        log.warn("PWO created UpdateTitleCopyToSummary");
        log.debug("Creation/Edit of a PWO, update of the reference");

        //Manager;
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class);
        SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class);

        //CustomField;
        CustomField ProgramField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PROGRAM);
        CustomField ReferenceField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REFERENCE);
        CustomField IncrementalIDField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INCREMENTALID);

        //Business;

        log.debug("issue : " + issue.getKey());

        String ValueToReturn;
        String ProgramValueBrut;
        //String[] ProgramValueList;
        String ProgramValue;
        String YearValue;
        String PWO0 = "PWO0";
        String PWO1 = "PWO1";
        String PWO2 = "PWO2";
        String PWO21 = "PWO21";
        String PWO22 = "PWO22";
        String PWO23 = "PWO23";
        String AWO = "AWO";
        String JQL_QUERY = "project = \"%s\" and issuetype in  (\"%s\") AND created > startOfYear() and \"Program\"=\"%s [Program_%s]\" and \"Incremental ID\" is not empty ORDER BY \"Incremental ID\" DESC";
        String IT;
        String CompleteCompteur;
        String query;
        double compteur = 0;

        List<Issue> issuesFound = new ArrayList<Issue>();
        Issue firstIssue = null;


        ProgramValueBrut = String.valueOf(issue.getCustomFieldValue(ProgramField));
        ProgramValue = ProgramValueBrut.replaceAll("Program_", "").replaceAll("<content>", "").replaceAll("<value>", "").replaceAll("</value>", ",").replaceAll("</content>", "").replaceAll(",", "").replaceAll("\n", "").replaceAll("  ", "");


        String DateCreated = String.valueOf(issue.getCreated());
        YearValue = DateCreated.replaceAll("/-.*/", "");

        log.debug("issue type : " + issue.getIssueTypeId());

        if (issue.getIssueTypeId() == "10004") {
            IT = PWO0;
        } else if (issue.getIssueTypeId() == "10005") {
            IT = PWO1;
        } else if (issue.getIssueTypeId() == "10006") {
            IT = PWO2;
        } else if (issue.getIssueTypeId() == "10007") {
            IT = PWO21;
        } else if (issue.getIssueTypeId() == "10008") {
            IT = PWO22;
        } else if (issue.getIssueTypeId() == "10009") {
            IT = PWO23;
        } else {
            IT = AWO;
        }

        query = String.format(JQL_QUERY, issue.getProjectObject().getName(), issue.getIssueType().getName(), ProgramValue, ProgramValue, issue.getId());
        log.debug("Query : " + query);
        Query JQLquery = jqlQueryParser.parseQuery(query);
        SearchResults results = searchProvider.search(JQLquery, user, PagerFilter.getUnlimitedFilter());
        issuesFound = results.getIssues();
        log.debug("issues found : " + issuesFound);
        log.debug("issues found size : " + issuesFound.size());
        if (issuesFound.size() > 0) {
            if (issuesFound.get(0).getKey() != issue.getKey()) {
                firstIssue = issuesFound.get(0);
            } else if (issuesFound.get(1) != null) {
                firstIssue = issuesFound.get(1);
            } else {
                compteur = 1;
            }
            log.debug("issue : " + firstIssue);
            if (compteur == 0 && firstIssue.getCustomFieldValue(IncrementalIDField) != null) {
                compteur = Math.round(Double.parseDouble(firstIssue.getCustomFieldValue(IncrementalIDField).toString())) + 1;
            }
            log.debug("compteur : " + compteur);
        } else {
            compteur = 1;
        }

        if (compteur < 10) {
            CompleteCompteur = "0000";
        } else if (compteur < 100) {
            CompleteCompteur = "000";
        } else if (compteur < 1000) {
            CompleteCompteur = "00";
        } else if (compteur < 10000) {
            CompleteCompteur = "0";
        } else {
            CompleteCompteur = "";
        }

        ValueToReturn = ProgramValue + "_" + YearValue + "_" + IT + "_" + CompleteCompteur + Math.round(compteur);
        log.debug("Value : " + ValueToReturn);


        issue.setCustomFieldValue(ReferenceField, ValueToReturn);
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);

        issue.setCustomFieldValue(IncrementalIDField, compteur);
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);

        issue.setSummary(ValueToReturn);
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }


    static void checkUnicityOfSummaryForPWOAndFillWarningFieldIfNeeded(IssueEvent event, ApplicationUser user, Logger log) throws JqlParseException, SearchException, IndexException {
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class);
        SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class);
        MutableIssue issue = (MutableIssue) event.getIssue();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);


        CustomField WarningField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WARNING);


        String JQL_QUERY = "issuetype in (\"%s\") AND summary ~ \"%s\" ORDER BY Key";
        List<Issue> issuesFound = new ArrayList<Issue>();
        List<Issue> issuesFound2 = new ArrayList<Issue>();
        String issuesResult = "";
        String issuesResult2 = "";
        String Debut = "*{color:#ff0000}Other issue with the same title exists :{color}* ";
        String toRet;

        log.debug("issue : "+issue.getKey());

        String query = String.format(JQL_QUERY,issue.getIssueType().getName(),issue.getSummary());
        log.debug("Query : " + query);
        Query JQLquery = jqlQueryParser.parseQuery(query);
        SearchResults results = searchProvider.search(JQLquery, user, PagerFilter.getUnlimitedFilter());
        issuesFound = results.getIssues();

        if (issuesFound.size()>1){
            for (Issue issueFound : issuesFound){
                if (issueFound.getKey() != issue.getKey()){
                    issuesResult += "*" + issueFound.getKey()+"* ";

                    String query2 = String.format(JQL_QUERY,issueFound.getIssueType().getName(),issueFound.getSummary());
                    log.debug("Query 2 : " + query2);
                    Query JQLquery2 = jqlQueryParser.parseQuery(query2);
                    SearchResults results2 = searchProvider.search(JQLquery2, user, PagerFilter.getUnlimitedFilter());
                    issuesFound2 = results2.getIssues();

                    for (Issue issueFound2 : issuesFound2){
                        if (issueFound2.getKey() != issueFound.getKey()){
                            issuesResult2 += "*" + issueFound2.getKey()+"* ";
                        }
                    }

                    MutableIssue issueFoundMutable = issueManager.getIssueObject(issueFound.getKey());
                    String toRet2 = Debut + issuesResult2;
                    issueFoundMutable.setCustomFieldValue(WarningField,toRet2);
                    issueManager.updateIssue(user, issueFoundMutable, EventDispatchOption.DO_NOT_DISPATCH, false);
                    issueIndexingService.reIndex(issueFound);
                    toRet2 = "";
                    issuesResult2="";
                }
            }
            toRet = Debut+issuesResult;

            issue.setCustomFieldValue(WarningField,toRet);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    static void copySelectedGroupInProgramFieldToSecurityLevel(IssueEvent event, ApplicationUser user, Logger log) throws IndexException {
        //Manager;
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        MutableIssue issue = (MutableIssue) event.getIssue();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        IssueSecurityLevelManager issueSecurityLevelManager = ComponentAccessor.getIssueSecurityLevelManager();

        //CustomField;
        CustomField ProgramField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PROGRAM);

        //Business;
        log.debug("issue : " + issue.getKey());


        String ProgramnFeed = issue.getCustomFieldValue(ProgramField).toString();
        Collection<IssueSecurityLevel> SecurityLevel;
        long SecurityId;

        String[] MOAnFeedFormatted = ProgramnFeed.substring(19).replaceAll("</value>/\n/</content>", "").replaceAll("</value>", ",").replaceAll("<value>", "").replaceAll("/\n/", "").replace("  ", " ").split(",");

        SecurityLevel = issueSecurityLevelManager.getIssueSecurityLevelsByName(MOAnFeedFormatted[0]);

        SecurityId = ((IssueSecurityLevel)SecurityLevel.toArray()[0]).getId();

        if (issue.getSecurityLevelId() != SecurityId) {
            issue.setSecurityLevelId(SecurityId);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false); //JIRA7;
            issueIndexingService.reIndex(issue); //JIRA7;
        }
    }
}
