package com.airbus.ims.business.event


import com.airbus.ims.util.ConstantKeys;
import com.airbus.ims.util.Toolbox;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp;

/**
 * Projects : TRG
 * Description : Creation / Edit on Trigger
 * Events : Issue Created, Trigger updated
 * isDisabled : false
 */
class TriggerBusinessEvent{

    /**
     *
     * @param event
     * @param user
     * @param log
     * @throws IndexException
     * @throws CreateException
     */
    static void editTrigger(IssueEvent event, ApplicationUser user, Logger log) throws IndexException, CreateException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GenericValue changeLog = event.getChangeLog();

        List<String> ObjFreezeIT = new ArrayList<String>();
        ObjFreezeIT.add(ConstantKeys.ID_IT_MOAOBJFREEZE);
        ObjFreezeIT.add(ConstantKeys.ID_IT_AIRBUSOBJFREEZE);

        MutableIssue issue = (MutableIssue) event.getIssue();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField;
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATE);
        CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TRIGGERTOTALEFFECTIVECOSTS);
        CustomField PlannedQG2Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PLANNEDQG2);
        CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);
        CustomField ObjFreezeField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OBJFREEZE);
        CustomField PreIA_PartsData_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_PARTSDATA_REQUESTEDDATE);
        CustomField PreIA_PartsData_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_PARTSDATA_VALIDATION_REQUESTEDDATE);
        CustomField PartsData_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_PREIA_1STCOMMITEDDATE);
        CustomField PartsData_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_PREIA_LASTCOMMITEDDATE);
        CustomField PartsData_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_PREIA_VALIDATION_1STCOMMITEDDATE);
        CustomField PartsData_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_PREIA_VALIDATION_LASTCOMMITEDDATE);

        CustomField PreIA_MaintPlanning_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINTPLANNING_REQUESTEDDATE);
        CustomField PreIA_MaintPlanning_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINTPLANNING_VALIDATION_REQUESTEDDATE);
        CustomField MaintPlanning_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_PREIA_1STCOMMITEDDATE);
        CustomField MaintPlanning_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_PREIA_LASTCOMMITEDDATE);
        CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_VALIDATION_PREIA_1STCOMMITEDDATE);
        CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_VALIDATION_PREIA_LASTCOMMITEDDATE);

        CustomField PreIA_Maint_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINT_REQUESTEDDATE);
        CustomField PreIA_Maint_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINT_VALIDATION_REQUESTEDDATE);
        CustomField Maint_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_PREIA_1STCOMMITEDDATE);
        CustomField Maint_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_PREIA_LASTCOMMITEDDATE);
        CustomField Maint_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_PREIA_VALIDATION_1STCOMMITEDDATE);
        CustomField Maint_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_PREIA_VALIDATION_LASTCOMMITEDDATE);

        CustomField IA_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_REQUESTEDDATE);
        CustomField IA_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_REQUESTEDDATE);
        CustomField IA_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_1STCOMMITTEDDATE);
        CustomField IA_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_LASTCOMMITTEDDATE);
        CustomField IA_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_1STCOMMITTEDDATE);
        CustomField IA_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE);

        CustomField Auth_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_REQUESTEDDATE);
        CustomField Auth_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_REQUESTEDDATE);
        CustomField Auth_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_REQUESTEDDATE);

        CustomField Auth_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_FIRSTCOMMITTEDDATE);
        CustomField Auth_Verification_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_FIRSTCOMMITTEDDATE);
        CustomField Auth_Validation_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE);

        CustomField Auth_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_LASTCOMMITTEDDATE);
        CustomField Auth_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField Auth_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);

        CustomField Form_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_REQUESTEDDATE);
        CustomField Form_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_REQUESTEDDATE);
        CustomField Form_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_REQUESTEDDATE);

        CustomField Form_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_1STCOMMITTEDDATE);
        CustomField Form_Verification_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_1STCOMMITTEDDATE);
        CustomField Form_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE);

        CustomField Form_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_LASTCOMMITTEDDATE);
        CustomField Form_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField Form_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE);

        CustomField Int_RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_REQUESTEDDATE);
        CustomField Int_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_1STCOMMITTEDDATE);
        CustomField Int_LastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_LASTCOMMITTEDDATE);


        //Variables;
        Timestamp PlannedQG2;
        double PartsDataEffectiveCosts = 0;
        double MaintPlanningEffectiveCosts = 0;
        double MaintEffectiveCosts = 0;
        double TriggerEffectiveCost = 0;
        String RestrictedDataResult = "No";
        MutableIssue ClusterIssue;
        Date RequestedDatePWO0;
        Date RequestedDatePWO1;
        Date RequestedDateTrigger;
        Date PreIA_PartsData_RequestedDate;
        Date PreIA_PartsData_Validation_RequestedDate;
        Date PreIA_MaintPlanning_RequestedDate;
        Date PreIA_MaintPlanning_Validation_RequestedDate;
        Date PreIA_Maint_RequestedDate;
        Date PreIA_Maint_Validation_RequestedDate;
        Date IA_RequestedDate;
        Date IA_Validation_RequestedDate;

        Date Auth_RequestedDate;
        Date Auth_Verification_RequestedDate;
        Date Auth_Validation_RequestedDate;
        Date Form_RequestedDate;
        Date Form_Verification_RequestedDate;
        Date Form_Validation_RequestedDate;
        Date Int_RequestedDate;

        List<String> itemsChanged = new ArrayList<String>();
        itemsChanged.add(ObjFreezeField.getFieldName());

        log.debug("Edit of " + issue.getKey());
        //If Obj. Freeze has been updated, change link;
        boolean isWorlogUpdated = Toolbox.workloadIsUpdated(changeLog, itemsChanged);
        if (isWorlogUpdated) {
            //Delete old link;
            Collection<Issue> linkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssue : linkedIssues) {
                if (ObjFreezeIT.contains(linkedIssue.getIssueTypeId())) {
                    IssueLink linkToRemove = issueLinkManager.getIssueLink(linkedIssue.getId(), issue.getId(), ConstantKeys.ID_LINK_AGGREGATES);
                    issueLinkManager.removeIssueLink(linkToRemove, user);
                }
            }
            //Create new link;
            if (issue.getCustomFieldValue(ObjFreezeField) != null) {
                Issue toLink = Toolbox.getIssueFromNFeedFieldSingle(ObjFreezeField, issue, log);
                log.debug("to link : " + toLink.getKey());
                Long toLink_id = toLink.getId();
                issueLinkManager.createIssueLink(toLink_id, issue.getId(), ConstantKeys.ID_LINK_AGGREGATES, 1L, user);
            }
            //Set data on Trigger;
            if (PlannedQG2 != null) {
                issue.setCustomFieldValue(RequestedDateField, PlannedQG2);
            }
            issue.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost);

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }

        //Get linked issue;
        Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssue : LinkedIssues) {
            //If linked issue is Obj. Freeze, get data;
            if (ObjFreezeIT.contains(linkedIssue.getIssueTypeId())) {
                PlannedQG2 = (Timestamp) linkedIssue.getCustomFieldValue(PlannedQG2Field);
            }
            //If linked issue is Cluster, calculate Restricted Data;
            if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                //Get linked issue of Cluster;
                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues();
                for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                    //If linked issue of Cluster is Trigger;
                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                        if (linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                            RestrictedDataResult = "Yes";
                        } else if (RestrictedDataResult != "Yes" && linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                            RestrictedDataResult = "Unknown";
                        }
                    }
                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                        MutableIssue mutableIssue;
                        Collection<Issue> LinkedIssuesOfPWO0RequestedDate = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                        for (Issue linkedIssueOfPWO0RequestedDate : LinkedIssuesOfPWO0RequestedDate) {
                            if (linkedIssueOfPWO0RequestedDate.getIssueTypeId()==ConstantKeys.ID_IT_CLUSTER){
                                Collection<Issue> LinkedIssuesOfClusterOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0RequestedDate, user).getAllIssues();
                                for (Issue linkedIssueOfClusterOfPWO0 : LinkedIssuesOfClusterOfPWO0) {
                                    if (linkedIssueOfClusterOfPWO0.getIssueTypeId()==ConstantKeys.ID_IT_TRIGGER){
                                        if (linkedIssueOfClusterOfPWO0.getCustomFieldValue(RequestedDateField) != null){
                                            if(RequestedDateTrigger == null || RequestedDateTrigger.after((Date)linkedIssueOfClusterOfPWO0.getCustomFieldValue(RequestedDateField))){
                                                RequestedDateTrigger = (Date) linkedIssueOfClusterOfPWO0.getCustomFieldValue(RequestedDateField);
                                                log.debug("Requested date trigger : " + RequestedDateTrigger);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //RequestedDateTrigger = PlannedQG2;
                        if (RequestedDateTrigger != null) {
                            if (linkedIssueOfCluster.getStatus().getName() == "Not started") {
                                RequestedDatePWO0 = Toolbox.incrementTimestamp(RequestedDateTrigger, - 119);

                                PreIA_PartsData_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 126);
                                PreIA_PartsData_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 119);
                                Date PartsData_PreIA_1stCommitedDate = PreIA_PartsData_RequestedDate;
                                Date PartsData_PreIA_LastCommitedDate = PartsData_PreIA_1stCommitedDate;
                                Date PartsData_PreIA_Validation_1stCommitedDate = PreIA_PartsData_Validation_RequestedDate;
                                Date PartsData_PreIA_Validation_LastCommitedDate = PartsData_PreIA_Validation_1stCommitedDate;

                                PreIA_MaintPlanning_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 126);
                                PreIA_MaintPlanning_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 119);
                                Date MaintPlanning_PreIA_1stCommitedDate = PreIA_MaintPlanning_RequestedDate;
                                Date MaintPlanning_PreIA_LastCommitedDate = MaintPlanning_PreIA_1stCommitedDate;
                                Date MaintPlanning_PreIA_Validation_1stCommitedDate = PreIA_MaintPlanning_Validation_RequestedDate;
                                Date MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_PreIA_Validation_1stCommitedDate;

                                PreIA_Maint_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 126);
                                PreIA_Maint_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 119);
                                Date Maint_PreIA_1stCommitedDate = PreIA_Maint_RequestedDate;
                                Date Maint_PreIA_LastCommitedDate = Maint_PreIA_1stCommitedDate;
                                Date Maint_PreIA_Validation_1stCommitedDate = PreIA_Maint_Validation_RequestedDate;
                                Date Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_1stCommitedDate;

                                mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                                mutableIssue.setCustomFieldValue(RequestedDateField, RequestedDatePWO0);
                                mutableIssue.setCustomFieldValue(PreIA_PartsData_RequestedDateField, PreIA_PartsData_RequestedDate);
                                mutableIssue.setCustomFieldValue(PreIA_PartsData_Validation_RequestedDateField, PreIA_PartsData_Validation_RequestedDate);
                                mutableIssue.setCustomFieldValue(PartsData_PreIA_1stCommitedDateField, PartsData_PreIA_1stCommitedDate);
                                mutableIssue.setCustomFieldValue(PartsData_PreIA_LastCommitedDateField, PartsData_PreIA_LastCommitedDate);
                                mutableIssue.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate);
                                mutableIssue.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate);

                                mutableIssue.setCustomFieldValue(PreIA_MaintPlanning_RequestedDateField, PreIA_MaintPlanning_RequestedDate);
                                mutableIssue.setCustomFieldValue(PreIA_MaintPlanning_Validation_RequestedDateField, PreIA_MaintPlanning_Validation_RequestedDate);
                                mutableIssue.setCustomFieldValue(MaintPlanning_PreIA_1stCommitedDateField, MaintPlanning_PreIA_1stCommitedDate);
                                mutableIssue.setCustomFieldValue(MaintPlanning_PreIA_LastCommitedDateField, MaintPlanning_PreIA_LastCommitedDate);
                                mutableIssue.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate);
                                mutableIssue.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate);

                                mutableIssue.setCustomFieldValue(PreIA_Maint_RequestedDateField, PreIA_Maint_RequestedDate);
                                mutableIssue.setCustomFieldValue(PreIA_Maint_Validation_RequestedDateField, PreIA_Maint_Validation_RequestedDate);
                                mutableIssue.setCustomFieldValue(Maint_PreIA_1stCommitedDateField, Maint_PreIA_1stCommitedDate);
                                mutableIssue.setCustomFieldValue(Maint_PreIA_LastCommitedDateField, Maint_PreIA_LastCommitedDate);
                                mutableIssue.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate);
                                mutableIssue.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate);

                                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                issueIndexingService.reIndex(linkedIssueOfCluster);
                            }
                            Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                            for (Issue LinkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                                if (LinkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1 && LinkedIssueOfPWO0.getStatus().getName() == "Not started") {
                                    RequestedDatePWO0 = Toolbox.incrementTimestamp(RequestedDateTrigger, - 98);
                                    IA_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 105);
                                    IA_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 98);
                                    Date IA_1stCommittedDate = IA_RequestedDate;
                                    Date IA_LastCommittedDate = IA_1stCommittedDate;
                                    Date IA_Validation_1stCommittedDate = IA_Validation_RequestedDate;
                                    Date IA_Validation_LastCommittedDate = IA_Validation_1stCommittedDate;

                                    mutableIssue = issueManager.getIssueObject(LinkedIssueOfPWO0.getKey());
                                    mutableIssue.setCustomFieldValue(RequestedDateField, RequestedDatePWO0);
                                    mutableIssue.setCustomFieldValue(IA_RequestedDateField, IA_RequestedDate);
                                    mutableIssue.setCustomFieldValue(IA_Validation_RequestedDateField, IA_Validation_RequestedDate);
                                    mutableIssue.setCustomFieldValue(IA_1stCommittedDateField, IA_1stCommittedDate);
                                    mutableIssue.setCustomFieldValue(IA_LastCommittedDateField, IA_LastCommittedDate);
                                    mutableIssue.setCustomFieldValue(IA_Validation_1stCommittedDateField, IA_Validation_1stCommittedDate);
                                    mutableIssue.setCustomFieldValue(IA_Validation_LastCommittedDateField, IA_Validation_LastCommittedDate);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(LinkedIssueOfPWO0);
                                }
                            }
                        }

                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                        for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                if (linkedIssueOfPWO0.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                    RequestedDatePWO1 = Toolbox.incrementTimestamp(RequestedDateTrigger, - 98);
                                    IA_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 105);
                                    IA_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 98);
                                    Date IA_1stCommittedDate = IA_RequestedDate;
                                    Date IA_LastCommittedDate = IA_1stCommittedDate;
                                    Date IA_Validation_1stCommittedDate = IA_Validation_RequestedDate;
                                    Date IA_Validation_LastCommittedDate = IA_Validation_1stCommittedDate;

                                    mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO0.getKey());
                                    mutableIssue.setCustomFieldValue(RequestedDateField, RequestedDatePWO1);
                                    mutableIssue.setCustomFieldValue(IA_RequestedDateField, IA_RequestedDate);
                                    mutableIssue.setCustomFieldValue(IA_Validation_RequestedDateField, IA_Validation_RequestedDate);
                                    mutableIssue.setCustomFieldValue(IA_1stCommittedDateField, IA_1stCommittedDate);
                                    mutableIssue.setCustomFieldValue(IA_LastCommittedDateField, IA_LastCommittedDate);
                                    mutableIssue.setCustomFieldValue(IA_Validation_1stCommittedDateField, IA_Validation_1stCommittedDate);
                                    mutableIssue.setCustomFieldValue(IA_Validation_LastCommittedDateField, IA_Validation_LastCommittedDate);
                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(linkedIssueOfPWO0);
                                }

                                Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                        //To remove if update not needed;
                                        if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2 && linkedIssueOfPWO1.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                            Date RequestedDatePWO2 = Toolbox.incrementTimestamp(RequestedDateTrigger, - 28);

                                            mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO1.getKey());
                                            mutableIssue.setCustomFieldValue(RequestedDateField, RequestedDatePWO2);
                                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(linkedIssueOfPWO1);
                                        }

                                        Collection<Issue> LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                        for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
                                            if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
                                                //To remove if update not needed;
                                                if (linkedIssueOfPWO2.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                                    Auth_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 84);
                                                    Auth_Verification_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 70);
                                                    Auth_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 56);

                                                    Date Auth_FirstCommittedDate = Auth_RequestedDate;
                                                    Date Auth_Verification_FirstCommittedDate = Auth_Verification_RequestedDate;
                                                    Date Auth_Validation_FirstCommittedDate = Auth_Validation_RequestedDate;

                                                    Date Auth_LastCommittedDate = Auth_FirstCommittedDate;
                                                    Date Auth_Verification_LastCommittedDate = Auth_Verification_FirstCommittedDate;
                                                    Date Auth_Validation_LastCommittedDate = Auth_Validation_FirstCommittedDate;

                                                    mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO2.getKey());
                                                    mutableIssue.setCustomFieldValue(Auth_RequestedDateField, Auth_RequestedDate);
                                                    mutableIssue.setCustomFieldValue(Auth_Verification_RequestedDateField, Auth_Verification_RequestedDate);
                                                    mutableIssue.setCustomFieldValue(Auth_Validation_RequestedDateField, Auth_Validation_RequestedDate);

                                                    mutableIssue.setCustomFieldValue(Auth_FirstCommittedDateField, Auth_FirstCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Auth_Verification_FirstCommittedDateField, Auth_Verification_FirstCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Auth_Validation_FirstCommittedDateField, Auth_Validation_FirstCommittedDate);

                                                    mutableIssue.setCustomFieldValue(Auth_LastCommittedDateField, Auth_LastCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Auth_Verification_LastCommittedDateField, Auth_Verification_LastCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Auth_Validation_LastCommittedDateField, Auth_Validation_LastCommittedDate);

                                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                    issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                }
                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
                                                //To remove if update not needed;
                                                if (linkedIssueOfPWO2.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                                    Form_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 49);
                                                    Form_Verification_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 42);
                                                    Form_Validation_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 35);

                                                    Date Form_FirstCommittedDate = Form_RequestedDate;
                                                    Date Form_Verification_FirstCommittedDate = Form_Verification_RequestedDate;
                                                    Date Form_Validation_FirstCommittedDate = Form_Validation_RequestedDate;

                                                    Date Form_LastCommittedDate = Form_FirstCommittedDate;
                                                    Date Form_Verification_LastCommittedDate = Form_Verification_FirstCommittedDate;
                                                    Date Form_Validation_LastCommittedDate = Form_Validation_FirstCommittedDate;

                                                    mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO2.getKey());
                                                    mutableIssue.setCustomFieldValue(Form_RequestedDateField, Form_RequestedDate);
                                                    mutableIssue.setCustomFieldValue(Form_Verification_RequestedDateField, Form_Verification_RequestedDate);
                                                    mutableIssue.setCustomFieldValue(Form_Validation_RequestedDateField, Form_Validation_RequestedDate);

                                                    mutableIssue.setCustomFieldValue(Form_1stCommittedDateField, Form_FirstCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Form_Verification_1stCommittedDateField, Form_Verification_FirstCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Form_Validation_1stCommittedDateField, Form_Validation_FirstCommittedDate);

                                                    mutableIssue.setCustomFieldValue(Form_LastCommittedDateField, Form_LastCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Form_Verification_LastCommittedDateField, Form_Verification_LastCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Form_Validation_LastCommittedDateField, Form_Validation_LastCommittedDate);

                                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                    issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                }
                                            }else if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
                                                //To remove if update not needed;
                                                if (linkedIssueOfPWO2.getIssueTypeId() == ConstantKeys.ID_IT_PWO23 && linkedIssueOfPWO2.getStatus().getName() == "Not started" && RequestedDateTrigger != null) {
                                                    Int_RequestedDate = Toolbox.incrementTimestamp(RequestedDateTrigger, - 28);
                                                    Date Int_FirstCommittedDate = Int_RequestedDate;
                                                    Date Int_LastCommittedDate = Int_FirstCommittedDate;

                                                    mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO2.getKey());
                                                    mutableIssue.setCustomFieldValue(Int_RequestedDateField, Int_RequestedDate);
                                                    mutableIssue.setCustomFieldValue(Int_1stCommittedDateField, Int_FirstCommittedDate);
                                                    mutableIssue.setCustomFieldValue(Int_LastCommittedDateField, Int_LastCommittedDate);
                                                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                    issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //Set data on Cluster;
                ClusterIssue = issueManager.getIssueObject(linkedIssue.getId());
                FieldConfig fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(ClusterIssue);
                Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find {
                    it.toString() == RestrictedDataResult;
                }
                ClusterIssue.setCustomFieldValue(RestrictedDataField, valueRestrictedData);

                issueManager.updateIssue(user, ClusterIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(ClusterIssue);
            }
        }
        //Bug ??????
        //TriggerEffectiveCost = PartsDataEffectiveCosts + MaintPlanningEffectiveCosts + MaintEffectiveCosts;


        //Set Priority if changed;
        setPriorityIfChanged(event, user, log);
    }

    /**
     *
     * @param event
     * @param user
     * @param log
     * @throws IndexException
     */
    private static void setPriorityIfChanged(IssueEvent event, ApplicationUser user, Logger log) throws IndexException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        MutableIssue issue = (MutableIssue) event.getIssue();

        MutableIssue mutableIssue;
        Collection<Issue> LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfTrigger : LinkedIssuesOfTrigger) {
            if (linkedIssueOfTrigger.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
                mutableIssue = issueManager.getIssueObject(linkedIssueOfTrigger.getKey());
                mutableIssue.setPriority(issue.getPriority());
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(linkedIssueOfTrigger);
                //Get linked issue of Cluster;
                Collection<Issue> LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues();
                for (Issue linkedIssueOfCluster : LinkedIssuesOfCluster) {
                    //If linked issue of Cluster is Trigger;
                    if (linkedIssueOfCluster.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
                        mutableIssue = issueManager.getIssueObject(linkedIssueOfCluster.getKey());
                        mutableIssue.setPriority(issue.getPriority());
                        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(linkedIssueOfCluster);
                        Collection<Issue> LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                        for (Issue linkedIssueOfPWO0 : LinkedIssuesOfPWO0) {
                            //If linked issue of Cluster is Trigger;
                            if (linkedIssueOfPWO0.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
                                mutableIssue = issueManager.getIssueObject(linkedIssueOfPWO0.getKey());
                                mutableIssue.setPriority(issue.getPriority());
                                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                issueIndexingService.reIndex(linkedIssueOfPWO0);
                                Collection<Issue> LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                                for (Issue linkedIssueOfPWO1 : LinkedIssuesOfPWO1) {
                                    //If linked issue of Cluster is Trigger;
                                    if (linkedIssueOfPWO1.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
                                        Collection<Issue> subTasksOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                        for (Issue subTaskOkPWO : subTasksOfPWO2) {
                                            mutableIssue = issueManager.getIssueObject(subTaskOkPWO.getKey());
                                            mutableIssue.setPriority(issue.getPriority());
                                            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(subTaskOkPWO);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param issue
     * @param user
     * @param log
     * @throws IndexException
     * @throws CreateException
     */
    static void createTrigger(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException, CreateException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> ObjFreezeIT = new ArrayList<String>();
        ObjFreezeIT.add(ConstantKeys.ID_IT_MOAOBJFREEZE);
        ObjFreezeIT.add(ConstantKeys.ID_IT_AIRBUSOBJFREEZE);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField;
        CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TRIGGERTOTALEFFECTIVECOSTS);
        CustomField PlannedQG2Field = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PLANNEDQG2);
        CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);
        CustomField ObjFreezeField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OBJFREEZE);


        //Variables;
        Timestamp PlannedQG2 = null;
        double PartsDataEffectiveCosts = 0;
        double MaintPlanningEffectiveCosts = 0;
        double MaintEffectiveCosts = 0;
        double TriggerEffectiveCost = 0;

        //Create link with Obj. Freeze;
        if (ObjFreezeField != null) {
            Issue toLink = Toolbox.getIssueFromNFeedFieldSingle(ObjFreezeField, issue, log);
            if (toLink != null) {
                Long toLink_id = toLink.getId();
                issueLinkManager.createIssueLink(toLink_id, issue.getId(), 10003L, 1L, user);
            }
        }
        //Get linked issue (Obj. Freeze);
        Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssue : LinkedIssues) {
            if (ObjFreezeIT.contains(linkedIssue.getIssueTypeId())) {
                PlannedQG2 = (Timestamp) linkedIssue.getCustomFieldValue(PlannedQG2Field);
            }
        }
        //Get data;
        TriggerEffectiveCost = PartsDataEffectiveCosts + MaintPlanningEffectiveCosts + MaintEffectiveCosts;

        //Set data on Trigger;
        if (PlannedQG2 != null) {
            issue.setCustomFieldValue(RequestedDateField, PlannedQG2);
        }
        issue.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        issueIndexingService.reIndex(issue);
    }

    /**
     *
     * @param event
     * @param user
     * @param log
     * @return
     */
    static boolean createPWO0Automatically(IssueEvent event, ApplicationUser user, Logger log) {
        //Manager;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        GenericValue changeLog = event.getChangeLog();

        //CustomField;
        CustomField FirstSNField=customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSN);

        //Variables
        boolean result = false;

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNField.getFieldName());

        boolean isCreation = Toolbox.workloadIsUpdated(changeLog, itemsChangedAtCreation);
        result = isCreation
        return result;
    }

    /**
     *
     * @param issue
     * @param user
     * @param log
     * @throws IndexException
     */
    static void additionOrDeletionOfLinkBetweenTriggerAndCluster(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField;
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATA);

        //Variables;
        String RestrictedDataResult = "No";

        //Business;
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
            log.debug("issue : " + issue.getKey());
            Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssue : LinkedIssues) {
                if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                    if (linkedIssue.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                        RestrictedDataResult = "Yes";
                    } else if (RestrictedDataResult != "Yes" && linkedIssue.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                        RestrictedDataResult = "Unknown";
                    }
                }
            }

            FieldConfig fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue);
            Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find{
                it.toString() == RestrictedDataResult;
            }
            issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }

    /**
     *
     * @param event
     * @param user
     * @param log
     * @throws IndexException
     */
    static void createCluster(IssueEvent event, ApplicationUser user, Logger log) throws IndexException {
        //Manager;
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        MutableIssue issue = (MutableIssue) event.getIssue();

        //CustomField;
        CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_RESTRICTEDDATE);

        //Variables;
        String RestrictedDataResult = "No";

        //Business;
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER) {
            log.debug("issue : " + issue.getKey());
            Collection<Issue> LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
            for (Issue linkedIssue : LinkedIssues) {
                if (linkedIssue.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
                    if (linkedIssue.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                        RestrictedDataResult = "Yes";
                    } else if (RestrictedDataResult != "Yes" && linkedIssue.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                        RestrictedDataResult = "Unknown";
                    }
                }
            }

            FieldConfig fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue);
            Option valueRestrictedData = ComponentAccessor.getOptionsManager().getOptions(fieldConfigRestrictedData)?.find{
                it.toString() == RestrictedDataResult;
            }
            issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData);
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(issue);
        }
    }
}
