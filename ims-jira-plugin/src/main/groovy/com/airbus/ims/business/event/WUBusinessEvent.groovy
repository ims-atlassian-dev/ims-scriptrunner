package com.airbus.ims.business.event

import com.airbus.ims.util.ConstantKeys;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;

class WUBusinessEvent {


    static void fillCompanyAllowedFieldForWU(MutableIssue issue, ApplicationUser user, Logger log) throws IndexException {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        //Get CustomFields;
        CustomField CompanyAllowed = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_COMPANY_ALLOWED);
        CustomField WU_Company = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_WU_COMPANY);
        List<Group> CompanyAllowedList = new ArrayList<Group>();

        log.debug("WU");
        //DU;
        String WU_Company_Value = String.valueOf(issue.getCustomFieldValue(WU_Company));
        //log.debug "WU company : +" WU_Company_Value;

        String WU_Company_Formatted = "Partner_" + WU_Company_Value;

        Group WU_Company_Formatted_Group = groupManager.getGroup(WU_Company_Formatted);
        //log.debug "WU company group : +" WU_Company_Formatted_Group;

        CompanyAllowedList.add(WU_Company_Formatted_Group);

        issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList);

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);//JIRA7;
        issueIndexingService.reIndex(issue); //JIRA7;
    }
}
