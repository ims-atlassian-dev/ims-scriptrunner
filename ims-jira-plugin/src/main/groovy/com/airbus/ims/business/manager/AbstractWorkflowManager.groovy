package com.airbus.ims.business.manager;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

abstract class AbstractWorkflowManager {

    public Issue issue;
    public MutableIssue mutableIssue;
    public Logger log;
    public ApplicationUser user;

    public static final String DEFAULT_LOGGER_NAME = "com.airbus.ims.business.operation";

    AbstractWorkflowManager(Issue i) {
        initWithDefaultParameters(i);
    }

    AbstractWorkflowManager(Issue i, String loggerName, Level level) {
        initWithDefaultParameters(i);
        log = Logger.getLogger(loggerName);
        log.setLevel(level);
    }

    /**
     * Initialize the operation with default parameters
     * @param i
     */
    private void initWithDefaultParameters(Issue i) {
        issue = i;
        IssueManager issueManager = ComponentAccessor.getComponent(IssueManager.class);
        mutableIssue = issueManager.getIssueObject(issue.getKey());
        log = Logger.getLogger(DEFAULT_LOGGER_NAME);
        log.setLevel(Level.DEBUG);
        log.debug("initWithDefaultParameters:Check issue id: " + issue);
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        user = authenticationContext.getLoggedInUser();
    }
}
