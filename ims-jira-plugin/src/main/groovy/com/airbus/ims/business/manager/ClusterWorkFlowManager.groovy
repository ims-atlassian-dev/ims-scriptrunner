package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.ClusterBusinessOperation;
import com.airbus.ims.business.operation.GlobalBusinessOperation
import com.atlassian.jira.issue.Issue;

class ClusterWorkFlowManager extends AbstractWorkflowManager {

    ClusterWorkFlowManager(Issue i) {
        super(i);
    }

    boolean onCancelSimpleScriptedCondition(){
        return GlobalBusinessOperation.checkIfStatusIsNotCompletedOrCancelled(issue);
    }

    boolean onAutoCancelCustomScriptedCondition() {
        return ClusterBusinessOperation.autoCancelCluster(mutableIssue, user, log);
    }
}