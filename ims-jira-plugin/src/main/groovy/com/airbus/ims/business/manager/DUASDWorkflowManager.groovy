package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.DUBusinessOperation
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

class DUASDWorkflowManager extends AbstractWorkflowManager {
    DUASDWorkflowManager(Issue i) {
        super(i);
    }

    DUASDWorkflowManager(Issue i, String loggerName, Level level) {
        super(i, loggerName, level);
    }

    boolean onCopyDUCustomScriptedCondition() {
        return DUBusinessOperation.isCopyDUConditionSatisfied(mutableIssue, user, log);
    }
}
