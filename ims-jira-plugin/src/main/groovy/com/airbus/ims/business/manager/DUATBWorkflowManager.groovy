package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.DUBusinessOperation
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

class DUATBWorkflowManager extends AbstractWorkflowManager {
    DUATBWorkflowManager(Issue i) {
        super(i);
    }

    DUATBWorkflowManager(Issue i, String loggerName, Level level) {
        super(i, loggerName, level);
    }

    boolean onCopyDUCustomScriptedCondition() {
        return DUBusinessOperation.isCopyDUConditionSatisfied(mutableIssue, user, log);
    }
}
