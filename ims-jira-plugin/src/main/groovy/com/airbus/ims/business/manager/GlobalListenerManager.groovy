package com.airbus.ims.business.manager

import com.airbus.ims.business.event.AWOBusinessEvent
import com.airbus.ims.business.event.DUBusinessEvent
import com.airbus.ims.business.event.ObjectiveFreezeBusinessEvent
import com.airbus.ims.business.event.POBusinessEvent
import com.airbus.ims.business.event.PWO0BusinessEvent
import com.airbus.ims.business.event.PWO1BusinessEvent
import com.airbus.ims.business.event.PWO21BusinessEvent
import com.airbus.ims.business.event.PWO22BusinessEvent
import com.airbus.ims.business.event.PWO23BusinessEvent
import com.airbus.ims.business.event.PWO2BusinessEvent
import com.airbus.ims.business.event.PWOBusinessEvent
import com.airbus.ims.business.event.TriggerBusinessEvent
import com.airbus.ims.business.event.WUBusinessEvent
import com.airbus.ims.util.ConstantKeys;
import com.airbus.ims.util.Toolbox;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.atlassian.jira.event.issue.link.IssueLinkDeletedEvent
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Level;
import org.apache.log4j.Logger
import org.ofbiz.core.entity.GenericValue;

class GlobalListenerManager {
    static final String DEFAULT_LOGGER_NAME = "com.airbus.ims.business.event";
    static Logger log = Logger.getLogger(DEFAULT_LOGGER_NAME);

    GlobalListenerManager() {
        log.setLevel(Level.DEBUG);
    }

    GlobalListenerManager(String loggerName, Level level) {
        // initWithDefaultParameters();
        log = Logger.getLogger(loggerName);
        log.setLevel(level);
    }

/**
 * Init class with default parameters
 */
    static void init() {
        log = Logger.getLogger(DEFAULT_LOGGER_NAME);
        log.setLevel(Level.DEBUG);
    }

    /**
     * Init class with custom parameters
     */
    static void init(String loggerName, Level level) {
        log = Logger.getLogger(loggerName);
        log.setLevel(level);
    }

    /**
     * Projects : PWO
     * Description : Creation/Edit of PWO-2 / PWO-2.1
     * Events : Admin re-open, PWO-2 updated, PWO-2.1 updated, Issue Created
     * isDisabled : false
     */
    static void onCreationOrEditOfPWO2OrPWO21(IssueEvent event) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        //Business
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
            PWO2BusinessEvent.resolveCreationOrEditionOnPWO2(event, log);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
            PWO21BusinessEvent.resolveCreationOrEditionOnPWO21(event, log);
        }
    }

    /**
     * Projects : PWO
     * Description : Creation/Edit of PWO-2.2 / PWO-2.3
     * Events : Issue Updated, Admin re-open, PWO-2.2 updated, PWO-2.3 updated
     * isDisabled : null
     */
    static void onCreationOrEditOfPwo22OrPWO23(IssueEvent event) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        //Business
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
            PWO22BusinessEvent.resolveCreationOrEditionOnPWO22(event, log);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO23) {
            PWO23BusinessEvent.resolveCreationOrEditionOnPWO23(event, log);
        }
    }

    /**
     * Projects : PWO
     * Description : Creation / Edit PWO-1
     * Events : Admin re-open, PWO-1 updated, Issue Created
     * isDisabled : false
     */
    static void onCreationOrEditOfPWO1(IssueEvent event) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        //Business
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
            PWO1BusinessEvent.resolveCreationOrEditionOnPWO1(event, log);
        }
    }

    /**
     * Projects : PWO
     * Description : Creation / Edit of PWO-0
     * Events : Admin re-open, PWO-0 updated, Issue Created
     * isDisabled : null
     */
    static void onCreationOrEditOfPWO0(IssueEvent event) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        //Business
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
            PWO0BusinessEvent.resolveCreationOrEditionOnPWO0(event, log);
        }
    }

    /**
     * Projects : PWO
     * Description : Addition of link to PWO-0 and PWO-1
     * Events : IssueLinkCreatedEvent
     * isDisabled : false
     */
    static void onAdditionOfLinkToPWO0OrPWO1(IssueLinkCreatedEvent event) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject();
        MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject();
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();
        log.debug("current issue : " + issue.getKey());
        log.debug("linked issue : " + issueSource.getKey());


        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //Business;
        log.debug("issue.getIssueTypeId() : " + issue.getIssueTypeId());
        if (issueSource.getIssueTypeId() == ConstantKeys.ID_IT_CLUSTER && issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
            //if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO0){
            PWO0BusinessEvent.linkClusterToPWO0(issue, user, log);
        } else if (issueSource.getIssueTypeId() == ConstantKeys.ID_IT_PWO0 && issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
            PWO1BusinessEvent.linkPWO0ToPWO1(issue, user, log);
        }
    }

    /**
     * Projects : AWO
     * Description : Creation/Edit on AWO
     * Events : Issue Updated, AWO updated
     * isDisabled : false
     */
    static void onCreationOrEditOfAWO(IssueEvent event) throws IndexException {
        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        GenericValue changeLog = event.getChangeLog();

        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSNAPPLI);

        //Business;

        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());
        log.debug("issue = " + issue.getKey());

        boolean isCreation = Toolbox.workloadIsUpdated(changeLog, itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation) {//Creation;
            AWOBusinessEvent.createAWO(issue, user, log);
        } else {//Edit
            AWOBusinessEvent.editAWO(issue, user, log);
        }
    }

    /**
     * Projects : TRG
     * Description : Creation / Edit on Trigger
     * Events : Issue Created, Trigger updated
     * isDisabled : false
     */
    static void onCreationOrEditOfTrigger(IssueEvent event) throws IndexException, CreateException, ClassNotFoundException {
        //Manager;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();

        List<String> ObjFreezeIT = new ArrayList<String>();
        ObjFreezeIT.add(ConstantKeys.ID_IT_MOAOBJFREEZE);
        ObjFreezeIT.add(ConstantKeys.ID_IT_AIRBUSOBJFREEZE);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField;
        CustomField ObjFreezeField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_OBJFREEZE);

        //Variables
        List<String> itemsChanged = new ArrayList<String>();
        itemsChanged.add(ObjFreezeField.getFieldName());

        //Business;
        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_TRIGGER) {
            log.debug("issue : " + issue.getKey());
            if (event.getEventTypeId() == ConstantKeys.ID_EVENT_CREATE) { //Creation;
                TriggerBusinessEvent.createTrigger(issue, user, log);
            } else { //Edit;
                TriggerBusinessEvent.editTrigger(event, user, log);
            }
        }
        log.debug("end of script");
    }

    /**
     * Projects : FRZ
     * Description : Edit of an Obj. Freeze
     * Events : FRZ updated
     * isDisabled : false
     */
    static void onEditOfObjectiveFreeze(IssueEvent event) {

        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();
        ObjectiveFreezeBusinessEvent.edit(issue, user, log);
    }

    /**
     * Projects : PWO
     * Description : Calculation of the field "<manual> nb reworks"
     * Events : PWO-0 updated, PWO-1 updated, PWO-2.1 updated, PWO-2.2 updated, PWO-2.3 updated
     * isDisabled : false
     */
    static void onCalculationOfManualNbReworksOnPWO(IssueEvent event) throws IndexException, CreateException, ClassNotFoundException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();

        log.debug("Start");
        log.debug("Issue : " + issue.getKey());

        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
            PWO0BusinessEvent.calculationForPWO0(event, user, log);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
            PWO1BusinessEvent.calculationForPWO1(event, user, log);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
            PWO21BusinessEvent.calculationForPWO21(event, user, log);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
            PWO22BusinessEvent.calculationForPWO22(event, user, log);
        }
    }

    /**
     * Projects : PWO
     * Description : Fill the field "Company allowed" at creation of an object
     * Events : Issue Created, Start, PWO-0 updated, PWO-1 updated, PWO-2 updated, PWO-2.1 updated, PWO-2.2 updated, PWO-2.3 updated, PO updated, WU updated
     * isDisabled : true
     */
    static void onUpdateOfCompanyAllowedFieldOnPWO(IssueEvent event) throws IndexException, CreateException, ClassNotFoundException {
        //Get issue from event
        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();

        log.debug("issue type : " + issue.getIssueTypeId());

        if (issue.getIssueTypeId().equals(ConstantKeys.ID_IT_PWO0)) {
            PWO0BusinessEvent.fillCompanyAllowedFieldForPWO0(issue, user, log);
        } else if (issue.getIssueTypeId().equals(ConstantKeys.ID_IT_PWO1)) {
            PWO1BusinessEvent.fillCompanyAllowedFieldForPWO1(issue, user, log);
        } else if (issue.getIssueTypeId().equals(ConstantKeys.ID_IT_PWO21)) {
            PWO21BusinessEvent.fillCompanyAllowedFieldForPWO21(issue, user, log);
        } else if (issue.getIssueTypeId().equals(ConstantKeys.ID_IT_PWO22)) {
            PWO22BusinessEvent.fillCompanyAllowedFieldForPWO22(issue, user, log);
        } else if (issue.getIssueTypeId().equals(ConstantKeys.ID_IT_PWO23)) {
            PWO23BusinessEvent.fillCompanyAllowedFieldForPWO23(issue, user, log);
        } else if (issue.getIssueTypeId().equals(ConstantKeys.ID_IT_PO)) {
            POBusinessEvent.fillCompanyAllowedFieldForPO(issue, user, log);
        } else if (issue.getIssueTypeId().equals(ConstantKeys.ID_IT_WU) || issue.getIssueTypeId().equals(ConstantKeys.ID_IT_WU_ADDITIONALCOST)) {
            WUBusinessEvent.fillCompanyAllowedFieldForWU(issue, user, log);
        }
    }

    /**
     * Projects : PWO
     * Description : Addition/deletion of link between AWO and PWO-1/2.x
     * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
     * isDisabled : null
     */
    static void onAdditionOrDeletionOfLinkBetweenAWOAndPWO(IssueLinkCreatedEvent event) throws IndexException, CreateException, ClassNotFoundException {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();
        MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject();
        MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject();

        AWOBusinessEvent.additionOrDeletionOfLinkBetweenAWOAndPWO(issue, issueSource, user, log);
    }

    /**
     * Projects : PWO
     * Description : Addition/deletion of link between AWO and PWO-1/2.x
     * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
     * isDisabled : null
     */
    static void onAdditionOrDeletionOfLinkBetweenAWOAndPWO(IssueLinkDeletedEvent event) throws IndexException, CreateException, ClassNotFoundException {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();
        MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject();
        MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject();

        AWOBusinessEvent.additionOrDeletionOfLinkBetweenAWOAndPWO(issue, issueSource, user, log);
    }

    /**
     * Projects : PWO
     * Description : Link of AWO to PWO-2.x
     * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
     * isDisabled : null
     */
    static void whenLinkingAWOToPWO2x(IssueLinkCreatedEvent event) throws IndexException, CreateException, ClassNotFoundException {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();
        MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject();
        MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject();

        AWOBusinessEvent.setCostDataOnPWO2x(issue, issueSource, user, log);
    }

    /**
     * Projects : PWO
     * Description : Link of AWO to PWO-2.x
     * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
     * isDisabled : null
     */
    static void whenLinkingAWOToPWO2x(IssueLinkDeletedEvent event) throws IndexException, CreateException, ClassNotFoundException {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();
        MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject();
        MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject();

        AWOBusinessEvent.setCostDataOnPWO2x(issue, issueSource, user, log);
    }

    /**
     * Projects : PWO
     * Description : Update of the field "<manual> manually edited" when manually editing the field "<manual> nb reworks"
     * Events : PWO-0 updated, PWO-1 updated, PWO-2.1 updated, PWO-2.2 updated, PWO-2.3 updated
     * isDisabled : false
     */
    static void whenEditingNbReworksFieldOnPWO(IssueEvent event) throws IndexException {
        log.debug("Edit of issue, update of the field Manually Edited");

        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO0) {
            PWO0BusinessEvent.fillManuallyEditedFieldForPWO0(event, user, log);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO1) {
            PWO1BusinessEvent.fillManuallyEditedFieldForPWO1(event, user, log);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO21) {
            PWO21BusinessEvent.fillManuallyEditedFieldForPWO21(event, user, log);
        } else if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO22) {
            PWO22BusinessEvent.fillManuallyEditedFieldForPWO22(event, user, log);
        }
    }

    /**
     * Projects : PWO
     * Description : Link selected AWO upon creation of PWO-2
     * Events : PWO-2 updated, Issue Created
     * isDisabled : false
     */
    static void whenLinkingSelectedAWOUponCreation(IssueEvent event) throws IndexException, CreateException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();
        log.debug("issue : " + issue.getKey());

        if (issue.getIssueTypeId() == ConstantKeys.ID_IT_PWO2) {
            PWO2BusinessEvent.linkSelectedAWOUponCreationForPWO2(event, user, log);
        }
    }

    /**
     * Projects : PO
     * Description : Creation / Edit of PO / WU (link between the two objects)
     * Events : Issue Created, PO updated
     * isDisabled : null
     */
    static void onCreationOrEditionOfPO(IssueEvent event) throws IndexException, CreateException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();

        if (event.getEventTypeId() == ConstantKeys.ID_PO_EVENT_CREATE) { //Creation
            POBusinessEvent.createLinkBetweenPOAndWUCreationMode(issue, user, log);
        } else if (event.getEventTypeId() == ConstantKeys.ID_PO_EVENT_EDIT) { //Edition
            POBusinessEvent.createLinkBetweenPOAndWUEditionMode(event, user, log);
        }
    }

    /**
     * Projects : PWO
     * Description : Manage the time tracking in PWO-0 / PWO-1
     * Events : Work Logged On Issue
     * isDisabled : false
     */
    static void whenWorkLoggedOnPWO(IssueEvent event) throws IndexException {
        MutableIssue issue = (MutableIssue) event.getIssue();
        ApplicationUser user = event.getUser();

        PWOBusinessEvent.manageTimeTrackingInPWO(issue, user, log);
    }

    /**
     * Projects : PWO
     * Description : Transition PWO-0 to "Delivered" when all selectionned Domain are delivered
     * Events : Issue Assigned, PWO-0 updated
     * isDisabled : false
     */
    static Boolean whenAllSelectionnedDomainAreDeliveredOnPWO0(IssueEvent event) throws IndexException {
        ApplicationUser user = event.getUser();

        return PWO0BusinessEvent.transitionPWO0ToDelivered(event, user, log);
    }


    /**
     * Projects : AWO
     * Description : "Go if" link new AWO to DU
     * Events : Issue Updated
     * isDisabled : false
     */
    static void onGoIfLinkingBetweenNewAWOAndDU(IssueEvent event) throws IndexException {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        //Manager;
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        //CustomFields
        CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FIRSTSNAPPLI);

        GenericValue changeLog = event.getChangeLog();
        List<String> itemsChangedAtCreation = new ArrayList<String>();
        itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());

        boolean isCreation = Toolbox.workloadIsUpdated(changeLog, itemsChangedAtCreation);
        log.debug("is creation : " + isCreation);
        if (isCreation && issue.getCustomFieldValue(FirstSNAppliField).toString() == "Copied from Go if") {//Creation;
            AWOBusinessEvent.createLinkBetweenNewAWOAndDU(issue, user, log);
        }
    }


    /**
     * Projects : PWO
     * Description : Addition of link in PWO-2
     * Events : IssueLinkCreatedEvent
     * isDisabled : false
     */
    static void onAdditionOfLinkInPWO2(IssueLinkCreatedEvent event) throws IndexException {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();
        MutableIssue issue = (MutableIssue) event.getIssueLink().getSourceObject();
        MutableIssue issueSource = (MutableIssue) event.getIssueLink().getDestinationObject();

        log.debug("issue : " + issue.getKey());
        log.debug("source issue : " + issueSource.getKey());

        PWO2BusinessEvent.updateRequestedDateFieldAndRestrictedDataFieldOnPWO2(issue, issueSource, user, log);
    }

    /**
     * Projects : PWO
     * Description : Transition PWO-0 to "Completed" when all selected domain are completed
     * Events : PWO-0 updated
     * isDisabled : null
     */
    static Boolean whenAllSelectedDomainAreCompletedOnPWO0(IssueEvent event) throws IndexException {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        return PWO0BusinessEvent.transitionPWO0ToCompleted(issue, user, log);
    }

    /**
     * Projects : PWO
     * Description : Transition PWO-0 to "In progress" from Start when all domain are in progress
     * Events : PWO-0 updated
     * isDisabled : null
     */
    static Boolean whenAllDomainAreInProgressOnPWO0(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        return PWO0BusinessEvent.transitionPWO0ToInProgress(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Update of the field "title" for the PWO upon creation and copy it to the summary
     * Events : Issue Created
     * isDisabled : true
     */
    static void whenCreatingPWO(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        PWOBusinessEvent.updateTitleFieldForPWOAndCopyItToSummary(issue, user, log);
    }

    /**
     * Projects : PWO
     * Description : At first link from PWO-1 to AWO, update field of AWO
     * Events : IssueLinkCreatedEvent
     * isDisabled : null
     */
    static void whenFirstLinkBetweenPWO1AndAWO(IssueLinkCreatedEvent event) {
        MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject();
        MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject();

        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        log.debug("issue : " + issue.getKey());
        log.debug("source issue : " + issueSource.getKey());

        PWO1BusinessEvent.updateFieldOfAWO(issue, issueSource, user, log);
    }


    /**
     * Projects : PWO
     * Description : Close PWO-2.1 when Technically closed = Yes, Auth_DN and Auth_Verified_DN is filled
     * Events : PWO-2.1 updated
     * isDisabled : false
     */
    static Boolean whenTechnicallyClosedAndAuthDNAndAuthVerifiedDNNotNullOnPWO21(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        return PWO21BusinessEvent.closePWO21(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Transition PWO-0 to Reject if one selected domain is rejected
     * Events : PWO-0 updated
     * isDisabled : null
     */
    static Boolean whenOneSelectedDomainIsRejectedOnPWO0(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());
        return PWO0BusinessEvent.transitionPWO0ToReject(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Set the PWO-0 to In progress if all selected domain are "In progress"
     * Events : PWO-0 updated
     * isDisabled : null
     */
    static Boolean whenAllSelectedDomainInProgressOnPWO0(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());
        return PWO0BusinessEvent.setPWO0ToInProgress(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Close PWO-2.2 when Technically closed, Form_DN and Form_Verification_DN
     * Events : PWO-2.2 updated
     * isDisabled : null
     */
    static Boolean whenTechnicallyIsClosedAndFormDNAndFormVerificationDNNotNullOnPWO22(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        return PWO22BusinessEvent.closePWO22(event, user, log);
    }

    /**
     * Projects : DU
     * Description : Set the DU to Active when created from Go if
     * Events : Issue Updated
     * isDisabled : null
     */
    static Boolean whenCreatedFromGoIf(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        return DUBusinessEvent.setDUToActive(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Automatic creation of PWO-1 (Parts data) from PWO-0
     * Events : PWO-0 updated
     * isDisabled : true
     */
    static void onUpdatePartDataOfPWO0(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        PWO0BusinessEvent.createAutomaticallyPWO1FromPWO0BasedOnPartData(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Transition "Reject" in PWO-2.1 if Verified / Validated = Rejected
     * Events : PWO-2.1 updated
     * isDisabled : false
     */
    static Boolean whenVerifiedOrValidatedOnPWO21(IssueEvent event) {
        ApplicationUser user = event.getUser();
        MutableIssue issue = (MutableIssue) event.getIssue();

        log.debug("issue : " + issue.getKey());

        return PWO21BusinessEvent.transitionRejectOnPWO21(event, user, log);
    }

    /**
     * Projects : AWO
     * Description : At first link from PWO-2 to PWO-1, update "Restricted data" of PWO-2
     * Events : IssueLinkCreatedEvent
     * isDisabled : null
     */
    static void whenFirstLinkBetweenPWO2AndPWO1(IssueLinkCreatedEvent event) {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject();
        MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject();

        log.debug("issue : " + issue.getKey());
        log.debug("source issue : " + issueSource.getKey());

        PWO2BusinessEvent.updateRestrictedDataOfPWO2(issue, issueSource, user, log);
    }

    /**
     * Projects : PWO
     * Description : Automatic creation of PWO-1 (Maint Planning) from PWO-0
     * Events : PWO-0 updated
     * isDisabled : true
     */
    static void onUpdateMaintPlanningOfPWO0(IssueEvent event) {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        PWO0BusinessEvent.createAutomaticallyPWO1FromPWO0BasedOnMaintPlanning(event, user, log);
    }


    /**
     * Projects : PWO
     * Description : Reject PWO-2.2 when Form_Validated = Rejected
     * Events : PWO-2.2 updated
     * isDisabled : null
     */
    static Boolean whenFormValidatedFieldIsRejected(IssueEvent event) {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        return PWO22BusinessEvent.rejectPWO22(event, user, log);
    }

    /**
     * Projects : DU
     * Description : Set the DU to Inactive when created from Release of the AWO
     * Events : Issue Updated
     * isDisabled : null
     */
    static Boolean whenCreatedFromReleaseOfAWO(IssueEvent event) {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        return DUBusinessEvent.setDUToActive(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Close PWO-2.3 when Int_DN
     * Events : Deliver PWO-2.3, PWO-2.3 updated
     * isDisabled : null
     */
    static Boolean onUpdateIntDNOfPWO23(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        return PWO23BusinessEvent.closePWO23(event, user, log);
    }


    /**
     * Projects : DU
     * Description : When AWO is released, link the newly DU to the AWO
     * Events : Issue Updated
     * isDisabled : null
     */
    static void whenAWOIsReleasedForDU(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        DUBusinessEvent.linkNewlyCreatedDUToAWO(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Automatic creation of PWO-1 (Maint) from PWO-0
     * Events : PWO-0 updated
     * isDisabled : true
     */
    static void onUpdateMaintOfPWO0(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        PWO0BusinessEvent.createAutomaticallyPWO1FromPWO0BasedOnMaint(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Create PWO-2.2 when PWO-2.1 is validated
     * Events : Issue Updated
     * isDisabled : true
     */
    static void whenPWO21IsValidated(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        PWO21BusinessEvent.createPWO22FromPWO21(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Check unicity of summary for PWO and fill the field Warning if needed
     * Events : Change of summary
     * isDisabled : null
     */
    static void whenSummaryChangedOnPWO(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        PWOBusinessEvent.checkUnicityOfSummaryForPWOAndFillWarningFieldIfNeeded(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Create PWO-2.1 at creation of PWO-2
     * Events : Issue Updated
     * isDisabled : true
     */
    static void onCreatePWO2(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        PWO2BusinessEvent.createPWO21(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Create PWO-2.3 when PWO-2.2 is validated
     * Events : Issue Updated
     * isDisabled : true
     */
    static Boolean whenPWO22IsValidated(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        return PWO22BusinessEvent.createPWO23FromPWO22(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Transition PWO-1 to Completed when Validated and DN is filled
     * Events : PWO-1 updated
     * isDisabled : false
     */
    static Boolean whenPWO1IsValidatedAndDNIsFilled(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        return PWO1BusinessEvent.transitionPWO1ToCompleted(event, user, log);
    }

    /**
     * Projects : PWO
     * Description : Transition PWO-1 to Reject if Validated is rejected
     * Events : PWO-1 updated
     * isDisabled : false
     */
    static Boolean whenPWO1ValidatedIsRejected(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        return PWO1BusinessEvent.transitionPWO1ToRejected(event, user, log);
    }


    /**
     * Projects : TRG
     * Description : Automatic creation of PWO-0 from Initialize
     * Events : Issue Updated
     * isDisabled : true
     */
    static void whenTriggerIsInitialized(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        TriggerBusinessEvent.createPWO0Automatically(event, user, log);
    }


    /**
     * Projects : TRG
     * Description : Addition or Deletion of link in a Cluster
     * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
     * isDisabled : false
     */
    static void onAdditionOrDeletionOfLinkBetweenTriggerAndCluster(IssueLinkCreatedEvent event) {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        MutableIssue issue = (MutableIssue) event.getIssueLink().getSourceObject();
        log.debug("issue : " + issue.getKey());

        TriggerBusinessEvent.additionOrDeletionOfLinkBetweenTriggerAndCluster(issue, user, log);
    }

    /**
     * Projects : TRG
     * Description : Addition or Deletion of link in a Cluster
     * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
     * isDisabled : false
     */
    static void onAdditionOrDeletionOfLinkBetweenTriggerAndCluster(IssueLinkDeletedEvent event) {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        MutableIssue issue = (MutableIssue) event.getIssueLink().getSourceObject();
        log.debug("issue : " + issue.getKey());

        TriggerBusinessEvent.additionOrDeletionOfLinkBetweenTriggerAndCluster(issue, user, log);
    }

    /**
     * Projects : TRG
     * Description : Create of a Cluster
     * Events : Issue Created
     * isDisabled : false
     */
    static void onCreationOfTrigger(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        TriggerBusinessEvent.createCluster(event, user, log);
    }

    /**
     * Projects :
     * Description : Copy the group selected in field "Program" to the Security Level
     * Events : Issue Created, Issue Moved, PWO-0 updated, PWO-1 updated, PWO-2 updated, PWO-2.1 updated, PWO-2.2 updated, PWO-2.3 updated,
     * Trigger updated, AWO updated, DU updated, PO updated, WU updated, FRZ updated
     * isDisabled : false
     */
    static void copySelectedGroupInProgramFieldToSecurityLevel(IssueEvent event) {
        ApplicationUser user = event.getUser();

        MutableIssue issue = (MutableIssue) event.getIssue();
        log.debug("issue : " + issue.getKey());

        PWOBusinessEvent.copySelectedGroupInProgramFieldToSecurityLevel(event, user, log);
    }

    /**
     * Projects : AWO
     * Description : Synchronize cwx_quicksearch on issue link deleted
     * Events : IssueLinkDeletedEvent
     * isDisabled : null
     */
    static void onIssueLinkDeletedFromAWO(IssueLinkDeletedEvent event) {
        MutableIssue issue = (MutableIssue) event.getIssueLink().getSourceObject();

        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        log.debug("issue : " + issue.getKey());

        AWOBusinessEvent.synchronizeCWXQuicksearch(issue, user, log);
    }

    /**
     * Projects : AWO
     * Description : Copy "Title" to "Summary"
     * Events : Issue Created, Issue Updated, WU updated, FRZ updated, AWO updated
     * isDisabled : false
     */
    static void onCreationOrUpdateOfAWO(IssueEvent event) {
        MutableIssue issue = (MutableIssue) event.getIssue();

        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser user = authenticationContext.getLoggedInUser();

        log.debug("issue : " + issue.getKey());

        AWOBusinessEvent.copyTitleToSummary(issue, user, log);
    }
}
