package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.PWO0BusinessOperation
import com.airbus.ims.util.ConstantKeys;
import com.airbus.ims.business.operation.GlobalBusinessOperation;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

class PWO0WorkflowManager extends AbstractWorkflowManager {
    PWO0WorkflowManager(Issue i) {
        super(i);
    }

    PWO0WorkflowManager(Issue i, String loggerName, Level level) {
        super(i, loggerName, level);
    }

    void onCreateCustomScriptPostFunction() {
        GlobalBusinessOperation.createOrEditPWO(issue, user, log, ConstantKeys.PWO0);
    }

    boolean onCancelSimpleScriptedCondition() {
        return GlobalBusinessOperation.checkIfStatusIsNotCompletedOrCancelled(issue);
    }

    void onCancelCustomScriptPostFunction() {
        PWO0BusinessOperation.cancelPWO0(mutableIssue, user, log);
    }

    void onAdminReOpenCustomScriptPostFunction() {
        PWO0BusinessOperation.reOpenPWO0AsAdministrator(mutableIssue, user, log);
    }

    boolean onStart1CustomScriptedCondition() {
        return GlobalBusinessOperation.isLinkedToIssueType(issue, user, log, ConstantKeys.ID_IT_CLUSTER);
    }

    void onStart1CustomScriptPostFunction() {
        GlobalBusinessOperation.updateFieldLinkToData(mutableIssue, user, log);
    }

    void onStart2CustomScriptPostFunction() {
        PWO0BusinessOperation.updatePWO0Dependencies(issue, user, log);
    }

    void onStart3CustomScriptPostFunction() {
        PWO0BusinessOperation.processPWO0PreDates(mutableIssue, user, log);
    }

    void onDeliverCustomScriptPostFunction() {
        PWO0BusinessOperation.processPWO0FinalDates(mutableIssue, user, log);
    }
}