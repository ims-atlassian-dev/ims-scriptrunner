package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.GlobalBusinessOperation
import com.airbus.ims.business.operation.PWO1BusinessOperation
import com.airbus.ims.util.ConstantKeys
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level

class PWO1WorkflowManager extends AbstractWorkflowManager {
    PWO1WorkflowManager(Issue i) {
        super(i);
    }

    PWO1WorkflowManager(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    void onCreateCustomScriptPostFunction() {
        GlobalBusinessOperation.createOrEditPWO(mutableIssue, user, log, ConstantKeys.PWO1);
    }

    boolean onStart1CustomScriptedCondition() {
        return GlobalBusinessOperation.isLinkedToIssueType(mutableIssue, user, log, ConstantKeys.ID_IT_PWO0);
    }

    boolean onStart2CustomScriptedCondition() {
        return PWO1BusinessOperation.isPWO1StartConditionSatisfied(mutableIssue, user, log);
    }

    void onStart1CustomScriptPostFunction() {
        GlobalBusinessOperation.updateFieldLinkToData(mutableIssue, user, log);
    }

    void onStart2CustomScriptPostFunction() {
        PWO1BusinessOperation.processPWO1Dates(mutableIssue, user, log);
    }

    void onDeliver1CustomScriptPostFunction(){
        PWO1BusinessOperation.processPWO1IAValidatedField(mutableIssue, user, log);
    }

    void onDeliver2CustomScriptPostFunction() {
        PWO1BusinessOperation.processPWO1NbReworkField(mutableIssue, user, log);
    }
}