package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.PWO21BusinessOperation
import com.airbus.ims.util.ConstantKeys;
import com.airbus.ims.business.operation.GlobalBusinessOperation;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

class PWO21WorkflowManager extends AbstractWorkflowManager {
    PWO21WorkflowManager(Issue i) {
        super(i);
    }

    PWO21WorkflowManager(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    void onCreate1CustomScriptPostFunction() {
        GlobalBusinessOperation.setSecurityLevelForSubPWO(mutableIssue, log);
    }

    void onCreate2CustomScriptPostFunction() {
        GlobalBusinessOperation.createOrEditPWO(mutableIssue, user, log, ConstantKeys.PWO21);
    }

    boolean onStart1CustomScriptedCondition() {
        return PWO21BusinessOperation.isPWO21Ready(issue, user, log);
    }

    void onStart1CustomScriptPostFunction() {
        PWO21BusinessOperation.processPWO21Dates(mutableIssue, user, log);
    }

    void onDeliver1CustomScriptPostFunction() {
        PWO21BusinessOperation.processPWO21AuthoringRequest(mutableIssue, user, log);
    }

    void onDeliver2CustomScriptPostFunction() {
        PWO21BusinessOperation.processPWO21ReworkField(mutableIssue, user, log);
    }

    void onReject1CustomScriptPostFunction() {
        PWO21BusinessOperation.processPWO21AuthoringRework(mutableIssue, user, log);
    }

    void onReject2CustomScriptPostFunction() {
        PWO21BusinessOperation.processPWO21AuthoringValidation(mutableIssue, user, log);
    }
}