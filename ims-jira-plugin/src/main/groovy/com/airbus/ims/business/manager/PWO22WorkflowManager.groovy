package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.GlobalBusinessOperation
import com.airbus.ims.business.operation.PWO22BusinessOperation
import com.airbus.ims.util.ConstantKeys
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level

class PWO22WorkflowManager extends AbstractWorkflowManager {
    PWO22WorkflowManager(Issue i) {
        super(i);
    }

    PWO22WorkflowManager(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    void onCreate1CustomScriptPostFunction() {
        GlobalBusinessOperation.setSecurityLevelForSubPWO(mutableIssue, log);
    }

    void onCreate2CustomScriptPostFunction() {
        GlobalBusinessOperation.createOrEditPWO(mutableIssue, user, log, ConstantKeys.PWO22);
    }

    boolean onStart1CustomScriptedCondition() {
        return PWO22BusinessOperation.isPWO22Ready(issue, user, log);
    }

    void onStart1CustomScriptPostFunction() {
        PWO22BusinessOperation.processPWO22VerificationDates(mutableIssue, user, log);
    }

    void onDeliver1CustomScriptPostFunction() {
        PWO22BusinessOperation.processPWO22FormDeliveryDates(mutableIssue, user, log);
    }

    void onDeliver2CustomScriptPostFunction() {
        PWO22BusinessOperation.processPWO22NbReworkField(mutableIssue, user, log);
    }

    void onReject1CustomScriptPostFunction() {
        PWO22BusinessOperation.processPWO22FormReworkDate(mutableIssue, user, log);
    }

    void onReject2CustomScriptPostFunction() {
        PWO22BusinessOperation.processPWO22CommittedDates(mutableIssue, user, log);
    }
}
