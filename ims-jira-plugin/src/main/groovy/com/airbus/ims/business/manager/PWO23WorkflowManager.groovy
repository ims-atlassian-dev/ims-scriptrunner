package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.PWO23BusinessOperation
import com.airbus.ims.util.ConstantKeys;
import com.airbus.ims.business.operation.GlobalBusinessOperation;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

class PWO23WorkflowManager extends AbstractWorkflowManager {
    PWO23WorkflowManager(Issue i) {
        super(i);
    }

    PWO23WorkflowManager(Issue i, String loggerName, Level level){
        super(i, loggerName, level);
    }

    void onCreate1CustomScriptPostFunction() {
        GlobalBusinessOperation.setSecurityLevelForSubPWO(mutableIssue, log);
    }

    void onCreate2CustomScriptPostFunction() {
        GlobalBusinessOperation.createOrEditPWO(mutableIssue, user, log, ConstantKeys.PWO23);
    }

    boolean onStart1CustomScriptedCondition() {
        return PWO23BusinessOperation.isPWO23Ready(mutableIssue, user, log);
    }

    void onStartCustomScriptPostFunction() {
        PWO23BusinessOperation.processPWO23IntValidation(mutableIssue, user, log);
    }

    void onDeliverCustomScriptPostFunction() {
        PWO23BusinessOperation.processPWO23TechnicalClosure(mutableIssue, user, log);
    }
}
