package com.airbus.ims.business.manager


import com.airbus.ims.util.ConstantKeys;
import com.airbus.ims.business.operation.GlobalBusinessOperation;
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Level;

class PWO2WorkflowManager extends AbstractWorkflowManager {
    PWO2WorkflowManager(Issue i) {
        super(i);
    }

    PWO2WorkflowManager(Issue i, String loggerName, Level level) {
        super(i, loggerName, level);
    }

    void onCreateCustomScriptPostFunction() {
        GlobalBusinessOperation.createOrEditPWO(mutableIssue,user, log, ConstantKeys.PWO2);
    }

    boolean onStart1CustomScriptedCondition() {
        return GlobalBusinessOperation.isLinkedToIssueType(mutableIssue,user, log, ConstantKeys.ID_IT_PWO1);
    }

    void onStart1CustomScriptPostFunction() {
        GlobalBusinessOperation.updateFieldLinkToData(mutableIssue,user, log);
    }
}
