package com.airbus.ims.business.manager

import com.airbus.ims.business.operation.GlobalBusinessOperation
import com.airbus.ims.business.operation.TriggerBusinessOperation
import com.atlassian.jira.issue.Issue
import org.apache.log4j.Level

class TriggerWorkflowManager extends AbstractWorkflowManager {
    TriggerWorkflowManager(Issue i) {
        super(i);
    }

    TriggerWorkflowManager(Issue i, String loggerName, Level level) {
        super(i, loggerName, level);
    }

    boolean onCancelSimpleScriptedCondition() {
        return GlobalBusinessOperation.checkIfStatusIsNotCompletedOrCancelled(issue);
    }

    boolean onCreateSimpleScriptedValidator(Issue originalIssue) {
        return TriggerBusinessOperation.validateTrigger(issue, originalIssue, user, log);
    }

    boolean onEditSimpleScriptedValidator(Issue originalIssue) {
        return TriggerBusinessOperation.validateTrigger(issue, originalIssue, user, log);
    }

    boolean onCreate2SimpleScriptedValidator() {
        return TriggerBusinessOperation.validateSummaryTextValue(issue, log);
    }

    boolean onInitializeCustomScriptedCondition() {
        return TriggerBusinessOperation.initializeTrigger(issue, log);
    }
}
