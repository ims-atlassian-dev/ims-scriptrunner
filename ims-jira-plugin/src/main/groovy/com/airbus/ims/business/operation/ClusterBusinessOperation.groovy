package com.airbus.ims.business.operation


import com.airbus.ims.util.ConstantKeys
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger

class ClusterBusinessOperation {

    /**
     * Auto cancel the cluster
     * @param mutableIssue
     * @param user
     * @param log
     * @return
     */
    static boolean autoCancelCluster(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();

        String cancelled = "cancelled";
        String result = "OK";

        /*def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
            //If cluster;
            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {*/
        //Get linked issues of Cluster;
        Collection<Issue> linkedIssuesOfCluster = issueLinkManager.getLinkCollection(mutableIssue, user).getAllIssues();
        for (Issue linkedIssueOfCluster : linkedIssuesOfCluster) {
            //If PWO-0;
            if (linkedIssueOfCluster.getIssueTypeId().equals(String.valueOf(ConstantKeys.ID_IT_PWO0))) {
                log.debug("PWO-0 : " + linkedIssueOfCluster.getKey());
                log.debug("PWO-0 status : " + linkedIssueOfCluster.getStatus().getSimpleStatus().getName());
                //Get status;
                if (linkedIssueOfCluster.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                    result = "OK";
                } else {
                    result = "KO";
                }
                //Get linked issues of PWO-0;
                Collection<Issue> linkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues();
                for (Issue linkedIssueOfPWO0 : linkedIssuesOfPWO0) {
                    //if PWO-1;
                    if (linkedIssueOfPWO0.getIssueTypeId().equals(String.valueOf(ConstantKeys.ID_IT_PWO1))) {
                        log.debug("PWO-1 : " + linkedIssueOfPWO0.getKey());
                        log.debug("PWO-1 status : " + linkedIssueOfPWO0.getStatus().getSimpleStatus().getName());
                        //Get status;
                        if (linkedIssueOfPWO0.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                            result = "OK";
                        } else {
                            result = "KO";
                        }
                        //Get linked issues of PWO-1;
                        Collection<Issue> linkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues();
                        for (Issue linkedIssueOfPWO1 : linkedIssuesOfPWO1) {
                            //If PWO-2;
                            if (linkedIssueOfPWO1.getIssueTypeId().equals(String.valueOf(ConstantKeys.ID_IT_PWO2))) {
                                //Get sub-task;
                                Collection<Issue> LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects();
                                for (Issue linkedIssueOfPWO2 : LinkedIssuesOfPWO2) {
                                    if (linkedIssueOfPWO2.getIssueTypeId().equals(String.valueOf(ConstantKeys.ID_IT_PWO21))) {
                                        log.debug("PWO-2.1 : " + linkedIssueOfPWO2.getKey());
                                        log.debug("PWO-2.1 status : " + linkedIssueOfPWO2.getStatus().getSimpleStatus().getName());
                                        //Get status;
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                                            result = "OK";
                                        } else {
                                            result = "KO";
                                        }
                                    } else if (linkedIssueOfPWO2.getIssueTypeId().equals(String.valueOf(ConstantKeys.ID_IT_PWO22))) {
                                        log.debug("PWO-2.2 : " + linkedIssueOfPWO2.getKey());
                                        log.debug("PWO-2.2 status : " + linkedIssueOfPWO2.getStatus().getSimpleStatus().getName());
                                        //Get status;
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                                            result = "OK";
                                        } else {
                                            result = "KO";
                                        }
                                    } else if (linkedIssueOfPWO2.getIssueTypeId().equals(String.valueOf(ConstantKeys.ID_IT_PWO23))) {
                                        log.debug("PWO-2.3 : " + linkedIssueOfPWO2.getKey());
                                        log.debug("PWO-2.3 status : " + linkedIssueOfPWO2.getStatus().getSimpleStatus().getName());
                                        //Get status;
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == cancelled && result == "OK") {
                                            result = "OK";
                                        } else {
                                            result = "KO";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //}
        //}
        boolean passesCondition = false;
        if (result == "OK") {
            passesCondition = true;
        }
        return passesCondition;
    }
}
