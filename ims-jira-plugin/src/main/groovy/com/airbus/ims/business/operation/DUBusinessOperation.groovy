package com.airbus.ims.business.operation

import com.airbus.ims.util.ConstantKeys
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger

class DUBusinessOperation {

    /**
     * Condition to copy DU
     * @param mutableIssue
     * @param user
     * @param log
     * @return
     */
    static boolean isCopyDUConditionSatisfied(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();

        List<Issue> linkedIssuesOfDU = issueLinkManager.getLinkCollection(mutableIssue, user).getInwardIssues("Copies");
        boolean passesCondition = false;
        if (linkedIssuesOfDU != null) {
            for (Issue linkedIssueOfDU : linkedIssuesOfDU) {
                log.debug("Outward link of DU = " + linkedIssueOfDU.getKey());
                passesCondition = linkedIssueOfDU.getProjectId() != ConstantKeys.ID_PROJECT_DU
            }

        } else {
            passesCondition = true;
        }

        return passesCondition;
    }
}
