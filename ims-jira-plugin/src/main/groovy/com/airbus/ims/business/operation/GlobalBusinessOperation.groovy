package com.airbus.ims.business.operation

import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.issue.security.IssueSecurityLevel
import com.atlassian.jira.issue.security.IssueSecurityLevelManager
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager
import com.atlassian.jira.jql.parser.JqlParseException
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.scheme.Scheme
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.query.Query
import org.apache.log4j.Logger
import org.ofbiz.core.entity.GenericValue

/**
 * IMS Business Rules Manager
 */
class GlobalBusinessOperation {

    /**
     * Verify Links
     * @param issue
     * @param user
     * @param log
     * @param issueTypeId
     * @return
     */
    static boolean isLinkedToIssueType(Issue issue, ApplicationUser user, Logger log, String issueTypeId) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        boolean passesCondition = false;
        Issue sourceIssue;
        if (issueLinkManager.getInwardLinks(issue.getId()) != null) {
            log.debug("inward link");
            issueLinkManager.getInwardLinks(issue.getId()).each { IssueLink issueLink ->
                sourceIssue = issueLink.getSourceObject();
                log.debug("source issue : " + sourceIssue);
                String sourceIssueTypeId = sourceIssue.getIssueTypeId();
                log.debug("sourceIssueTypeId: " + sourceIssueTypeId);
                if (sourceIssueTypeId.equals(String.valueOf(issueTypeId))) {
                    passesCondition = true;
                }
            };
        }

        if (issueLinkManager.getOutwardLinks(issue.getId()) != null) {
            log.debug("outward link");
            issueLinkManager.getOutwardLinks(issue.getId()).each { IssueLink issueLink ->
                sourceIssue = issueLink.getDestinationObject();
                log.debug("source issue : " + sourceIssue);
                String sourceIssueTypeId = sourceIssue.getIssueTypeId();
                log.debug("sourceIssueTypeId: " + sourceIssueTypeId);
                if (sourceIssueTypeId.equals(String.valueOf(issueTypeId))) {
                    passesCondition = true;
                }

            }
        }
        return passesCondition;
    }


    /**
     * Call a remote script from Start transition for the issue
     * and Field Link to data???
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void updateFieldLinkToData(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        log.debug("Call a remote script from Start transition for " + mutableIssue.getKey());

        // Managers
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //Customfield
        CustomFieldManager cfManager = ComponentAccessor.getCustomFieldManager();
        CustomField linkToDataField = cfManager.getCustomFieldObject(ConstantKeys.ID_CF_LINKTODATA);

        // Windows
        //String script = "\\\\"+SERVER+"\\"+NETWORK+"\\"+FOLDER+"\\"+SUBFOLDER+"\\IMS_Script.bat "+issue.getSummary()
        String path = "\\\\" + ConstantKeys.SERVER + "\\" + ConstantKeys.NETWORK + "\\" + ConstantKeys.FOLDER + "\\" + mutableIssue.getSummary();
        String script = "\\\\D:\\JIRA-HOME\\NAS\\" + "IMS_Script.bat " + path;
        // Unix
        //String script = "sh /"+SERVER+"/"+NETWORK+"/script-IMS-Test-TLFWP1-81.sh"
        log.debug("script : " + script);
        Object proc = script.execute();

        log.debug("Executed");
        proc.waitForOrKill(5000);
        if (proc.exitValue() != null) {
            log.debug("Error");
            log.debug(proc.err.text);
        } else {
            log.debug("Applied");
            log.debug(proc.text);
        }

        try {
            mutableIssue.setCustomFieldValue(linkToDataField, path);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
            log.debug("Field Link to data updated");
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }


    /**
     * Check if the status of the issue is different than 'Completed' or 'Cancelled'
     * @param issue
     * @return
     */
    static boolean checkIfStatusIsNotCompletedOrCancelled(Issue issue) {
        return !("Completed".equals(issue.getStatus().getName()) || "Cancelled".equals(issue.getStatus().getName()));
    }


    /**
     * Check if the workload is updated for Sub PWO
     * @param changeLog
     * @param itemsChanged
     * @return
     */
    static boolean workloadIsUpdatedForSubPWO(GenericValue changeLog, List<String> itemsChanged) {
        boolean isUpdated = false;
        if (changeLog != null) {
            ChangeHistory change = ComponentAccessor.getChangeHistoryManager().getChangeHistoryById(changeLog.getLong("id"));
            List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
            for (ChangeItemBean changeIteamBean : changeItemBeans) {
                if (itemsChanged.contains(changeIteamBean.getField())) {
                    isUpdated = true;
                }
            }
        }
        return isUpdated;
    }

    /**
     * Set the security Level for Sub-PWO Sub PWO (PWO21, PWO22, PWO23)
     * @param mutableIssue
     * @param log
     */
    static void setSecurityLevelForSubPWO(MutableIssue mutableIssue, Logger log) {
        IssueSecuritySchemeManager issueSecuritySchemeManager = ComponentAccessor.getComponent(IssueSecuritySchemeManager.class);
        IssueSecurityLevelManager issueSecurityLevelManager = ComponentAccessor.getComponent(IssueSecurityLevelManager.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        Scheme schemeFromName = issueSecuritySchemeManager.getSchemeObjects()?.find {
            it.getName().equals("Program");
        };

        CustomField programField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PROGRAM);

        String programnFeed = mutableIssue.getCustomFieldValue(programField).toString();
        String[] moanFeedFormatted = programnFeed.substring(19).replaceAll("</value>/\n/</content>", "").replaceAll("</value>", ",").replaceAll("<value>", "").replaceAll("/\n /", "").replace("  ", " ").split(",");
        log.debug("MOA nFeed formatted : " + moanFeedFormatted[0]);

        IssueSecurityLevel securityLevelfound = issueSecurityLevelManager.getIssueSecurityLevels(schemeFromName.getId())?.find {
            it.getName().equals(moanFeedFormatted[0]);
        };
        Long securityLvl = (securityLevelfound == null ? null : securityLevelfound.getId());
        log.debug("securityLvl : " + securityLvl);

        mutableIssue.setSecurityLevelId(securityLvl);
    }

    /**
     * Creation/Edit of a PWO, update of the reference
     * @param issue
     * @param user
     * @param log
     * @param pwoType
     */
    static void createOrEditPWO(Issue issue, ApplicationUser user, Logger log, String pwoType) {
        log.warn("PWO created UpdateTitleCopyToSummary");
        log.debug("Creation/Edit of a PWO, update of the reference");

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class);
        SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class);

        //CustomField
        CustomField programField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PROGRAM);
        CustomField referenceField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REFERENCE);
        CustomField incrementalIDField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INCREMENTALID);

        //Business
        log.debug("issue : " + issue.getKey());

        String valueToReturn;
        String programValueBrut;
        //String[] ProgramValueList
        String programValue;
        String yearValue;
        String JQL_QUERY = "project = \"%s\" and issuetype in  (\"%s\") AND created > startOfYear() and \"Program\"=\"%s [Program_%s]\" and \"Incremental ID\" is not empty ORDER BY \"Incremental ID\" DESC";
        String completeCompteur;
        String query;
        double compteur = 0;

        Issue firstIssue = null;

        //programValueBrut = ((String) (issue.getCustomFieldValue(programField)));
        //programValue = programValueBrut.replaceAll("Program_", "").replaceAll("<content>", "").replaceAll("<value>", "").replaceAll("</value>", ",").replaceAll("</content>", "").replaceAll(",", "").replaceAll("\n", "").replaceAll("  ", "");
        programValue = Toolbox.getValueFromNFeedFieldSingle(programField, issue, log);
        programValue = programValue.replace("Program_", "");

        Date dateCreated = Date.parse("yyyy-MM-dd", issue.getCreated().toString()) // Retrieve created date from issue
        log.debug("dateCreated: " + dateCreated);
        yearValue = dateCreated.toCalendar().get(Calendar.YEAR).toString() // Get only the year
        log.debug("Year: " + yearValue);

        log.debug("issue type : " + issue.getIssueTypeId());

        try {
            query = String.format(JQL_QUERY, issue.getProjectObject().getName(), issue.getIssueType().getName(), programValue, programValue, issue.getId());
            log.debug("Query : " + query);
            Query jqlQuery = jqlQueryParser.parseQuery(query);
            SearchResults results = searchProvider.search(jqlQuery, user, PagerFilter.getUnlimitedFilter());
            List<Issue> issuesFound = new ArrayList<Issue>();
            issuesFound = results.getIssues();
            log.debug("issues found : " + issuesFound);
            log.debug("issues found size : " + issuesFound.size());
            if (issuesFound.size() > 0) {
                if (!issuesFound.get(0).getKey().equals(issue.getKey())) {
                    firstIssue = issuesFound.get(0);
                } else if (issuesFound.get(1) != null) {
                    firstIssue = issuesFound.get(1);
                } else {
                    compteur = 1;
                }

                log.debug("issue : " + firstIssue);
                if (firstIssue.getCustomFieldValue(incrementalIDField) != null) {
                    compteur = Double.parseDouble(firstIssue.getCustomFieldValue(incrementalIDField).toString());
                    compteur = Math.round(compteur + 1);
                }

                log.debug("compteur : " + compteur);
            } else {
                compteur = 1;
            }


            if (compteur < 10) {
                completeCompteur = "0000";
            } else if (compteur < 100) {
                completeCompteur = "000";
            } else if (compteur < 1000) {
                completeCompteur = "00";
            } else if (compteur < 10000) {
                completeCompteur = "0";
            } else {
                completeCompteur = "";
            }

            valueToReturn = programValue + "_" + yearValue + "_" + pwoType + "_" + completeCompteur + Math.round(compteur);

            MutableIssue mutableIssue = (MutableIssue)issueManager.getIssueObject(issue.getKey());
            mutableIssue.setCustomFieldValue(referenceField, valueToReturn);
            mutableIssue.setCustomFieldValue(incrementalIDField, compteur);
            mutableIssue.setSummary(valueToReturn);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);

            log.debug("Value : " + valueToReturn);
        } catch (SearchException e1) {
            e1.printStackTrace();
        } catch (JqlParseException e2) {
            e2.printStackTrace();
        }
    }
}