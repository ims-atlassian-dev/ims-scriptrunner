package com.airbus.ims.business.operation


import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.config.FieldConfig
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.user.ApplicationUser
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import org.apache.log4j.Logger
import org.joda.time.LocalDate

import java.sql.Timestamp

class PWO0BusinessOperation {

    /**
     * Cancel PWO0
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void cancelPWO0(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        //String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        //def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
        //    loadScriptByName(path);
        //}

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField partsDataStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_STATUS);
        CustomField maintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_STATUS);
        CustomField maintStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_STATUS);

        //Variables

        //Business
        FieldConfig fieldConfigPartsDataStatus = partsDataStatusField.getRelevantConfig(mutableIssue);
        Object valuePartsDataStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataStatus)?.find{
            it.toString().equals("Cancelled");
        };
        FieldConfig fieldConfigMaintPlanningStatus = maintPlanningStatusField.getRelevantConfig(mutableIssue);
        Object valueMaintPlanningStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningStatus)?.find{
            it.toString().equals("Cancelled");
        };
        FieldConfig fieldConfigMaintStatus = maintStatusField.getRelevantConfig(mutableIssue);
        Object valueMaintStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintStatus)?.find{
            it.toString().equals("Cancelled");
        };

        try {
            mutableIssue.setCustomFieldValue(partsDataStatusField, valuePartsDataStatus);
            mutableIssue.setCustomFieldValue(maintPlanningStatusField, valueMaintPlanningStatus);
            mutableIssue.setCustomFieldValue(maintStatusField, valueMaintStatus);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }


    /**
     * re-open PWO0 as Administrator
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void reOpenPWO0AsAdministrator(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager.class);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField partsDataStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_STATUS);
        CustomField maintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_STATUS);
        CustomField maintStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_STATUS);

        //Variables

        //Business

        FieldConfig fieldConfigPartsDataStatus = partsDataStatusField.getRelevantConfig(mutableIssue);
        Object valuePartsDataStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataStatus)?.find{
            it.toString().equals(ConstantKeys.NOT_STARTED);
        };
        FieldConfig fieldConfigMaintPlanningStatus = maintPlanningStatusField.getRelevantConfig(mutableIssue);
        Object valueMaintPlanningStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningStatus)?.find{
            it.toString().equals(ConstantKeys.NOT_STARTED);
        };
        FieldConfig fieldConfigMaintStatus = maintStatusField.getRelevantConfig(mutableIssue);
        Object valueMaintStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintStatus)?.find{  it->
            it.toString().equals(ConstantKeys.NOT_STARTED);
        };

        mutableIssue.setCustomFieldValue(partsDataStatusField, valuePartsDataStatus);
        mutableIssue.setCustomFieldValue(maintPlanningStatusField, valueMaintPlanningStatus);
        mutableIssue.setCustomFieldValue(maintStatusField, valueMaintStatus);

        issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
        try {
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }


    /**
     * Process PWO0 Rules on dependencies
     * @param issue
     * @param user
     * @param log
     */
    static void updatePWO0Dependencies(Issue issue, ApplicationUser user, Logger log){
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField startDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_STARTDATE);

        //def DateDuJour = new TimeStamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        //log.debug "Date du jour : " + DateDuJour

        log.debug("issue en cours : " + issue.getKey());

        if (issueLinkManager.getOutwardLinks(issue.getId())!= null) {
            issueLinkManager.getOutwardLinks(issue.getId()).each{ issueLink ->
                Issue sourceIssue = (Issue)issueLink.getDestinationObject();
                log.debug("Linked issue of PWO-0 : " + sourceIssue.getKey());
                String issueType = sourceIssue.getIssueTypeId();
                if (issueType.equals(ConstantKeys.ID_IT_CLUSTER)) {
                    Issue clusterIssue = (Issue)sourceIssue;
                    if (issueLinkManager.getOutwardLinks(clusterIssue.getId()) != null) {
                        return issueLinkManager.getOutwardLinks(clusterIssue.getId()).each{ issueLink2 ->
                            Issue sourceIssue2 = (Issue)issueLink2.getDestinationObject();
                            log.debug("Linked issue of Cluster : " + sourceIssue2.getKey());
                            String issueType2 = sourceIssue2.getIssueTypeId();
                            if (issueType2.equals(ConstantKeys.ID_IT_TRIGGER)) {
                                log.debug("Trigger found : " + sourceIssue2.getKey());
                                Issue triggerissue = issueManager.getIssueObject(sourceIssue2.getId());
                                if (!triggerissue.getStatusId().equals(ConstantKeys.REQUIRED_TRIGGER_STATUS)) {
                                    ((MutableIssue) triggerissue).setCustomFieldValue(startDateField, now);

                                    issueManager.updateIssue(user, (MutableIssue) triggerissue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    try {
                                        issueIndexingService.reIndex(triggerissue);
                                    } catch (IndexException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else if (issueLinkManager.getInwardLinks(issue.getId()) != null) {
            issueLinkManager.getInwardLinks(issue.getId()).each{ issueLink ->
                Issue sourceIssue = (Issue)issueLink.getSourceObject();
                log.debug("Linked issue of PWO-0 : " + sourceIssue.getKey());
                String issueType = sourceIssue.getIssueTypeId().toString();
                if (issueType.equals(ConstantKeys.ID_IT_CLUSTER)) {
                    Issue clusterIssue = sourceIssue;
                    if (issueLinkManager.getOutwardLinks(clusterIssue.getId()) != null) {
                        return issueLinkManager.getOutwardLinks(clusterIssue.getId()).each{ issueLink2 ->
                            Issue sourceIssue2 = (Issue)issueLink2.getDestinationObject();
                            log.debug("Linked issue of Cluster : " + sourceIssue2.getKey());
                            String issueType2 = sourceIssue2.getIssueTypeId();
                            if (issueType2.equals(ConstantKeys.ID_IT_TRIGGER)) {
                                log.debug("Trigger found : " + sourceIssue2.getKey());
                                Issue triggerissue = (Issue)issueManager.getIssueObject(sourceIssue2.getId());
                                if (!triggerissue.getStatusId().equals(ConstantKeys.REQUIRED_TRIGGER_STATUS)) {
                                    ((MutableIssue) triggerissue).setCustomFieldValue(startDateField, now);
                                    issueManager.updateIssue(user, (MutableIssue) triggerissue, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    try {
                                        issueIndexingService.reIndex(triggerissue);
                                    } catch (IndexException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        };
                    }
                }
            };
        }
    }


    /**
     * Process PWO0 Business Rules on dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO0PreDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
        loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField preIAPartsDataRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_PARTSDATA_REQUESTEDDATE);
        CustomField preIAPartsDataValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_PARTSDATA_VALIDATION_REQUESTEDDATE);
        CustomField preIAMaintPlanningRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINTPLANNING_REQUESTEDDATE);
        CustomField preIAMaintPlanningValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINTPLANNING_VALIDATION_REQUESTEDDATE);
        CustomField preIAMaintRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINT_REQUESTEDDATE);
        CustomField preIAMaintValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINT_VALIDATION_REQUESTEDDATE);
        CustomField partsDataStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_STATUS);
        CustomField maintPlanningStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_STATUS);
        CustomField maintStatusField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINT_STATUS);
        CustomField PartsData_1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_1STCOMMITTEDDATE);
        CustomField maintPlanning1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_1STCOMMITTEDDATE);
        CustomField maint1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.IF_CF_MAINT_1STCOMMITTEDDATE);
        CustomField partsDataLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_LASTCOMMITTEDDATE);
        CustomField maintPlanningLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNING_LASTCOMMITTEDDATE);
        CustomField maintLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.IF_CF_MAINT_LASTCOMMITTEDDATE);
        CustomField partsDataValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_VALIDATION_1STCOMMITTEDDATE);
        CustomField maintPlanningValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNINGVALIDATION_1STCOMMITTEDDATE);
        CustomField maintValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.IF_CF_MAINTVALIDATION_1STCOMMITTEDDATE);
        CustomField partsDataValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATAVALIDATION_LASTCOMMITTEDDATE);
        CustomField maintPlanningValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNINGVALIDATION_LASTCOMMITTEDDATE);
        CustomField maintValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.IF_CF_MAINTVALIDATION_LASTCOMMITTEDDATE);
        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);
        CustomField domainField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_DOMAIN);

        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        log.debug("Todays Date : " + now);
        Date preIAPartsDataRequestedDate;
        Date preIAMaintPlanningRequestedDate;
        Date preIAMaintRequestedDate;
        Date preIAPartsDataValidationRequestedDate;
        Date preIAMaintPlanningValidationRequestedDate;
        Date preIAMaintValidationRequestedDate;
        Date partsData1stCommittedDate;
        Date maintplanning1stCommittedDate;
        Date maint1stCommittedDate;
        Date partsDataLastCommittedDate;
        Date maintPlanningLastCommittedDate;
        Date maintLastCommittedDate;
        Date partsDataValidation1stCommittedDate;
        Date maintPlanningValidation1stCommittedDate;
        Date maintValidation1stCommittedDate;
        Date partsDataValidationLastCommittedDate;
        Date maintPlanningValidationLastCommittedDate;
        Date maintValidationLastCommittedDate;

        try {
            //Business

            preIAPartsDataRequestedDate = Toolbox.incrementTimestamp(now, 7);
            preIAMaintPlanningRequestedDate = Toolbox.incrementTimestamp(now, 7);
            preIAMaintRequestedDate = Toolbox.incrementTimestamp(now, 7);
            log.debug("preIAMaintRequestedDate : " + preIAMaintRequestedDate);

            preIAPartsDataValidationRequestedDate = Toolbox.incrementTimestamp(now, 14);
            preIAMaintPlanningValidationRequestedDate = Toolbox.incrementTimestamp(now, 14);
            preIAMaintValidationRequestedDate = Toolbox.incrementTimestamp(now, 14)

            FieldConfig fieldConfigPartsDataStatus = partsDataStatusField.getRelevantConfig(mutableIssue);
            FieldConfig fieldConfigMaintPlanningStatus = maintPlanningStatusField.getRelevantConfig(mutableIssue);
            FieldConfig fieldConfigMaintStatus = maintStatusField.getRelevantConfig(mutableIssue);

            String resultsPartsData;
            String resultsMaintPlanning;
            String resultsMaint;

            Object[] domainValues = (Object[])mutableIssue.getCustomFieldValue(domainField);
            log.debug("Domains values = " + Arrays.toString(domainValues));
            for (Object domainValue : domainValues) {
                if ("Parts Data".equals(domainValue.toString())) {
                    resultsPartsData = "In progress";
                } else if (!"In progress".equals(resultsPartsData)) {
                    resultsPartsData = "Cancelled";
                }

                if ("Maint Planning".equals(domainValue.toString())) {
                    resultsMaintPlanning = "In progress";
                } else if (!"In progress".equals(resultsMaintPlanning)) {
                    resultsMaintPlanning = "Cancelled";
                }

                if ("Maintenance".equals(domainValue.toString())) {
                    resultsMaint = "In progress";
                } else if (!"In progress".equals(resultsMaint)) {
                    resultsMaint = "Cancelled";
                }
            }

            Object valuePartsDataStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigPartsDataStatus)?.find{
                it.toString().equals(resultsPartsData);
            };
            Object valueMaintPlanningStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintPlanningStatus)?.find{
                it.toString().equals(resultsMaintPlanning);
            };
            Object valueMaintStatus = ComponentAccessor.getOptionsManager().getOptions(fieldConfigMaintStatus)?.find{ it->
                it.toString().equals(resultsMaint);
            };

            mutableIssue.setCustomFieldValue(partsDataStatusField, valuePartsDataStatus);
            mutableIssue.setCustomFieldValue(maintPlanningStatusField, valueMaintPlanningStatus);
            mutableIssue.setCustomFieldValue(maintStatusField, valueMaintStatus);

            partsData1stCommittedDate = preIAPartsDataRequestedDate;
            maintplanning1stCommittedDate = preIAMaintPlanningRequestedDate;
            log.debug("maint planning 1st committed : " + maintplanning1stCommittedDate);
            maint1stCommittedDate = preIAMaintRequestedDate;

            partsDataLastCommittedDate = partsData1stCommittedDate;
            maintPlanningLastCommittedDate = maintplanning1stCommittedDate;
            log.debug("maint planning last committed : " + maintPlanningLastCommittedDate);
            maintLastCommittedDate = maint1stCommittedDate;

            partsDataValidation1stCommittedDate = preIAPartsDataValidationRequestedDate;
            maintPlanningValidation1stCommittedDate = preIAMaintPlanningValidationRequestedDate;
            maintValidation1stCommittedDate = preIAMaintValidationRequestedDate;

            partsDataValidationLastCommittedDate = partsDataValidation1stCommittedDate;
            maintPlanningValidationLastCommittedDate = maintPlanningValidation1stCommittedDate;
            maintValidationLastCommittedDate = maintValidation1stCommittedDate;

            mutableIssue.setCustomFieldValue(preIAPartsDataRequestedDateField, preIAPartsDataRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintPlanningRequestedDateField, preIAMaintPlanningRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintRequestedDateField, preIAMaintRequestedDate);

            mutableIssue.setCustomFieldValue(preIAPartsDataValidationRequestedDateField, preIAPartsDataValidationRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintPlanningValidationRequestedDateField, preIAMaintPlanningValidationRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintValidationRequestedDateField, preIAMaintValidationRequestedDate);

            mutableIssue.setCustomFieldValue(PartsData_1stCommittedDateField, partsData1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanning1stCommittedDateField, maintplanning1stCommittedDate);
            mutableIssue.setCustomFieldValue(maint1stCommittedDateField, maint1stCommittedDate);

            mutableIssue.setCustomFieldValue(partsDataLastCommittedDateField, partsDataLastCommittedDate);
            log.debug("maint planning last committed avant setting: " + maintPlanningValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningLastCommittedDateField, maintPlanningValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintLastCommittedDateField, maintLastCommittedDate);

            mutableIssue.setCustomFieldValue(partsDataValidation1stCommittedDateField, partsDataValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningValidation1stCommittedDateField, maintPlanningValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintValidation1stCommittedDateField, maintValidation1stCommittedDate);

            mutableIssue.setCustomFieldValue(partsDataValidationLastCommittedDateField, partsDataValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningValidationLastCommittedDateField, maintPlanningValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintValidationLastCommittedDateField, maintValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO0 Business Rules on final dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO0FinalDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        final String PATH = "/atlassian/jira/data/scripts/util/Functions.groovy";

        try {
            Object script = new GroovyScriptEngine(".", ScriptRunnerImpl.getScriptRunner().getGroovyScriptEngine().getGroovyClassLoader()).with{
                return loadScriptByName(PATH);
            };
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField preIAPartsDataValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_PARTSDATA_VALIDATION_REQUESTEDDATE);
        CustomField preIAMaintPlanningValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINTPLANNING_VALIDATION_REQUESTEDDATE);
        CustomField preIAMaintValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PREIA_MAINT_VALIDATION_REQUESTEDDATE);
        CustomField partsDataValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATA_VALIDATION_1STCOMMITTEDDATE);
        CustomField maintPlanningValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNINGVALIDATION_1STCOMMITTEDDATE);
        CustomField maintValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.IF_CF_MAINTVALIDATION_1STCOMMITTEDDATE);
        CustomField partsDataValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PARTSDATAVALIDATION_LASTCOMMITTEDDATE);
        CustomField maintPlanningValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MAINTPLANNINGVALIDATION_LASTCOMMITTEDDATE);
        CustomField maintValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.IF_CF_MAINTVALIDATION_LASTCOMMITTEDDATE);
        CustomField requestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_REQUESTEDDATE);

        //Variables
        Timestamp now = new Timestamp(new Date().getTime());
        log.debug("Todays Date : " + now);
        Date preIAPartsDataValidationRequestedDate;
        Date preIAMaintPlanningValidationRequestedDate;
        Date preIAMaintValidationRequestedDate;
        Date partsDataValidation1stCommittedDate;
        Date maintPlanningValidation1stCommittedDate;
        Date maintValidation1stCommittedDate;
        Date partsDataValidationLastCommittedDate;
        Date maintPlanningValidationLastCommittedDate;
        Date maintValidationLastCommittedDate;

        //Business

        preIAPartsDataValidationRequestedDate = Toolbox.incrementTimestamp(now, 7);
        preIAMaintPlanningValidationRequestedDate = Toolbox.incrementTimestamp(now, 7);
        preIAMaintValidationRequestedDate = Toolbox.incrementTimestamp(now, 7);

        partsDataValidation1stCommittedDate = preIAPartsDataValidationRequestedDate;
        maintPlanningValidation1stCommittedDate = preIAMaintPlanningValidationRequestedDate;
        maintValidation1stCommittedDate = preIAMaintValidationRequestedDate;

        partsDataValidationLastCommittedDate = partsDataValidation1stCommittedDate;
        maintPlanningValidationLastCommittedDate = maintPlanningValidation1stCommittedDate;
        maintValidationLastCommittedDate = maintValidation1stCommittedDate;

        try {
            mutableIssue.setCustomFieldValue(preIAPartsDataValidationRequestedDateField, preIAPartsDataValidationRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintPlanningValidationRequestedDateField, preIAMaintPlanningValidationRequestedDate);
            mutableIssue.setCustomFieldValue(preIAMaintValidationRequestedDateField, preIAMaintValidationRequestedDate);

            mutableIssue.setCustomFieldValue(partsDataValidation1stCommittedDateField, partsDataValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningValidation1stCommittedDateField, maintPlanningValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(maintValidation1stCommittedDateField, maintValidation1stCommittedDate);

            mutableIssue.setCustomFieldValue(partsDataValidationLastCommittedDateField, partsDataValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintPlanningValidationLastCommittedDateField, maintPlanningValidationLastCommittedDate);
            mutableIssue.setCustomFieldValue(maintValidationLastCommittedDateField, maintValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }
}
