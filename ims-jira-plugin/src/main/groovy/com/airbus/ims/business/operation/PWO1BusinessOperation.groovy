package com.airbus.ims.business.operation


import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.config.FieldConfig
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger
import org.joda.time.LocalDate

import java.sql.Timestamp

class PWO1BusinessOperation {

    /**
     * Process PWO1 rules on dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO1Dates(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField iaRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_REQUESTEDDATE);
        CustomField iaValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_REQUESTEDDATE);
        CustomField ia1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_1STCOMMITTEDDATE);
        CustomField iaLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_LASTCOMMITTEDDATE);
        CustomField iaValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_1STCOMMITTEDDATE);
        CustomField iaValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        log.debug("Todays Date : " + now);
        Date iaRequestedDate;
        Date iaValidationRequestedDate;
        Date ia1stCommittedDate;
        Date iaLastCommittedDate;
        Date iaValidation1stCommittedDate;
        Date iaValidationLastCommittedDate;

        //Business

        try {
            iaRequestedDate = Toolbox.incrementTimestamp(now, 14);
            ia1stCommittedDate = iaRequestedDate;
            iaLastCommittedDate = ia1stCommittedDate;
            iaValidationRequestedDate =  Toolbox.incrementTimestamp(now, 21);
            iaValidation1stCommittedDate = iaValidationRequestedDate;
            iaValidationLastCommittedDate = iaValidation1stCommittedDate;

            mutableIssue.setCustomFieldValue(iaRequestedDateField, iaRequestedDate);
            mutableIssue.setCustomFieldValue(iaValidationRequestedDateField, iaValidationRequestedDate);
            mutableIssue.setCustomFieldValue(ia1stCommittedDateField, ia1stCommittedDate);
            mutableIssue.setCustomFieldValue(iaLastCommittedDateField, iaLastCommittedDate);
            mutableIssue.setCustomFieldValue(iaValidation1stCommittedDateField, iaValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(iaValidationLastCommittedDateField, iaValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     *Process PWO1 Business Rules on the field iaValidatedField (First step on delivery)
     *
     *(TO BE COMPLETED)
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO1IAValidatedField(MutableIssue mutableIssue, ApplicationUser user, Logger log){
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        //CustomField
        CustomField iaValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_REQUESTEDDATE);
        CustomField iaValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_1STCOMMITTEDDATE);
        CustomField iaValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATION_LASTCOMMITTEDDATE);
        CustomField ia1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_1STDELIVERYDATE);
        CustomField iaLastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_LASTDELIVERYDATE);
        CustomField iaValidatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_VALIDATED);

        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date iaValidationRequestedDate;
        Date iaValidation1stCommittedDate;
        Date iaValidationLastCommittedDate;
        Date ia1stDeliveryDate;
        Date iaLastDeliveryDate;

        try {
            //Business
            iaValidationRequestedDate = Toolbox.incrementTimestamp(now, 7);
            iaValidation1stCommittedDate = iaValidationRequestedDate;
            iaValidationLastCommittedDate = iaValidation1stCommittedDate;
            ia1stDeliveryDate = now;
            iaLastDeliveryDate = now;
            log.debug("IA LAst delivery date to set : " + iaLastDeliveryDate);

            mutableIssue.setCustomFieldValue(iaValidationRequestedDateField, iaValidationRequestedDate);
            mutableIssue.setCustomFieldValue(iaValidation1stCommittedDateField, iaValidation1stCommittedDate);
            mutableIssue.setCustomFieldValue(iaValidationLastCommittedDateField, iaValidationLastCommittedDate);
            log.debug("setting last delivery date");
            mutableIssue.setCustomFieldValue(iaLastDeliveryDateField, iaLastDeliveryDate);
            log.debug("last delivery date have been set");
            if (mutableIssue.getCustomFieldValue(ia1stDeliveryDateField) != null) {
                mutableIssue.setCustomFieldValue(ia1stDeliveryDateField, ia1stDeliveryDate);
            }


            if (mutableIssue.getCustomFieldValue(iaValidatedField).toString().equals("Rejected")) {
                FieldConfig fieldConfigValidated = iaValidatedField.getRelevantConfig(mutableIssue);
                Option valueValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigValidated)?.find{
                    it.toString().equals(ConstantKeys.VALIDATION_PENDING);
                };
                mutableIssue.setCustomFieldValue(iaValidatedField, valueValidated);
            }

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO1 Business rules on nbReworkField (Second step on delivery)
     *
     * (TO BE COMPLETED)
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO1NbReworkField(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        log.debug("Start");

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(mutableIssue);
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        log.debug("Issue : " + mutableIssue.getKey());

        //CustomField
        CustomField manuallyEditedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MANUALLYEDITED_PWO1);
        CustomField nbReworkField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_NBREWORK_PWO1);

        //Valeur
        String iaManuallyEdited = (String) mutableIssue.getCustomFieldValue(manuallyEditedField);
        double nbRework = 0;
        if (mutableIssue.getCustomFieldValue(nbReworkField) != null) {
            nbRework = Double.valueOf(mutableIssue.getCustomFieldValue(nbReworkField).toString());
        }

        //Business
        double valeurToReturn;
        double compteur = 0;
        if (iaManuallyEdited.equals("Yes")) {
            valeurToReturn = nbRework + 1;
        } else {
            log.debug("Pas d'edit manuel");
            for (ChangeHistoryItem item : changeItems) {
                log.debug("Change history item : " + item.getField() + ", change history value : " + item.getTos().values());
                if (item.getField().equals("status") && item.getTos().values().contains("Delivered") && item.getFroms().values().contains("In Progress")) {
                    log.debug("je compte");
                    compteur = compteur + 1;
                    log.debug("compteur : " + compteur);
                }

            }

            valeurToReturn = compteur - 1;
            log.debug("Valeur : " + valeurToReturn);
        }

        try {
            mutableIssue.setCustomFieldValue(nbReworkField, valeurToReturn);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Condition for PWO1 to start
     * @param mutableIssue
     * @param user
     * @param log
     * @return
     */
    static boolean isPWO1StartConditionSatisfied(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField iaWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_WU);
        CustomField iaNbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_IA_NBWU);

        String result = "OK";

        if (!result.equals("KO") && mutableIssue.getCustomFieldValue(iaWUField) != null && mutableIssue.getCustomFieldValue(iaNbWUField) != null) {
            result = "OK";
        } else {
            result = "KO";
        }

        boolean passesCondition = false;
        if (result.equals("OK")) {
            passesCondition = true;
        }
        return passesCondition;
    }

}
