package com.airbus.ims.business.operation


import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.config.FieldConfig
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.user.ApplicationUser
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import org.apache.log4j.Logger
import org.joda.time.LocalDate

import java.sql.Timestamp

class PWO21BusinessOperation {

    /**
     *
     * @param issue
     * @param user
     * @return TO BE COMPLETED
     */
    static boolean isPWO21Ready(Issue issue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField authWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_WU);
        CustomField authNbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_NBWU);
        CustomField authVerifWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIF_WU);
        CustomField authVerifNbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIF_NBWU);

        String result = "OK";

        Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO : linkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                if (!result.equals("KO") && linkedIssueOfCurrentPWO.getCustomFieldValue(authWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(authNbWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(authVerifWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(authVerifNbWUField) != null) {
                    result = "OK";
                } else {
                    result = "KO";
                }
            }
        }

        boolean passesCondition = false;
        if (result.equals("OK")) {
            passesCondition = true;
        }
        return passesCondition;
    }


    /**
     * Process PWO21 Business Rules on dates TO BE COMPLETED
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO21Dates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */


        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField authRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_REQUESTEDDATE);
        CustomField authVerificationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_REQUESTEDDATE);
        CustomField authValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_REQUESTEDDATE);
        CustomField authFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_FIRSTCOMMITTEDDATE);
        CustomField authVerificationFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_FIRSTCOMMITTEDDATE);
        CustomField authValidationFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE);
        CustomField authLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_LASTCOMMITTEDDATE);
        CustomField authVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField authValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date authRequestedDate;
        Date authVerificationRequestedDate;
        Date authValidationRequestedDate;
        Date authFirstCommittedDate;
        Date authVerificationFirstCommittedDate;
        Date authValidationFirstCommittedDate;
        Date authLastCommittedDate;
        Date authVerificationLastCommittedDate;
        Date authValidationLastCommittedDate;

        try {
            //Business
            //Calculate date on PWO-2.1
            authRequestedDate = Toolbox.incrementTimestamp(now, 14);
            authVerificationRequestedDate = Toolbox.incrementTimestamp(now, 28);
            authValidationRequestedDate = Toolbox.incrementTimestamp(now, 42);

            authFirstCommittedDate = authRequestedDate;
            authVerificationFirstCommittedDate = authVerificationRequestedDate;
            authValidationFirstCommittedDate = authValidationRequestedDate;

            authLastCommittedDate = authFirstCommittedDate;
            authVerificationLastCommittedDate = authVerificationFirstCommittedDate;
            authValidationLastCommittedDate = authValidationFirstCommittedDate;


            mutableIssue.setCustomFieldValue(authRequestedDateField, authRequestedDate);
            mutableIssue.setCustomFieldValue(authVerificationRequestedDateField, authVerificationRequestedDate);
            mutableIssue.setCustomFieldValue(authValidationRequestedDateField, authValidationRequestedDate);

            mutableIssue.setCustomFieldValue(authFirstCommittedDateField, authFirstCommittedDate);
            mutableIssue.setCustomFieldValue(authVerificationFirstCommittedDateField, authVerificationFirstCommittedDate);
            mutableIssue.setCustomFieldValue(authValidationFirstCommittedDateField, authValidationFirstCommittedDate);

            mutableIssue.setCustomFieldValue(authLastCommittedDateField, authLastCommittedDate);
            mutableIssue.setCustomFieldValue(authVerificationLastCommittedDateField, authVerificationLastCommittedDate);
            mutableIssue.setCustomFieldValue(authValidationLastCommittedDateField, authValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO21 Business Rules on Authoring Validation
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO21AuthoringValidation(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        try {
            Object script = new GroovyScriptEngine(".", ScriptRunnerImpl.getScriptRunner().getGroovyScriptEngine().getGroovyClassLoader()).with{
                return loadScriptByName(ConstantKeys.GROOVY_PATH_FUNCTION);
            };
        } catch(IOException e) {
            e.printStackTrace();
        }

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField authLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_LASTCOMMITTEDDATE);
        CustomField authVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField authValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);

        //Variables
        Date authLastCommittedDate;
        Date authVerificationLastCommittedDate;
        Date authValidationLastCommittedDate;

        Timestamp now = new Timestamp(new Date().getTime());

        try {
            //Business
            authLastCommittedDate = Toolbox.incrementTimestamp(now,14);
            mutableIssue.setCustomFieldValue(authLastCommittedDateField, authLastCommittedDate);

            authVerificationLastCommittedDate =  Toolbox.incrementTimestamp(now,28);
            mutableIssue.setCustomFieldValue(authVerificationLastCommittedDateField, authVerificationLastCommittedDate);

            authValidationLastCommittedDate =  Toolbox.incrementTimestamp(now,42);
            mutableIssue.setCustomFieldValue(authValidationLastCommittedDateField, authValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO21 Business Rules on the field ReworkDateField
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO21AuthoringRework(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        CustomField authReworkDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_REWORKDATE);

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date authReworkDate;
        Date futureDate = Toolbox.incrementTimestamp(now, 7);

        if (!mutableIssue.getCustomFieldValue(authReworkDateField).equals(futureDate)) {
            authReworkDate = futureDate;

            try {
                mutableIssue.setCustomFieldValue(authReworkDateField, authReworkDate);

                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            } catch (IndexException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Process PWO21 Business Rules on the Field Rework
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO21ReworkField(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        log.debug("Start");

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(mutableIssue);
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        log.debug("Issue : " + mutableIssue.getKey());

        //CustomField
        CustomField manuallyEditedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MANUALLYEDITED);
        CustomField nbReworkField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_NBREWORK);

        //Valeur
        String authManuallyEdited = (String) mutableIssue.getCustomFieldValue(manuallyEditedField);
        double nbRework = 0;
        if (mutableIssue.getCustomFieldValue(nbReworkField) != null) {
            nbRework = Double.parseDouble(mutableIssue.getCustomFieldValue(nbReworkField).toString());
        }


        //Business
        double valeurToReturn;
        double compteur = 0;
        if (authManuallyEdited.equals("Yes")) {
            valeurToReturn = nbRework + 1;
        } else {
            log.debug("Pas d'Ã©dit manuel");
            for (ChangeHistoryItem item : changeItems) {
                if (item.getField().equals("status") && item.getFroms().values().contains("In Progress") && item.getTos().values().contains("Delivered")) {
                    compteur = compteur + 1;
                }

            }

            valeurToReturn = compteur - 1;
        }

        try {
            mutableIssue.setCustomFieldValue(nbReworkField, valeurToReturn);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process dates calculation for PWO21 on Authoring Request
     * @param mutableIssue
     * @param log
     */
    static void processPWO21AuthoringRequest(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        }*/

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField authVerifiedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFIED);
        CustomField authValidatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATED);
        CustomField auth1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_1STDELIVERYDATE);
        CustomField authLastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_LASTDELIVERYDATE);
        CustomField authVerificationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_REQUESTEDDATE);
        CustomField authVerificationFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_FIRSTCOMMITTEDDATE);
        CustomField authVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField authValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_REQUESTEDDATE);
        CustomField authValidationFirstCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_FIRSTCOMMITTEDDATE);
        CustomField authValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VALIDATION_LASTCOMMITTEDDATE);
        CustomField authVerificationReworkDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AUTH_VERIFICATION_REWORKDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date auth1stDeliveryDate;
        Date authLastDeliveryDate;
        Date authVerificationRequestedDate;
        Date authVerification1stCommittedDate;
        Date authVerificationLastCommittedDate;
        Date authValidationRequestedDate;
        Date authValidation1stCommittedDate;
        Date authValidationLastCommittedDate;
        Date authVerificationReworkDate;


        //Business
        //Calculate date on PWO-2.1
        auth1stDeliveryDate = now;
        authLastDeliveryDate = now;

        authVerificationRequestedDate = Toolbox.incrementTimestamp(now, 14);
        authValidationRequestedDate = Toolbox.incrementTimestamp(now, 28);

        Timestamp authVerificationFirstCommittedDate = ((Timestamp) (authVerificationRequestedDate));
        Timestamp authValidationFirstCommittedDate = ((Timestamp) (authValidationRequestedDate));

        authVerificationLastCommittedDate = authVerificationFirstCommittedDate;
        authValidationLastCommittedDate = authValidationFirstCommittedDate;

        authVerificationReworkDate = Toolbox.incrementTimestamp(now, 7);

        if (mutableIssue.getCustomFieldValue(auth1stDeliveryDateField) != null) {
            log.debug("1st delivery, 1stdelivery field is empty");
            mutableIssue.setCustomFieldValue(auth1stDeliveryDateField, auth1stDeliveryDate);
            mutableIssue.setCustomFieldValue(authVerificationRequestedDateField, authVerificationRequestedDate);
            mutableIssue.setCustomFieldValue(authVerificationFirstCommittedDateField, authVerificationFirstCommittedDate);
            mutableIssue.setCustomFieldValue(authValidationRequestedDateField, authValidationRequestedDate);
            mutableIssue.setCustomFieldValue(authValidationFirstCommittedDateField, authValidationFirstCommittedDate);
            mutableIssue.setCustomFieldValue(authVerificationLastCommittedDateField, authVerificationLastCommittedDate);
            mutableIssue.setCustomFieldValue(authValidationLastCommittedDateField, authValidationLastCommittedDate);
        } else {
            log.debug("not 1st delivery, 1stdelivery field is filled");
            mutableIssue.setCustomFieldValue(authVerificationReworkDateField, authVerificationReworkDate);
        }

        try {
            log.debug("LastDelivery date : " + authLastDeliveryDate);
            mutableIssue.setCustomFieldValue(authLastDeliveryDateField, authLastDeliveryDate);


            FieldConfig fieldConfigVerified = authVerifiedField.getRelevantConfig(mutableIssue);
            Option valueVerified = ComponentAccessor.getOptionsManager().getOptions(fieldConfigVerified)?.find{
                it.toString().equals(ConstantKeys.AUTH_VERIFIED);
            };
            mutableIssue.setCustomFieldValue(authVerifiedField, valueVerified);

            FieldConfig fieldConfigValidated = authValidatedField.getRelevantConfig(mutableIssue);
            Option valueValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigValidated)?.find{
                it.toString().equals(ConstantKeys.AUTH_VALIDATED);
            };
            mutableIssue.setCustomFieldValue(authValidatedField, valueValidated);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }
}
