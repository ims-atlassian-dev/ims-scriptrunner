package com.airbus.ims.business.operation


import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager
import com.atlassian.jira.issue.customfields.manager.OptionsManager
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.config.FieldConfig
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger
import org.joda.time.LocalDate

import java.sql.Timestamp

class PWO22BusinessOperation {

    /**
     * Process PWO22 Business Rules on Form Delivery Dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO22FormDeliveryDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField formRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_REQUESTEDDATE);
        CustomField formVerificationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_REQUESTEDDATE);
        CustomField formValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_REQUESTEDDATE);
        CustomField form1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_1STCOMMITTEDDATE);
        CustomField formVerification1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_1STCOMMITTEDDATE);
        CustomField formValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE);
        CustomField formLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_LASTCOMMITTEDDATE);
        CustomField formVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField formValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE);
        CustomField formVerifiedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFIED);
        CustomField formValidatedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATED);
        CustomField form1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_1STDELIVERYDATE);
        CustomField formLastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_LASTDELIVERYDATE);
        CustomField formVerificationReworkDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_REWORKDATE);

        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date formRequestedDate;
        Date formVerificationRequestedDate;
        Date formValidationRequestedDate;
        Date form1stCommittedDate;
        Date formVerification1stCommittedDate;
        Date formValidation1stCommittedDate;
        Date formLastCommittedDate;
        Date formVerificationLastCommittedDate;
        Date formValidationLastCommittedDate;
        Date form1stDeliveryDate;
        Date formLastDeliveryDate;
        Date formVerificationReworkDate;

        try{
            //Business
            //Calculate date on PWO-2.1
            if (mutableIssue.getCustomFieldValue(form1stDeliveryDateField) != null) {
                form1stDeliveryDate = now;
                formVerificationRequestedDate =  Toolbox.incrementTimestamp(now,7);
                formValidationRequestedDate =  Toolbox.incrementTimestamp(now,14);
                formVerification1stCommittedDate = formVerificationRequestedDate;
                formVerificationLastCommittedDate =  Toolbox.incrementTimestamp(now,7);
                formValidation1stCommittedDate = formValidationRequestedDate;
                formValidationLastCommittedDate =  Toolbox.incrementTimestamp(now,14);

                mutableIssue.setCustomFieldValue(form1stDeliveryDateField, form1stDeliveryDate);
                mutableIssue.setCustomFieldValue(formVerificationRequestedDateField, formVerificationRequestedDate);
                mutableIssue.setCustomFieldValue(formValidationRequestedDateField, formValidationRequestedDate);
                mutableIssue.setCustomFieldValue(formVerification1stCommittedDateField, formVerification1stCommittedDate);
                mutableIssue.setCustomFieldValue(formVerificationLastCommittedDateField, formVerificationLastCommittedDate);
                mutableIssue.setCustomFieldValue(formValidation1stCommittedDateField, formValidation1stCommittedDate);
                mutableIssue.setCustomFieldValue(formValidationLastCommittedDateField, formValidationLastCommittedDate);
            } else {
                formVerificationReworkDate =  Toolbox.incrementTimestamp(now,7);
                mutableIssue.setCustomFieldValue(formVerificationReworkDateField, formVerificationReworkDate);
            }


            formLastDeliveryDate = now;
            //formVerificationLastCommittedDate=(Timestamp) (now+7)
            //formValidationLastCommittedDate=(Timestamp) (now+14)

            mutableIssue.setCustomFieldValue(formLastDeliveryDateField, formLastDeliveryDate);
            //issue.setCustomFieldValue(formVerificationLastCommittedDateField,formVerificationLastCommittedDate)
            //issue.setCustomFieldValue(formValidationLastCommittedDateField,formValidationLastCommittedDate)

            FieldConfig fieldConfigVerified = formVerifiedField.getRelevantConfig(mutableIssue);
            Option valueVerified = ComponentAccessor.getOptionsManager().getOptions(fieldConfigVerified)?.find{
                it.toString().equals(ConstantKeys.FORM_VERIFIED);
            };
            mutableIssue.setCustomFieldValue(formVerifiedField, valueVerified);

            FieldConfig fieldConfigValidated = formValidatedField.getRelevantConfig(mutableIssue);
            Option valueValidated = ComponentAccessor.getOptionsManager().getOptions(fieldConfigValidated)?.find{it ->
                it.toString().equals(ConstantKeys.FORM_VALIDATED);
            };
            mutableIssue.setCustomFieldValue(formValidatedField, valueValidated);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on Form Committed Dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO22CommittedDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        //def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
        //    loadScriptByName(path);
        //}


        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField formLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_LAST_COMMITTED_DATE);
        CustomField formVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_LAST_COMMITTED_DATE);
        CustomField formValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_LAST_COMMITTED_DATE);

        //Variables
        Date formLastCommittedDate;
        Date formVerificationLastCommittedDate;
        Date formValidationLastCommittedDate;

        Timestamp now = new Timestamp(new Date().getTime());


        //Business
        try {
            formLastCommittedDate = Toolbox.incrementTimestamp(now,7);
            mutableIssue.setCustomFieldValue(formLastCommittedDateField, formLastCommittedDate);

            formVerificationLastCommittedDate = Toolbox.incrementTimestamp(now,14);
            mutableIssue.setCustomFieldValue(formVerificationLastCommittedDateField, formVerificationLastCommittedDate);

            formValidationLastCommittedDate = Toolbox.incrementTimestamp(now,21);
            mutableIssue.setCustomFieldValue(formValidationLastCommittedDateField, formValidationLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on Form Rework Date Field
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO22FormReworkDate(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        CustomField formReworkDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_REWORKDATE);

        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date formReworkDate;
        Date futureDate =  Toolbox.incrementTimestamp(now,7);

        try{
            if (!mutableIssue.getCustomFieldValue(formReworkDateField).equals(futureDate)) {
                formReworkDate = futureDate;
                mutableIssue.setCustomFieldValue(formReworkDateField, formReworkDate);

                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                issueIndexingService.reIndex(mutableIssue);
            }
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on NbReworkField
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO22NbReworkField(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        log.debug("Start");

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        ChangeHistoryManager changeHistoryManager = ComponentAccessor.getChangeHistoryManager();
        ChangeHistoryItem changeHistoryItem = ComponentAccessor.getComponentOfType(ChangeHistoryItem.class);
        List<ChangeHistoryItem> changeItems = ComponentAccessor.getChangeHistoryManager().getAllChangeItems(mutableIssue);
        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        log.debug("Issue : " + mutableIssue.getKey());

        //CustomField
        CustomField manuallyEditedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_MANUALLYEDITED);
        CustomField nbReworkField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_NBREWORK);

        //Valeur
        String formManuallyEdited = (String) mutableIssue.getCustomFieldValue(manuallyEditedField);
        double nbRework = 0;
        if (mutableIssue.getCustomFieldValue(nbReworkField) != null) {
            nbRework = Double.parseDouble(mutableIssue.getCustomFieldValue(nbReworkField).toString());
        }

        try {
            //Business
            double valeurToReturn;
            double compteur = 0;
            if (formManuallyEdited.equals("Yes")) {
                valeurToReturn = nbRework + 1;
            } else {
                log.debug("Pas d'Ã©dit manuel");
                for (ChangeHistoryItem item : changeItems) {
                    if (item.getField().equals("status") && item.getFroms().values().contains("In Progress") && item.getTos().values().contains("Delivered")) {
                        compteur = compteur + 1;
                    }

                }
                valeurToReturn = compteur - 1;
            }

            mutableIssue.setCustomFieldValue(nbReworkField, valeurToReturn);
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO22 Business Rules on Verification Dates
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO22VerificationDates(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        //String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        /*def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);
        OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField formRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_REQUESTEDDATE);
        CustomField formVerificationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_REQUESTEDDATE);
        CustomField formValidationRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_REQUESTEDDATE);
        CustomField form1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_1STCOMMITTEDDATE);
        CustomField formVerification1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_1STCOMMITTEDDATE);
        CustomField formValidation1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_1STCOMMITTEDDATE);
        CustomField formLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_LASTCOMMITTEDDATE);
        CustomField formVerificationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VERIFICATION_LASTCOMMITTEDDATE);
        CustomField formValidationLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_FORM_VALIDATION_LASTCOMMITTEDDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date formRequestedDate;
        Date formVerificationRequestedDate;
        Date formValidationRequestedDate;
        Date form1stCommittedDate;
        Date formVerification1stCommittedDate;
        Date formValidation1stCommittedDate;
        Date formLastCommittedDate;
        Date formVerificationLastCommittedDate;
        Date formValidationLastCommittedDate;


        //Business
        //Calculate date on PWO-2.1
        formRequestedDate = Toolbox.incrementTimestamp(now,7);
        formVerificationRequestedDate =  Toolbox.incrementTimestamp(now,14);
        formValidationRequestedDate =  Toolbox.incrementTimestamp(now,21);

        form1stCommittedDate = formRequestedDate;
        formVerification1stCommittedDate = formVerificationRequestedDate;
        formValidation1stCommittedDate = formValidationRequestedDate;

        formLastCommittedDate = form1stCommittedDate;
        formVerificationLastCommittedDate = formVerification1stCommittedDate;
        formValidationLastCommittedDate = formValidation1stCommittedDate;

        mutableIssue.setCustomFieldValue(formRequestedDateField, formRequestedDate);
        mutableIssue.setCustomFieldValue(formVerificationRequestedDateField, formVerificationRequestedDate);
        mutableIssue.setCustomFieldValue(formValidationRequestedDateField, formValidationRequestedDate);

        mutableIssue.setCustomFieldValue(form1stCommittedDateField, form1stCommittedDate);
        mutableIssue.setCustomFieldValue(formVerification1stCommittedDateField, formVerification1stCommittedDate);
        mutableIssue.setCustomFieldValue(formValidation1stCommittedDateField, formValidation1stCommittedDate);

        mutableIssue.setCustomFieldValue(formLastCommittedDateField, formLastCommittedDate);
        mutableIssue.setCustomFieldValue(formVerificationLastCommittedDateField, formVerificationLastCommittedDate);
        mutableIssue.setCustomFieldValue(formValidationLastCommittedDateField, formValidationLastCommittedDate);

        //issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
        //issueIndexingService.reIndex(issue);
    }

    /**
     * Check if PWO22 is ready (TO BE COMPLETED)
     * @param issue
     * @param user
     * @return
     */
    static boolean isPWO22Ready(Issue issue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField awoTransWUfield = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TRANSWU);
        CustomField woTransNbWUfield = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TRANSNBWU);
        CustomField awoTransCommentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TRANSCOMMENT);
        CustomField awoFormWU = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMWU);
        CustomField awoFormNbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMNBWU);
        CustomField awoFormCommentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMCOMMENT);
        CustomField awoFormVerifWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFWU);
        CustomField awoFormVerifNbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFNBWU);
        CustomField awoFormVerifCommentField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_FORMVERIFCOMMENT);
        CustomField awoTransObjLangField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_AWO_TRANSOBJLAN);

        String result = "OK";

        Collection<Issue> linkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO : linkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                if (linkedIssueOfCurrentPWO.getCustomFieldValue(awoTransObjLangField) != null) {
                    if (!result.equals("KO") && linkedIssueOfCurrentPWO.getCustomFieldValue(awoTransWUfield) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(woTransNbWUfield) != null) {
                        result = "OK";
                    } else {
                        result = "KO";
                    }

                } else {
                    if (!result.equals("KO") && linkedIssueOfCurrentPWO.getCustomFieldValue(awoFormWU) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(awoFormNbWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(awoFormVerifWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(awoFormVerifNbWUField) != null) {
                        result = "OK";
                    } else {
                        result = "KO";
                    }

                }
            }
        }

        boolean passesCondition = false;
        if (result.equals("OK")) {
            passesCondition = true;
        }
        return passesCondition;
    }
}
