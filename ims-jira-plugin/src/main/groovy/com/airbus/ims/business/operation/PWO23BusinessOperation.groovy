package com.airbus.ims.business.operation


import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.config.FieldConfig
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger
import org.joda.time.LocalDate

import java.sql.Timestamp

class PWO23BusinessOperation {

    /**
     * TO BE COMPLETED
     * @param issue
     * @param user
     * @return
     */
    static boolean isPWO23Ready(Issue issue, ApplicationUser user, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        CustomField intWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_WU);
        CustomField intNbWUField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_NBWU);

        String result = "OK";

        Collection<Issue> LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues();
        for (Issue linkedIssueOfCurrentPWO : LinkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO.getProjectId() == ConstantKeys.ID_PROJECT_AWO) {
                if (!result.equals("KO") && linkedIssueOfCurrentPWO.getCustomFieldValue(intWUField) != null && linkedIssueOfCurrentPWO.getCustomFieldValue(intNbWUField) != null) {
                    result = "OK";
                } else {
                    result = "KO";
                }

            }

        }

        boolean passesCondition = false;
        if (result.equals("OK")) {
            passesCondition = true;
        }
        return passesCondition;
    }

    /**
     * Process PWO23 Business Rules on INT Validation???
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO23IntValidation(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField intRequestedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_REQUESTEDDATE);
        CustomField int1stCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_1STCOMMITTEDDATE);
        CustomField intLastCommittedDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_LASTCOMMITTEDDATE);


        //Variables
        //def now = new Timestamp(new Date().getTime())
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date intRequestedDate;
        Date int1stCommittedDate;
        Date intLastCommittedDate;

        try {
            //Business
            //Calculate date on PWO-2.1
            intRequestedDate = Toolbox.incrementTimestamp(now, 7);
            int1stCommittedDate = intRequestedDate;
            intLastCommittedDate = int1stCommittedDate;

            mutableIssue.setCustomFieldValue(intRequestedDateField, intRequestedDate);
            mutableIssue.setCustomFieldValue(int1stCommittedDateField, int1stCommittedDate);
            mutableIssue.setCustomFieldValue(intLastCommittedDateField, intLastCommittedDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process PWO23 Business Rules on Technical Closure
     * @param mutableIssue
     * @param user
     * @param log
     */
    static void processPWO23TechnicalClosure(MutableIssue mutableIssue, ApplicationUser user, Logger log) {
        /*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

        def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
            loadScriptByName(path);
        } */

        //Manager
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class);

        List<String> PWOIT = new ArrayList<String>();
        PWOIT.add(ConstantKeys.ID_IT_PWO0);
        PWOIT.add(ConstantKeys.ID_IT_PWO1);
        PWOIT.add(ConstantKeys.ID_IT_PWO2);
        PWOIT.add(ConstantKeys.ID_IT_PWO21);
        PWOIT.add(ConstantKeys.ID_IT_PWO22);
        PWOIT.add(ConstantKeys.ID_IT_PWO23);

        //CustomField
        CustomField int1stDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_1STDELIVERYDATE);
        CustomField intLastDeliveryDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_INT_LASTDELIVERYDATE);
        CustomField technicallyClosedField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TECHNICALLYCLOSED);
        CustomField technicalClosureDateField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TECHNICALCLOSUREDATE);

        //Variables
        Date todayDate = new LocalDate(new Date()).toDate();
        Timestamp now = new Timestamp(todayDate.getTime());
        Date int1stDeliveryDate;
        Date intLastDeliveryDate;
        Date technicalClosureDate;
        String technicallyClosed = "Yes";

        try {
            //Business
            //Calculate date on PWO-2.1
            if (mutableIssue.getCustomFieldValue(int1stDeliveryDateField) != null) {
                int1stDeliveryDate = now;

                mutableIssue.setCustomFieldValue(int1stDeliveryDateField, int1stDeliveryDate);
            }


            intLastDeliveryDate = now;
            technicalClosureDate = now;

            mutableIssue.setCustomFieldValue(intLastDeliveryDateField, intLastDeliveryDate);

            FieldConfig fieldConfigTechnicallyClosed = technicallyClosedField.getRelevantConfig(mutableIssue);
            Option valueTechnicallyClosed = ComponentAccessor.getOptionsManager().getOptions(fieldConfigTechnicallyClosed).find{
                it.toString().equals(technicallyClosed);
            };
            mutableIssue.setCustomFieldValue(technicallyClosedField, valueTechnicallyClosed);
            mutableIssue.setCustomFieldValue(technicalClosureDateField, technicalClosureDate);

            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(mutableIssue);
        } catch (IndexException e) {
            e.printStackTrace();
        }
    }
}
