package com.airbus.ims.business.operation

import com.airbus.ims.util.ConstantKeys
import com.airbus.ims.util.Toolbox
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.jql.parser.JqlParseException
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.query.Query
import org.apache.log4j.Logger

class TriggerBusinessOperation {

    /**
     * Check if text contain at least on Jira special characters at jql text search
     * @param text
     * @return
     */
    static boolean hasSpecialCharRegex(String text) {
        return text =~ ConstantKeys.JQL_SEARCH_SPECIAL_CHAR_REGEX;
    }
    /**
     * Initialize the trigger
     * @param issue
     * @return
     */
    static boolean initializeTrigger(Issue issue, Logger log) {
        //Manager
        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        boolean returnCondition = true;
        if (issueLinkManager.getInwardLinks(issue.getId()) != null) {
            issueLinkManager.getInwardLinks(issue.getId()).each { issueLink ->
                Issue sourceIssue = (Issue) issueLink.getSourceObject();
                String issueType = sourceIssue.getIssueTypeId();
                if (issueType.equals(ConstantKeys.ID_IT_CLUSTER)) {
                    returnCondition = false;
                }
            };
        }
        return returnCondition;
    }

    /**
     * Validate Trigger
     * @param mutableIssue
     * @param user
     * @param log
     * @return
     */
    static boolean validateTrigger(Issue issue, Issue originalIssue, ApplicationUser user, Logger log) {
        //Manager
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
        JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class);
        SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class);

        //CustomField
        CustomField programField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_PROGRAM);
        CustomField typeOfHCField = customFieldManager.getCustomFieldObject(ConstantKeys.ID_CF_TYPEOFHC);

        log.debug("validateTrigger:Check issue id: " + issue);

        //Variables
        String currentSummary = issue.getSummary().replaceAll("\\\\","\\\\\\\\");
        String programValue = Toolbox.getValueFromNFeedFieldSingle(programField, issue, log);
        String currentTypeOfHC = String.valueOf(issue.getCustomFieldValue(typeOfHCField));

        String JQL_QUERY = "issuetype = \"Trigger\" and issueFunction in issueFieldExactMatch(\"\",\"Summary\", \"%s\") and Program = \"%s\" ";

        if(currentTypeOfHC == "ALL"){
            JQL_QUERY += " and \"Type of H/C\" IS NOT EMPTY ";
        } else {
            JQL_QUERY += " and (\"Type of H/C\" = \"ALL\" or \"Type of H/C\" = \"%s\") ";
        }

        //Resolve Edit or Creation mode
        if(originalIssue){ //edition
            JQL_QUERY += " and issuekey != " + originalIssue.getKey();
        }
        List<Issue> issuesFound = new ArrayList<Issue>();

        String query = String.format(JQL_QUERY, currentSummary, programValue, currentTypeOfHC);
        log.debug("Query : " + query);

        try {
            Query searchJQLQuery = jqlQueryParser.parseQuery(query);
            SearchResults results = searchProvider.search(searchJQLQuery, user, PagerFilter.getUnlimitedFilter());
            issuesFound = results.getIssues();
        } catch (JqlParseException e) {
            e.printStackTrace();
        } catch (SearchException e) {
            e.printStackTrace();
        }

        log.debug("issues found : " + issuesFound);

        return issuesFound.isEmpty();
    }

    /**
     * Business rule validation failed if special characters is used
     * @deprecated This business rule is deprecated for IMS release R1v9
     * @param issue
     * @param log
     * @return
     */
    static boolean validateSummaryTextValue(Issue issue, Logger log) {
        String currentSummary = issue.getSummary();
        log.debug("validateSummaryTextValue: Issue summary: " + currentSummary)
        return !hasSpecialCharRegex(currentSummary);
    }
}
