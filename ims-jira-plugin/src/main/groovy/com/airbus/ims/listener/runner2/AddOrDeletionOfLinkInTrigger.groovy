package com.airbus.ims.listener.runner2;

import com.airbus.ims.business.manager.GlobalListenerManager
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.atlassian.jira.event.issue.link.IssueLinkDeletedEvent

//event variable is an inferred argument by ScriptRunner contextual listener
//Runner script
if(event instanceof IssueLinkCreatedEvent) {
    IssueLinkCreatedEvent createdEvent = (IssueLinkCreatedEvent)event;
    GlobalListenerManager.onAdditionOrDeletionOfLinkBetweenTriggerAndCluster(createdEvent);
} else {
    IssueLinkDeletedEvent deletedEvent = (IssueLinkDeletedEvent)event;
    GlobalListenerManager.onAdditionOrDeletionOfLinkBetweenTriggerAndCluster(deletedEvent);
}