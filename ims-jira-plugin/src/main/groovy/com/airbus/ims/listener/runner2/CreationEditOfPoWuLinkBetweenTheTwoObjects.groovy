package com.airbus.ims.listener.runner2

import com.airbus.ims.business.manager.GlobalListenerManager

//event variable is an inferred argument by ScriptRunner contextual listener
//Runner script
GlobalListenerManager.onCreationOrEditionOfPO(event)