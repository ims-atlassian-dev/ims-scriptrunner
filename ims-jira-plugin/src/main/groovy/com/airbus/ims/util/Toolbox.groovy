package com.airbus.ims.util;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.history.ChangeItemBean;
import groovy.util.slurpersupport.GPathResult
import org.apache.log4j.Level;
import org.ofbiz.core.entity.GenericValue;
import java.sql.Timestamp;
import java.util.*;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import org.joda.time.Days;

public class Toolbox {
    public static Logger log;

    public static Integer daysBetween(Date endDate, Date startDate) {
        return Days.daysBetween(new DateTime(endDate), new DateTime(startDate)).getDays();
    }

    public static Timestamp incrementTimestamp(Date date, int increment) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_WEEK, increment);
        Timestamp newDate = new Timestamp(calendar.getTime().getTime());
        return newDate;
    }

    public static <T> T findIterator(T searchedItem, Iterable<T> iterable) {
        Iterator<T> iterator = iterable.iterator();
        T foundItem = null;
        T item;
        while (iterator.hasNext() && foundItem == null) {
            item = iterator.next();
            if (item.equals(searchedItem)) {
                foundItem = item;
            }
        }
        return foundItem;
    }

    public static String getValueFromNFeedFieldSingle(CustomField nFeedfield, Issue issue, Logger log) {
        String result = null;

        String fieldValueString = issue.getCustomFieldValue(nFeedfield).toString();
        log.debug("End Function getIssueFromNFeedField on nFeedfield : " + fieldValueString);
        if(!fieldValueString.equals("null")) {
            GPathResult fieldDisplayResult = new XmlSlurper().parseText(fieldValueString);
            if (fieldDisplayResult != null) {
                if (fieldDisplayResult instanceof GPathResult) {
                    fieldDisplayResult["value"].each { GPathResult item ->
                        result = item.text();
                    }
                }

                if (result != null) {
                    log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": value : " + result);
                } else {
                    log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result");
                }
            }
        }

        return result;
    }


    public static Issue getIssueFromNFeedFieldSingle(CustomField nFeedfield, Issue issue, Logger log) {
        Issue issueFound = null;

        IssueManager issueManager = ComponentAccessor.getIssueManager();
        String fieldValueString = issue.getCustomFieldValue(nFeedfield).toString();
        log.debug("End Function getIssueFromNFeedField on nFeedfield : " + fieldValueString);
        if(!fieldValueString.equals("null")) {
            GPathResult fieldDisplayResult = new XmlSlurper().parseText(fieldValueString);
            if (fieldDisplayResult != null) {
                if (fieldDisplayResult instanceof GPathResult) {
                    fieldDisplayResult["value"].each { GPathResult item ->
                        issueFound = issueManager.getIssueObject(Long.parseLong(item.text()));
                    }
                }

                if (issueFound != null) {
                    log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + issueFound);
                } else {
                    log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result");
                }
            }
        }

        return issueFound;
    }


    public static List<Issue> getIssueFromNFeedFieldMultiple(CustomField nFeedfield, Issue issue, Logger log) {
        IssueManager issueManager = ComponentAccessor.getIssueManager();

        Issue issueFound;
        List<Issue> multipleIssue = new ArrayList<Issue>();
        String fieldValueString = issue.getCustomFieldValue(nFeedfield).toString();
        log.debug("End Function getIssueFromNFeedField on nFeedfield : " + fieldValueString);
        if (!fieldValueString.equals("null")) {
            GPathResult fieldDisplayResult = new XmlSlurper().parseText(fieldValueString);
            if (fieldDisplayResult != null) {
                if (fieldDisplayResult instanceof GPathResult) {
                    fieldDisplayResult["value"].each { GPathResult item ->
                        issueFound = issueManager.getIssueObject(Long.parseLong(item.text()));
                        multipleIssue.add(issueFound);
                    }
                }
            }
        }
        if (!multipleIssue.isEmpty()) {
            log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + multipleIssue);
        } else {
            log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result");
        }

        return multipleIssue;
    }

    public static boolean workloadIsUpdated(GenericValue changeLog, List<String> itemsChanged) {
        log = Logger.getLogger("com.airbus.ims.util");
        log.setLevel(Level.DEBUG);
        boolean isUpdated = false;
        log.debug("Toolbox:changeLog object : " + changeLog);
        if (changeLog != null) {
            ChangeHistory change = ComponentAccessor.getChangeHistoryManager().getChangeHistoryById(changeLog.getLong("id"));
            List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
            log.debug("Toolbox: get itemsChanged items: " + itemsChanged);
            for (ChangeItemBean changeIteamBean : changeItemBeans) {
                log.debug("Toolbox: get changeIteamBean field: " + changeIteamBean.getField());

                if (itemsChanged.contains(changeIteamBean.getField())) {
                    isUpdated = true;
                }
            }
        }

        return isUpdated;
    }

    public static String formatSecondesValueInHoursMinutes(Long secondes) {
        String result = "";
        if (secondes != null && secondes > 0) {
            Integer hoursExtracted = (int) (secondes / 3600);
            Integer totalMinutes = (int) (secondes / 60);
            Integer minutesExtracted = (int) totalMinutes % 60;
            result = hoursExtracted.toString() + "h " + minutesExtracted.toString() + "m";
        }
        return result;
    }

    public static Long formatHoursMinutesFormatInSecondes(String hoursMinutes) {
        Long result = 0L;
        if (!hoursMinutes.equals("null")) {
            String[] hoursMinutesTable;
            Long hourConvertedInMinutes = Long.valueOf(0);
            String hourNumberString;
            Long minutesNumber = Long.valueOf(0);

            hoursMinutesTable = hoursMinutes.split("h");
            hourConvertedInMinutes = Long.parseLong(hoursMinutesTable[0].toString()) * 60;
            minutesNumber = Long.parseLong(hoursMinutesTable[1].toString().substring(0, hoursMinutesTable[1].toString().length() - 1));
            result = (hourConvertedInMinutes + minutesNumber) * 60;
            return result;
        }
        return result;
    }

    public static String sumTimeSpentForDomain(MutableIssue issue, CustomField timeSpentForSelectedDomainCF, Long lastTimeSpent) {
        //get current time logged for domain
        String timeLogged = issue.getCustomFieldValue(timeSpentForSelectedDomainCF);

        Long totalTime = 0L;
        if (timeLogged) {
            //Convert time in second
            Long currentTime = formatHoursMinutesFormatInSecondes(timeLogged);

            //Sum
            totalTime = currentTime + lastTimeSpent;
        } else {
            totalTime = lastTimeSpent;
        }
        return formatSecondesValueInHoursMinutes(totalTime);
    }

    @Deprecated
    /**
     * Use getIssueFromNFeedFieldSingle
     */
    public static Issue getIssueFromNFeedFieldNewFieldsSingle(CustomField nFeedfield, Issue issue, Logger log) {
        Issue issueFound = null;
        String fieldValue = issue.getCustomFieldValue(nFeedfield).toString();

        IssueManager issueManager = ComponentAccessor.getIssueManager();

        if (!fieldValue.equals("null")) {
            String formattedValues = fieldValue.replaceAll("<content>", "").replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replace("\n", "").replace("  ", " ");
            issueFound = issueManager.getIssueObject(Long.parseLong(formattedValues));

            if (issueFound != null) {
                log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + issueFound);
            } else {
                log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result");

            }
        }
        return issueFound;
    }

    @Deprecated
    /**
     * Use getIssueFromNFeedFieldMultiple
     */
    public static List<Issue> getIssueFromNFeedFieldNewFieldsMultiple(CustomField nFeedfield, Issue issue, Logger log) {
        IssueManager issueManager = ComponentAccessor.getIssueManager();

        Issue issueFound;
        List<Issue> multipleIssue = new ArrayList<Issue>();
        String fieldValue = issue.getCustomFieldValue(nFeedfield).toString();

        if (!fieldValue.equals("null")) {
            String[] formattedValues = fieldValue.replaceAll("<content>", "").replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replace("\n", "").replace("  ", " ");
            for (String formatedValue : formattedValues) {
                issueFound = issueManager.getIssueObject(Long.parseLong(formatedValue));
                multipleIssue.add(issueFound);
            }
        }

        if (multipleIssue != null) {
            log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + multipleIssue);
        } else {
            log.debug("End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result");
        }

        return multipleIssue;
    }

}
