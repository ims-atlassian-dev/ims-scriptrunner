package com.airbus.ims.workflow.runner2
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.link.IssueLinkManager
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import org.joda.time.LocalDate

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager);
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();

MutableIssue issue = issue;

//Constantes
//Field of PWO-0
long ID_CF_IA_Validation_RequestedDate=10154;
long ID_CF_IA_Validation_1stCommittedDate=10155;
long ID_CF_IA_Validation_LastCommittedDate=10156;
long ID_CF_IA_1stDeliveryDate =10147;
long ID_CF_IA_LastDeliveryDate = 10148;
long ID_CF_IA_Validated=10151;

//CustomField
CustomField IA_Validation_RequestdDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_RequestedDate)
CustomField IA_Validation_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stCommittedDate)
CustomField IA_Validation_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastCommittedDate)
CustomField IA_1stDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_1stDeliveryDate)
CustomField IA_LastDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_LastDeliveryDate)
CustomField IA_ValidatedField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validated)

//Variables
//def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate=new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
Date IA_Validation_RequestdDate
Date IA_Validation_1stCommittedDate
Date IA_Validation_LastCommittedDate
Date IA_1stDeliveryDate
Date IA_LastDeliveryDate
String ValidationPending="Validation pending"


//Business
IA_Validation_RequestdDate=(Timestamp) (TodaysDate+7)
IA_Validation_1stCommittedDate=IA_Validation_RequestdDate
IA_Validation_LastCommittedDate=IA_Validation_1stCommittedDate
IA_1stDeliveryDate=TodaysDate
IA_LastDeliveryDate=TodaysDate
log.debug "IA LAst delivery date to set : "+IA_LastDeliveryDate

issue.setCustomFieldValue(IA_Validation_RequestdDateField,IA_Validation_RequestdDate)
issue.setCustomFieldValue(IA_Validation_1stCommittedDateField,IA_Validation_1stCommittedDate)
issue.setCustomFieldValue(IA_Validation_LastCommittedDateField,IA_Validation_LastCommittedDate)
log.debug "setting last delivery date"
issue.setCustomFieldValue(IA_LastDeliveryDateField,IA_LastDeliveryDate)
log.debug "last delivery date have been set"
if (!issue.getCustomFieldValue(IA_1stDeliveryDateField)){
    issue.setCustomFieldValue(IA_1stDeliveryDateField,IA_1stDeliveryDate)
}


if (issue.getCustomFieldValue(IA_ValidatedField).toString()=="Rejected"){
    def fieldConfigValidated = IA_ValidatedField.getRelevantConfig(issue)
    def valueValidated = ComponentAccessor.optionsManager.getOptions(fieldConfigValidated)?.find { it.toString() == ValidationPending }
    
    issue.setCustomFieldValue(IA_ValidatedField,valueValidated)
}

issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);