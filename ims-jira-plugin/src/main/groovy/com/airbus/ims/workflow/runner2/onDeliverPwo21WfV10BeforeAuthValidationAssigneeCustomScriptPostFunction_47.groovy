package com.airbus.ims.workflow.runner2
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean
import org.joda.time.LocalDate

String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} 

/*boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List itemsChanged){
    boolean isUpdated = false;
        if (changeLog != null) {
            ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
            List changeItemBeans = change.getChangeItemBeans();
            for (changeIteamBean in changeItemBeans){
                if (itemsChanged.contains(changeIteamBean.getField())){
                    isUpdated = true;
                }
            }
        }

    return isUpdated;
}*/

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();


//Constante
//Issue type
String ID_IT_PWO0 = "10004";
String ID_IT_PWO1 = "10005";
String ID_IT_PWO2 = "10006";
String ID_IT_PWO21 = "10007";
String ID_IT_PWO22 = "10008";
String ID_IT_PWO23 = "10009";
String ID_IT_Cluster="10001";
String ID_IT_Trigger="10000";

List PWOIT = new ArrayList();
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_Auth_Verified=10183;
long ID_CF_Auth_Validated=10189;
long ID_CF_Auth_1stDeliveryDate=10178;
long ID_CF_Auth_LastDeliveryDate=10179;
long ID_CF_Auth_Verification_RequestedDate=10192;
long ID_CF_Auth_Verification_FirstCommittedDate=10184;
long ID_CF_Auth_Verification_LastCommittedDate=10185;
long ID_CF_Auth_Validation_RequestedDate=10190;
long ID_CF_Auth_Validation_FirstCommittedDate=10191;
long ID_CF_Auth_Validation_LastCommittedDate=10193;
long ID_CF_Auth_Verification_ReworkDate=11124

//CustomField
CustomField Auth_VerifiedField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Verified)
CustomField Auth_ValidatedField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Validated)
CustomField Auth_1stDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_1stDeliveryDate)
CustomField Auth_LastDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_LastDeliveryDate)
CustomField Auth_Verification_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_RequestedDate)
CustomField Auth_Verification_FirstCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_FirstCommittedDate)
CustomField Auth_Verification_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_LastCommittedDate)
CustomField Auth_Validation_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_RequestedDate)
CustomField Auth_Validation_FirstCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_FirstCommittedDate)
CustomField Auth_Validation_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_LastCommittedDate)
CustomField Auth_Verification_ReworkDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_ReworkDate)


//Variables
//def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate=new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
String Auth_Verified="Verification pending"
String Auth_Validated="Validation pending"
Date Auth_1stDeliveryDate
Date Auth_LastDeliveryDate
Date Auth_Verification_RequestedDate
Date Auth_Verification_1stCommittedDate
Date Auth_Verification_LastCommittedDate
Date Auth_Validation_RequestedDate
Date Auth_Validation_1stCommittedDate
Date Auth_Validation_LastCommittedDate
Date Auth_Verification_ReworkDate


//Business
//Calculate date on PWO-2.1
Auth_1stDeliveryDate=TodaysDate
Auth_LastDeliveryDate=TodaysDate

Auth_Verification_RequestedDate= (Timestamp) (TodaysDate+14)
Auth_Validation_RequestedDate=(Timestamp) (TodaysDate+28)

Auth_Verification_FirstCommittedDate=Auth_Verification_RequestedDate
Auth_Validation_FirstCommittedDate=Auth_Validation_RequestedDate

Auth_Verification_LastCommittedDate=Auth_Verification_FirstCommittedDate
Auth_Validation_LastCommittedDate=Auth_Validation_FirstCommittedDate

Auth_Verification_ReworkDate= (Timestamp) (TodaysDate+7)

if (!issue.getCustomFieldValue(Auth_1stDeliveryDateField)){
    log.debug "1st delivery, 1stdelivery field is empty"
    issue.setCustomFieldValue(Auth_1stDeliveryDateField,Auth_1stDeliveryDate)
    issue.setCustomFieldValue(Auth_Verification_RequestedDateField,Auth_Verification_RequestedDate)
    issue.setCustomFieldValue(Auth_Verification_FirstCommittedDateField,Auth_Verification_FirstCommittedDate)
    issue.setCustomFieldValue(Auth_Validation_RequestedDateField,Auth_Validation_RequestedDate)
    issue.setCustomFieldValue(Auth_Validation_FirstCommittedDateField,Auth_Validation_FirstCommittedDate)
    issue.setCustomFieldValue(Auth_Verification_LastCommittedDateField,Auth_Verification_LastCommittedDate)
	issue.setCustomFieldValue(Auth_Validation_LastCommittedDateField,Auth_Validation_LastCommittedDate)
}
else{
    log.debug "not 1st delivery, 1stdelivery field is filled"
    issue.setCustomFieldValue(Auth_Verification_ReworkDateField,Auth_Verification_ReworkDate)
}

log.debug "LastDelivery date : "+Auth_LastDeliveryDate
issue.setCustomFieldValue(Auth_LastDeliveryDateField,Auth_LastDeliveryDate)


def fieldConfigVerified = Auth_VerifiedField.getRelevantConfig(issue)
def valueVerified = ComponentAccessor.optionsManager.getOptions(fieldConfigVerified)?.find { it.toString() == Auth_Verified }
issue.setCustomFieldValue(Auth_VerifiedField,valueVerified)

def fieldConfigValidated = Auth_ValidatedField.getRelevantConfig(issue)
def valueValidated = ComponentAccessor.optionsManager.getOptions(fieldConfigValidated)?.find { it.toString() == Auth_Validated }
issue.setCustomFieldValue(Auth_ValidatedField,valueValidated)

issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);