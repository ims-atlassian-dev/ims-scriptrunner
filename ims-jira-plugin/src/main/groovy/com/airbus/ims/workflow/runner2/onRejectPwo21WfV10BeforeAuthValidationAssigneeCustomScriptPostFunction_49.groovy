package com.airbus.ims.workflow.runner2
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean

String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} 


//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();

//Constante
//Issue type
String ID_IT_PWO0 = "10004";
String ID_IT_PWO1 = "10005";
String ID_IT_PWO2 = "10006";
String ID_IT_PWO21 = "10007";
String ID_IT_PWO22 = "10008";
String ID_IT_PWO23 = "10009";
String ID_IT_Cluster="10001";
String ID_IT_Trigger="10000";

List<String> PWOIT = new ArrayList<String>();
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_Auth_LastCommittedDate=10177;
long ID_CF_Auth_Verification_LastCommittedDate=10185;
long ID_CF_Auth_Validation_LastCommittedDate=10193;

//CustomField
CustomField Auth_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_LastCommittedDate)
CustomField Auth_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_LastCommittedDate)
CustomField Auth_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_LastCommittedDate)

//Variables
Date Auth_LastCommittedDate
Date Auth_Verification_LastCommittedDate
Date Auth_Validation_LastCommittedDate

def TodaysDate = new Timestamp(new Date().getTime())


//Business

Auth_LastCommittedDate=TodaysDate+14
issue.setCustomFieldValue(Auth_LastCommittedDateField,Auth_LastCommittedDate)
                
Auth_Verification_LastCommittedDate=TodaysDate+28
issue.setCustomFieldValue(Auth_Verification_LastCommittedDateField,Auth_Verification_LastCommittedDate)
                
Auth_Validation_LastCommittedDate=TodaysDate+42
issue.setCustomFieldValue(Auth_Validation_LastCommittedDateField,Auth_Validation_LastCommittedDate)
        
issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);