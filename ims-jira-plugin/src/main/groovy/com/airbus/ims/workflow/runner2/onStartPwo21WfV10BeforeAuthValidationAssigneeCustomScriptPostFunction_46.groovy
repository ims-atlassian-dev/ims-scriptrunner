package com.airbus.ims.workflow.runner2
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean
import org.joda.time.LocalDate

String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} 

/*boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
    boolean isUpdated = false;
        if (changeLog != null) {
            ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
            List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
            for (changeIteamBean in changeItemBeans){
                if (itemsChanged.contains(changeIteamBean.getField())){
                    isUpdated = true;
                }
            }
        }

    return isUpdated;
}*/

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();


//Constante
//Issue type
String ID_IT_PWO0 = "10004";
String ID_IT_PWO1 = "10005";
String ID_IT_PWO2 = "10006";
String ID_IT_PWO21 = "10007";
String ID_IT_PWO22 = "10008";
String ID_IT_PWO23 = "10009";
String ID_IT_Cluster="10001";
String ID_IT_Trigger="10000";

List<String> PWOIT = new ArrayList<String>();
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_RequestedDate=10010;
long ID_CF_Auth_RequestedDate=10175;
long ID_CF_Auth_Verification_RequestedDate=10192;
long ID_CF_Auth_Validation_RequestedDate=10190;
long ID_CF_Auth_FirstCommittedDate=10176;
long ID_CF_Auth_Verification_FirstCommittedDate=10184;
long ID_CF_Auth_Validation_FirstCommittedDate=10191;
long ID_CF_Auth_LastCommittedDate=10177;
long ID_CF_Auth_Verification_LastCommittedDate=10185;
long ID_CF_Auth_Validation_LastCommittedDate=10193;


//CustomField
CustomField Auth_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_RequestedDate)
CustomField Auth_Verification_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_RequestedDate)
CustomField Auth_Validation_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_RequestedDate)
CustomField Auth_FirstCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_FirstCommittedDate)
CustomField Auth_Verification_FirstCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_FirstCommittedDate)
CustomField Auth_Validation_FirstCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_FirstCommittedDate)
CustomField Auth_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_LastCommittedDate)
CustomField Auth_Verification_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_LastCommittedDate)
CustomField Auth_Validation_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_LastCommittedDate)


//Variables
//def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate=new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
Date Auth_RequestedDate
Date Auth_Verification_RequestedDate
Date Auth_Validation_RequestedDate
Date Auth_FirstCommittedDate
Date Auth_Verification_FirstCommittedDate
Date Auth_Validation_FirstCommittedDate
Date Auth_LastCommittedDate
Date Auth_Verification_LastCommittedDate
Date Auth_Validation_LastCommittedDate


//Business
//Calculate date on PWO-2.1
Auth_RequestedDate= (Timestamp) (TodaysDate+14)
Auth_Verification_RequestedDate= (Timestamp) (TodaysDate+28)
Auth_Validation_RequestedDate=(Timestamp) (TodaysDate+42)

Auth_FirstCommittedDate=Auth_RequestedDate
Auth_Verification_FirstCommittedDate=Auth_Verification_RequestedDate
Auth_Validation_FirstCommittedDate=Auth_Validation_RequestedDate

Auth_LastCommittedDate=Auth_FirstCommittedDate
Auth_Verification_LastCommittedDate=Auth_Verification_FirstCommittedDate
Auth_Validation_LastCommittedDate=Auth_Validation_FirstCommittedDate

issue.setCustomFieldValue(Auth_RequestedDateField,Auth_RequestedDate)
issue.setCustomFieldValue(Auth_Verification_RequestedDateField,Auth_Verification_RequestedDate)
issue.setCustomFieldValue(Auth_Validation_RequestedDateField,Auth_Validation_RequestedDate)

issue.setCustomFieldValue(Auth_FirstCommittedDateField,Auth_FirstCommittedDate)
issue.setCustomFieldValue(Auth_Verification_FirstCommittedDateField,Auth_Verification_FirstCommittedDate)
issue.setCustomFieldValue(Auth_Validation_FirstCommittedDateField,Auth_Validation_FirstCommittedDate)

issue.setCustomFieldValue(Auth_LastCommittedDateField,Auth_LastCommittedDate)
issue.setCustomFieldValue(Auth_Verification_LastCommittedDateField,Auth_Verification_LastCommittedDate)
issue.setCustomFieldValue(Auth_Validation_LastCommittedDateField,Auth_Validation_LastCommittedDate)

issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);