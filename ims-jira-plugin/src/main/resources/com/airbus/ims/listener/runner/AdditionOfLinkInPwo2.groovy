package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Addition of link in PWO-2
 * Events : IssueLinkCreatedEvent
 * isDisabled : false
 */

// import

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import java.sql.Timestamp
import org.apache.log4j.Level
import org.apache.log4j.Logger



/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder()

MutableIssue issue = (MutableIssue) event.getIssueLink().getSourceObject()
MutableIssue issueSource = (MutableIssue) event.getIssueLink().getDestinationObject()
log.debug "issue : " + issue.getKey()
log.debug "source issue : " + issueSource.getKey()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"
String ID_IT_Cluster = "10001"
String ID_IT_Trigger = "10000"

List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_FirstSNAppli = 10011
long ID_CF_RequestedDate = 10010
long ID_CF_RestrictedData = 10800

//CustomField
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)
CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_RequestedDate)
CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ID_CF_RestrictedData)

//Variables
Date RequestedDateTrigger
Date RequestedDate
String RestrictedDataResult
long CompteurPWO1 = 0

//Business
if (issueSource.getIssueTypeId() == ID_IT_PWO1 && issue.getIssueTypeId() == ID_IT_PWO2) {
    //Get linked issues
    def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssue in LinkedIssues) {
        //If linked issue is Cluster
        if (linkedIssue.getIssueTypeId() == ID_IT_PWO1) {
            CompteurPWO1 = CompteurPWO1 + 1
            log.debug "PWO-1 : " + linkedIssue.getKey()
            RestrictedDataResult = linkedIssue.getCustomFieldValue(RestrictedDataField).toString()
            def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
            for (LinkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                if (LinkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO0) {
                    log.debug "PWO-0 : " + LinkedIssueOfPWO1.getKey()
                    def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfPWO1, user).getAllIssues()
                    for (LinkedIssueOfPW0 in LinkedIssuesOfPWO0) {
                        if (LinkedIssueOfPW0.getIssueTypeId() == ID_IT_Cluster) {
                            log.debug "Cluster : " + LinkedIssueOfPW0.getKey()
                            def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPW0, user).getAllIssues()
                            for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                    log.debug "Trigger : " + LinkedIssueOfCluster.getKey()
                                    RequestedDateTrigger = LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (!issue.getCustomFieldValue(RequestedDateField)) {//Creation
        if (RequestedDateTrigger) {
            RequestedDate = (Timestamp) (RequestedDateTrigger - 28)

            issue.setCustomFieldValue(RequestedDateField, RequestedDate)
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    }
    if (CompteurPWO1 == 1) {
        if (RestrictedDataResult) {
            def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue)
            def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
                it.toString() == RestrictedDataResult
            }
            issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData)

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    }
}
