package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Addition/deletion of link between AWO and PWO-1/2.x
 * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
 * isDisabled : null
 */

import com.atlassian.crowd.embedded.api.Group
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.user.ApplicationUser

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
}*/

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
GroupManager groupManager = ComponentAccessor.getGroupManager()

MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject()
MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject()

log.debug "issue : " + issue.getKey()
log.debug "source issue : " + issueSource.getKey()


//Constante
//Issue type
String ID_IT_PWO1 = "10005"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"

List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

long ID_Project_AWO = 10003

//Fields
long ID_CF_IA_Company = 10142
long ID_CF_CompanyAllowed = 10702
long ID_CF_Auth_Company = 10173
long ID_CF_Auth_VerifCompany = 10181
long ID_CF_Form_Company = 10281
long ID_CF_Form_VerifCompany = 10289
long ID_CF_Int_Company = 10324
long ID_CF_AuthVerifCompany = 11130
long ID_CF_FormVerifCompany = 10703
long ID_CF_IntCompany = 11200

//CustomField
CustomField IACompanyField = customFieldManager.getCustomFieldObject(ID_CF_IA_Company)
CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_CompanyAllowed)
CustomField AuthCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Company)
CustomField AuthVerifCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Auth_VerifCompany)
CustomField FormCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Form_Company)
CustomField FormVerifCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Form_VerifCompany)
CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Int_Company)
CustomField AuthVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_AuthVerifCompany)
CustomField FormVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_FormVerifCompany)
CustomField IntCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_IntCompany)

List<Group> CompanyAllowedList = new ArrayList<Group>()
List<Group> AuthVerifCompanyAllowedList = new ArrayList<Group>()
List<Group> FormVerifCompanyAllowedList = new ArrayList<Group>()
List<Group> IntCompanyAllowedList = new ArrayList<Group>()

if (issue.getProjectId() == ID_Project_AWO) {
    if (PWOIT.contains(issueSource.getIssueTypeId())) {
        def LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssueOfAWOForCompany in LinkedIssuesOfAWOForCompany) {
            if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO1) {
                if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                    String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField)
                    String IA_Company_Formatted = "Partner_" + IA_Company_Value

                    Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted)

                    if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                        CompanyAllowedList.add(IA_Company_Management_Formatted_Group)
                    }
                }
            }
            if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO21) {
                if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != "AIRBUS") {
                    String Auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField)
                    String Auth_Company_Formatted = "Partner_" + Auth_Company_Value

                    Group Auth_Company_Management_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted)

                    if (!CompanyAllowedList.contains(Auth_Company_Management_Formatted_Group)) {
                        CompanyAllowedList.add(Auth_Company_Management_Formatted_Group)
                    }
                }
                if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != "AIRBUS") {
                    String Auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField)
                    String Auth_VerifCompany_Formatted = "Partner_" + Auth_VerifCompany_Value

                    Group Auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Auth_VerifCompany_Formatted)

                    if (!CompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                        CompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group)
                    }
                    if (!AuthVerifCompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                        AuthVerifCompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group)
                    }
                }
            }
            if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO22) {
                if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != "AIRBUS") {
                    String Form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField)
                    String Form_Company_Formatted = "Partner_" + Form_Company_Value

                    Group Form_Company_Management_Formatted_Group = groupManager.getGroup(Form_Company_Formatted)

                    if (!CompanyAllowedList.contains(Form_Company_Management_Formatted_Group)) {
                        CompanyAllowedList.add(Form_Company_Management_Formatted_Group)
                    }
                }
                if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != "AIRBUS") {
                    String Form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField)
                    String Form_VerifCompany_Formatted = "Partner_" + Form_VerifCompany_Value

                    Group Form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Form_VerifCompany_Formatted)

                    if (!CompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                        CompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group)
                    }
                    if (!FormVerifCompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                        FormVerifCompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group)
                    }
                }
            }
            if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO23) {
                if (linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != "AIRBUS") {
                    String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField)
                    String Int_Company_Formatted = "Partner_" + Int_Company_Value

                    Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted)

                    if (!CompanyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                        CompanyAllowedList.add(Int_Company_Management_Formatted_Group)
                        IntCompanyAllowedList.add(Int_Company_Management_Formatted_Group)
                    }
                }
            }
        }
        if (CompanyAllowedList != null) {
            issue.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList)
            if (AuthVerifCompanyAllowedList != null) {
                issue.setCustomFieldValue(AuthVerifCompanyAllowedField, AuthVerifCompanyAllowedList)
            }
            if (FormVerifCompanyAllowedList != null) {
                issue.setCustomFieldValue(FormVerifCompanyAllowedField, FormVerifCompanyAllowedList)
            }
            if (IntCompanyAllowedList != null) {
                issue.setCustomFieldValue(IntCompanyAllowedField, IntCompanyAllowedList)
            }
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
            issueIndexingService.reIndex(issue)//JIRA7
            CompanyAllowedList.clear()
            AuthVerifCompanyAllowedList.clear()
            FormVerifCompanyAllowedList.clear()
            IntCompanyAllowedList.clear()
        }
    }
}