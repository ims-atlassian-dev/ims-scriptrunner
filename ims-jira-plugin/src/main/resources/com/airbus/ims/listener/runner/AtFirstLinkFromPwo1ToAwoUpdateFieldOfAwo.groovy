package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : At first link from PWO-1 to AWO, update field of AWO
 * Events : IssueLinkCreatedEvent
 * isDisabled : null
 */

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
}*/


boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()

MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject()
MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject()

log.debug "issue : " + issue.getKey()
log.debug "source issue : " + issueSource.getKey()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"


List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

long ID_Project_AWO = 10003


//Fields
long ID_CF_Scenario = 10152
long ID_CF_TypeOfVerif = 10354
long ID_CF_Domain = 10139


//CustomField
CustomField ScenarioField = customFieldManager.getCustomFieldObject(ID_CF_Scenario)
CustomField TypeOfVerifField = customFieldManager.getCustomFieldObject(ID_CF_TypeOfVerif)
CustomField DomainField = customFieldManager.getCustomFieldObject(ID_CF_Domain)



List<String> Scenario
List valuesToSet
String Domain

double compteur = 0

//Business
def LinkedIssuesOfAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
for (linkedIssueOfAWO in LinkedIssuesOfAWO) {
    log.debug "linked issue of AWO : " + linkedIssueOfAWO.getKey()
    if (linkedIssueOfAWO.getIssueTypeId() == ID_IT_PWO1) {
        compteur = compteur + 1
        log.debug "compteur : " + compteur
    }
}

log.debug "compteur final : " + compteur
if (issueSource.getIssueTypeId() == ID_IT_PWO1 && issue.getProjectId() == ID_Project_AWO) {
    if (compteur < 2) {
        Scenario = (List<String>)issueSource.getCustomFieldValue(ScenarioField)
        log.debug "Scneario : " + Scenario

        for (scenari in Scenario) {
            log.debug "Scenari : " + scenari
            def fieldConfigTypeOfVerif = TypeOfVerifField.getRelevantConfig(issue)
            def valueTypeOfChange = ComponentAccessor.optionsManager.getOptions(fieldConfigTypeOfVerif)?.find {
                it.toString() == scenari.toString().trim()
            }
            log.debug "valueTypeOfChange : " + valueTypeOfChange
            if (valuesToSet) {
                valuesToSet = valuesToSet + valueTypeOfChange
            } else {
                valuesToSet = [valueTypeOfChange]
            }
            log.debug "valuesToSet" + valuesToSet
        }

        if (valuesToSet) {
            issue.setCustomFieldValue(TypeOfVerifField, valuesToSet)
        }

        Domain = issueSource.getCustomFieldValue(DomainField)
        if (Domain) {
            def fieldConfigDomain = DomainField.getRelevantConfig(issue)
            def valueDomain = ComponentAccessor.optionsManager.getOptions(fieldConfigDomain)?.find {
                it.toString() == Domain.toString().trim()
            }
            issue.setCustomFieldValue(DomainField, valueDomain)
        }

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
        log.debug "index"
    }
}
