package com.airbus.ims.listener.runner
/**
 * Projects : AWO
 * Description : At first link from PWO-2 to PWO-1, update "Restricted data" of PWO-2
 * Events : IssueLinkCreatedEvent
 * isDisabled : null
 */

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
}*/


log.debug "Creation/Edit of a WU/Obj. Freeze/AWO, copy reference to summary"

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()

MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject()
MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject()

log.debug "issue : " + issue.getKey()
log.debug "source issue : " + issueSource.getKey()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"


List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_RestrictedData = 10800

//CustomField
CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ID_CF_RestrictedData)

String RestrictedData

double compteur = 0

//Business
def LinkedIssuesOfPWO2 = issueLinkManager.getLinkCollection(issueSource, user).getAllIssues()
for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
    log.debug "linked issue of PWO2 : " + linkedIssueOfPWO2.getKey()
    if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO1) {
        compteur = compteur + 1
        log.debug "compteur : " + compteur
    }
}

log.debug "compteur final : " + compteur
if (issue.getIssueTypeId() == ID_IT_PWO1 && issueSource.getIssueTypeId() == ID_IT_PWO2 && compteur < 2) {
    RestrictedData = issue.getCustomFieldValue(RestrictedDataField)
    log.debug "Restricted data : " + RestrictedData

    if (RestrictedData) {
        def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issueSource)
        def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
            it.toString() == RestrictedData.toString().trim()
        }
        issueSource.setCustomFieldValue(RestrictedDataField, valueRestrictedData)
    }

    issueManager.updateIssue(user, issueSource, EventDispatchOption.DO_NOT_DISPATCH, false)
    issueIndexingService.reIndex(issueSource)
    log.debug "index"
}
