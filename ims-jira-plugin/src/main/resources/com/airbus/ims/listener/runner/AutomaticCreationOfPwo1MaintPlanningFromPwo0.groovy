package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Automatic creation of PWO-1 (Maint Planning) from PWO-0
 * Events : PWO-0 updated
 * isDisabled : true
 */

// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean

log.setLevel(Level.DEBUG)
/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */
boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
        boolean isUpdated = false;
            if (changeLog != null) {
                ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
                List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
                for (changeIteamBean in changeItemBeans){
                    if (itemsChanged.contains(changeIteamBean.getField())){
                        isUpdated = true;
                    }
                }
            }

        return isUpdated;
	}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog;

MutableIssue issue = (MutableIssue)event.getIssue();

//Constante

//Fields
long ID_CF_Domain=10034;
long ID_CF_MaintPlanningValidated=10084;

//CustomField
CustomField DomainField=customFieldManager.getCustomFieldObject(ID_CF_Domain)
CustomField MaintPlanningValidatedField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningValidated)

//Variables
List<String> DomainValues

//Business
List<String> itemsChangedMaintPlanningValidated = new ArrayList<String>();
itemsChangedMaintPlanningValidated.add(MaintPlanningValidatedField.getFieldName());

boolean isMaintPlanningValidated = (boolean)workloadIsUpdated(changeLog, itemsChangedMaintPlanningValidated);
log.debug "isMaintPlanning : "+isMaintPlanningValidated
if(isMaintPlanningValidated){//Creation
    DomainValues = issue.getCustomFieldValue(DomainField)
    log.debug "Domain values : "+DomainValues
    for (domainValue in DomainValues){
        if(domainValue.toString()=="Maint Planning"){
            if (issue.getCustomFieldValue(MaintPlanningValidatedField).toString()=="Validated"){
                return true
            }
            else{
                return false
            }
        }
    }
}