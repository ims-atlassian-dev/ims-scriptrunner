package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Check unicity of summary for PWO and fill the field Warning if needed
 * Events : Change of summary
 * isDisabled : null
 */

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.query.Query
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption


IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class)
SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class)
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
MutableIssue issue = (MutableIssue) event.getIssue()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)

long ID_CF_Warning = 12200
CustomField WarningField = customFieldManager.getCustomFieldObject(ID_CF_Warning)


String JQL_QUERY = "issuetype in  (\"%s\") AND summary ~ \"%s\" ORDER BY Key";
String query = "";
List<Issue> issuesFound = [];
List<Issue> issuesFound2 = [];
String issuesResult = ""
String issuesResult2 = ""
String Debut ="*{color:#ff0000}Other issue with the same title exists :{color}* "
String toRet

log.debug "issue : "+issue.getKey()

query = String.format(JQL_QUERY,issue.getIssueType().getName(),issue.getSummary());
log.debug "Query : " + query;
Query JQLquery = jqlQueryParser.parseQuery(query)
SearchResults results = searchProvider.search(JQLquery, user, PagerFilter.getUnlimitedFilter())
issuesFound = results.getIssues();

if (issuesFound.size()>1){
    for (issueFound in issuesFound){
        if (issueFound.getKey() != issue.getKey()){
        	issuesResult += "*"+issueFound.getKey()+"* "
            
            String query2 = String.format(JQL_QUERY,issueFound.getIssueType().getName(),issueFound.getSummary());
            log.debug "Query 2 : " + query2;
            Query JQLquery2 = jqlQueryParser.parseQuery(query2)
            SearchResults results2 = searchProvider.search(JQLquery2, user, PagerFilter.getUnlimitedFilter())
            issuesFound2 = results2.getIssues();
            
            for (issueFound2 in issuesFound2){
                if (issueFound2.getKey() != issueFound.getKey()){
                    issuesResult2 += "*"+issueFound2.getKey()+"* "
                }
            }
            
            MutableIssue issueFoundMutable = issueManager.getIssueObject(issueFound.getKey())
            String toRet2 = Debut + issuesResult2
            issueFoundMutable.setCustomFieldValue(WarningField,toRet2)
            issueManager.updateIssue(user, issueFoundMutable, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issueFound)
            toRet2 = ""
            issuesResult2=""
        }
    }
    toRet = Debut+issuesResult

	issue.setCustomFieldValue(WarningField,toRet)
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
	issueIndexingService.reIndex(issue)
}