package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Close PWO-2.1 when Technically closed = Yes, Auth_DN and Auth_Verified_DN is filled
 * Events : PWO-2.1 updated
 * isDisabled : false
 */

// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean

log.setLevel(Level.DEBUG)
/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
        boolean isUpdated = false;
            if (changeLog != null) {
                ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
                List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
                for (changeIteamBean in changeItemBeans){
                    if (itemsChanged.contains(changeIteamBean.getField())){
                        isUpdated = true;
                    }
                }
            }

        return isUpdated;
	}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog;

MutableIssue issue = (MutableIssue)event.getIssue();

//Constante
//Issue type
String ID_IT_PWO21 = "10007";

//Fields
long ID_CF_TechnicallyClosed=10054
long ID_CF_Auth_Validated=10189;
long ID_CF_Auth_Verified=10183;
long ID_CF_Auh_DN=10241
long ID_CF_Auh_Verification_DN=10242

//CustomField
CustomField TechnicallyClosedField=customFieldManager.getCustomFieldObject(ID_CF_TechnicallyClosed)
CustomField Auth_ValidatedField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validated)
CustomField Auth_VerifiedField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verified)
CustomField AuthDNField=customFieldManager.getCustomFieldObject(ID_CF_Auh_DN)
CustomField AuthVerificationDNField=customFieldManager.getCustomFieldObject(ID_CF_Auh_Verification_DN)


//Variables

//Business
log.debug "issue : "+issue.getKey()
if (issue.getIssueTypeId()==ID_IT_PWO21){
   
    List<String> itemsChangedValidated = new ArrayList<String>();
    itemsChangedValidated.add(Auth_ValidatedField.getFieldName());
    itemsChangedValidated.add(AuthDNField.getFieldName());
    itemsChangedValidated.add(AuthVerificationDNField.getFieldName());
    
    List<String> itemsChangedVerified = new ArrayList<String>();
    //itemsChangedVerified.add(Auth_ValidatedField.getFieldName());
    itemsChangedVerified.add(Auth_VerifiedField.getFieldName());

    boolean isValidated = (boolean)workloadIsUpdated(changeLog, itemsChangedValidated);
    
    boolean isVerified = (boolean)workloadIsUpdated(changeLog, itemsChangedVerified);
    log.debug "isValidated : "+isValidated
    log.debug "technically closed = "+issue.getCustomFieldValue(TechnicallyClosedField).toString()
    if(isValidated || !isVerified){//Creation
        if (issue.getCustomFieldValue(Auth_ValidatedField).toString()=="Validated" && issue.getCustomFieldValue(AuthDNField) && issue.getCustomFieldValue(AuthVerificationDNField)){
            return true
        }
        else{
            return false
        }
    }    
}