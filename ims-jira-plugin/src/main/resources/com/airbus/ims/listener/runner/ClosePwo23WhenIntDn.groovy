package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Close PWO-2.3 when Int_DN
 * Events : Deliver PWO-2.3, PWO-2.3 updated
 * isDisabled : null
 */

// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean

/*log.setLevel(Level.DEBUG)
String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
        boolean isUpdated = false;
            if (changeLog != null) {
                ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
                List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
                for (changeIteamBean in changeItemBeans){
                    if (itemsChanged.contains(changeIteamBean.getField())){
                        isUpdated = true;
                    }
                }
            }

        return isUpdated;
	}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog;

MutableIssue issue = (MutableIssue)event.getIssue();

//Constante
//Issue type
String ID_IT_PWO23 = "10009";

//Fields
long ID_CF_Int_DN=10335
long ID_CF_TechnicallyClosed=10054;

long ID_Event_Deliver = 10301

//CustomField
CustomField Int_DNField=customFieldManager.getCustomFieldObject(ID_CF_Int_DN)
CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ID_CF_TechnicallyClosed)

long eventId = event.getEventTypeId()
log.debug "event id :"+eventId


//Variables

//Business
log.debug "issue : "+issue.getKey()
if (issue.getIssueTypeId()==ID_IT_PWO23){
   
    List<String> itemsChangedValidated = new ArrayList<String>();
    itemsChangedValidated.add(Int_DNField.getFieldName());
    itemsChangedValidated.add(TechnicallyClosedField.getFieldName());

    boolean isValidated = (boolean)workloadIsUpdated(changeLog, itemsChangedValidated);
    if(isValidated || eventId == ID_Event_Deliver){//Creation
        if (issue.getCustomFieldValue(Int_DNField)){
            return true
        }
        else{
            return false
        }
    }    
}