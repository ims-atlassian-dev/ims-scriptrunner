package com.airbus.ims.listener.runner
/**
 * Projects : PO
 * Description : Creation / Edit of PO / WU (link between the two objects)
 * Events : Issue Created, PO updated
 * isDisabled : null
 */

// import


import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser


/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
}*/

def getIssueFromNFeedFieldNewFieldsSingle(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ")
        issueFound = issueManager.getIssueObject(Long.parseLong(formattedValues))
    }

    if (issueFound) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + issueFound
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }
    return issueFound
}

public def getIssueFromNFeedFieldNewFieldsMulitple(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    List<Issue> multipleIssue = new ArrayList<Issue>()
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String[] formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ").split(" ")
        for (String formatedValue in formattedValues) {
            issueFound = issueManager.getIssueObject(Long.parseLong(formatedValue))
            multipleIssue.add(issueFound)
        }
    }

    if (multipleIssue) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + multipleIssue
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }

    return multipleIssue
}

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager)
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog

MutableIssue issue = (MutableIssue) event.getIssue()

//Constantes
//Issue type
String ID_IT_WU = "10022"
String ID_IT_WU_AdditionalCost = "10200"

//Event


long ID_CF_WorkUnit = 11142
long ID_CF_WorkUnitAdditionalCost = 11163


CustomField WorkUnitField = customFieldManager.getCustomFieldObject(ID_CF_WorkUnit)
CustomField WorkUnitAdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_WorkUnitAdditionalCost)

//Creation
if (event.getEventTypeId() == 1) {
    //Create link with WU
    if (WorkUnitField != null) {
        //Collection toLink = fieldValueService.getFieldValues(issue.getKey(), ID_CF_WorkUnitString)
        Collection<Issue> toLink = (Collection<Issue>) getIssueFromNFeedFieldNewFieldsMulitple(WorkUnitField, issue, issueManager)
        //toLink.each {
        if (toLink) {
            for (issueToLink in toLink) {
                Long toLink_id = issueToLink.getId()
                issueLinkManager.createIssueLink(issue.id, toLink_id, 10003L, 1L, user)
            }
        }
    }
    if (WorkUnitAdditionalCostField != null) {
        //Collection toLink = fieldValueService.getFieldValues(issue.getKey(), ID_CF_WorkUnitAdditionalCostString)
        Collection<Issue> toLink = (Collection<Issue>) getIssueFromNFeedFieldNewFieldsMulitple(WorkUnitAdditionalCostField, issue, issueManager)
        //toLink.each {
        if (toLink) {
            for (issueToLink in toLink) {
                Long toLink_id = issueToLink.getId()
                issueLinkManager.createIssueLink(issue.id, toLink_id, 10003L, 1L, user)
            }
        }
    }
}
//Edition
else if (event.getEventTypeId() == 2) {
    List<String> itemsChanged = new ArrayList<String>()
    itemsChanged.add(WorkUnitField.getFieldName())
    //If WU has been updated, change link
    boolean isWorlogUpdated = (boolean) workloadIsUpdated(changeLog, itemsChanged)
    log.debug "worklog updated : " + isWorlogUpdated
    if (isWorlogUpdated) {
        //Delete old link
        def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssue in LinkedIssues) {
            if (linkedIssue.getIssueTypeId() == ID_IT_WU) {
                IssueLink linkToRemove = issueLinkManager.getIssueLink(issue.id, linkedIssue.id, 10003L)
                issueLinkManager.removeIssueLink(linkToRemove, user)
            }
        }
        //Create new link
        if (WorkUnitField != null) {
            log.debug "Adding link"
            //Collection toLink = fieldValueService.getFieldValues(issue.getKey(), ID_CF_WorkUnitString)
            Collection<Issue> toLink = (Collection<Issue>) getIssueFromNFeedFieldNewFieldsMulitple(WorkUnitField, issue, issueManager)
            //toLink.each {
            if (toLink) {
                for (issueToLink in toLink) {
                    Long toLink_id = issueToLink.getId()
                    issueLinkManager.createIssueLink(issue.id, toLink_id, 10003L, 1L, user)
                }
            }
        }
    }

    List<String> itemsChangedAdditionalCost = new ArrayList<String>()
    itemsChangedAdditionalCost.add(WorkUnitAdditionalCostField.getFieldName())
    //If WU has been updated, change link
    boolean isWorlogAdditionalCostUpdated = (boolean) workloadIsUpdated(changeLog, itemsChangedAdditionalCost)
    if (isWorlogAdditionalCostUpdated) {
        //Delete old link
        def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssue in LinkedIssues) {
            if (linkedIssue.getIssueTypeId() == ID_IT_WU_AdditionalCost) {
                IssueLink linkToRemove = issueLinkManager.getIssueLink(issue.id, linkedIssue.id, 10003L)
                issueLinkManager.removeIssueLink(linkToRemove, user)
            }
        }
        //Create new link
        if (WorkUnitAdditionalCostField != null) {
            log.debug "Adding link"
            //Collection toLink = fieldValueService.getFieldValues(issue.getKey(), ID_CF_WorkUnitAdditionalCostString)
            Collection<Issue> toLink = (Collection<Issue>) getIssueFromNFeedFieldNewFieldsMulitple(WorkUnitAdditionalCostField, issue, issueManager)
            //toLink.each {
            if (toLink) {
                for (issueToLink in toLink) {
                    Long toLink_id = issueToLink.getId()
                    issueLinkManager.createIssueLink(issue.id, toLink_id, 10003L, 1L, user)
                }
            }
        }
    }
}
