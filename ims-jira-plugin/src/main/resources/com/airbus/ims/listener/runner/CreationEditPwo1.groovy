package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Creation / Edit PWO-1
 * Events : Admin re-open, PWO-1 updated, Issue Created
 * isDisabled : false
 */

// import

import com.atlassian.crowd.embedded.api.Group
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.user.ApplicationUser
import org.joda.time.LocalDate

import java.sql.Timestamp
/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
} */


boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

def getIssueFromNFeedFieldNewFieldsSingle(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ")
        issueFound = issueManager.getIssueObject(Long.parseLong(formattedValues))
    }

    if (issueFound) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + issueFound
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }
    return issueFound
}

def getIssueFromNFeedFieldNewFieldsMulitple(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    List<Issue> multipleIssue = new ArrayList<Issue>()
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String[] formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ").split(" ")
        for (String formatedValue in formattedValues) {
            issueFound = issueManager.getIssueObject(Long.parseLong(formatedValue))
            multipleIssue.add(issueFound)
        }
    }

    if (multipleIssue) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + multipleIssue
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }

    return multipleIssue
}

String formatSecondesValueInHoursMinutes(Long secondes) {
    log.debug "Start formatSecondesValueInHoursMinutes - secondes : " + secondes
    if (secondes && secondes > 0) {
        String result
        int hoursExtracted = (int) (secondes / 3600)
        int totalMinutes = (int) (secondes / 60)
        int minutesExtracted = (int) totalMinutes.mod(60)
        result = hoursExtracted.toString() + "h " + minutesExtracted.toString() + "m"
        log.debug "End formatSecondesValueInHoursMinutes - result  : " + result
        return result
    } else {
        log.debug "End formatSecondesValueInHoursMinutes - minutes is null"
    }
}

Long formatHoursMinutesFormatInSecondes(String hoursMinutes) {
    log.debug "Start formatHoursMinutesFormatInSecondes - hoursMinutes : " + hoursMinutes
    if (!hoursMinutes.equals("null")) {
        Long result = 0L
        String[] hoursMinutesTable
        Long hourConvertedInMinutes = 0
        String hourNumberString
        Long minutesNumber = 0

        hoursMinutesTable = hoursMinutes.split("h")
        hourConvertedInMinutes = hoursMinutesTable[0].toString().toLong() * 60
        minutesNumber = hoursMinutesTable[1].toString().substring(0, hoursMinutesTable[1].toString().length() - 1).toLong()
        result = (hourConvertedInMinutes + minutesNumber) * 60

        log.debug "End formatHoursMinutesFormatInSecondes - result : " + result
        return result
    } else {
        log.debug "End formatHoursMinutesFormatInSecondes - hoursMinutes is null"
    }
}


//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog
GroupManager groupManager = ComponentAccessor.getGroupManager()

MutableIssue issue = (MutableIssue) event.getIssue()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"
String ID_IT_Cluster = "10001"
String ID_IT_Trigger = "10000"

List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

long ID_PR_AWO = 10003
long ID_Event_AdminReOpen = 10400

//Fields
long ID_CF_IA_Company = 10142
long ID_CF_CompanyAllowed = 10702
long ID_CF_Auth_Company = 10173
long ID_CF_Auth_VerifCompany = 10181
long ID_CF_Form_Company = 10281
long ID_CF_Form_VerifCompany = 10289
long ID_CF_Int_Company = 10324
long ID_CF_IntCompany = 11200
long ID_CF_AuthVerifCompany = 11130
long ID_CF_FormVerifCompany = 10703

long ID_CF_PartsData_EffectiveCost = 10113
long ID_CF_PartsData_PreIA_RequestedDate = 10058
long ID_CF_PartsData_PreIA_1stCommitedDate = 10060
long ID_CF_PartsData_PreIA_LastCommitedDate = 10062
long ID_CF_PartsData_Validation_PreIA_RequestedDate = 10066
long ID_CF_PartsData_PreIA_Validation_1stCommitedDate = 10067
long ID_CF_PartsData_PreIA_Validation_LastCommitedDate = 10090
long IF_CF_PartsData_WU = 10123
long IF_CF_PartsData_NbWU = 10124
long ID_CF_PartsData_Validated = 10065
long ID_CF_PartsData_DN = 10120
long ID_CF_PartsData_1stDelivery = 10061
long ID_CF_PartsData_LastDelivery = 10063
long ID_CF_PartsData_Validation_1stDelivery = 10068
long ID_CF_PartsData_Validation_LastDelivery = 10070
long ID_CF_PartsDataStatus = 10049
long ID_CF_PartsData_EstimatedCosts = 10024
long ID_CF_PartsData_TimeSpent = 11012
long ID_CF_PartsData_EffectiveCosts = 10113
long ID_CF_PartsData_Valdidation_1stDeliveryDate = 10068
long ID_CF_PartsData_Valdidation_LastDeliveryDate = 10070
long ID_CF_PartsData_ReworkDate = 11119

long ID_CF_MaintPlanning_EffectiveCost = 10114
long ID_CF_MaintPlanning_PreIA_RequestedDate = 10077
long ID_CF_MaintPlanning_PreIA_1stCommitedDate = 10079
long ID_CF_MaintPlanning_PreIA_LastCommitedDate = 10088
long ID_CF_MaintPlanning_Validation_PreIA_RequestedDate = 10085
long ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate = 10086
long ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate = 10081
long IF_CF_MaintPlanning_WU = 10126
long IF_CF_MaintPlanning_NbWU = 10127
long ID_CF_MaintPlanning_Validated = 10084
long ID_CF_MaintPlanning_DN = 10121
long ID_CF_MaintPlanning_1stDelivery = 10080
long ID_CF_MaintPlanning_LastDelivery = 10082
long ID_CF_MaintPlanning_Validation_1stDelivery = 10087
long ID_CF_MaintPlanning_Validation_LastDelivery = 10089
long ID_CF_MaintPlanningStatus = 10071
long ID_CF_MaintPlanning_EstimatedCosts = 10027
long ID_CF_MaintPlanning_TimeSpent = 11013
long ID_CF_MaintPlanning_EffectiveCosts = 10114
long ID_CF_MaintPlanning_Valdidation_1stDeliveryDate = 10087
long ID_CF_MaintPlanning_Valdidation_LastDeliveryDate = 10089
long ID_CF_MaintPlanning_ReworkDate = 11120

long ID_CF_Maint_EffectiveCost = 10115
long ID_CF_Maint_PreIA_RequestedDate = 10097
long ID_CF_Maint_PreIA_1stCommitedDate = 10099
long ID_CF_Maint_PreIA_LastCommitedDate = 10101
long ID_CF_Maint_PreIA_Validation_RequestedDate = 10105
long ID_CF_Maint_PreIA_Validation_1stCommitedDate = 10106
long ID_CF_Maint_PreIA_Validation_LastCommitedDate = 10108
long IF_CF_Maint_WU = 10129
long IF_CF_Maint_NbWU = 10130
long ID_CF_Maint_Validated = 10104
long ID_CF_Maint_DN = 10122
long ID_CF_Maint_1stDelivery = 10100
long ID_CF_Maint_LastDelivery = 10102
long ID_CF_Maint_Validation_1stDelivery = 10107
long ID_CF_Maint_Validation_LastDelivery = 10109
long ID_CF_MaintStatus = 10091
long ID_CF_Maint_EstimatedCosts = 10030
long ID_CF_Maint_TimeSpent = 11014
long ID_CF_Maint_EffectiveCosts = 10115
long ID_CF_Maint_Valdidation_1stDeliveryDate = 10107
long ID_CF_Maint_Valdidation_LastDeliveryDate = 10109
long ID_CF_Maint_ReworkDate = 11121

long ID_CF_Domain = 10034
long ID_CF_DomainPWO1 = 10139

long ID_CF_IA_RequestedDate = 10144
long ID_CF_IA_Validation_RequestedDate = 10154
long ID_CF_IA_1stCommittedDate = 10145
long ID_CF_IA_LastCommittedDate = 10146
long ID_CF_IA_Validation_1stCommittedDate = 10155
long ID_CF_IA_Validation_LastCommittedDate = 10156
long ID_CF_IA_WU = 10163
long ID_CF_IA_NbWU = 10164
long ID_CF_IA_EffectiveCost = 10161
long ID_CF_IA_Validated = 10151
long ID_CF_IA_Validation_1stDeliveryDate = 10157
long ID_CF_IA_Validation_LastDeliveryDate = 10158
long ID_CF_IA_Blocked = 10138
long ID_CF_IA_TimeSpent = 11017
long ID_CF_IA_ReworkDate = 11122


long ID_CF_RestrictedData = 10800
long ID_CF_RequestedDate = 10010
long ID_CF_WUValue = 10625
long ID_CF_FirstSNAppli = 10011
long ID_CF_TechnicalClosure = 10040
long ID_CF_TechnicallyClosed = 10054
long ID_CF_Blocked = 10041
long ID_CF_BlockStartDate = 10043
long ID_CF_OldBlockStartDate = 11800
long ID_CF_BlockEndDate = 10044
long ID_CF_OldBlockEndDate = 12000
long ID_CF_Escalated = 10055
long ID_CF_EscalateStartDate = 10047
long ID_CF_OldEscalateStartDate = 12100
long ID_CF_EscalateEndDate = 10048
long ID_CF_OldEscalateEndDate = 12101
long ID_CF_TotalBlockingDuration = 11011
long ID_CF_TotalEscalatedDuration = 11019
long ID_CF_Auth_TimeSpent = 11015
long ID_CF_Auth_EffectiveCost = 10240
long ID_CF_Form_TimeSpent = 11016
long ID_CF_Form_EffectiveCost = 10305
long ID_CF_Int_TimeSpent = 11018
long ID_CF_Int_EffectiveCost = 10332
long ID_CF_TriggerTotalEffectiveCosts = 10116
long ID_CF_ImpactConfirmed = 10141
long ID_CF_ImpactOnTD = 10008
long ID_CF_IA_AdditionalCost = 11116
long ID_CF_PreIA_PartsData_AddtionalCost = 11115
long ID_CF_PreIA_MaintPlanning_AddtionalCost = 11117
long ID_CF_PreIA_Maint_AddtionalCost = 11118

//CustomField
CustomField IACompanyField = customFieldManager.getCustomFieldObject(ID_CF_IA_Company)
CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_CompanyAllowed)
CustomField AuthCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Company)
CustomField AuthVerifCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Auth_VerifCompany)
CustomField FormCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Form_Company)
CustomField FormVerifCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Form_VerifCompany)
CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Int_Company)
CustomField IntCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_IntCompany)
CustomField AuthVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_AuthVerifCompany)
CustomField FormVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_FormVerifCompany)

CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EffectiveCost)
CustomField PartsData_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EstimatedCosts)
CustomField PartsData_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_TimeSpent)
CustomField PartsData_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EffectiveCosts)

CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EffectiveCost)
CustomField MaintPlanning_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EstimatedCosts)
CustomField MaintPlanning_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_TimeSpent)
CustomField MaintPlanning_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EffectiveCosts)

CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EffectiveCost)
CustomField Maint_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EstimatedCosts)
CustomField Maint_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Maint_TimeSpent)
CustomField Maint_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EffectiveCosts)

/*
CustomField PartsData_PreIA_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_RequestedDate)
CustomField PartsData_PreIA_1stCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_1stCommitedDate)
CustomField PartsData_PreIA_LastCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_LastCommitedDate)
CustomField PartsData_Validation_PreIA_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_PreIA_RequestedDate)
CustomField PartsData_PreIA_Validation_1stCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_1stCommitedDate)
CustomField PartsData_PreIA_Validation_LastCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_LastCommitedDate)
CustomField PartsData_WUField=customFieldManager.getCustomFieldObject(IF_CF_PartsData_WU)
CustomField PartsData_NbWUField=customFieldManager.getCustomFieldObject(IF_CF_PartsData_NbWU)
CustomField PartsDataValidatedField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validated)
CustomField PartsData_DN_Field=customFieldManager.getCustomFieldObject(ID_CF_PartsData_DN)
CustomField PartsData_1stDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_1stDelivery)
CustomField PartsData_LastDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_LastDelivery)
CustomField PartsData_Validation_1stDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_1stDelivery)
CustomField PartsData_Validation_LastDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_LastDelivery)
CustomField PartsDataStatusField = customFieldManager.getCustomFieldObject(ID_CF_PartsDataStatus)
CustomField PartsData_Valdidation_1stDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Valdidation_1stDeliveryDate)
CustomField PartsData_Valdidation_LastDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Valdidation_LastDeliveryDate)
CustomField PartsData_ReworkDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_ReworkDate)

CustomField MaintPlanning_PreIA_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_RequestedDate)
CustomField MaintPlanning_PreIA_1stCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_1stCommitedDate)
CustomField MaintPlanning_PreIA_LastCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_LastCommitedDate)
CustomField MaintPlanning_Validation_PreIA_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_RequestedDate)
CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate)
CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate)
CustomField MaintPlanning_WUField=customFieldManager.getCustomFieldObject(IF_CF_MaintPlanning_WU)
CustomField MaintPlanning_NbWUField=customFieldManager.getCustomFieldObject(IF_CF_MaintPlanning_NbWU)
CustomField MaintPlanningValidatedField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validated)
CustomField MaintPlanning_DN_Field=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_DN)
CustomField MaintPlanning_1stDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_1stDelivery)
CustomField MaintPlanning_LastDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_LastDelivery)
CustomField MaintPlanning_Validation_1stDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_1stDelivery)
CustomField MaintPlanning_Validation_LastDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_LastDelivery)
CustomField MaintPlanningStatusField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningStatus)
CustomField MaintPlanning_Valdidation_1stDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Valdidation_1stDeliveryDate)
CustomField MaintPlanning_Valdidation_LastDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Valdidation_LastDeliveryDate)
CustomField MaintPlanning_ReworkDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_ReworkDate)

CustomField Maint_PreIA_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_RequestedDate)
CustomField Maint_PreIA_1stCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_1stCommitedDate)
CustomField Maint_PreIA_LastCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_LastCommitedDate)
CustomField Maint_PreIA_Validation_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_RequestedDate)
CustomField Maint_PreIA_Validation_1stCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_1stCommitedDate)
CustomField Maint_PreIA_Validation_LastCommitedDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_LastCommitedDate)
CustomField Maint_WUField=customFieldManager.getCustomFieldObject(IF_CF_Maint_WU)
CustomField Maint_NbWUField=customFieldManager.getCustomFieldObject(IF_CF_Maint_NbWU)
CustomField MaintValidatedField=customFieldManager.getCustomFieldObject(ID_CF_Maint_Validated)
CustomField Maint_DN_Field=customFieldManager.getCustomFieldObject(ID_CF_Maint_DN)
CustomField Maint_1stDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_Maint_1stDelivery)
CustomField Maint_LastDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_Maint_LastDelivery)
CustomField Maint_Validation_1stDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_Maint_Validation_1stDelivery)
CustomField Maint_Validation_LastDeliveryField=customFieldManager.getCustomFieldObject(ID_CF_Maint_Validation_LastDelivery)
CustomField MaintStatusField = customFieldManager.getCustomFieldObject(ID_CF_MaintStatus)
CustomField Maint_Valdidation_1stDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_Valdidation_1stDeliveryDate)
CustomField Maint_Valdidation_LastDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_Valdidation_LastDeliveryDate)
CustomField Maint_ReworkDateField=customFieldManager.getCustomFieldObject(ID_CF_Maint_ReworkDate)*/

//CustomField DomainField=customFieldManager.getCustomFieldObject(ID_CF_Domain)
CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ID_CF_DomainPWO1)

CustomField IA_RequestdDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_RequestedDate)
CustomField IA_Validation_RequestdDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_RequestedDate)
CustomField IA_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_1stCommittedDate)
CustomField IA_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_LastCommittedDate)
CustomField IA_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stCommittedDate)
CustomField IA_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastCommittedDate)
CustomField IA_WUField = customFieldManager.getCustomFieldObject(ID_CF_IA_WU)
CustomField IA_NbWUField = customFieldManager.getCustomFieldObject(ID_CF_IA_NbWU)
CustomField IA_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_EffectiveCost)
CustomField IA_ValidatedField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validated)
CustomField IA_Validation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stDeliveryDate)
CustomField IA_Validation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastDeliveryDate)
CustomField IA_BlockedField = customFieldManager.getCustomFieldObject(ID_CF_IA_Blocked)
CustomField IATimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_IA_TimeSpent)
CustomField IAEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_EffectiveCost)
CustomField IAReworkDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_ReworkDate)


CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ID_CF_RestrictedData)
CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_RequestedDate)
CustomField WUValueField = customFieldManager.getCustomFieldObject(ID_CF_WUValue)
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)
CustomField TechnicalClosureDateField = customFieldManager.getCustomFieldObject(ID_CF_TechnicalClosure)
CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ID_CF_TechnicallyClosed)
CustomField BlockedField = customFieldManager.getCustomFieldObject(ID_CF_Blocked)
CustomField OldBlockStartDateField = customFieldManager.getCustomFieldObject(ID_CF_OldBlockStartDate)
CustomField BlockStartDateField = customFieldManager.getCustomFieldObject(ID_CF_BlockStartDate)
CustomField OldBlockEndDateField = customFieldManager.getCustomFieldObject(ID_CF_OldBlockEndDate)
CustomField BlockEndDateField = customFieldManager.getCustomFieldObject(ID_CF_BlockEndDate)
CustomField EscalatedField = customFieldManager.getCustomFieldObject(ID_CF_Escalated)
CustomField EscalateStartDateField = customFieldManager.getCustomFieldObject(ID_CF_EscalateStartDate)
CustomField OldEscalateStartDateField = customFieldManager.getCustomFieldObject(ID_CF_OldEscalateStartDate)
CustomField EscalateEndDateField = customFieldManager.getCustomFieldObject(ID_CF_EscalateEndDate)
CustomField OldEscalateEndDateField = customFieldManager.getCustomFieldObject(ID_CF_OldEscalateEndDate)
CustomField TotalBlockingDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalBlockingDuration)
CustomField TotalEscalatedDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalEscalatedDuration)
CustomField ImpactConfirmedField = customFieldManager.getCustomFieldObject(ID_CF_ImpactConfirmed)
CustomField ImpactOnTDField = customFieldManager.getCustomFieldObject(ID_CF_ImpactOnTD)
CustomField AuthTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Auth_TimeSpent)
CustomField AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Auth_EffectiveCost)
CustomField FormTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Form_TimeSpent)
CustomField FormEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Form_EffectiveCost)
CustomField IntTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Int_TimeSpent)
CustomField IntEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Int_EffectiveCost)
CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_TriggerTotalEffectiveCosts)
CustomField IA_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_AdditionalCost)
CustomField PreIA_PartsData_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_AddtionalCost)
CustomField PreIA_MaintPlanning_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_AddtionalCost)
CustomField PreIA_Maint_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_AddtionalCost)


//Variables
String RestrictedDataResult
String TechnicallyClosed = "Yes"
String PartsDataStatus
String MaintPlanningStatus
String MaintStatus
String TotalBlockingDurationString
String TotalEscalatedDurationString
String DomainOfPWO2
Date RequestedDateTrigger
Date RequestedDate
Date PartsData_PreIA_RequestedDate
Date PartsData_Validation_PreIA_RequestedDate
Date PartsData_PreIA_1stCommitedDate
Date PartsData_PreIA_LastCommitedDate
Date PartsData_PreIA_Validation_1stCommitedDate
Date PartsData_PreIA_Validation_LastCommitedDate
Date PartsData_ReworkDate
Date MaintPlanning_PreIA_RequestedDate
Date MaintPlanning_Validation_PreIA_RequestedDate
Date MaintPlanning_PreIA_1stCommitedDate
Date MaintPlanning_PreIA_LastCommitedDate
Date MaintPlanning_PreIA_Validation_1stCommitedDate
Date MaintPlanning_PreIA_Validation_LastCommitedDate
Date MaintPlanning_ReworkDate
Date Maint_PreIA_RequestedDate
Date Maint_PreIA_Validation_RequestedDate
Date Maint_PreIA_1stCommitedDate
Date Maint_PreIA_LastCommitedDate
Date Maint_PreIA_Validation_1stCommitedDate
Date Maint_PreIA_Validation_LastCommitedDate
Date Maint_ReworkDate
Date TechnicalClosure
Date PartsData_1stDelivery
Date PartsData_LastDelivery
Date MaintPlanning_1stDelivery
Date MaintPlanning_LastDelivery
Date Maint_1stDelivery
Date Maint_LastDelivery
def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate = new LocalDate(new Date()).toDate()
Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime())

Date BlockStartDate
Date BlockEndDate
Date EscalateStartDate
Date EscalateEndDate
Date IA_RequestdDate
Date IA_Validation_RequestdDate
Date IA_1stCommittedDate
Date IA_LastCommittedDate
Date IA_Validation_1stCommittedDate
Date IA_Validation_LastCommittedDate
Date IA_Validation_1stDelivery
Date IA_Validation_LastDelivery
Date IA_ReworkDate
//Issue issuePartsDataWU = (Issue)getIssueFromNFeedFieldSingle(PartsData_WUField,issue,issueManager)
//Issue issueMaintPlanningWU = (Issue)getIssueFromNFeedFieldSingle(MaintPlanning_WUField,issue,issueManager)
//Issue issueMaintWU = (Issue)getIssueFromNFeedFieldSingle(Maint_WUField,issue,issueManager)
Issue issueIAWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(IA_WUField, issue, issueManager)
double PartsDataEffectiveCost
double MaintPlanningEffectiveCost
double MaintEffectiveCost
double PartsDataWUValue
double MaintPlanningWUValue
double MaintWUValue
double PartsDataNbWU
double MaintPlanningNbWU
double MaintNbWU
double IANbWu
double IAEffectiveCost
double IAWuValue
double TotalBlockingDuration
double TotalEscalatedDuration

double PartsDataEstimatedCosts = 0
double MaintPlanningEstimatedCosts = 0
double MaintEstimatedCosts = 0
long PartsDataTimeSpent = 0
long MaintPlanningTimeSpent = 0
long MaintTimeSpent = 0
double PartsDataEffectiveCostsOfTrigger = 0
double MaintPlanningEffectiveCostsOfTrigger = 0
double MaintEffectiveCostsOfTrigger = 0
String PartsDataTimeSpentString
String MaintPlanningTimeSpentString
String MaintTimeSpenString
String MaintValidated
String MaintPlanningValidated
String PartsDataValidated

long IATimeSpentCurrent
long AuthTimeSpentCurrent
long FormTimeSpentCurrent
long IntTimeSpentCurrent
double AuthEffectiveCost
double FormEffectiveCost
double IntEffectiveCost
double TriggerEffectiveCost

List<Group> CompanyAllowedList = new ArrayList<Group>()
List<Group> IntCompanyAllowedList = new ArrayList<Group>()
List<Group> AuthVerifCompanyAllowedList = new ArrayList<Group>()
List<Group> FormVerifCompanyAllowedList = new ArrayList<Group>()

//double IAEffectiveCost

String RestrictedDataTrigger = "No"
String RestrictedDataPWO0 = "No"

Date PartsData_Valdidation_1stDeliveryDate
Date MaintPlanning_Valdidation_1stDeliveryDate
Date Maint_Valdidation_1stDeliveryDate
Date PartsData_Valdidation_LastDeliveryDate
Date MaintPlanning_Valdidation_LastDeliveryDate
Date Maint_Valdidation_LastDeliveryDate

String ImpactOnTDResult

String TotalBlockingDurationTriggerString
double TotalBlockingDurationTrigger
double TotalBlockingDurationOld
String TotalEscalatedDurationTriggerString
double TotalEscalatedDurationTrigger
double TotalEscalatedDurationOld

String BlockedTrigger
String EscalatedTrigger

//Business
List<String> itemsChangedAtCreation = new ArrayList<String>()
itemsChangedAtCreation.add(FirstSNAppliField.getFieldName())
log.debug "issue = " + issue.getKey()

long eventId = event.getEventTypeId()

boolean isCreation = (boolean) workloadIsUpdated(changeLog, itemsChangedAtCreation)
log.debug "is creation : " + isCreation
if (isCreation || eventId == ID_Event_AdminReOpen) {//Creation
    if (issue.getIssueTypeId() == ID_IT_PWO1) {
        //Get linked issues
        def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssue in LinkedIssues) {
            //If linked issue is PWO0
            if (linkedIssue.getIssueTypeId() == ID_IT_PWO0) {
                RestrictedDataResult = linkedIssue.getCustomFieldValue(RestrictedDataField).toString()
                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
                for (LinkedIssueOfPWO in LinkedIssuesOfPWO0) {
                    if (LinkedIssueOfPWO.getIssueTypeId() == ID_IT_Cluster) {
                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPWO, user).getAllIssues()
                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                if(!RequestedDateTrigger || RequestedDateTrigger > (Date)LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)){
                                    RequestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)
                                    log.debug "Requested date trigger : " + RequestedDateTrigger
                                }
                            }
                        }
                    }
                }
            }
        }
        if (RequestedDateTrigger) {//Calculate date
            RequestedDate = (Timestamp) (RequestedDateTrigger - 98)
            IA_RequestdDate = (Timestamp) (RequestedDateTrigger - 105)
            IA_Validation_RequestdDate = (Timestamp) (RequestedDateTrigger - 98)
            IA_1stCommittedDate = IA_RequestdDate
            IA_LastCommittedDate = IA_1stCommittedDate
            IA_Validation_1stCommittedDate = IA_Validation_RequestdDate
            IA_Validation_LastCommittedDate = IA_Validation_1stCommittedDate
        }

        //Calculate effective costs
        if (issueIAWU && issue.getCustomFieldValue(IA_NbWUField)) {
            IAWuValue = (Double) issueIAWU.getCustomFieldValue(WUValueField)
            IANbWu = (Double) issue.getCustomFieldValue(IA_NbWUField)
            IAEffectiveCost = IAWuValue * IANbWu
        }
        if (issue.getCustomFieldValue(IA_AdditionalCostField)) {
            IAEffectiveCost = IAEffectiveCost + (double) issue.getCustomFieldValue(IA_AdditionalCostField)
        }

        //Set Data on PWO-1
        issue.setCustomFieldValue(RequestedDateField, RequestedDate)

        issue.setCustomFieldValue(IA_RequestdDateField, IA_RequestdDate)
        issue.setCustomFieldValue(IA_1stCommittedDateField, IA_1stCommittedDate)
        issue.setCustomFieldValue(IA_LastCommittedDateField, IA_LastCommittedDate)
        issue.setCustomFieldValue(IA_Validation_RequestdDateField, IA_Validation_RequestdDate)
        issue.setCustomFieldValue(IA_Validation_1stCommittedDateField, IA_Validation_1stCommittedDate)
        issue.setCustomFieldValue(IA_Validation_LastCommittedDateField, IA_Validation_LastCommittedDate)

        issue.setCustomFieldValue(IA_EffectiveCostField, IAEffectiveCost)

        def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue)
        def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
            it.toString() == RestrictedDataResult
        }
        issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)

        //Set costs data on Trigger
        def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
            if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                //Trigger
                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField)) {
                                                    PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField)
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField)) {
                                                    MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField)
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)) {
                                                    MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)
                                                }

                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField)) {
                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField)
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField)) {
                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField)
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField)) {
                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField)
                                                }

                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)) {
                                                            IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField)
                                                        } else {
                                                            IAEffectiveCost = 0
                                                        }

                                                        if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost
                                                        } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost
                                                        } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost
                                                        }

                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field).toString()
                                                                //Get PWO-2's subtask
                                                                def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                                                for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                                                    if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)) {
                                                                            AuthEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)
                                                                        }
                                                                        if (DomainOfPWO2 == "Parts Data") {
                                                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                        } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                        } else if (DomainOfPWO2 == "Maintenance") {
                                                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                        }
                                                                    } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)) {
                                                                            FormEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)
                                                                        }
                                                                        if (DomainOfPWO2 == "Parts Data") {
                                                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost
                                                                        } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost
                                                                        } else if (DomainOfPWO2 == "Maintenance") {
                                                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost
                                                                        }
                                                                    } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)) {
                                                                            IntEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)
                                                                        }
                                                                        if (DomainOfPWO2 == "Parts Data") {
                                                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost
                                                                        } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost
                                                                        } else if (DomainOfPWO2 == "Maintenance") {
                                                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //Set data on trigger
                                TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger
                                LinkedIssueOfCluster.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)
                                LinkedIssueOfCluster.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts)
                                LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts)
                                LinkedIssueOfCluster.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts)
                                LinkedIssueOfCluster.setCustomFieldValue(PartsData_EffectiveCostsField, PartsDataEffectiveCostsOfTrigger)
                                LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EffectiveCostsField, MaintPlanningEffectiveCostsOfTrigger)
                                LinkedIssueOfCluster.setCustomFieldValue(Maint_EffectiveCostsField, MaintEffectiveCostsOfTrigger)
                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                            }
                        }
                    }
                }
            }
        }
    }
} else {
    log.debug "edit"
    //Edit
    if (issue.getIssueTypeId() == ID_IT_PWO1) {
        List<String> itemsChangedRestrictedData = new ArrayList<String>()
        itemsChangedRestrictedData.add(RestrictedDataField.getFieldName())

        boolean isRestrictedDataChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedRestrictedData)
        if (isRestrictedDataChanged) {
            def LinkedIssuesOfCurrentPW01 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
            for (linkedIssueOfCurrentPWO1 in LinkedIssuesOfCurrentPW01) {
                if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                    def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                    for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                        if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_Cluster) {
                            def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                            for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                                if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_PWO0) {
                                    if (linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                        RestrictedDataTrigger = "Yes"
                                    } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                        RestrictedDataTrigger = "Unknown"
                                    }
                                    def LinkedIssuesOfLinkedPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                                    for (linkedIssueOfLinkedPWO0 in LinkedIssuesOfLinkedPWO0) {
                                        if (linkedIssueOfLinkedPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                            if (linkedIssueOfLinkedPWO0.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                                RestrictedDataTrigger = "Yes"
                                            } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfLinkedPWO0.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                                RestrictedDataTrigger = "Unknown"
                                            }
                                            def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfLinkedPWO0, user).getAllIssues()
                                            for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                    if (linkedIssueOfPWO1.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                                        RestrictedDataTrigger = "Yes"
                                                    } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfPWO1.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                                        RestrictedDataTrigger = "Unknown"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(linkedIssueOfPWO0)
                            def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
                                it.toString() == RestrictedDataTrigger
                            }

                            //Set Restricted data on Cluster
                            linkedIssueOfPWO0.setCustomFieldValue(RestrictedDataField, valueRestrictedData)
                            issueManager.updateIssue(user, linkedIssueOfPWO0, EventDispatchOption.DO_NOT_DISPATCH, false)
                            issueIndexingService.reIndex(linkedIssueOfPWO0)

                            //Set Restricted data on Triggers
                            def LinkedTriggersOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                            for (linkedTriggerOfCluster in LinkedTriggersOfCluster) {
                                if (linkedTriggerOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                    def fieldConfigRestrictedDataTrigger = RestrictedDataField.getRelevantConfig(linkedTriggerOfCluster)
                                    def valueRestrictedDataTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedDataTrigger)?.find {
                                        it.toString() == RestrictedDataTrigger
                                    }

                                    linkedTriggerOfCluster.setCustomFieldValue(RestrictedDataField, valueRestrictedDataTrigger)
                                    issueManager.updateIssue(user, linkedTriggerOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                    issueIndexingService.reIndex(linkedTriggerOfCluster)
                                }
                            }
                        }
                    }
                }
            }
        }

        List<String> itemsChangedImpactOnTD = new ArrayList<String>()
        itemsChangedImpactOnTD.add(ImpactConfirmedField.getFieldName())

        boolean isImpactOnTDChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedImpactOnTD)
        if (isImpactOnTDChanged) {
            def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
            for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                    def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                    for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                            def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                            for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                    //Trigger
                                    def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                    for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                        if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                            def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                            for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                    def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                    for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                        if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                            ImpactOnTDResult = "No"
                                                        }
                                                    }
                                                    for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                        if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(ImpactConfirmedField).toString() == "Yes") {
                                                                ImpactOnTDResult = "Yes"
                                                            } else if (ImpactOnTDResult != "Yes" && linkedIssueOfPWO0.getCustomFieldValue(ImpactConfirmedField).toString() == "Analysis not completed") {
                                                                ImpactOnTDResult = "Analysis not completed"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //Set Trigger Impact on TD
                                    if (ImpactOnTDResult) {
                                        def fieldConfigImpactOnTD = ImpactOnTDField.getRelevantConfig(LinkedIssueOfCluster)
                                        def valueImpactOnTD = ComponentAccessor.optionsManager.getOptions(fieldConfigImpactOnTD)?.find {
                                            it.toString() == ImpactOnTDResult
                                        }
                                        LinkedIssueOfCluster.setCustomFieldValue(ImpactOnTDField, valueImpactOnTD)
                                        issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                        issueIndexingService.reIndex(LinkedIssueOfCluster)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        //Calculate Techical Closure date
        List<String> itemsChangedIAValidatedImpactConfirmed = new ArrayList<String>()
        itemsChangedIAValidatedImpactConfirmed.add(IA_ValidatedField.getFieldName())
        itemsChangedIAValidatedImpactConfirmed.add(ImpactConfirmedField.getFieldName())

        boolean isIAValidatedImpactConfirmedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedIAValidatedImpactConfirmed)
        if (isIAValidatedImpactConfirmedChanged) {
            if (issue.getCustomFieldValue(IA_ValidatedField).toString() == "Validated" && issue.getCustomFieldValue(ImpactConfirmedField).toString() != "Analysis not completed") {
                TechnicalClosure = TodaysDateCompare
                TechnicallyClosed = "Yes"
                def fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue)
                def valueTechnicallyClosed = ComponentAccessor.optionsManager.getOptions(fieldConfigTechnicallyClosed)?.find {
                    it.toString() == TechnicallyClosed
                }
                issue.setCustomFieldValue(TechnicallyClosedField, valueTechnicallyClosed)
                issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosure)
            } else {
                TechnicalClosure = null
                TechnicallyClosed = "No"
                def fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue)
                def valueTechnicallyClosed = ComponentAccessor.optionsManager.getOptions(fieldConfigTechnicallyClosed)?.find {
                    it.toString() == TechnicallyClosed
                }
                issue.setCustomFieldValue(TechnicallyClosedField, valueTechnicallyClosed)
                issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosure)
            }

            if (issue.getCustomFieldValue(IA_ValidatedField).toString() == "Validated" || issue.getCustomFieldValue(IA_ValidatedField).toString() == "Rejected") {
                if (!issue.getCustomFieldValue(IA_Validation_1stDeliveryDateField)) {
                    IA_Validation_1stDelivery = TodaysDateCompare
                    issue.setCustomFieldValue(IA_Validation_1stDeliveryDateField, IA_Validation_1stDelivery)
                }
                IA_Validation_LastDelivery = TodaysDateCompare
                issue.setCustomFieldValue(IA_Validation_LastDeliveryDateField, IA_Validation_LastDelivery)
            }

        }

        List<String> itemsChangedIAValidated = new ArrayList<String>()
        itemsChangedIAValidated.add(IA_ValidatedField.getFieldName())

        boolean isIAValidatedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedIAValidated)
        if (isIAValidatedChanged) {
            if (issue.getCustomFieldValue(IA_ValidatedField).toString() == "Rejected") {
                IA_ReworkDate = TodaysDateCompare + 7

                issue.setCustomFieldValue(IAReworkDateField, IA_ReworkDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
            }
        }


        //Calculate effective costs
        if (issueIAWU && issue.getCustomFieldValue(IA_NbWUField)) {
            IAWuValue = (Double) issueIAWU.getCustomFieldValue(WUValueField)
            IANbWu = (Double) issue.getCustomFieldValue(IA_NbWUField)
            IAEffectiveCost = IAWuValue * IANbWu
        }
        if (issue.getCustomFieldValue(IA_AdditionalCostField)) {
            IAEffectiveCost = IAEffectiveCost + (double) issue.getCustomFieldValue(IA_AdditionalCostField)
        }

        //Calculate value when Blocked is edited
        List<String> itemsChangedIABlocked = new ArrayList<String>()
        itemsChangedIABlocked.add(IA_BlockedField.getFieldName())

        boolean isIABlockedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedIABlocked)
        if (isIABlockedChanged) {
            if (issue.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                BlockEndDate = null
                issue.setCustomFieldValue(BlockEndDateField, BlockEndDate)
                //if (!issue.getCustomFieldValue(BlockStartDateField)){
                BlockStartDate = TodaysDateCompare
                issue.setCustomFieldValue(BlockStartDateField, BlockStartDate)

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                if (!issue.getCustomFieldValue(OldBlockStartDateField) || issue.getCustomFieldValue(OldBlockStartDateField) != TodaysDateCompare) {
                    log.debug "Old start date to update"
                    issue.setCustomFieldValue(OldBlockStartDateField, BlockStartDate)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }
                //}
                BlockedTrigger = "No"
                def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                        def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                        for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                        //Trigger
                                        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                        log.debug "PWO-0 : " + linkedIssueOfClusterTrigger.getKey()
                                                        //Get total blocking duration of PWO0
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockedField)) {
                                                            log.debug "PWO-0 " + linkedIssueOfClusterTrigger.getKey() + " is blocked"
                                                            BlockedTrigger = "Yes"
                                                        }
                                                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                //Get total blocking duration of PWO1
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                    BlockedTrigger = "Yes"
                                                                }
                                                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                            BlockedTrigger = "Yes"
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //Set data on trigger
                                        def fieldConfigBlockedTrigger = IA_BlockedField.getRelevantConfig(LinkedIssueOfCluster)
                                        def valueBlockedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigBlockedTrigger)?.find {
                                            it.toString() == BlockedTrigger
                                        }
                                        LinkedIssueOfCluster.setCustomFieldValue(IA_BlockedField, valueBlockedTrigger)
                                        issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                        issueIndexingService.reIndex(LinkedIssueOfCluster)
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                BlockEndDate = TodaysDateCompare
                issue.setCustomFieldValue(BlockEndDateField, BlockEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                Date OldBlockDate = (Date) issue.getCustomFieldValue(OldBlockEndDateField)

                if (!issue.getCustomFieldValue(OldBlockEndDateField) || issue.getCustomFieldValue(OldBlockEndDateField) != TodaysDateCompare) {
                    log.debug "Old end date to update"
                    issue.setCustomFieldValue(OldBlockEndDateField, BlockEndDate)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                    BlockStartDate = (Date) issue.getCustomFieldValue(BlockStartDateField)
                    if (issue.getCustomFieldValue(TotalBlockingDurationField)) {
                        TotalBlockingDurationOld = issue.getCustomFieldValue(TotalBlockingDurationField).replace(" d", "").toDouble()
                        if (issue.getCustomFieldValue(OldBlockStartDateField) == TodaysDateCompare || issue.getCustomFieldValue(OldBlockStartDateField) > OldBlockDate) {
                            TotalBlockingDuration = BlockEndDate - BlockStartDate + 1
                        } else {
                            TotalBlockingDuration = BlockEndDate - BlockStartDate
                        }
                        TotalBlockingDuration = TotalBlockingDurationOld + TotalBlockingDuration
                    } else {
                        TotalBlockingDuration = BlockEndDate - BlockStartDate + 1
                    }
                    TotalBlockingDuration = TotalBlockingDuration.trunc()
                    TotalBlockingDurationString = TotalBlockingDuration.trunc() + " d"

                    //issue.setCustomFieldValue(BlockEndDateField,BlockEndDate)
                    issue.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationString)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }


                TotalBlockingDurationTrigger = 0
                BlockedTrigger = "No"
                def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                        def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                        for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                        //Trigger
                                        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                        //Get total blocking duration of PWO0
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField)) {
                                                            TotalBlockingDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField)
                                                            TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                            TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockedField)) {
                                                            BlockedTrigger = "Yes"
                                                        }
                                                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                //Get total blocking duration of PWO1
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField)) {
                                                                    TotalBlockingDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField)
                                                                    TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                                    TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                                }
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                    BlockedTrigger = "Yes"
                                                                }
                                                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                            BlockedTrigger = "Yes"
                                                                        }
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField)) {
                                                                            TotalBlockingDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField)
                                                                            TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                                            TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //Set data on trigger
                                        def fieldConfigBlockedTrigger = IA_BlockedField.getRelevantConfig(LinkedIssueOfCluster)
                                        def valueBlockedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigBlockedTrigger)?.find {
                                            it.toString() == BlockedTrigger
                                        }
                                        LinkedIssueOfCluster.setCustomFieldValue(IA_BlockedField, valueBlockedTrigger)
                                        TotalBlockingDurationTrigger = TotalBlockingDurationTrigger.trunc()
                                        TotalBlockingDurationTriggerString = TotalBlockingDurationTrigger.trunc() + " d"
                                        LinkedIssueOfCluster.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationTriggerString)
                                        issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                        issueIndexingService.reIndex(LinkedIssueOfCluster)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //Calculate value when Esclated is edited
        List<String> itemsChangedEscalated = new ArrayList<String>()
        itemsChangedEscalated.add(EscalatedField.getFieldName())

        boolean isEscalatedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedEscalated)
        if (isEscalatedChanged) {
            if (issue.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                EscalateEndDate = null
                issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                if (!issue.getCustomFieldValue(OldEscalateStartDateField) || issue.getCustomFieldValue(OldEscalateStartDateField) != TodaysDateCompare) {
                    log.debug "Start date to update"
                    EscalateStartDate = TodaysDateCompare
                    issue.setCustomFieldValue(EscalateStartDateField, EscalateStartDate)
                    issue.setCustomFieldValue(OldEscalateStartDateField, EscalateStartDate)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }

                EscalatedTrigger = "No"
                def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                        def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                        for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                        //Trigger
                                        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                        log.debug "PWO-0 : " + linkedIssueOfClusterTrigger.getKey()
                                                        //Get total blocking duration of PWO0
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField)) {
                                                            EscalatedTrigger = "Yes"
                                                        }
                                                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                //Get total blocking duration of PWO1
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                    EscalatedTrigger = "Yes"
                                                                }
                                                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                            EscalatedTrigger = "Yes"
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        def fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(LinkedIssueOfCluster)
                                        def valueEscalatedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigEscalatedTrigger)?.find {
                                            it.toString() == EscalatedTrigger
                                        }
                                        LinkedIssueOfCluster.setCustomFieldValue(EscalatedField, valueEscalatedTrigger)
                                        issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                        issueIndexingService.reIndex(LinkedIssueOfCluster)
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                EscalateEndDate = TodaysDateCompare
                issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                EscalateStartDate = (Date) issue.getCustomFieldValue(EscalateStartDateField)
                Date OldEscalatedDate = (Date) issue.getCustomFieldValue(OldEscalateEndDateField)
                log.debug "old escalated date : " + OldEscalatedDate
                log.debug "todaysdate : " + TodaysDateCompare

                if (!issue.getCustomFieldValue(OldEscalateEndDateField) || issue.getCustomFieldValue(OldEscalateEndDateField) != TodaysDateCompare) {
                    log.debug "set old date"
                    issue.setCustomFieldValue(OldEscalateEndDateField, EscalateEndDate)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                    if (issue.getCustomFieldValue(TotalEscalatedDurationField)) {
                        TotalEscalatedDurationOld = issue.getCustomFieldValue(TotalEscalatedDurationField).replace(" d", "").toDouble()
                        if (issue.getCustomFieldValue(OldEscalateStartDateField) == TodaysDateCompare || issue.getCustomFieldValue(OldEscalateStartDateField) > OldEscalatedDate) {
                            TotalEscalatedDuration = EscalateEndDate - EscalateStartDate + 1
                        } else {
                            TotalEscalatedDuration = EscalateEndDate - EscalateStartDate
                        }
                        TotalEscalatedDuration = TotalEscalatedDurationOld + TotalEscalatedDuration
                    } else {
                        TotalEscalatedDuration = EscalateEndDate - EscalateStartDate + 1
                    }
                    TotalEscalatedDuration = TotalEscalatedDuration.trunc()
                    TotalEscalatedDurationString = TotalEscalatedDuration.trunc() + " d"

                    issue.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationString)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                }

                TotalEscalatedDurationTrigger = 0
                EscalatedTrigger = "No"
                def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                    if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                        def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                        for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                        //Trigger
                                        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                        //Get total blocking duration of PWO0
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                            TotalEscalatedDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField)
                                                            TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                            TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                            EscalatedTrigger = "Yes"
                                                        }
                                                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                //Get total blocking duration of PWO1
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                                    TotalEscalatedDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField)
                                                                    TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                                    TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                                }
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                    EscalatedTrigger = "Yes"
                                                                }
                                                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                            EscalatedTrigger = "Yes"
                                                                        }
                                                                        if (linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                                            TotalEscalatedDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField)
                                                                            TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                                            TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //Set data on trigger

                                        def fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(LinkedIssueOfCluster)
                                        def valueEscalatedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigEscalatedTrigger)?.find {
                                            it.toString() == EscalatedTrigger
                                        }
                                        LinkedIssueOfCluster.setCustomFieldValue(EscalatedField, valueEscalatedTrigger)
                                        TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger.trunc()
                                        TotalEscalatedDurationTriggerString = TotalEscalatedDurationTrigger.trunc() + " d"
                                        LinkedIssueOfCluster.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationTriggerString)
                                        issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                        issueIndexingService.reIndex(LinkedIssueOfCluster)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        issue.setCustomFieldValue(IA_EffectiveCostField, IAEffectiveCost)


        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)


        //Set costs data on Trigger
        def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
            if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                //Reset variable
                                PartsDataTimeSpent=0
                                MaintPlanningTimeSpent=0
                                MaintTimeSpent=0
                                PartsDataEstimatedCosts=0
                                MaintPlanningEstimatedCosts=0
                                MaintEstimatedCosts=0
                                //Trigger
                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField)) {
                                                    PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField)
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField)) {
                                                    MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField)
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)) {
                                                    MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)
                                                }

                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField)) {
                                                    PartsDataTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField).toString())
                                                    PartsDataTimeSpent = PartsDataTimeSpent + PartsDataTimeSpentCurrent
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField)) {
                                                    MaintPlanningTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField).toString())
                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + MaintPlanningTimeSpentCurrent
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField)) {
                                                    MaintTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField).toString())
                                                    MaintTimeSpent = MaintTimeSpent + MaintTimeSpentCurrent
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField)) {
                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField)
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField)) {
                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField)
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField)) {
                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField)
                                                }

                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                        log.debug "PWO1 : " + linkedIssueOfPWO0.getKey()
                                                        log.debug "domain of PWO-1 : " + linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field)
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                            IATimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField).toString())
                                                        } else {
                                                            IATimeSpentCurrent = 0
                                                        }
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)) {
                                                            IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField)
                                                        } else {
                                                            IAEffectiveCost = 0
                                                        }

                                                        if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                                PartsDataTimeSpent = PartsDataTimeSpent + IATimeSpentCurrent
                                                            }
                                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost
                                                        } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                                MaintPlanningTimeSpent = MaintPlanningTimeSpent + IATimeSpentCurrent
                                                            }
                                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost
                                                        } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                                MaintTimeSpent = MaintTimeSpent + IATimeSpentCurrent
                                                            }
                                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost
                                                        }

                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field).toString()
                                                                //Get PWO-2's subtask
                                                                def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                                                for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                                                    if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField)) {
                                                                            AuthTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField).toString())
                                                                            //AuthTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField)
                                                                        } else {
                                                                            AuthTimeSpentCurrent = 0
                                                                        }
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)) {
                                                                            AuthEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)
                                                                        }
                                                                        if (DomainOfPWO2 == "Parts Data"){
                                                                            if(AuthEffectiveCost != 0) {
                                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                            }
                                                                            PartsDataTimeSpent = PartsDataTimeSpent + AuthTimeSpentCurrent
                                                                        } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                            if (AuthEffectiveCost != 0){
                                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                            }
                                                                            MaintPlanningTimeSpent = MaintPlanningTimeSpent + AuthTimeSpentCurrent
                                                                        } else if (DomainOfPWO2 == "Maintenance"){
                                                                            if(AuthEffectiveCost != 0) {
                                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                            }
                                                                            MaintTimeSpent = MaintTimeSpent + AuthTimeSpentCurrent
                                                                        }
                                                                    } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField)) {
                                                                            FormTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField).toString())
                                                                            //FormTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField)
                                                                        } else {
                                                                            FormTimeSpentCurrent = 0
                                                                        }
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)) {
                                                                            FormEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)
                                                                        }
                                                                        if (DomainOfPWO2 == "Parts Data"){
                                                                            if(FormEffectiveCost != 0) {
                                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost
                                                                            }
                                                                            PartsDataTimeSpent = PartsDataTimeSpent + FormTimeSpentCurrent
                                                                        } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                            if(FormEffectiveCost != 0) {
                                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost
                                                                            }
                                                                            MaintPlanningTimeSpent = MaintPlanningTimeSpent + FormTimeSpentCurrent
                                                                        } else if (DomainOfPWO2 == "Maintenance"){
                                                                            if(FormEffectiveCost != 0) {
                                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost
                                                                            }
                                                                            MaintTimeSpent = MaintTimeSpent + FormTimeSpentCurrent
                                                                        }
                                                                    } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField)) {
                                                                            IntTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField).toString())
                                                                            //IntTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField)
                                                                        } else {
                                                                            IntTimeSpentCurrent = 0
                                                                        }
                                                                        if (linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)) {
                                                                            IntEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)
                                                                        }
                                                                        if (DomainOfPWO2 == "Parts Data"){
                                                                            if(IntEffectiveCost != 0) {
                                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost
                                                                            }
                                                                            PartsDataTimeSpent = PartsDataTimeSpent + IntTimeSpentCurrent
                                                                        } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                            if(IntEffectiveCost != 0) {
                                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost
                                                                            }
                                                                            MaintPlanningTimeSpent = MaintPlanningTimeSpent + IntTimeSpentCurrent
                                                                        } else if (DomainOfPWO2 == "Maintenance"){
                                                                            if(IntEffectiveCost != 0) {
                                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost
                                                                            }
                                                                            MaintTimeSpent = MaintTimeSpent + IntTimeSpentCurrent
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //Set data on trigger
                                TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger
                                LinkedIssueOfCluster.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)
                                LinkedIssueOfCluster.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts)
                                LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts)
                                LinkedIssueOfCluster.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts)
                                LinkedIssueOfCluster.setCustomFieldValue(PartsData_EffectiveCostsField, PartsDataEffectiveCostsOfTrigger)
                                LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EffectiveCostsField, MaintPlanningEffectiveCostsOfTrigger)
                                LinkedIssueOfCluster.setCustomFieldValue(Maint_EffectiveCostsField, MaintEffectiveCostsOfTrigger)
                                PartsDataTimeSpentString = formatSecondesValueInHoursMinutes(PartsDataTimeSpent)
                                LinkedIssueOfCluster.setCustomFieldValue(PartsData_TimeSpentField, PartsDataTimeSpentString)
                                MaintPlanningTimeSpentString = formatSecondesValueInHoursMinutes(MaintPlanningTimeSpent)
                                LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_TimeSpentField, MaintPlanningTimeSpentString)
                                MaintTimeSpenString = formatSecondesValueInHoursMinutes(MaintTimeSpent)
                                LinkedIssueOfCluster.setCustomFieldValue(Maint_TimeSpentField, MaintTimeSpenString)
                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                            }
                        }
                    }
                }
            }
        }


        List<String> itemsChangedIACompany = new ArrayList<String>()
        itemsChangedRestrictedData.add(IACompanyField.getFieldName())

        boolean isIACompanyChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedRestrictedData)
        log.debug "IA Company is changed : " + isIACompanyChanged
        if (isIACompanyChanged) {
            def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
            for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                if (linkedIssueOfPWO1.getProjectId() == ID_PR_AWO) {
                    log.debug "AWO : " + linkedIssueOfPWO1
                    def LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO1, user).getAllIssues()
                    for (linkedIssueOfAWOForCompany in LinkedIssuesOfAWOForCompany) {
                        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO1) {
                            log.debug "PWO1 found : " + linkedIssueOfAWOForCompany
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                                String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField)
                                String IA_Company_Formatted = "Partner_" + IA_Company_Value

                                Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted)
                                log.debug "Adding group : " + IA_Company_Management_Formatted_Group

                                if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(IA_Company_Management_Formatted_Group)
                                }
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO21) {
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != "AIRBUS") {
                                String Auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField)
                                String Auth_Company_Formatted = "Partner_" + Auth_Company_Value

                                Group Auth_Company_Management_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted)

                                if (!CompanyAllowedList.contains(Auth_Company_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Auth_Company_Management_Formatted_Group)
                                }
                            }
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != "AIRBUS") {
                                String Auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField)
                                String Auth_VerifCompany_Formatted = "Partner_" + Auth_VerifCompany_Value

                                Group Auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Auth_VerifCompany_Formatted)

                                if (!CompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group)
                                }
                                if (!AuthVerifCompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                    AuthVerifCompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group)
                                }
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO22) {
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != "AIRBUS") {
                                String Form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField)
                                String Form_Company_Formatted = "Partner_" + Form_Company_Value

                                Group Form_Company_Management_Formatted_Group = groupManager.getGroup(Form_Company_Formatted)

                                if (!CompanyAllowedList.contains(Form_Company_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Form_Company_Management_Formatted_Group)
                                }
                            }
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != "AIRBUS") {
                                String Form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField)
                                String Form_VerifCompany_Formatted = "Partner_" + Form_VerifCompany_Value

                                Group Form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Form_VerifCompany_Formatted)

                                if (!CompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group)
                                }
                                if (!FormVerifCompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                    FormVerifCompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group)
                                }
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO23) {
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != "AIRBUS") {
                                String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField)
                                String Int_Company_Formatted = "Partner_" + Int_Company_Value

                                Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted)

                                if (!CompanyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Int_Company_Management_Formatted_Group)
                                    IntCompanyAllowedList.add(Int_Company_Management_Formatted_Group)
                                }
                            }
                        }
                    }
                    if (CompanyAllowedList != null) {
                        linkedIssueOfPWO1.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList)
                        if (AuthVerifCompanyAllowedList != null) {
                            issue.setCustomFieldValue(AuthVerifCompanyAllowedField, AuthVerifCompanyAllowedList)
                        }
                        if (FormVerifCompanyAllowedList != null) {
                            issue.setCustomFieldValue(FormVerifCompanyAllowedField, FormVerifCompanyAllowedList)
                        }
                        if (IntCompanyAllowedList != null) {
                            issue.setCustomFieldValue(IntCompanyAllowedField, IntCompanyAllowedList)
                        }
                        issueManager.updateIssue(user, linkedIssueOfPWO1, EventDispatchOption.DO_NOT_DISPATCH, false)
//JIRA7
                        issueIndexingService.reIndex(linkedIssueOfPWO1)//JIRA7
                        CompanyAllowedList.clear()
                        AuthVerifCompanyAllowedList.clear()
                        FormVerifCompanyAllowedList.clear()
                        IntCompanyAllowedList.clear()
                    }
                }
            }
        }
    }
}
