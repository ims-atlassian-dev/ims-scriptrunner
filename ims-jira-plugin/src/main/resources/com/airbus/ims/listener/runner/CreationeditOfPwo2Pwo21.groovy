package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Creation/Edit of PWO-2 / PWO-2.1
 * Events : Admin re-open, PWO-2 updated, PWO-2.1 updated, Issue Created
 * isDisabled : false
 */

import com.atlassian.crowd.embedded.api.Group
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.user.ApplicationUser
import org.joda.time.LocalDate

import java.sql.Timestamp


boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

String formatSecondesValueInHoursMinutes(Long secondes) {
    if (secondes && secondes > 0) {
        String result
        int hoursExtracted = (int) (secondes / 3600)
        int totalMinutes = (int) (secondes / 60)
        int minutesExtracted = (int) totalMinutes.mod(60)
        result = hoursExtracted.toString() + "h " + minutesExtracted.toString() + "m"
        return result
    } else {
    }
}

Long formatHoursMinutesFormatInSecondes(String hoursMinutes) {
    if (!hoursMinutes.equals("null")) {
        Long result = 0L
        String[] hoursMinutesTable
        Long hourConvertedInMinutes = 0
        String hourNumberString
        Long minutesNumber = 0

        hoursMinutesTable = hoursMinutes.split("h")
        hourConvertedInMinutes = hoursMinutesTable[0].toString().toLong() * 60
        minutesNumber = hoursMinutesTable[1].toString().substring(0, hoursMinutesTable[1].toString().length() - 1).toLong()
        result = (hourConvertedInMinutes + minutesNumber) * 60
        return result
    } else {
    }
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog
GroupManager groupManager = ComponentAccessor.getGroupManager()


MutableIssue issue = (MutableIssue) event.getIssue()
def changeItems = ComponentAccessor.changeHistoryManager.getAllChangeItems(issue)

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"
String ID_IT_Cluster = "10001"
String ID_IT_Trigger = "10000"

List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

long ID_PR_AWO = 10003
long ID_Event_AdminReOpen = 10400

//Fields
long ID_CF_IA_Company = 10142
long ID_CF_CompanyAllowed = 10702
long ID_CF_Auth_Company = 10173
long ID_CF_Auth_VerifCompany = 10181
long ID_CF_Form_Company = 10281
long ID_CF_Form_VerifCompany = 10289
long ID_CF_Int_Company = 10324
long ID_CF_CompanyAuthVerif = 11130
long ID_CF_CompanyFormVerif = 10703
long ID_CF_IntCompany = 11200

long ID_CF_FirstSNAppli = 10011
long ID_CF_RequestedDate = 10010
long ID_CF_Auth_Verified = 10183
long ID_CF_Auth_Verification_1stDeliveryDate = 10186
long ID_CF_Auth_Verification_LastDeliveryDate = 10187
long ID_CF_Auth_Validated = 10189
long ID_CF_Auth_Validation_1stDeliveryDate = 10194
long ID_CF_Auth_Validation_LastDeliveryDate = 10195
long ID_CF_Auth_LastCommittedDate = 10177
long ID_CF_Auth_Verification_LastCommittedDate = 10185
long ID_CF_Auth_Validation_LastCommittedDate = 10193
long ID_CF_TechnicallyClosed = 10054
long ID_CF_TechnicalClosureDate = 10040
long ID_CF_Auth_RequestedDate = 10175
long ID_CF_Auth_Verification_RequestedDate = 10192
long ID_CF_Auth_Validation_RequestedDate = 10190
long ID_CF_Auth_FirstCommittedDate = 10176
long ID_CF_Auth_Verification_FirstCommittedDate = 10184
long ID_CF_Auth_Validation_FirstCommittedDate = 10191
long ID_CF_Auth_ReworkDate = 11123
long ID_CF_Auth_Verification_ReworkDate = 11124

long ID_CF_Blocked = 10138
long ID_CF_BlockedDomain = 10041
long ID_CF_BlockStartDate = 10043
long ID_CF_OldBlockStartDate = 11800
long ID_CF_BlockEndDate = 10044
long ID_CF_OldBlockEndDate = 12000
long ID_CF_Escalated = 10055
long ID_CF_EscalateStartDate = 10047
long ID_CF_OldEscalateStartDate = 12100
long ID_CF_EscalateEndDate = 10048
long ID_CF_OldEscalateEndDate = 12101
long ID_CF_TotalBlockingDuration = 11011
long ID_CF_TotalEscalatedDuration = 11019
long ID_CF_Domain = 10139

long ID_CF_Form_RequestedDate = 10283
long ID_CF_Form_Verification_RequestedDate = 10292
long ID_CF_Form_Validation_RequestedDate = 10299
long ID_CF_Form_1stCommittedDate = 10284
long ID_CF_Form_Verification_1stCommittedDate = 10293
long ID_CF_Form_Validation_1stCommittedDate = 10300
long ID_CF_Form_LastCommittedDate = 10285
long ID_CF_Form_Verification_LastCommittedDate = 10294
long ID_CF_Form_Validation_LastCommittedDate = 10301
long ID_CF_Form_Verified = 10291
long ID_CF_Form_Validated = 10298
long ID_CF_Form_Verified_1stDelivery = 10295
long ID_CF_Form_Verified_LastDelivery = 10296
long ID_CF_Form_Validation_1stDelivery = 10302
long ID_CF_Form_Validation_LastDelivery = 10303
long ID_CF_Form_ReworkDate = 11125
long ID_CF_Form_Verification_ReworkDate = 11126

long ID_CF_Int_RequestedDate = 10326
long ID_CF_Int_1stCommittedDate = 10327
long ID_CF_Int_LastCommittedDate = 10328

long ID_CF_PartsData_TimeSpent = 11012
long ID_CF_MaintPlanning_TimeSpent = 11013
long ID_CF_Maint_TimeSpent = 11014
long ID_CF_IA_TimeSpent = 11017
long ID_CF_DomainPWO1 = 10139
long ID_CF_Auth_TimeSpent = 11015
long ID_CF_Form_TimeSpent = 11016
long ID_CF_Int_TimeSpent = 11018

long ID_CF_RestrictedData = 10800


//CustomField
CustomField IACompanyField = customFieldManager.getCustomFieldObject(ID_CF_IA_Company)
CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_CompanyAllowed)
CustomField AuthCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Company)
CustomField AuthVerifCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Auth_VerifCompany)
CustomField FormCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Form_Company)
CustomField FormVerifCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Form_VerifCompany)
CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Int_Company)
CustomField AuthVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_CompanyAuthVerif)
CustomField FormVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_CompanyFormVerif)
CustomField IntCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_IntCompany)


CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ID_CF_RestrictedData)

CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)
CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_RequestedDate)
CustomField Auth_VerifiedField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verified)
CustomField Auth_Verified_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_1stDeliveryDate)
CustomField Auth_Verified_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_LastDeliveryDate)
CustomField Auth_ValidatedField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validated)
CustomField Auth_Validation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_1stDeliveryDate)
CustomField Auth_Validation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_LastDeliveryDate)
CustomField Auth_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_LastCommittedDate)
CustomField Auth_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_LastCommittedDate)
CustomField Auth_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_LastCommittedDate)
CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ID_CF_TechnicallyClosed)
CustomField TechnicalClosureDateField = customFieldManager.getCustomFieldObject(ID_CF_TechnicalClosureDate)
CustomField Auth_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_RequestedDate)
CustomField Auth_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_RequestedDate)
CustomField Auth_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_RequestedDate)
CustomField Auth_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_FirstCommittedDate)
CustomField Auth_Verification_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_FirstCommittedDate)
CustomField Auth_Validation_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_FirstCommittedDate)
CustomField Auth_ReworkDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_ReworkDate)
CustomField Auth_Verification_ReworkDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_ReworkDate)

CustomField BlockedField = customFieldManager.getCustomFieldObject(ID_CF_Blocked)
CustomField BlockeDomainField = customFieldManager.getCustomFieldObject(ID_CF_BlockedDomain)
CustomField BlockStartDateField = customFieldManager.getCustomFieldObject(ID_CF_BlockStartDate)
CustomField OldBlockStartDateField = customFieldManager.getCustomFieldObject(ID_CF_OldBlockStartDate)
CustomField BlockEndDateField = customFieldManager.getCustomFieldObject(ID_CF_BlockEndDate)
CustomField OldBlockEndDateField = customFieldManager.getCustomFieldObject(ID_CF_OldBlockEndDate)
CustomField EscalatedField = customFieldManager.getCustomFieldObject(ID_CF_Escalated)
CustomField EscalateStartDateField = customFieldManager.getCustomFieldObject(ID_CF_EscalateStartDate)
CustomField OldEscalateStartDateField = customFieldManager.getCustomFieldObject(ID_CF_OldEscalateStartDate)
CustomField EscalateEndDateField = customFieldManager.getCustomFieldObject(ID_CF_EscalateEndDate)
CustomField OldEscalateEndDateField = customFieldManager.getCustomFieldObject(ID_CF_OldEscalateEndDate)
CustomField TotalBlockingDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalBlockingDuration)
CustomField TotalEscalatedDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalEscalatedDuration)
CustomField DomainField = customFieldManager.getCustomFieldObject(ID_CF_Domain)

CustomField Form_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_RequestedDate)
CustomField Form_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_RequestedDate)
CustomField Form_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_RequestedDate)
CustomField Form_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_1stCommittedDate)
CustomField Form_Verification_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_1stCommittedDate)
CustomField Form_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_1stCommittedDate)
CustomField Form_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_LastCommittedDate)
CustomField Form_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_LastCommittedDate)
CustomField Form_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_LastCommittedDate)
CustomField Form_VerifiedField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verified)
CustomField Form_ValidatedField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validated)
CustomField Form_Verified_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verified_1stDelivery)
CustomField Form_Verified_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verified_LastDelivery)
CustomField Form_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_1stDelivery)
CustomField Form_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_LastDelivery)
CustomField Form_ReworkDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_ReworkDate)
CustomField Form_Verification_ReworkDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_ReworkDate)

CustomField Int_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_RequestedDate)
CustomField Int_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_1stCommittedDate)
CustomField Int_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_LastCommittedDate)

CustomField PartsData_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_TimeSpent)
CustomField MaintPlanning_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_TimeSpent)
CustomField Maint_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Maint_TimeSpent)
CustomField IATimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_IA_TimeSpent)
CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ID_CF_DomainPWO1)
CustomField AuthTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Auth_TimeSpent)
CustomField FormTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Form_TimeSpent)
CustomField IntTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Int_TimeSpent)


//Variables
Date RequestedDateTrigger
Date RequestedDate
Date AuthVerification1stDeliveryDate
Date AuthVerificationLastDeliveryDate
Date AuthValidation1stDeliveryDate
Date AuthValidationLastDeliveryDate
Date Auth_LastCommittedDate
Date Auth_Verification_LastCommittedDate
Date Auth_Validation_LastCommittedDate
Date TechnicalClosureDate
Date Auth_RequestedDate
Date Auth_Verification_RequestedDate
Date Auth_Validation_RequestedDate
Date Auth_FirstCommittedDate
Date Auth_Verification_FirstCommittedDate
Date Auth_Validation_FirstCommittedDate
Date Auth_ReworkDate
Date Auth_Verification_ReworkDate
String TechnicallyClosed = "No"
String TotalBlockingDurationString
String TotalEscalatedDurationString
Date BlockStartDate
Date BlockEndDate
Date EscalateStartDate
Date EscalateEndDate
Date LastBlockingEndDate

Date Form_RequestedDate
Date Form_Verification_RequestedDate
Date Form_Validation_RequestedDate
Date Form_1stCommittedDate
Date Form_Verification_1stCommittedDate
Date Form_Validation_1stCommittedDate
Date Form_LastCommittedDate
Date Form_Verification_LastCommittedDate
Date Form_Validation_LastCommittedDate
Date Form_Verification1stDeliveryDate
Date Form_VerificationLastDeliveryDate
Date FormValidation1stDeliveryDate
Date FormValidationLastDeliveryDate
Date Form_ReworkDate
Date Form_Verification_ReworkDate
Date Int_RequestedDate
Date Int_FirstCommittedDate
Date Int_LastCommittedDate

double TotalBlockingDuration
double TotalEscalatedDuration
String TotalBlockingDurationTriggerString
double TotalBlockingDurationTrigger
double TotalBlockingDurationOld
String TotalEscalatedDurationTriggerString
double TotalEscalatedDurationTrigger
double TotalEscalatedDurationOld
String BlockedTrigger
String EscalatedTrigger
String Domain
String RestrictedDataTrigger = "No"

Long PartsDataTimeSpentCurrent
Long MaintPlanningTimeSpentCurrent
Long MaintTimeSpentCurrent
Long IATimeSpentCurrent
Long AuthTimeSpentCurrent
Long FormTimeSpentCurrent
Long IntTimeSpentCurrent
Long PartsDataTimeSpent = 0
Long MaintPlanningTimeSpent = 0
Long MaintTimeSpent = 0
String PartsDataTimeSpentString
String MaintPlanningTimeSpentString
String MaintTimeSpentString

List<Group> CompanyAllowedList = new ArrayList<Group>()
List<Group> AuthVerifCompanyAllowedList = new ArrayList<Group>()
List<Group> FormVerifCompanyAllowedList = new ArrayList<Group>()
List<Group> IntCompanyAllowedList = new ArrayList<Group>()

Date TodayDate = new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
long eventId = event.getEventTypeId()
Double Compteur = 0

//Business
if (issue.getIssueTypeId() == ID_IT_PWO2) {
    List<String> itemsChangedAtCreation = new ArrayList<String>()
    itemsChangedAtCreation.add(FirstSNAppliField.getFieldName())
    log.debug "issue = " + issue.getKey()

    boolean isCreation = (boolean) workloadIsUpdated(changeLog, itemsChangedAtCreation)
    log.debug "is creation : " + isCreation
    if (isCreation || eventId == ID_Event_AdminReOpen) {//Creation
        def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssue in LinkedIssues) {
            if (linkedIssue.getIssueTypeId() == ID_IT_PWO1) {
                log.debug "PWO-1 : " + linkedIssue.getKey()
                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
                for (LinkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                    if (LinkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO0) {
                        log.debug "PWO-0 : " + LinkedIssueOfPWO1.getKey()
                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfPWO1, user).getAllIssues()
                        for (LinkedIssueOfPW0 in LinkedIssuesOfPWO0) {
                            if (LinkedIssueOfPW0.getIssueTypeId() == ID_IT_Cluster) {
                                log.debug "Cluster : " + LinkedIssueOfPW0.getKey()
                                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPW0, user).getAllIssues()
                                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                        log.debug "Trigger : " + LinkedIssueOfCluster.getKey()
                                        if(!RequestedDateTrigger || RequestedDateTrigger > (Date)LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)){
                                            RequestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)
                                            log.debug "Requested date trigger : " + RequestedDateTrigger
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (RequestedDateTrigger) {
            RequestedDate = (Timestamp) (RequestedDateTrigger - 28)

            issue.setCustomFieldValue(RequestedDateField, RequestedDate)
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    } else {//Edit
        List<String> itemsChangedRestrictedData = new ArrayList<String>()
        itemsChangedRestrictedData.add(RestrictedDataField.getFieldName())

        boolean isRestrictedDataChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedRestrictedData)
        if (isRestrictedDataChanged) {
            def LinkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
            for (linkedIssueOfCurrentPWO2 in LinkedIssuesOfCurrentPWO2) {
                if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ID_IT_PWO1) {
                    def LinkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues()
                    for (linkedIssueOfCurrentPWO1 in LinkedIssuesOfCurrentPWO1) {
                        if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                            def LinkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                            for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO0) {
                                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                    log.debug "Cluster is : " + linkedIssueOfCurrentPWO0.getKey()
                                    def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                    for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                                        if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_PWO0) {
                                            if (linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                                RestrictedDataTrigger = "Yes"
                                            } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                                RestrictedDataTrigger = "Unknown"
                                            }
                                            def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                                            for (linkedIssueOfPWO in LinkedIssuesOfPWO0) {
                                                if (linkedIssueOfPWO.getIssueTypeId() == ID_IT_PWO1) {
                                                    if (linkedIssueOfPWO.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                                        RestrictedDataTrigger = "Yes"
                                                    } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfPWO.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                                        RestrictedDataTrigger = "Unknown"
                                                    }
                                                    def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO, user).getAllIssues()
                                                    for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                            if (linkedIssueOfPWO1.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                                                RestrictedDataTrigger = "Yes"
                                                            } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfPWO1.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                                                RestrictedDataTrigger = "Unknown"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(linkedIssueOfCurrentPWO0)
                                def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
                                    it.toString() == RestrictedDataTrigger
                                }

                                //Set Restricted data on Cluster
                                linkedIssueOfCurrentPWO0.setCustomFieldValue(RestrictedDataField, valueRestrictedData)
                                issueManager.updateIssue(user, linkedIssueOfCurrentPWO0, EventDispatchOption.DO_NOT_DISPATCH, false)
                                issueIndexingService.reIndex(linkedIssueOfCurrentPWO0)

                                //Set Restricted data on Triggers
                                def LinkedTriggersOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                for (linkedTriggerOfCluster in LinkedTriggersOfCluster) {
                                    if (linkedTriggerOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                        def fieldConfigRestrictedDataTrigger = RestrictedDataField.getRelevantConfig(linkedTriggerOfCluster)
                                        def valueRestrictedDataTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedDataTrigger)?.find {
                                            it.toString() == RestrictedDataTrigger
                                        }

                                        linkedTriggerOfCluster.setCustomFieldValue(RestrictedDataField, valueRestrictedDataTrigger)
                                        issueManager.updateIssue(user, linkedTriggerOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                        issueIndexingService.reIndex(linkedTriggerOfCluster)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        List<String> itemsChangedDomain = new ArrayList<String>()
        itemsChangedDomain.add(DomainField.getFieldName())

        boolean isDomainChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedDomain)
        if (isDomainChanged) {
            Domain = issue.getCustomFieldValue(DomainField)
            Collection<Issue> subTasks = issue.getSubTaskObjects()
            for (subTask in subTasks) {
                def fieldConfigDomain = DomainField.getRelevantConfig(subTask)
                def valueDomain = ComponentAccessor.optionsManager.getOptions(fieldConfigDomain)?.find {
                    it.toString() == Domain
                }
                subTask.setCustomFieldValue(DomainField, valueDomain)
                issueManager.updateIssue(user, subTask, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(subTask)
            }
        }

        List<String> itemsChangedBlocked = new ArrayList<String>()
        itemsChangedBlocked.add(BlockedField.getFieldName())

        boolean isBlockedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedBlocked)
        if (isBlockedChanged) {
            if (issue.getCustomFieldValue(BlockedField).toString() == "Yes") {
                BlockEndDate = null
                issue.setCustomFieldValue(BlockEndDateField, BlockEndDate)
                BlockStartDate = TodaysDate
                issue.setCustomFieldValue(BlockStartDateField, BlockStartDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                if (!issue.getCustomFieldValue(OldBlockStartDateField) || issue.getCustomFieldValue(OldBlockStartDateField).toString().substring(0, 10) != TodaysDate.toString().substring(0, 10)) {
                    log.debug "Old start date to update"
                    issue.setCustomFieldValue(OldBlockStartDateField, BlockStartDate)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }
                BlockedTrigger = "No"
                def LinkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO2 in LinkedIssuesOfCurrentPWO2) {
                    if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ID_IT_PWO1) {
                        def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues()
                        for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                            if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                                //Trigger
                                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                                //Get total blocking duration of PWO0
                                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockeDomainField)) {
                                                                    BlockedTrigger = "Yes"
                                                                }
                                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                        //Get total blocking duration of PWO1
                                                                        if (linkedIssueOfPWO0.getCustomFieldValue(BlockedField).toString() == "Yes") {
                                                                            BlockedTrigger = "Yes"
                                                                        }
                                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                                if (linkedIssueOfPWO1.getCustomFieldValue(BlockedField).toString() == "Yes") {
                                                                                    BlockedTrigger = "Yes"
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //Set data on trigger
                                                def fieldConfigBlockedTrigger = BlockedField.getRelevantConfig(LinkedIssueOfCluster)
                                                def valueBlockedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigBlockedTrigger)?.find {
                                                    it.toString() == BlockedTrigger
                                                }
                                                LinkedIssueOfCluster.setCustomFieldValue(BlockedField, valueBlockedTrigger)
                                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                BlockEndDate = TodaysDate
                issue.setCustomFieldValue(BlockEndDateField, BlockEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                Date OldBlockDate = (Date) issue.getCustomFieldValue(OldBlockEndDateField)

                if (!issue.getCustomFieldValue(OldBlockEndDateField) || issue.getCustomFieldValue(OldBlockEndDateField) != TodaysDate) {
                    log.debug "Old end date to update"
                    issue.setCustomFieldValue(OldBlockEndDateField, BlockEndDate)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                    BlockStartDate = (Date) issue.getCustomFieldValue(BlockStartDateField)
                    if (issue.getCustomFieldValue(TotalBlockingDurationField)) {
                        TotalBlockingDurationOld = issue.getCustomFieldValue(TotalBlockingDurationField).replace(" d", "").toDouble()
                        if (issue.getCustomFieldValue(OldBlockStartDateField) == TodaysDate || issue.getCustomFieldValue(OldBlockStartDateField) > OldBlockDate) {
                            TotalBlockingDuration = BlockEndDate - BlockStartDate + 1
                        } else {
                            TotalBlockingDuration = BlockEndDate - BlockStartDate
                        }
                        TotalBlockingDuration = TotalBlockingDurationOld + TotalBlockingDuration
                    } else {
                        TotalBlockingDuration = BlockEndDate - BlockStartDate + 1
                    }
                    TotalBlockingDuration = TotalBlockingDuration.trunc()
                    TotalBlockingDurationString = TotalBlockingDuration.trunc() + " d"
                    issue.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationString)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }


                TotalBlockingDurationTrigger = 0
                BlockedTrigger = "No"
                def LinkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO2 in LinkedIssuesOfCurrentPWO2) {
                    def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues()
                    for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                        if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                            def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                            for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                    def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                    for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                        if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                            def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                            for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                                if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                    def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                    for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                        if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField)) {
                                                                TotalBlockingDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField)
                                                                TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                                TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                            }
                                                            if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockeDomainField)) {
                                                                BlockedTrigger = "Yes"
                                                            }
                                                            def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                            for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                                if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField)) {
                                                                        TotalBlockingDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField)
                                                                        TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                                        TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                                    }
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(BlockedField).toString() == "Yes") {
                                                                        BlockedTrigger = "Yes"
                                                                    }
                                                                    def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                    for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                        if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(BlockedField).toString() == "Yes") {
                                                                                BlockedTrigger = "Yes"
                                                                            }
                                                                            if (linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField)) {
                                                                                TotalBlockingDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField)
                                                                                TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                                                TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            def fieldConfigBlockedTrigger = BlockedField.getRelevantConfig(LinkedIssueOfCluster)
                                            def valueBlockedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigBlockedTrigger)?.find {
                                                it.toString() == BlockedTrigger
                                            }
                                            LinkedIssueOfCluster.setCustomFieldValue(BlockedField, valueBlockedTrigger)
                                            TotalBlockingDurationTrigger = TotalBlockingDurationTrigger.trunc()
                                            TotalBlockingDurationTriggerString = TotalBlockingDurationTrigger.trunc() + " d"
                                            LinkedIssueOfCluster.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationTriggerString)
                                            issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                            issueIndexingService.reIndex(LinkedIssueOfCluster)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        List<String> itemsChangedEscalated = new ArrayList<String>()
        itemsChangedEscalated.add(EscalatedField.getFieldName())

        boolean isEscalatedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedEscalated)
        if (isEscalatedChanged) {
            if (issue.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                EscalateEndDate = null
                issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                if (!issue.getCustomFieldValue(OldEscalateStartDateField) || issue.getCustomFieldValue(OldEscalateStartDateField) != TodaysDate) {
                    log.debug "Start date to update"
                    EscalateStartDate = TodaysDate
                    issue.setCustomFieldValue(EscalateStartDateField, EscalateStartDate)
                    issue.setCustomFieldValue(OldEscalateStartDateField, EscalateStartDate)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }


                EscalatedTrigger = "No"
                def linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO2 in linkedIssuesOfCurrentPWO2) {
                    if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ID_IT_PWO1) {
                        def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues()
                        for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                            if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                                //Trigger
                                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                                //Get total blocking duration of PWO0
                                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField)) {
                                                                    EscalatedTrigger = "Yes"
                                                                }
                                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                        //Get total blocking duration of PWO1
                                                                        if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                            EscalatedTrigger = "Yes"
                                                                        }
                                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                                if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                                    EscalatedTrigger = "Yes"
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                def fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(LinkedIssueOfCluster)
                                                def valueEscalatedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigEscalatedTrigger)?.find {
                                                    it.toString() == EscalatedTrigger
                                                }
                                                LinkedIssueOfCluster.setCustomFieldValue(EscalatedField, valueEscalatedTrigger)
                                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                EscalateEndDate = TodaysDate
                issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                EscalateStartDate = (Date) issue.getCustomFieldValue(EscalateStartDateField)
                Date OldEscalatedDate = (Date) issue.getCustomFieldValue(OldEscalateEndDateField)

                if (!issue.getCustomFieldValue(OldEscalateEndDateField) || issue.getCustomFieldValue(OldEscalateEndDateField) != TodaysDate) {
                    log.debug "set old date"
                    issue.setCustomFieldValue(OldEscalateEndDateField, EscalateEndDate)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                    if (issue.getCustomFieldValue(TotalEscalatedDurationField)) {
                        TotalEscalatedDurationOld = issue.getCustomFieldValue(TotalEscalatedDurationField).replace(" d", "").toDouble()
                        if (issue.getCustomFieldValue(OldEscalateStartDateField) == TodaysDate || issue.getCustomFieldValue(OldEscalateStartDateField) > OldEscalatedDate) {
                            TotalEscalatedDuration = EscalateEndDate - EscalateStartDate + 1
                        } else {
                            TotalEscalatedDuration = EscalateEndDate - EscalateStartDate
                        }
                        TotalEscalatedDuration = TotalEscalatedDurationOld + TotalEscalatedDuration
                    } else {
                        TotalEscalatedDuration = EscalateEndDate - EscalateStartDate + 1
                    }
                    TotalEscalatedDuration = TotalEscalatedDuration.trunc()
                    TotalEscalatedDurationString = TotalEscalatedDuration.trunc() + " d"

                    issue.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationString)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                }

                TotalEscalatedDurationTrigger = 0
                EscalatedTrigger = "No"
                def linkedIssuesOfCurrentPWO2 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO2 in linkedIssuesOfCurrentPWO2) {
                    if (linkedIssueOfCurrentPWO2.getIssueTypeId() == ID_IT_PWO1) {
                        def linkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO2, user).getAllIssues()
                        for (linkedIssueOfCurrentPWO1 in linkedIssuesOfCurrentPWO1) {
                            if (linkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO1, user).getAllIssues()
                                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                                //Trigger
                                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                                //Get total blocking duration of PWO0
                                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                                    TotalEscalatedDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField)
                                                                    TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                                    TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                                }
                                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                    EscalatedTrigger = "Yes"
                                                                }
                                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                        //Get total blocking duration of PWO1
                                                                        if (linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                                            TotalEscalatedDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField)
                                                                            TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                                            TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                                        }
                                                                        if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                            EscalatedTrigger = "Yes"
                                                                        }
                                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                                if (linkedIssueOfPWO1.getCustomFieldValue(BlockedField).toString() == "Yes") {
                                                                                    EscalatedTrigger = "Yes"
                                                                                }
                                                                                if (linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                                                    TotalEscalatedDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField)
                                                                                    TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                                                    TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //Set data on trigger

                                                def fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(LinkedIssueOfCluster)
                                                def valueEscalatedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigEscalatedTrigger)?.find {
                                                    it.toString() == EscalatedTrigger
                                                }
                                                LinkedIssueOfCluster.setCustomFieldValue(EscalatedField, valueEscalatedTrigger)
                                                TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger.trunc()
                                                TotalEscalatedDurationTriggerString = TotalEscalatedDurationTrigger.trunc() + " d"
                                                LinkedIssueOfCluster.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationTriggerString)
                                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
} else if (issue.getIssueTypeId() == ID_IT_PWO21) {
    List<String> itemsChangedAtCreation = new ArrayList<String>()
    itemsChangedAtCreation.add(FirstSNAppliField.getFieldName())
    log.debug "issue = " + issue.getKey()
    boolean isCreation = (boolean) workloadIsUpdated(changeLog, itemsChangedAtCreation)
    log.debug "is creation : " + isCreation
    if (isCreation || eventId == ID_Event_AdminReOpen) {//Creation

        //Get RequestedDate from Trigger
        Issue issueParent = issue.getParentObject()
        def LinkedIssues = issueLinkManager.getLinkCollection(issueParent, user).getAllIssues()
        for (linkedIssue in LinkedIssues) {
            //If linked issue is Cluster
            if (linkedIssue.getIssueTypeId() == ID_IT_PWO1) {
                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
                for (LinkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                    if (LinkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO0) {
                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfPWO1, user).getAllIssues()
                        for (LinkedIssueOfPW0 in LinkedIssuesOfPWO0) {
                            if (LinkedIssueOfPW0.getIssueTypeId() == ID_IT_Cluster) {
                                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPW0, user).getAllIssues()
                                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                        if(!RequestedDateTrigger || RequestedDateTrigger > (Date)LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)){
                                            RequestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)
                                            log.debug "Requested date trigger : " + RequestedDateTrigger
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (RequestedDateTrigger) {
            //Calculate date for PWO-2.1
            Auth_RequestedDate = (Timestamp) (RequestedDateTrigger - 84)
            Auth_Verification_RequestedDate = (Timestamp) (RequestedDateTrigger - 70)
            Auth_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 56)

            Auth_FirstCommittedDate = Auth_RequestedDate
            Auth_Verification_FirstCommittedDate = Auth_Verification_RequestedDate
            Auth_Validation_FirstCommittedDate = Auth_Validation_RequestedDate

            Auth_LastCommittedDate = Auth_FirstCommittedDate
            Auth_Verification_LastCommittedDate = Auth_Verification_FirstCommittedDate
            Auth_Validation_LastCommittedDate = Auth_Validation_FirstCommittedDate

            issue.setCustomFieldValue(Auth_RequestedDateField, Auth_RequestedDate)
            issue.setCustomFieldValue(Auth_Verification_RequestedDateField, Auth_Verification_RequestedDate)
            issue.setCustomFieldValue(Auth_Validation_RequestedDateField, Auth_Validation_RequestedDate)

            issue.setCustomFieldValue(Auth_FirstCommittedDateField, Auth_FirstCommittedDate)
            issue.setCustomFieldValue(Auth_Verification_FirstCommittedDateField, Auth_Verification_FirstCommittedDate)
            issue.setCustomFieldValue(Auth_Validation_FirstCommittedDateField, Auth_Validation_FirstCommittedDate)

            issue.setCustomFieldValue(Auth_LastCommittedDateField, Auth_LastCommittedDate)
            issue.setCustomFieldValue(Auth_Verification_LastCommittedDateField, Auth_Verification_LastCommittedDate)
            issue.setCustomFieldValue(Auth_Validation_LastCommittedDateField, Auth_Validation_LastCommittedDate)

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }

        def LinkedIssuesOfPWO21 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssueOfPWO21 in LinkedIssuesOfPWO21) {
            if (linkedIssueOfPWO21.getProjectId() == ID_PR_AWO) {
                def LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO21, user).getAllIssues()
                for (linkedIssueOfAWOForCompany in LinkedIssuesOfAWOForCompany) {
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO1) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                            String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField)
                            String IA_Company_Formatted = "Partner_" + IA_Company_Value

                            Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted)

                            if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(IA_Company_Management_Formatted_Group)
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO21) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != "AIRBUS") {
                            String Auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField)
                            String Auth_Company_Formatted = "Partner_" + Auth_Company_Value

                            Group Auth_Company_Management_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted)

                            if (!CompanyAllowedList.contains(Auth_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_Company_Management_Formatted_Group)
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != "AIRBUS") {
                            String Auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField)
                            String Auth_VerifCompany_Formatted = "Partner_" + Auth_VerifCompany_Value

                            Group Auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Auth_VerifCompany_Formatted)

                            if (!CompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group)
                            }
                            if (!AuthVerifCompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                AuthVerifCompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group)
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO22) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != "AIRBUS") {
                            String Form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField)
                            String Form_Company_Formatted = "Partner_" + Form_Company_Value

                            Group Form_Company_Management_Formatted_Group = groupManager.getGroup(Form_Company_Formatted)

                            if (!CompanyAllowedList.contains(Form_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_Company_Management_Formatted_Group)
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != "AIRBUS") {
                            String Form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField)
                            String Form_VerifCompany_Formatted = "Partner_" + Form_VerifCompany_Value

                            Group Form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Form_VerifCompany_Formatted)

                            if (!CompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group)
                            }
                            if (!FormVerifCompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                FormVerifCompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group)
                            }
                        }
                    }
                    if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO23) {
                        if (linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != "AIRBUS") {
                            String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField)
                            String Int_Company_Formatted = "Partner_" + Int_Company_Value

                            Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted)

                            if (!CompanyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                CompanyAllowedList.add(Int_Company_Management_Formatted_Group)
                                IntCompanyAllowedList.add(Int_Company_Management_Formatted_Group)
                            }
                        }
                    }
                }
                if (CompanyAllowedList != null) {
                    linkedIssueOfPWO21.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList)
                    if (AuthVerifCompanyAllowedList != null) {
                        linkedIssueOfPWO21.setCustomFieldValue(AuthVerifCompanyAllowedField, AuthVerifCompanyAllowedList)
                    }
                    if (FormVerifCompanyAllowedList != null) {
                        linkedIssueOfPWO21.setCustomFieldValue(FormVerifCompanyAllowedField, FormVerifCompanyAllowedList)
                    }
                    if (IntCompanyAllowedList != null) {
                        linkedIssueOfPWO21.setCustomFieldValue(IntCompanyAllowedField, IntCompanyAllowedList)
                    }
                    issueManager.updateIssue(user, linkedIssueOfPWO21, EventDispatchOption.DO_NOT_DISPATCH, false)
//JIRA7
                    issueIndexingService.reIndex(linkedIssueOfPWO21)//JIRA7
                    CompanyAllowedList.clear()
                    AuthVerifCompanyAllowedList.clear()
                    FormVerifCompanyAllowedList.clear()
                    IntCompanyAllowedList.clear()
                }
            }
        }

    } else {//Edit
        log.debug "is edit"

        List<String> itemsChangedPWO21Company = new ArrayList<String>()
        itemsChangedPWO21Company.add(AuthCompanyField.getFieldName())
        itemsChangedPWO21Company.add(AuthVerifCompanyField.getFieldName())

        boolean isPWO21CompanyChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedPWO21Company)
        if (isPWO21CompanyChanged) {
            def LinkedIssuesOfPWO21 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
            for (linkedIssueOfPWO21 in LinkedIssuesOfPWO21) {
                if (linkedIssueOfPWO21.getProjectId() == ID_PR_AWO) {
                    def LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(linkedIssueOfPWO21, user).getAllIssues()
                    for (linkedIssueOfAWOForCompany in LinkedIssuesOfAWOForCompany) {
                        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO1) {
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                                String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField)
                                String IA_Company_Formatted = "Partner_" + IA_Company_Value

                                Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted)

                                if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(IA_Company_Management_Formatted_Group)
                                }

                            }
                        }
                        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO21) {
                            log.debug "PWO-2.1 : " + linkedIssueOfAWOForCompany.getKey()
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField) != "AIRBUS") {
                                String Auth_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthCompanyField)
                                String Auth_Company_Formatted = "Partner_" + Auth_Company_Value

                                Group Auth_Company_Management_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted)

                                if (!CompanyAllowedList.contains(Auth_Company_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Auth_Company_Management_Formatted_Group)
                                }
                            }
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField) != "AIRBUS") {
                                String Auth_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(AuthVerifCompanyField)
                                String Auth_VerifCompany_Formatted = "Partner_" + Auth_VerifCompany_Value

                                Group Auth_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Auth_VerifCompany_Formatted)

                                if (!CompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group)
                                }
                                if (!AuthVerifCompanyAllowedList.contains(Auth_VerifCompany_Management_Formatted_Group)) {
                                    AuthVerifCompanyAllowedList.add(Auth_VerifCompany_Management_Formatted_Group)
                                }
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO22) {
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField) != "AIRBUS") {
                                String Form_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormCompanyField)
                                String Form_Company_Formatted = "Partner_" + Form_Company_Value

                                Group Form_Company_Management_Formatted_Group = groupManager.getGroup(Form_Company_Formatted)

                                if (!CompanyAllowedList.contains(Form_Company_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Form_Company_Management_Formatted_Group)
                                }
                            }
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField) != "AIRBUS") {
                                String Form_VerifCompany_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(FormVerifCompanyField)
                                String Form_VerifCompany_Formatted = "Partner_" + Form_VerifCompany_Value

                                Group Form_VerifCompany_Management_Formatted_Group = groupManager.getGroup(Form_VerifCompany_Formatted)

                                if (!CompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group)
                                }
                                if (!FormVerifCompanyAllowedList.contains(Form_VerifCompany_Management_Formatted_Group)) {
                                    FormVerifCompanyAllowedList.add(Form_VerifCompany_Management_Formatted_Group)
                                }
                            }
                        }
                        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO23) {
                            if (linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField) != "AIRBUS") {
                                String Int_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IntCompanyField)
                                String Int_Company_Formatted = "Partner_" + Int_Company_Value

                                Group Int_Company_Management_Formatted_Group = groupManager.getGroup(Int_Company_Formatted)

                                if (!CompanyAllowedList.contains(Int_Company_Management_Formatted_Group)) {
                                    CompanyAllowedList.add(Int_Company_Management_Formatted_Group)
                                    IntCompanyAllowedList.add(Int_Company_Management_Formatted_Group)
                                }
                            }
                        }
                    }

                    if (CompanyAllowedList != null) {
                        linkedIssueOfPWO21.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList)
                        if (AuthVerifCompanyAllowedList != null) {
                            linkedIssueOfPWO21.setCustomFieldValue(AuthVerifCompanyAllowedField, AuthVerifCompanyAllowedList)
                        }
                        if (FormVerifCompanyAllowedList != null) {
                            linkedIssueOfPWO21.setCustomFieldValue(FormVerifCompanyAllowedField, FormVerifCompanyAllowedList)
                        }
                        if (IntCompanyAllowedList != null) {
                            linkedIssueOfPWO21.setCustomFieldValue(IntCompanyAllowedField, IntCompanyAllowedList)
                        }
                        issueManager.updateIssue(user, linkedIssueOfPWO21, EventDispatchOption.DO_NOT_DISPATCH, false)
//JIRA7
                        issueIndexingService.reIndex(linkedIssueOfPWO21)//JIRA7
                        //CompanyAllowedList.removeAll
                        CompanyAllowedList.clear()
                        AuthVerifCompanyAllowedList.clear()
                        FormVerifCompanyAllowedList.clear()
                        IntCompanyAllowedList.clear()
                    }
                }
            }
        }


        List<String> itemsChangedAuthVerified = new ArrayList<String>()
        itemsChangedAuthVerified.add(Auth_VerifiedField.getFieldName())

        boolean isAuthVerifiedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedAuthVerified)
        log.debug "is Auth Verified changed : " + isAuthVerifiedChanged
        if (isAuthVerifiedChanged) {
            if (issue.getCustomFieldValue(Auth_VerifiedField).toString() == "Rejected") {
                if (!issue.getCustomFieldValue(Auth_Verified_1stDeliveryField)) {
                    AuthVerification1stDeliveryDate = TodaysDate
                    issue.setCustomFieldValue(Auth_Verified_1stDeliveryField, AuthVerification1stDeliveryDate)
                }
                AuthVerificationLastDeliveryDate = TodaysDate
                issue.setCustomFieldValue(Auth_Verified_LastDeliveryField, AuthVerificationLastDeliveryDate)

                //Auth_ReworkDate=TodaysDate+7
                //issue.setCustomFieldValue(Auth_ReworkDateField,Auth_ReworkDate)
            } else if (issue.getCustomFieldValue(Auth_VerifiedField).toString() == "Verified") {
                //if (!issue.getCustomFieldValue(Auth_Verified_1stDeliveryField)) {
                for (ChangeHistoryItem item : changeItems) {
                    if (item.field == 'Auth_Verified' && item.getTos().values().contains('Verified')) {
                        Compteur = Compteur + 1
                    }
                }
                if(Compteur<2){
                        Auth_Validation_RequestedDate = (Timestamp) TodaysDate + 14
                        Auth_Validation_FirstCommittedDate = Auth_Validation_RequestedDate
                        Auth_Validation_LastCommittedDate = Auth_Validation_FirstCommittedDate
                        
                         issue.setCustomFieldValue(Auth_Validation_RequestedDateField, Auth_Validation_RequestedDate)
                        issue.setCustomFieldValue(Auth_Validation_FirstCommittedDateField, Auth_Validation_FirstCommittedDate)
                        issue.setCustomFieldValue(Auth_Validation_LastCommittedDateField, Auth_Validation_LastCommittedDate)
                    }
                
                if (!issue.getCustomFieldValue(Auth_Verified_1stDeliveryField)) {
                    AuthVerification1stDeliveryDate = TodaysDate

                    issue.setCustomFieldValue(Auth_Verified_1stDeliveryField, AuthVerification1stDeliveryDate)
                   

                }
                AuthVerificationLastDeliveryDate = TodaysDate
                issue.setCustomFieldValue(Auth_Verified_LastDeliveryField, AuthVerificationLastDeliveryDate)
                
                def fieldConfigAuthValidated = Auth_ValidatedField.getRelevantConfig(issue)
                def valueAuthValidated = ComponentAccessor.optionsManager.getOptions(fieldConfigAuthValidated)?.find {
                    it.toString() == "Validation pending"
                }
                issue.setCustomFieldValue(Auth_ValidatedField, valueAuthValidated)
            }
            if (issue.getCustomFieldValue(Auth_VerifiedField).toString() != "Verification pending") {
                AuthVerificationLastDeliveryDate = TodaysDate
                issue.setCustomFieldValue(Auth_Verified_LastDeliveryField, AuthVerificationLastDeliveryDate)
            }
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }

        List<String> itemsChangedAuthValidated = new ArrayList<String>()
        itemsChangedAuthValidated.add(Auth_ValidatedField.getFieldName())

        boolean isAuthValidatedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedAuthValidated)
        if (isAuthValidatedChanged) {
            if (issue.getCustomFieldValue(Auth_ValidatedField).toString() == "Rejected") {
                if (!issue.getCustomFieldValue(Auth_Validation_1stDeliveryDateField)) {
                    AuthValidation1stDeliveryDate = TodaysDate
                    issue.setCustomFieldValue(Auth_Validation_1stDeliveryDateField, AuthValidation1stDeliveryDate)
                }
                AuthValidationLastDeliveryDate = TodaysDate
                issue.setCustomFieldValue(Auth_Validation_LastDeliveryDateField, AuthValidationLastDeliveryDate)


                if (issue.getCustomFieldValue(Auth_ReworkDateField) != TodaysDate + 7) {
                    Auth_ReworkDate = TodaysDate + 7
                    issue.setCustomFieldValue(Auth_ReworkDateField, Auth_ReworkDate)
                }

                Auth_Verification_ReworkDate = TodaysDate + 14
                issue.setCustomFieldValue(Auth_Verification_ReworkDateField, Auth_Verification_ReworkDate)

                TechnicallyClosed = "No"
                TechnicalClosureDate = null

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
            } else if (issue.getCustomFieldValue(Auth_ValidatedField).toString() == "Validated") {
                TechnicallyClosed = "Yes"
                TechnicalClosureDate = TodaysDate

                if (!issue.getCustomFieldValue(Auth_Validation_1stDeliveryDateField)) {
                    AuthValidation1stDeliveryDate = TodaysDate
                    issue.setCustomFieldValue(Auth_Validation_1stDeliveryDateField, AuthValidation1stDeliveryDate)
                }
                AuthValidationLastDeliveryDate = TodaysDate
                issue.setCustomFieldValue(Auth_Validation_LastDeliveryDateField, AuthValidationLastDeliveryDate)
            }

            def fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue)
            def valueTechnicallyClosed = ComponentAccessor.optionsManager.getOptions(fieldConfigTechnicallyClosed)?.find {
                it.toString() == TechnicallyClosed
            }
            issue.setCustomFieldValue(TechnicallyClosedField, valueTechnicallyClosed)

            issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosureDate)

            log.debug "indexing 2"
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }


        //Set costs data on Trigger
        Issue issueParent = issue.getParentObject()
        def LinkedIssues = issueLinkManager.getLinkCollection(issueParent, user).getAllIssues()
        for (linkedIssue in LinkedIssues) {
            if (linkedIssue.getIssueTypeId() == ID_IT_PWO1) {
                def LinkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
                for (LinkedIssueOCurrentfPWO1 in LinkedIssuesOfCurrentPWO1) {
                    if (LinkedIssueOCurrentfPWO1.getIssueTypeId() == ID_IT_PWO0) {
                        def LinkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOCurrentfPWO1, user).getAllIssues()
                        for (LinkedIssueOfCurrentPW0 in LinkedIssuesOfCurrentPWO0) {
                            if (LinkedIssueOfCurrentPW0.getIssueTypeId() == ID_IT_Cluster) {
                                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfCurrentPW0, user).getAllIssues()
                                for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                                    if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                        //Reset variable
                                        PartsDataTimeSpent=0
                                        MaintPlanningTimeSpent=0
                                        MaintTimeSpent=0
                                        PartsDataEstimatedCosts=0
                                        MaintPlanningEstimatedCosts=0
                                        MaintEstimatedCosts=0
                                        log.debug "Trigger is : " + linkedIssueOfCluster.getKey()
                                        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                                        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                                def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                                for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField)) {
                                                            PartsDataTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField).toString())
                                                            PartsDataTimeSpent = PartsDataTimeSpent + PartsDataTimeSpentCurrent
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField)) {
                                                            MaintPlanningTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField).toString())
                                                            MaintPlanningTimeSpent = MaintPlanningTimeSpent + MaintPlanningTimeSpentCurrent
                                                        }
                                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField)) {
                                                            MaintTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField).toString())
                                                            MaintTimeSpent = MaintTimeSpent + MaintTimeSpentCurrent
                                                        }
                                                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                                if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                                    IATimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField).toString())
                                                                } else {
                                                                    IATimeSpentCurrent = null
                                                                }

                                                                if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                                        PartsDataTimeSpent = PartsDataTimeSpent + IATimeSpentCurrent
                                                                    }
                                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                                        MaintPlanningTimeSpent = MaintPlanningTimeSpent + IATimeSpentCurrent
                                                                    }
                                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                                        MaintTimeSpent = MaintTimeSpent + IATimeSpentCurrent
                                                                    }
                                                                }
                                                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                        DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field).toString()
                                                                        //Get PWO-2's subtask
                                                                        def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                                                        for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField)) {
                                                                                    AuthTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField).toString())
                                                                                    //AuthTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField)
                                                                                } else {
                                                                                    AuthTimeSpentCurrent = null
                                                                                }
                                                                                if (DomainOfPWO2 == "Parts Data" && AuthTimeSpentCurrent) {
                                                                                    PartsDataTimeSpent = PartsDataTimeSpent + AuthTimeSpentCurrent
                                                                                } else if (DomainOfPWO2 == "Maintenance Planning" && AuthTimeSpentCurrent) {
                                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + AuthTimeSpentCurrent
                                                                                } else if (DomainOfPWO2 == "Maintenance" && AuthTimeSpentCurrent) {
                                                                                    MaintTimeSpent = MaintTimeSpent + AuthTimeSpentCurrent
                                                                                }
                                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField)) {
                                                                                    FormTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField).toString())
                                                                                    //FormTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField)
                                                                                } else {
                                                                                    FormTimeSpentCurrent = null
                                                                                }
                                                                                if (DomainOfPWO2 == "Parts Data" && FormTimeSpentCurrent) {
                                                                                    PartsDataTimeSpent = PartsDataTimeSpent + FormTimeSpentCurrent
                                                                                } else if (DomainOfPWO2 == "Maintenance Planning" && FormTimeSpentCurrent) {
                                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + FormTimeSpentCurrent
                                                                                } else if (DomainOfPWO2 == "Maintenance" && FormTimeSpentCurrent) {
                                                                                    MaintTimeSpent = MaintTimeSpent + FormTimeSpentCurrent
                                                                                }
                                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField)) {
                                                                                    IntTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField).toString())
                                                                                } else {
                                                                                    IntTimeSpentCurrent = null
                                                                                }
                                                                                if (DomainOfPWO2 == "Parts Data" && IntTimeSpentCurrent) {
                                                                                    PartsDataTimeSpent = PartsDataTimeSpent + IntTimeSpentCurrent
                                                                                } else if (DomainOfPWO2 == "Maintenance Planning" && IntTimeSpentCurrent) {
                                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + IntTimeSpentCurrent
                                                                                } else if (DomainOfPWO2 == "Maintenance" && IntTimeSpentCurrent) {
                                                                                    MaintTimeSpent = MaintTimeSpent + IntTimeSpentCurrent
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //Set data on trigger
                                        PartsDataTimeSpentString = formatSecondesValueInHoursMinutes(PartsDataTimeSpent)
                                        linkedIssueOfCluster.setCustomFieldValue(PartsData_TimeSpentField, PartsDataTimeSpentString)
                                        MaintPlanningTimeSpentString = formatSecondesValueInHoursMinutes(MaintPlanningTimeSpent)
                                        linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_TimeSpentField, MaintPlanningTimeSpentString)
                                        MaintTimeSpenString = formatSecondesValueInHoursMinutes(MaintTimeSpent)
                                        linkedIssueOfCluster.setCustomFieldValue(Maint_TimeSpentField, MaintTimeSpenString)
                                        issueManager.updateIssue(user, linkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                        issueIndexingService.reIndex(linkedIssueOfCluster)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}