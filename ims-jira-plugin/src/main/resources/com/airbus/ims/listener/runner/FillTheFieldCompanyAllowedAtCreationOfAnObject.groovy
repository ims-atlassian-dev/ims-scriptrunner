package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Fill the field "Company allowed" at creation of an object
 * Events : Issue Created, Start, PWO-0 updated, PWO-1 updated, PWO-2 updated, PWO-2.1 updated, PWO-2.2 updated, PWO-2.3 updated, PO updated, WU updated
 * isDisabled : true
 */

import com.atlassian.crowd.embedded.api.Group
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.user.ApplicationUser

//import com.atlassian.jira.issue.index.IssueIndexingParams.Builder

//Manager
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
GroupManager groupManager = ComponentAccessor.getGroupManager()

//IssueIndexingParams params = IssueIndexingParams.builder().setChangeHistory(false).build()

//User for treatments
ApplicationUser user = authenticationContext.getLoggedInUser()

//Get issue from event
MutableIssue issue = (MutableIssue) event.getIssue()

//Mock Issue
//String issueKey ="ANO-18137"
//MutableIssue issue = issueManager.getIssueByCurrentKey(issueKey)

//Data
//Issue Types
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"
String ID_IT_PO = "10021"
String ID_IT_WU = "10022"
String ID_IT_WU_AdditionalCost = "10200"

log.debug "issue : " + issue

//CF ID
long ID_CF_COMPANY_ALLOWED = 10702
long ID_CF_MAINT_PLANNING_PREIA_COMPANY = 10073
long ID_CF_MAINT_PREIA_COMPANY = 10093
long ID_CF_PARTSDTA_PREIA_COMPANY = 10051
long ID_CF_IA_COMPANY = 10142
long ID_CF_AUTH_COMPANY = 10173
long ID_CF_AUTH_VERIFICATION_COMPANY = 10181
long ID_CF_FORM_COMPANY = 10281
long ID_CF_FORM_VERIFICATION_COMPANY = 10289
long ID_CF_INT_COMPANY = 10324
long ID_CF_PO_COMPANY = 10618
long ID_CF_WU_COMPANY = 11154
long ID_CF_TECHNICAL_COMPANY_ALLOWED = 11900
long ID_CF_VERIF_COMPANY_ALLOWED = 11901

//Get CustomFields
CustomField CompanyAllowed = customFieldManager.getCustomFieldObject(ID_CF_COMPANY_ALLOWED)
CustomField MaintPlanning_PreIA_Company = customFieldManager.getCustomFieldObject(ID_CF_MAINT_PLANNING_PREIA_COMPANY)
CustomField Maint_PreIA_Company = customFieldManager.getCustomFieldObject(ID_CF_MAINT_PREIA_COMPANY)
CustomField PartsData_PreIA_Company = customFieldManager.getCustomFieldObject(ID_CF_PARTSDTA_PREIA_COMPANY)
CustomField IA_Company = customFieldManager.getCustomFieldObject(ID_CF_IA_COMPANY)
CustomField Auth_Company = customFieldManager.getCustomFieldObject(ID_CF_AUTH_COMPANY)
CustomField Auth_Verif_Company = customFieldManager.getCustomFieldObject(ID_CF_AUTH_VERIFICATION_COMPANY)
CustomField Form_Company = customFieldManager.getCustomFieldObject(ID_CF_FORM_COMPANY)
CustomField Form_Verif_Company = customFieldManager.getCustomFieldObject(ID_CF_FORM_VERIFICATION_COMPANY)
CustomField Int_Company = customFieldManager.getCustomFieldObject(ID_CF_INT_COMPANY)
CustomField PO_Company = customFieldManager.getCustomFieldObject(ID_CF_PO_COMPANY)
CustomField WU_Company = customFieldManager.getCustomFieldObject(ID_CF_WU_COMPANY)
CustomField TechnicalCompanyAllowed = customFieldManager.getCustomFieldObject(ID_CF_TECHNICAL_COMPANY_ALLOWED)
CustomField VerifCompanyAllowed = customFieldManager.getCustomFieldObject(ID_CF_VERIF_COMPANY_ALLOWED)


List<Group> CompanyAllowedList = new ArrayList<Group>()
List<Group> TechnicalCompanyAllowedList = new ArrayList<Group>()
List<Group> VerifCompanyAllowedList = new ArrayList<Group>()
log.debug "issue type : " + issue.getIssueTypeId()

if (issue.getIssueTypeId().equals(ID_IT_PWO0)) {
    log.debug "PWO-0"
    //PWO-0
    String MaintPlanning_PreIA_Company_Value = issue.getCustomFieldValue(MaintPlanning_PreIA_Company)
    String Maint_PreIA_Company_Value = issue.getCustomFieldValue(Maint_PreIA_Company)
    String PartsData_PreIA_Company_Value = issue.getCustomFieldValue(PartsData_PreIA_Company)

    String MaintPlanning_PreIA_Company_Formatted = "Partner_" + MaintPlanning_PreIA_Company_Value
    String Maint_PreIA_Company_Value_Formatted = "Partner_" + Maint_PreIA_Company_Value
    String PartsData_PreIA_Company_Formatted = "Partner_" + PartsData_PreIA_Company_Value

    log.debug "Maint planning : " + MaintPlanning_PreIA_Company_Formatted

    Group MaintPlanning_PreIA_Company_Group = groupManager.getGroup(MaintPlanning_PreIA_Company_Formatted)
    Group Maint_PreIA_Company_Value_Group = groupManager.getGroup(Maint_PreIA_Company_Value_Formatted)
    Group PartsData_PreIA_Company_Group = groupManager.getGroup(PartsData_PreIA_Company_Formatted)

    CompanyAllowedList.add(MaintPlanning_PreIA_Company_Group)
    CompanyAllowedList.add(Maint_PreIA_Company_Value_Group)
    CompanyAllowedList.add(PartsData_PreIA_Company_Group)

    log.debug "Company allowed : " + CompanyAllowedList

    issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
    issueIndexingService.reIndex(issue)//JIRA7
} else if (issue.getIssueTypeId().equals(ID_IT_PWO1)) {
    //PWO-1
    String IA_Company_Value = issue.getCustomFieldValue(IA_Company)

    String IA_Company_Formatted = "Partner_" + IA_Company_Value

    Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted)

    CompanyAllowedList.add(IA_Company_Management_Formatted_Group)


    issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
    issueIndexingService.reIndex(issue)//JIRA7
} else if (issue.getIssueTypeId().equals(ID_IT_PWO21)) {
    //PWO-2.1
    String Auth_Company_Value = issue.getCustomFieldValue(Auth_Company)
    String Auth_Company_Verif_Value = issue.getCustomFieldValue(Auth_Verif_Company)

    String Auth_Company_Formatted = "Partner_" + Auth_Company_Value
    String Auth_Company_Verif_Formatted = "Partner_" + Auth_Company_Verif_Value

    Group Auth_Company_Formatted_Group = groupManager.getGroup(Auth_Company_Formatted)
    Group Auth_Company_Verif_Formatted_Group = groupManager.getGroup(Auth_Company_Verif_Formatted)

    CompanyAllowedList.add(Auth_Company_Formatted_Group)
    CompanyAllowedList.add(Auth_Company_Verif_Formatted_Group)

    TechnicalCompanyAllowedList.add(Auth_Company_Formatted_Group)
    VerifCompanyAllowedList.add(Auth_Company_Verif_Formatted_Group)

    issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList)
    issue.setCustomFieldValue(TechnicalCompanyAllowed, TechnicalCompanyAllowedList)
    issue.setCustomFieldValue(VerifCompanyAllowed, VerifCompanyAllowedList)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
    issueIndexingService.reIndex(issue)//JIRA7
} else if (issue.getIssueTypeId().equals(ID_IT_PWO22)) {
    //PWO-2.2
    String Form_Company_Value = issue.getCustomFieldValue(Form_Company)
    String Form_Company_Verif_Value = issue.getCustomFieldValue(Form_Verif_Company)

    String Form_Company_Formatted = "Partner_" + Form_Company_Value
    String Form_Company_Verif_Formatted = "Partner_" + Form_Company_Verif_Value

    Group Form_Company_Formatted_Group = groupManager.getGroup(Form_Company_Formatted)
    Group Form_Company_Verif_Formatted_Group = groupManager.getGroup(Form_Company_Verif_Formatted)

    CompanyAllowedList.add(Form_Company_Formatted_Group)
    CompanyAllowedList.add(Form_Company_Verif_Formatted_Group)

    TechnicalCompanyAllowedList.add(Form_Company_Formatted_Group)
    VerifCompanyAllowedList.add(Form_Company_Verif_Formatted_Group)

    issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList)
    issue.setCustomFieldValue(TechnicalCompanyAllowed, TechnicalCompanyAllowedList)
    issue.setCustomFieldValue(VerifCompanyAllowed, VerifCompanyAllowedList)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
    issueIndexingService.reIndex(issue)//JIRA7
} else if (issue.getIssueTypeId().equals(ID_IT_PWO23)) {
    //PWO-2.3
    String Int_Company_Value = issue.getCustomFieldValue(Int_Company)

    String Int_Company_Formatted = "Partner_" + Int_Company_Value

    Group Int_Company_Formatted_Group = groupManager.getGroup(Int_Company_Formatted)

    CompanyAllowedList.add(Int_Company_Formatted_Group)

    issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
    issueIndexingService.reIndex(issue)//JIRA7
} else if (issue.getIssueTypeId().equals(ID_IT_PO)) {
    log.debug "PO"
    //PO
    String PO_Company_Value = issue.getCustomFieldValue(PO_Company)

    String PO_Company_Formatted = "Partner_" + PO_Company_Value

    Group PO_Company_Formatted_Group = groupManager.getGroup(PO_Company_Formatted)

    CompanyAllowedList.add(PO_Company_Formatted_Group)

    issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
    issueIndexingService.reIndex(issue)//JIRA7
} else if (issue.getIssueTypeId().equals(ID_IT_WU) || issue.getIssueTypeId().equals(ID_IT_WU_AdditionalCost)) {
    log.debug "WU"
    //DU
    String WU_Company_Value = issue.getCustomFieldValue(WU_Company)
    //log.debug "WU company : +" WU_Company_Value

    String WU_Company_Formatted = "Partner_" + WU_Company_Value

    Group WU_Company_Formatted_Group = groupManager.getGroup(WU_Company_Formatted)
    //log.debug "WU company group : +" WU_Company_Formatted_Group

    CompanyAllowedList.add(WU_Company_Formatted_Group)

    issue.setCustomFieldValue(CompanyAllowed, CompanyAllowedList)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
    issueIndexingService.reIndex(issue)//JIRA7
}
