package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Link of AWO to PWO-2.x
 * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
 * isDisabled : null
 */

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
}*/

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

def getIssueFromNFeedFieldNewFieldsSingle(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ")
        issueFound = issueManager.getIssueObject(Long.parseLong(formattedValues))
    }

    if (issueFound) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + issueFound
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }
    return issueFound
}


String formatSecondesValueInHoursMinutes(Long secondes) {
    log.debug "Start formatSecondesValueInHoursMinutes - secondes : " + secondes
    if (secondes && secondes > 0) {
        String result
        int hoursExtracted = (int) (secondes / 3600)
        int totalMinutes = (int) (secondes / 60)
        int minutesExtracted = (int) totalMinutes.mod(60)
        result = hoursExtracted.toString() + "h " + minutesExtracted.toString() + "m"
        log.debug "End formatSecondesValueInHoursMinutes - result  : " + result
        return result
    } else {
        log.debug "End formatSecondesValueInHoursMinutes - minutes is null"
    }
}

Long formatHoursMinutesFormatInSecondes(String hoursMinutes) {
    log.debug "Start formatHoursMinutesFormatInSecondes - hoursMinutes : " + hoursMinutes
    if (!hoursMinutes.equals("null")) {
        Long result = 0L
        String[] hoursMinutesTable
        Long hourConvertedInMinutes = 0
        String hourNumberString
        Long minutesNumber = 0

        hoursMinutesTable = hoursMinutes.split("h")
        hourConvertedInMinutes = hoursMinutesTable[0].toString().toLong() * 60
        minutesNumber = hoursMinutesTable[1].toString().substring(0, hoursMinutesTable[1].toString().length() - 1).toLong()
        result = (hourConvertedInMinutes + minutesNumber) * 60

        log.debug "End formatHoursMinutesFormatInSecondes - result : " + result
        return result
    } else {
        log.debug "End formatHoursMinutesFormatInSecondes - hoursMinutes is null"
    }
}


//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()

MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject()
log.debug "issue : " + issue.getKey()
MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject()
log.debug "issueSource : " + issueSource.getKey()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"

List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

long ID_Project_AWO = 10003


//Fields
long ID_CF_AWO_AuthEffectiveCost = 11103
long ID_CF_AWO_AuthVerifEffectiveCost = 11105
long ID_CF_AWO_FormEffectiveCost = 11104
long ID_CF_AWO_FormVerifEffectiveCost = 11106
long ID_CF_AWO_EffectiveCost = 11107
long ID_CF_AWO_IntEffectiveCost = 11108


long ID_CF_Auth_EffectiveCost = 10240

long ID_CF_Form_EffectiveCost = 10305

long ID_CF_Int_EffectiveCost = 10332


//CustomField
CustomField AWO_AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_AuthEffectiveCost)
CustomField AWO_AuthVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_AuthVerifEffectiveCost)
CustomField AWO_FormEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_FormEffectiveCost)
CustomField AWO_FormVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_FormVerifEffectiveCost)
CustomField AWO_TransEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_EffectiveCost)
CustomField AWO_IntEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_IntEffectiveCost)


CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Auth_EffectiveCost)
CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Form_EffectiveCost)
CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Int_EffectiveCost)



double Auth_EffectiveCost
double Form_EffectiveCost
double Int_EffectiveCost


//Business

//Set cost data on PWO-2
log.debug "issue : " + issue.getKey()
if (issue.getIssueTypeId() == ID_IT_PWO21 && issueSource.getProjectId() == ID_Project_AWO) {
    def LinkedIssuesOfCurrentPWO21 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfCurrentPWO21 in LinkedIssuesOfCurrentPWO21) {
        if (linkedIssueOfCurrentPWO21.getProjectId() == ID_Project_AWO) {
            if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField)) {
                Auth_EffectiveCost = Auth_EffectiveCost + (double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField)
            }
            if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField)) {
                Auth_EffectiveCost = Auth_EffectiveCost + (double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField)
            }
        }
    }
    issue.setCustomFieldValue(Auth_EffectiveCostField, Auth_EffectiveCost)
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
    issueIndexingService.reIndex(issue)
} else if (issue.getIssueTypeId() == ID_IT_PWO22 && issueSource.getProjectId() == ID_Project_AWO) {
    def LinkedIssuesOfCurrentPWO22 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfCurrentPWO22 in LinkedIssuesOfCurrentPWO22) {
        if (linkedIssueOfCurrentPWO22.getProjectId() == ID_Project_AWO) {
            if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField)) {
                Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField)
            }
            if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField)) {
                Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField)
            }
            if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField)) {
                Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField)
            }
        }
    }
    issue.setCustomFieldValue(Form_EffectiveCostField, Form_EffectiveCost)
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
    issueIndexingService.reIndex(issue)
} else if (issue.getIssueTypeId() == ID_IT_PWO23 && issueSource.getProjectId() == ID_Project_AWO) {
    def LinkedIssuesOfCurrentPWO23 = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfCurrentPWO23 in LinkedIssuesOfCurrentPWO23) {
        if (linkedIssueOfCurrentPWO23.getProjectId() == ID_Project_AWO) {
            if (linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField)) {
                Int_EffectiveCost = Int_EffectiveCost + (double) linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField)
            }
        }
    }
    issue.setCustomFieldValue(Int_EffectiveCostField, Int_EffectiveCost)
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
    issueIndexingService.reIndex(issue)
}