package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Link selected AWO upon creation of PWO-2
 * Events : PWO-2 updated, Issue Created
 * isDisabled : false
 */

// import


import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.apache.log4j.Level
import org.apache.log4j.Logger
import com.atlassian.jira.issue.IssueInputParametersImpl
import org.apache.log4j.Level
import org.apache.log4j.Logger
import groovyx.net.http.AsyncHTTPBuilder

Logger log = Logger.getLogger("com.valiantys.script.listener")
log.setLevel(Level.DEBUG)
log.warn "PWO updated LinkAWOOnPWO2Creation"

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
} */


boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

def getIssueFromNFeedFieldNewFieldsMulitple(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    List<Issue> multipleIssue = new ArrayList<Issue>()

    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String[] formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ").split(" ")
        for (String formatedValue in formattedValues) {
            issueFound = issueManager.getIssueObject(Long.parseLong(formatedValue))
            multipleIssue.add(issueFound)
        }
    }

    if (multipleIssue) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + multipleIssue
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }

    return multipleIssue
}

long begin = System.currentTimeMillis()

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
IssueService issueService = ComponentAccessor.getIssueService()
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog


MutableIssue issue = (MutableIssue) event.getIssue()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"

List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_AWO_To_link = 11010
long ID_CF_FirstAppli = 10011
long ID_CF_CreatePWO21 = 11101

// Transitions
int CREATE_PWO_21_TR_ID = 101

//CustomField
CustomField AWOToLinkField = customFieldManager.getCustomFieldObject(ID_CF_AWO_To_link)
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstAppli)
CustomField CreatePWO21Field = customFieldManager.getCustomFieldObject(ID_CF_CreatePWO21)

//Variables
List<Issue> issuesAWOtoLink

//Business
List<String> itemsChangedAtCreation = new ArrayList<String>()
itemsChangedAtCreation.add(FirstSNAppliField.getFieldName())
log.debug "issue : " + issue.getKey()

long isCreationTime = 0l, getLinkCollectionTime = 0, endLinkCollectionTme = 0,
     getPWO1Time = 0, endAWOCollectionTime = 0, updateIssuePWO1Time = 0,
     indexPWO1Time = 0, updateIssueTime = 0, indexIssueTime = 0, transitionIssueTime = 0, getAWOCollectionTime = 0
if (issue.getIssueTypeId() == ID_IT_PWO2) {

    log.debug "creation of PWO2"

    boolean isCreation = (boolean) workloadIsUpdated(changeLog, itemsChangedAtCreation)

    isCreationTime = System.currentTimeMillis()
    log.debug "checked if creation in ${isCreationTime - begin}"
    if (isCreation) { //Creation
        //Get parent PWO-1
        def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        getLinkCollectionTime = System.currentTimeMillis()
        log.debug "got Link Collection in ${getLinkCollectionTime - isCreationTime}"
        for (linkedIssue in LinkedIssues) {
            //If linked issue is PWO-1
            if (linkedIssue.getIssueTypeId() == ID_IT_PWO1) {
                getPWO1Time = System.currentTimeMillis()
                log.debug "PWO-1 linked : ${linkedIssue.key} in ${getPWO1Time - getLinkCollectionTime}"
                log.debug "awo to link field : " + linkedIssue.getCustomFieldValue(AWOToLinkField)
                issuesAWOtoLink = (List<Issue>) getIssueFromNFeedFieldNewFieldsMulitple(AWOToLinkField, linkedIssue, issueManager)
                getAWOCollectionTime = System.currentTimeMillis()
                log.debug("Got ${issuesAWOtoLink.size()} AWOs in ${getAWOCollectionTime - getPWO1Time}")
                for (issueAWOtoLink in issuesAWOtoLink) {
                    // long beginAWO = System.currentTimeMillis()
                    issueLinkManager.createIssueLink(issue.getId(), issueAWOtoLink.getId(), 10301, 1, user)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                    // long endAWO = System.currentTimeMillis()
                    // log.debug "AWO evaluated : ${issueAWOtoLink.getKey()} in ${endAWO - beginAWO}"
                }
                endAWOCollectionTime = System.currentTimeMillis()
                log.debug "Ended AWO Link in ${endAWOCollectionTime - getAWOCollectionTime}"
                log.debug "après lien"
                linkedIssue.setCustomFieldValue(AWOToLinkField, null)
                issueManager.updateIssue(user, linkedIssue, EventDispatchOption.DO_NOT_DISPATCH, false)

                updateIssuePWO1Time = System.currentTimeMillis()
                log.debug "Updated PWO-1 in ${updateIssuePWO1Time - endAWOCollectionTime}"
                issueIndexingService.reIndex(linkedIssue)

                indexPWO1Time = System.currentTimeMillis()
                log.debug "Indexed PWO-1 in ${indexPWO1Time - updateIssuePWO1Time}"
            }
        }
        endLinkCollectionTme = System.currentTimeMillis()
        log.debug "Ended link collection time in  ${endLinkCollectionTme - getLinkCollectionTime}"

        issue.setCustomFieldValue(CreatePWO21Field, "Yes")
        //issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)

        updateIssueTime = System.currentTimeMillis()
        log.debug "Updated issue in  ${updateIssueTime - endLinkCollectionTme}"

        issueIndexingService.reIndex(issue)
        indexIssueTime = System.currentTimeMillis()
        log.debug "Indexed issue in  ${indexIssueTime - updateIssueTime}"


    }
}

long end = System.currentTimeMillis()
log.debug "Ended script in " + (end - begin)
