package com.airbus.ims.listener.runner
/**
 * Projects : AWO
 * Description : Synchronize cwx_quicksearch on issue link deleted
 * Events : IssueLinkDeletedEvent
 * isDisabled : null
 */

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.index.IssueIndexingService;


long issueLinkTypeId = event.getIssueLink().getLinkTypeId();
String issueKey = event.getIssueLink().getSourceObject().getKey();
long ID_CF_CWX_QUICKSEARCH = 12300;

//Managers
IssueManager issueManager = ComponentAccessor.getIssueManager();
MutableIssue mutableIssue = issueManager.getIssueObject(issueKey);
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();


CustomField cwxField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(ID_CF_CWX_QUICKSEARCH);
List<String> items = Arrays.asList(mutableIssue.getCustomFieldValue(cwxField).toString().trim().split(","));

List<String> common = new ArrayList<String>();
issueLinkManager.getOutwardLinks(mutableIssue.getId()).each{issueLink ->
    if(items.contains(issueLink.getDestinationObject().getKey())){
        common.add(issueLink.getDestinationObject().getKey());
    }
};

mutableIssue.setCustomFieldValue(cwxField, common.join(","));
issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(mutableIssue);