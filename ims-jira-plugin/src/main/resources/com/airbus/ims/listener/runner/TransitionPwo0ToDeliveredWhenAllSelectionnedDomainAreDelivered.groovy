package com.airbus.ims.listener.runner
/**
 * Projects : PWO
 * Description : Transition PWO-0 to "Delivered" when all selectionned Domain are delivered
 * Events : Issue Assigned, PWO-0 updated
 * isDisabled : false
 */

// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
        boolean isUpdated = false;
            if (changeLog != null) {
                ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
                List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
                for (changeIteamBean in changeItemBeans){
                    if (itemsChanged.contains(changeIteamBean.getField())){
                        isUpdated = true;
                    }
                }
            }

        return isUpdated;
	}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog

MutableIssue issue = (MutableIssue)event.getIssue();

//Constante
//Fields
long ID_CF_Domain=10034;
long ID_CF_PartsDataStatus=10049;
long ID_CF_MaintPlanningStatus=10071;
long ID_CF_MaintStatus=10091;
long ID_CF_FirstSNAppli = 10011
long ID_CF_PartsDataValidated=10065;
long ID_CF_MaintPlanningValidated=10084;
long ID_CF_MaintValidated=10104;

//CustomField
CustomField DomainField=customFieldManager.getCustomFieldObject(ID_CF_Domain)
CustomField PartsDataStatusField=customFieldManager.getCustomFieldObject(ID_CF_PartsDataStatus)
CustomField MaintPlanningStatusField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningStatus)
CustomField MaintStatusField=customFieldManager.getCustomFieldObject(ID_CF_MaintStatus)
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)
CustomField PartsDataValidatedField=customFieldManager.getCustomFieldObject(ID_CF_PartsDataValidated)
CustomField MaintPlanningValidatedField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningValidated)
CustomField MaintValidatedField=customFieldManager.getCustomFieldObject(ID_CF_MaintValidated)

//Variables
List<String> DomainValues
String Result="OK"
String Rejected="Rejected"

List<String> itemsChangedRejected = new ArrayList<String>()
itemsChangedRejected.add(PartsDataValidatedField.getFieldName())
itemsChangedRejected.add(MaintPlanningValidatedField.getFieldName())
itemsChangedRejected.add(MaintValidatedField.getFieldName())

boolean isRejected = (boolean) workloadIsUpdated(changeLog, itemsChangedRejected)

//Business
if (!isRejected && (issue.getCustomFieldValue(PartsDataValidatedField)==Rejected || issue.getCustomFieldValue(MaintPlanningValidatedField)==Rejected || issue.getCustomFieldValue(MaintValidatedField)==Rejected)){
    Result = "KO"
    log.error "isRejected"
}

//if(issue.getCustomFieldValue(FirstSNAppliField)=="Update"){
    log.error "entre dans le if"
    DomainValues = (List<String>) issue.getCustomFieldValue(DomainField)
    log.error "Domains : "+DomainValues
    if (!DomainValues){
        Result="KO"
    }
    for (domainValue in DomainValues){
        log.error "domain evaluated : "+domainValue
        if(domainValue.toString()=="Parts Data"){
            log.error "domain is part data"
            log.error "status of part data : "+issue.getCustomFieldValue(PartsDataStatusField)
            log.error "Result : "+Result
            if (issue.getCustomFieldValue(PartsDataStatusField).toString()=="Delivered" && Result=="OK"){
                log.error "OK pour parts data"
                Result = "OK"
            }else{
                log.error "KO pour parts data"
                Result = "KO"
            }
        }
        if(domainValue.toString()=="Maint Planning"){
            log.error "domain is maint planning"
            log.error "status of maint planning : "+issue.getCustomFieldValue(MaintPlanningStatusField)
            log.error "Result : "+Result
            if (issue.getCustomFieldValue(MaintPlanningStatusField).toString()=="Delivered" && Result=="OK"){
                log.error "OK pour maint planning"
                Result = "OK"
            }else{
                log.error "KO pour maint planning"
                Result = "KO"
            }
        }
        if(domainValue.toString()=="Maintenance"){
            log.error "domain is maint "
            log.error "status of maint  : "+issue.getCustomFieldValue(MaintStatusField)
            log.error "Result : "+Result
            if (issue.getCustomFieldValue(MaintStatusField).toString()=="Delivered" && Result=="OK"){
                log.error "OK pour maintenance"
                Result = "OK"
            }else{
                log.error "KO pour maintenance"
                Result = "KO"
            }
        }
    }
    log.error "Result : "+Result

    if (Result=="OK"){
        true
    }else{
        false
    }
//}
//else{
//    false
//}