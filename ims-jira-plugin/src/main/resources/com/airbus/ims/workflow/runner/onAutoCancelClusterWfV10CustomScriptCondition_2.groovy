package com.airbus.ims.workflow.runner


import com.airbus.ims.business.manager.ClusterWorkFlowManager;

def clusterWorkflowManager = new ClusterWorkFlowManager(issue);
clusterWorkflowManager.onAutoCancelCustomScriptedCondition();

