package com.airbus.ims.workflow.runner

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager

// import

import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();


String ID_IT_PWO0 = "10004";
String ID_IT_PWO1 = "10005";
String ID_IT_PWO2 = "10006";
String ID_IT_PWO21 = "10007";
String ID_IT_PWO22 = "10008";
String ID_IT_PWO23 = "10009";
String ID_IT_Cluster="10001";
String ID_IT_Trigger="10000";

String StatusCompleted="Completed"
String StatusCancelled="Cancelled"

List<String> StatusesOk = new ArrayList<String>();
StatusesOk.add(StatusCompleted)
StatusesOk.add(StatusCancelled)

String Result="Ok"


def LinkedIssuesOfCurrentCluster=issueLinkManager.getLinkCollection(issue,user).getAllIssues()
for (linkedIssueOfCurrentCluster in LinkedIssuesOfCurrentCluster){
	if (linkedIssueOfCurrentCluster.getIssueTypeId() == ID_IT_PWO0){
        if (!StatusesOk.contains(linkedIssueOfCurrentCluster.getStatusObject().getName().toString())){
            Result="KO"
        }
        def LinkedIssuesOfPWO0=issueLinkManager.getLinkCollection(linkedIssueOfCurrentCluster,user).getAllIssues()
        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0){
            if (linkedIssueOfPWO0.getIssueTypeId()==ID_IT_PWO1){
                if (!StatusesOk.contains(linkedIssueOfPWO0.getStatusObject().getName().toString())){
                    Result="KO"
                }
                def LinkedIssuesOfPWO1=issueLinkManager.getLinkCollection(linkedIssueOfPWO0,user).getAllIssues()
                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1){
                    if (linkedIssueOfPWO1.getIssueTypeId()==ID_IT_PWO2){
                        if (!StatusesOk.contains(linkedIssueOfPWO1.getStatusObject().getName().toString())){
                            Result="KO"
                        }
                    }
                }
            }
        }
	}
}

if (Result.toString()=="Ok"){
    passesCondition=true
}
else{
    passesCondition=false
}