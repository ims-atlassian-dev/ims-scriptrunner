package com.airbus.ims.workflow.runner

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager

// import

import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();

long ID_Project_DU=10002

def LinkedIssuesOfDU = issueLinkManager.getLinkCollection(issue,user).getInwardIssues("Copies")
if (LinkedIssuesOfDU){
    for (linkedIssueOfDU in LinkedIssuesOfDU){
        log.debug "Outward link of DU = "+linkedIssueOfDU.getKey()
        if (linkedIssueOfDU.getProjectId()== ID_Project_DU){
            passesCondition=false
        }
        else{
            passesCondition=true
        }
    }
}
else{
    passesCondition=true
}