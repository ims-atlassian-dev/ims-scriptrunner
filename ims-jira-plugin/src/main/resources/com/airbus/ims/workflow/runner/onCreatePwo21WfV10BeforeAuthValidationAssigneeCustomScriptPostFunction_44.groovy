package com.airbus.ims.workflow.runner

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.security.IssueSecurityLevelManager
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager

def issueSecuritySchemeManager = ComponentAccessor.getComponent(IssueSecuritySchemeManager)
def issueSecurityLevelManager = ComponentAccessor.getComponent(IssueSecurityLevelManager)
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

def schemeFromName = issueSecuritySchemeManager.getSchemeObjects().find { it.name == "Program" }
def schemeFromProject = issueSecuritySchemeManager.getSchemeFor(ComponentAccessor.projectManager.getProjectByCurrentKey("PWO"))

//Constantes
long ID_CF_Program = 10347;

CustomField ProgramField = customFieldManager.getCustomFieldObject(ID_CF_Program);

String ProgramnFeed = issue.getCustomFieldValue(ProgramField).toString()
String[] MOAnFeedFormatted = ProgramnFeed.substring(19).replaceAll("</value>/\n/</content>","").replaceAll("</value>",",").replaceAll("<value>","").replaceAll(/\n/,"").replace("  "," ").split(",");
log.debug "MOA nFeed formatted : "+MOAnFeedFormatted[0]

def securityLvl = issueSecurityLevelManager.getIssueSecurityLevels(schemeFromName.id).find { it ->
    it.name == MOAnFeedFormatted[0]
}?.id
log.debug "securityLvl : "+securityLvl

issue.setSecurityLevelId(securityLvl)