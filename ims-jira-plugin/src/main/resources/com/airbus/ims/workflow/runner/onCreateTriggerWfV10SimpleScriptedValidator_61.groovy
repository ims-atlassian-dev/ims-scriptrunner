package com.airbus.ims.workflow.runner


import com.airbus.ims.business.manager.TriggerWorkflowManager;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

def log = Logger.getLogger("\"com.airbus.ims.business.runner\"");
log.setLevel(Level.DEBUG);
log.debug("onCreateTrigger:Check issue: " + issue);
def triggerWorkflowManager = new TriggerWorkflowManager(issue);
triggerWorkflowManager.onCreateSimpleScriptedValidator();

