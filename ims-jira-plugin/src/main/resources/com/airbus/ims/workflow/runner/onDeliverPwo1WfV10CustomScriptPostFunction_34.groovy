package com.airbus.ims.workflow.runner

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager

// import

import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Category

Category log = log;
log.setLevel(org.apache.log4j.Level.DEBUG);

log.debug "Start";

//Manager
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
ChangeHistoryManager changeHistoryManager = ComponentAccessor.getChangeHistoryManager()
ChangeHistoryItem changeHistoryItem = ComponentAccessor.getComponentOfType(ChangeHistoryItem)
def changeItems = ComponentAccessor.changeHistoryManager.getAllChangeItems(issue)
def changeHolder = new DefaultIssueChangeHolder();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser()

log.debug "Issue : "+issue.getKey()

//Constantes
long ID_CF_NbRework = 10149;
long ID_CF_ManuallyEdited = 10172;

//CustomField
CustomField ManuallyEditedField = customFieldManager.getCustomFieldObject(ID_CF_ManuallyEdited);
CustomField NbReworkField = customFieldManager.getCustomFieldObject(ID_CF_NbRework);

//Valeur
String IA_ManuallyEdited=issue.getCustomFieldValue(ManuallyEditedField)
double NbRework =0
if (issue.getCustomFieldValue(NbReworkField)){
        NbRework=issue.getCustomFieldValue(NbReworkField).toString().toDouble()
}
    
//Business
double ValeurToReturn
double Compteur=0
if (IA_ManuallyEdited=="Yes"){
    ValeurToReturn = NbRework + 1
}
else{
    log.debug "Pas d'edit manuel"
    for (ChangeHistoryItem item : changeItems){
        log.debug "Change history item : "+item.field+", change history value : "+item.getTos().values()
        if (item.field == 'status' && item.getTos().values().contains('Delivered') && item.getFroms().values().contains('In Progress')) {
            log.debug "je compte"
            Compteur = Compteur +1
            log.debug "Compteur : "+Compteur
        }
    }
    ValeurToReturn = Compteur-1
    log.debug "Valeur : "+ValeurToReturn
}
issue.setCustomFieldValue(NbReworkField,ValeurToReturn)
issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);