package com.airbus.ims.workflow.runner


import com.airbus.ims.business.manager.TriggerWorkflowManager;

def triggerWorkflowManager = new TriggerWorkflowManager(issue);
passesCondition = triggerWorkflowManager.onInitializeCustomScriptedCondition();

