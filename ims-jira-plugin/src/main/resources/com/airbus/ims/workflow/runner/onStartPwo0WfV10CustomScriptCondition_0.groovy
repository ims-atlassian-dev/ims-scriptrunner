package com.airbus.ims.workflow.runner

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager

//Manager
def issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
passesCondition=false

if (issueLinkManager.getInwardLinks(issue.id)){
    log.debug "inward link"
    issueLinkManager.getInwardLinks(issue.id).each { issueLink ->
        Issue sourceIssue=issueLink.getSourceObject()
        log.debug "source issue : "+sourceIssue
        String issueType = sourceIssue.getIssueTypeId().toString()
        log.debug "issuetype: "+issueType
        if (issueType == "10001") { 
            passesCondition = true
        }
    }
}
if (issueLinkManager.getOutwardLinks(issue.id)){
    log.debug "outward link"
    issueLinkManager.getOutwardLinks(issue.id).each { issueLink ->
        Issue sourceIssue=issueLink.getDestinationObject()
        log.debug "source issue : "+sourceIssue
        String issueType = sourceIssue.getIssueTypeId().toString()
        log.debug "issuetype: "+issueType
        if (issueType == "10001") { 
            passesCondition = true
        }
    }
}