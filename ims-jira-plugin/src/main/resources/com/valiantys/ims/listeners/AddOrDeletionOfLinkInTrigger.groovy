package com.valiantys.ims.listeners
/**
 * Projects : TRG
 * Description : Add or Deletion of Link in Trigger
 * Events : IssueLinkCreatedEvent, IssueLinkDeletedEvent
 * isDisabled : false
 */

// import

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger

import java.sql.Timestamp

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
}*/

String formatSecondesValueInHoursMinutes(Long secondes) {
    log.debug "Start formatSecondesValueInHoursMinutes - secondes : " + secondes
    if (secondes && secondes > 0) {
        String result

        int hoursExtracted = (int) (secondes / 3600)
        int totalMinutes = (int) (secondes / 60)
        int minutesExtracted = (int) totalMinutes.mod(60)

        result = hoursExtracted.toString() + "h " + minutesExtracted.toString() + "m"
        log.debug "End formatSecondesValueInHoursMinutes - result  : " + result
        return result
    } else {
        log.debug "End formatSecondesValueInHoursMinutes - minutes is null"
    }
}

Long formatHoursMinutesFormatInSecondes(String hoursMinutes) {
    log.debug "Start formatHoursMinutesFormatInSecondes - hoursMinutes : " + hoursMinutes;
    if (!hoursMinutes.equals("null")) {
        Long result = 0L

        String[] hoursMinutesTable
        Long hourConvertedInMinutes = 0
        String hourNumberString
        Long minutesNumber = 0

        hoursMinutesTable = hoursMinutes.split("h")
        hourConvertedInMinutes = hoursMinutesTable[0].toString().toLong() * 60
        minutesNumber = hoursMinutesTable[1].toString().substring(0, hoursMinutesTable[1].toString().length() - 1).toLong()

        result = (hourConvertedInMinutes + minutesNumber) * 60

        log.debug "End formatHoursMinutesFormatInSecondes - result : " + result;
        return result
    } else {
        log.debug "End formatHoursMinutesFormatInSecondes - hoursMinutes is null"
    }
}

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder()

MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject()

//Constante
//Issue type
String ID_IT_MOAObjFreeze = "10023"
String ID_IT_AirbusObjFreeze = "10024"
String ID_IT_Cluster = "10001"
String ID_IT_Trigger = "10000"
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"

List<String> ObjFreezeIT = new ArrayList<String>();
ObjFreezeIT.add(ID_IT_MOAObjFreeze)
ObjFreezeIT.add(ID_IT_AirbusObjFreeze)

List<String> PWOIT = new ArrayList<String>();
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Field of Trigger
long ID_CF_RestrictedDate = 10800
long ID_CF_PartsData_EstimatedCosts = 10024
long ID_CF_MaintPlanning_EstimatedCosts = 10027
long ID_CF_Maint_EstimatedCosts = 10030
long ID_CF_PartsData_TimeSpent = 11012
long ID_CF_MaintPlanning_TimeSpent = 11013
long ID_CF_Maint_TimeSpent = 11014
long ID_CF_PartsData_EffectiveCosts = 10113
long ID_CF_MaintPlanning_EffectiveCosts = 10114
long ID_CF_Maint_EffectiveCosts = 10115
long ID_CF_TriggerTotalEffectiveCosts = 10116
long ID_CF_TotalBlockingDuration = 10015
long ID_CF_RequestedDate = 10010
long ID_CF_ImpactOnTD = 10008
long ID_CF_Blocked = 10138
long ID_CF_BlockedDomain = 10041
long ID_CF_Domain = 10139
long ID_CF_Escalated = 10055
long ID_CF_TotalEscalatedDuration = 11019
long ID_CF_IA_TimeSpent = 11017
long ID_CF_IA_EffectiveCost = 10161
long ID_CF_Auth_TimeSpent = 11015
long ID_CF_Auth_EffectiveCost = 10240
long ID_CF_Form_TimeSpent = 11016
long ID_CF_Form_EffectiveCost = 10305
long ID_CF_Int_TimeSpent = 11018
long ID_CF_Int_EffectiveCost = 10332

//Field of other object
long ID_CF_PlannedQG2 = 10631
long ID_CF_ImpactConfirmed = 10141
long ID_CF_PreIA_PartsData_RequestedDate = 10058
long ID_CF_PreIA_PartsData_Validation_RequestedDate = 10066
long ID_CF_PartsData_PreIA_1stCommitedDate = 10060
long ID_CF_PartsData_PreIA_LastCommitedDate = 10062
long ID_CF_PartsData_PreIA_Validation_1stCommitedDate = 10067
long ID_CF_PartsData_PreIA_Validation_LastCommitedDate = 10090

long ID_CF_PreIA_MaintPlanning_RequestedDate = 10077
long ID_CF_MaintPlanning_PreIA_1stCommitedDate = 10079
long ID_CF_MaintPlanning_PreIA_LastCommitedDate = 10088
long ID_CF_PreIA_MaintPlanning_Validation_RequestedDate = 10085
long ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate = 10086
long ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate = 10081

long ID_CF_PreIA_Maint_RequestedDate = 10097
long ID_CF_Maint_PreIA_1stCommitedDate = 10099
long ID_CF_Maint_PreIA_LastCommitedDate = 10101
long ID_CF_PreIA_Maint_Validation_RequestedDate = 10105
long ID_CF_Maint_PreIA_Validation_1stCommitedDate = 10106
long ID_CF_Maint_PreIA_Validation_LastCommitedDate = 10108

long ID_CF_IA_RequestedDate = 10144
long ID_CF_IA_Validation_RequestedDate = 10154
long ID_CF_IA_1stCommittedDate = 10145
long ID_CF_IA_LastCommittedDate = 10146
long ID_CF_IA_Validation_1stCommittedDate = 10155
long ID_CF_IA_Validation_LastCommittedDate = 10156

long ID_CF_Auth_RequestedDate = 10175
long ID_CF_Auth_Verification_RequestedDate = 10192
long ID_CF_Auth_Validation_RequestedDate = 10190

long ID_CF_Auth_LastCommittedDate = 10177
long ID_CF_Auth_Verification_LastCommittedDate = 10185
long ID_CF_Auth_Validation_LastCommittedDate = 10193

long ID_CF_Auth_FirstCommittedDate = 10176
long ID_CF_Auth_Verification_FirstCommittedDate = 10184
long ID_CF_Auth_Validation_FirstCommittedDate = 10191

long ID_CF_Form_RequestedDate = 10283
long ID_CF_Form_Verification_RequestedDate = 10292
long ID_CF_Form_Validation_RequestedDate = 10299

long ID_CF_Form_1stCommittedDate = 10284
long ID_CF_Form_Verification_1stCommittedDate = 10293
long ID_CF_Form_Validation_1stCommittedDate = 10300

long ID_CF_Form_LastCommittedDate = 10285
long ID_CF_Form_Verification_LastCommittedDate = 10294
long ID_CF_Form_Validation_LastCommittedDate = 10301

long ID_CF_Int_RequestedDate = 10326
long ID_CF_Int_1stCommittedDate = 10327
long ID_CF_Int_LastCommittedDate = 10328

//CustomField
CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ID_CF_RestrictedDate)
CustomField PartsData_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EstimatedCosts)
CustomField MaintPlanning_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EstimatedCosts)
CustomField Maint_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EstimatedCosts)
CustomField PartsData_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_TimeSpent)
CustomField MaintPlanning_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_TimeSpent)
CustomField Maint_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Maint_TimeSpent)
CustomField PartsData_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EffectiveCosts)
CustomField MaintPlanning_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EffectiveCosts)
CustomField Maint_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EffectiveCosts)
CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_TriggerTotalEffectiveCosts)
CustomField TotalBlockingDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalBlockingDuration)
CustomField TotalEscalatedDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalEscalatedDuration)
CustomField PlannedQG2Field = customFieldManager.getCustomFieldObject(ID_CF_PlannedQG2)
CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_RequestedDate)
CustomField ImpactOnTDField = customFieldManager.getCustomFieldObject(ID_CF_ImpactOnTD)
CustomField ImpactConfirmedField = customFieldManager.getCustomFieldObject(ID_CF_ImpactConfirmed)
CustomField BlockedField = customFieldManager.getCustomFieldObject(ID_CF_Blocked)
CustomField EscalatedField = customFieldManager.getCustomFieldObject(ID_CF_Escalated)
CustomField BlockedDomainField = customFieldManager.getCustomFieldObject(ID_CF_BlockedDomain)
CustomField DomainField = customFieldManager.getCustomFieldObject(ID_CF_Domain)
CustomField IATimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_IA_TimeSpent)
CustomField IAEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_EffectiveCost)
CustomField AuthTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Auth_TimeSpent)
CustomField AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Auth_EffectiveCost)
CustomField FormTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Form_TimeSpent)
CustomField FormEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Form_EffectiveCost)
CustomField IntTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Int_TimeSpent)
CustomField IntEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Int_EffectiveCost)
CustomField PreIA_PartsData_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_RequestedDate)
CustomField PreIA_PartsData_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_Validation_RequestedDate)
CustomField PartsData_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_1stCommitedDate)
CustomField PartsData_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_LastCommitedDate)
CustomField PartsData_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_1stCommitedDate)
CustomField PartsData_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_LastCommitedDate)

CustomField PreIA_MaintPlanning_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_RequestedDate)
CustomField PreIA_MaintPlanning_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_Validation_RequestedDate)
CustomField MaintPlanning_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_1stCommitedDate)
CustomField MaintPlanning_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_LastCommitedDate)
CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate)
CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate)

CustomField PreIA_Maint_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_RequestedDate)
CustomField PreIA_Maint_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_Validation_RequestedDate)
CustomField Maint_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_1stCommitedDate)
CustomField Maint_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_LastCommitedDate)
CustomField Maint_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_1stCommitedDate)
CustomField Maint_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_LastCommitedDate)

CustomField IA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_RequestedDate)
CustomField IA_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_1stCommittedDate)
CustomField IA_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_LastCommittedDate)
CustomField IA_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_RequestedDate)
CustomField IA_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stCommittedDate)
CustomField IA_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastCommittedDate)

CustomField Auth_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_RequestedDate)
CustomField Auth_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_RequestedDate)
CustomField Auth_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_RequestedDate)

CustomField Auth_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_FirstCommittedDate)
CustomField Auth_Verification_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_FirstCommittedDate)
CustomField Auth_Validation_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_FirstCommittedDate)

CustomField Auth_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_LastCommittedDate)
CustomField Auth_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_LastCommittedDate)
CustomField Auth_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_LastCommittedDate)



CustomField Form_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_RequestedDate)
CustomField Form_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_RequestedDate)
CustomField Form_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_RequestedDate)

CustomField Form_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_1stCommittedDate)
CustomField Form_Verification_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_1stCommittedDate)
CustomField Form_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_1stCommittedDate)

CustomField Form_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_LastCommittedDate)
CustomField Form_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_LastCommittedDate)
CustomField Form_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_LastCommittedDate)

CustomField Int_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_RequestedDate)
CustomField Int_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_1stCommittedDate)
CustomField Int_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_LastCommittedDate)


//Variables
Timestamp PlannedQG2
Date RequestedDate
double PartsDataEstimatedCosts = 0
double MaintPlanningEstimatedCosts = 0
double MaintEstimatedCosts = 0
long PartsDataTimeSpent = 0
long MaintPlanningTimeSpent = 0
long MaintTimeSpent = 0
double PartsDataEffectiveCosts = 0
double MaintPlanningEffectiveCosts = 0
double MaintEffectiveCosts = 0
double TotalBlockingDuration = 0
double TotalEscalatedDuration = 0
double TriggerEffectiveCost = 0
String ImpactOnTDResult
String BlockedResult = "No"
String EscalatedResult = "No"
String RestrictedDataResult = "No"
MutableIssue ObjFreezeIssue
String PartsDataTimeSpentString
String MaintPlanningTimeSpentString
String MaintTimeSpenString
MutableIssue ClusterIssue
double TimeSpentPWO1
double IAEffectiveCost
String DomainOfPWO2
double AuthTimeSpent
double AuthEffectiveCost
double FormTimeSpent
double FormEffectiveCost
double IntTimeSpent
double IntEffectiveCost
long PartsDataTimeSpentCurrent
long MaintPlanningTimeSpentCurrent
long MaintTimeSpentCurrent
long IATimeSpentCurrent
long AuthTimeSpentCurrent
long FormTimeSpentCurrent
long IntTimeSpentCurrent

Date RequestedDateTrigger
Date Auth_RequestedDate
Date Auth_Verification_RequestedDate
Date Auth_Validation_RequestedDate
Date Form_RequestedDate
Date Form_Verification_RequestedDate
Date Form_Validation_RequestedDate
Date Int_RequestedDate


//Business
if (issue.getIssueTypeId() == ID_IT_Trigger) {
    log.debug "issue : " + issue.getKey()
    //Get linked issues
    def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssue in LinkedIssues) {
        //If linked issue is Obj. Freeze, get data
        if (ObjFreezeIT.contains(linkedIssue.getIssueTypeId())) {
            PlannedQG2 = (Timestamp) linkedIssue.getCustomFieldValue(PlannedQG2Field)
        }
        //If linked issue is Cluster
        if (linkedIssue.getIssueTypeId() == ID_IT_Cluster) {
            //Get linked issue of Cluster
            def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
            for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                //Get PWO-0 from Cluster
                if (PWOIT.contains(linkedIssueOfCluster.getIssueTypeId())) {
                    if (linkedIssueOfCluster.getCustomFieldValue(TotalBlockingDurationField)) {
                        TotalBlockingDuration = TotalBlockingDuration + (Double) linkedIssueOfCluster.getCustomFieldValue(TotalBlockingDurationField)
                    }
                    //Blocked
                    if (linkedIssueOfCluster.getCustomFieldValue(BlockedField).toString() == "Yes") {
                        BlockedResult = "Yes"
                    }

                    if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_PWO0) {
                        //To remove if update not needed
                        def LinkedIssuesOfPWO0RequestedDate = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                        for (linkedIssueOfPWO0RequestedDate in LinkedIssuesOfPWO0RequestedDate) {
                            if (linkedIssueOfPWO0RequestedDate.getIssueTypeId()==ID_IT_Cluster){
                                def LinkedIssuesOfClusterOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0RequestedDate, user).getAllIssues()
                                for (linkedIssueOfClusterOfPWO0 in LinkedIssuesOfClusterOfPWO0) {
                                    if (linkedIssueOfClusterOfPWO0.getIssueTypeId()==ID_IT_Trigger){
                                        if (linkedIssueOfClusterOfPWO0.getCustomFieldValue(RequestedDateField)){
                                            if(!RequestedDateTrigger || RequestedDateTrigger > (Date)linkedIssueOfClusterOfPWO0.getCustomFieldValue(RequestedDateField)){
                                                RequestedDateTrigger = (Date) linkedIssueOfClusterOfPWO0.getCustomFieldValue(RequestedDateField)
                                                log.debug "Requested date trigger : " + RequestedDateTrigger
                                            }
                                        }
                                    }
                                }
                            }
                        }
                            
                            
                            
                            
                        //Date RequestedDateTrigger = (Date) issue.getCustomFieldValue(RequestedDateField)
                        if (linkedIssueOfCluster.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                            Date RequestedDatePWO0 = (Timestamp) (RequestedDateTrigger - 119)
                            
                            Date PreIA_PartsData_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
                            Date PreIA_PartsData_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
                            Date PartsData_PreIA_1stCommitedDate = PreIA_PartsData_RequestedDate
                            Date PartsData_PreIA_LastCommitedDate = PartsData_PreIA_1stCommitedDate
                            Date PartsData_PreIA_Validation_1stCommitedDate = PreIA_PartsData_Validation_RequestedDate
                            Date PartsData_PreIA_Validation_LastCommitedDate = PartsData_PreIA_Validation_1stCommitedDate
                            
                            Date PreIA_MaintPlanning_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
                            Date PreIA_MaintPlanning_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
                            Date MaintPlanning_PreIA_1stCommitedDate = PreIA_MaintPlanning_RequestedDate
                            Date MaintPlanning_PreIA_LastCommitedDate = MaintPlanning_PreIA_1stCommitedDate
                            Date MaintPlanning_PreIA_Validation_1stCommitedDate = PreIA_MaintPlanning_Validation_RequestedDate
                            Date MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_PreIA_Validation_1stCommitedDate
                                
                            Date PreIA_Maint_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
                            Date PreIA_Maint_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
                            Date Maint_PreIA_1stCommitedDate = PreIA_Maint_RequestedDate
                            Date Maint_PreIA_LastCommitedDate = Maint_PreIA_1stCommitedDate
                            Date Maint_PreIA_Validation_1stCommitedDate = PreIA_Maint_Validation_RequestedDate
                            Date Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_1stCommitedDate

                            linkedIssueOfCluster.setCustomFieldValue(RequestedDateField, RequestedDatePWO0)
                            
                            linkedIssueOfCluster.setCustomFieldValue(PreIA_PartsData_RequestedDateField, PreIA_PartsData_RequestedDate)
                            linkedIssueOfCluster.setCustomFieldValue(PreIA_PartsData_Validation_RequestedDateField, PreIA_PartsData_Validation_RequestedDate)
                            linkedIssueOfCluster.setCustomFieldValue(PartsData_PreIA_1stCommitedDateField, PartsData_PreIA_1stCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(PartsData_PreIA_LastCommitedDateField, PartsData_PreIA_LastCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate)
                            
                            linkedIssueOfCluster.setCustomFieldValue(PreIA_MaintPlanning_RequestedDateField, PreIA_MaintPlanning_RequestedDate)
                            linkedIssueOfCluster.setCustomFieldValue(PreIA_MaintPlanning_Validation_RequestedDateField, PreIA_MaintPlanning_Validation_RequestedDate)
                            linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_PreIA_1stCommitedDateField, MaintPlanning_PreIA_1stCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_PreIA_LastCommitedDateField, MaintPlanning_PreIA_LastCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate)
                            
                            linkedIssueOfCluster.setCustomFieldValue(PreIA_Maint_RequestedDateField, PreIA_Maint_RequestedDate)
                            linkedIssueOfCluster.setCustomFieldValue(PreIA_Maint_Validation_RequestedDateField, PreIA_Maint_Validation_RequestedDate)
                            linkedIssueOfCluster.setCustomFieldValue(Maint_PreIA_1stCommitedDateField, Maint_PreIA_1stCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(Maint_PreIA_LastCommitedDateField, Maint_PreIA_LastCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate)
                            linkedIssueOfCluster.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate)
                            
                            issueManager.updateIssue(user, linkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false);
                            issueIndexingService.reIndex(linkedIssueOfCluster);
                        }

                        if (linkedIssueOfCluster.getCustomFieldValue(PartsData_EstimatedCostsField)) {
                            PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfCluster.getCustomFieldValue(PartsData_EstimatedCostsField)
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EstimatedCostsField)) {
                            MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EstimatedCostsField)
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(Maint_EstimatedCostsField)) {
                            MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfCluster.getCustomFieldValue(Maint_EstimatedCostsField)
                        }

                        if (linkedIssueOfCluster.getCustomFieldValue(PartsData_TimeSpentField)) {
                            PartsDataTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfCluster.getCustomFieldValue(PartsData_TimeSpentField).toString())
                            PartsDataTimeSpent = PartsDataTimeSpent + PartsDataTimeSpentCurrent
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_TimeSpentField)) {
                            MaintPlanningTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_TimeSpentField).toString())
                            MaintPlanningTimeSpent = MaintPlanningTimeSpent + MaintPlanningTimeSpentCurrent
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(Maint_TimeSpentField)) {
                            MaintTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfCluster.getCustomFieldValue(Maint_TimeSpentField).toString())
                            MaintTimeSpent = MaintTimeSpent + MaintTimeSpentCurrent
                        }


                        if (linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostsField)) {
                            PartsDataEffectiveCosts = PartsDataEffectiveCosts + (Double) linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostsField)
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostsField)) {
                            MaintPlanningEffectiveCosts = MaintPlanningEffectiveCosts + (Double) linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostsField)
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostsField)) {
                            MaintEffectiveCosts = MaintEffectiveCosts + (Double) linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostsField)
                        }

                        if (linkedIssueOfCluster.getCustomFieldValue(BlockedDomainField)) {
                            BlockedResult = "Yes"
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(EscalatedField)) {
                            EscalatedResult = "Yes"
                        }

                        //Get PWO-1 from PWO-0
                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                ImpactOnTDResult = "No"
                            }
                        }
                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                //To remove if update not needed
                                if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1 && linkedIssueOfPWO0.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                    Date RequestedDatePWO1 = (Timestamp) (RequestedDateTrigger - 98)
                                    Date IA_RequestedDate = (Timestamp) (RequestedDateTrigger - 105)
                                    Date IA_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 98)
                                    Date IA_1stCommittedDate = IA_RequestedDate
                                    Date IA_LastCommittedDate = IA_1stCommittedDate
                                    Date IA_Validation_1stCommittedDate = IA_Validation_RequestedDate
                                    Date IA_Validation_LastCommittedDate = IA_Validation_1stCommittedDate

                                    linkedIssueOfPWO0.setCustomFieldValue(RequestedDateField, RequestedDatePWO1)
                                    linkedIssueOfPWO0.setCustomFieldValue(IA_RequestedDateField, IA_RequestedDate)
                                    linkedIssueOfPWO0.setCustomFieldValue(IA_Validation_RequestedDateField, IA_Validation_RequestedDate)
                                    linkedIssueOfPWO0.setCustomFieldValue(IA_1stCommittedDateField, IA_1stCommittedDate)
        							linkedIssueOfPWO0.setCustomFieldValue(IA_LastCommittedDateField, IA_LastCommittedDate)
                                    linkedIssueOfPWO0.setCustomFieldValue(IA_Validation_1stCommittedDateField, IA_Validation_1stCommittedDate)
       							 	linkedIssueOfPWO0.setCustomFieldValue(IA_Validation_LastCommittedDateField, IA_Validation_LastCommittedDate)
                                    
                                    issueManager.updateIssue(user, linkedIssueOfPWO0, EventDispatchOption.DO_NOT_DISPATCH, false);
                                    issueIndexingService.reIndex(linkedIssueOfPWO0);
                                }

                                if (linkedIssueOfPWO0.getCustomFieldValue(ImpactConfirmedField).toString() == "Yes") {
                                    ImpactOnTDResult = "Yes"
                                } else if (ImpactOnTDResult != "Yes" && linkedIssueOfPWO0.getCustomFieldValue(ImpactConfirmedField).toString() == "Analysis not completed") {
                                    ImpactOnTDResult = "Analysis not completed"
                                }
                                if (linkedIssueOfPWO0.getCustomFieldValue(BlockedField).toString() == "Yes") {
                                    BlockedResult = "Yes"
                                }
                                if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                    EscalatedResult = "Yes"
                                }

                                if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                    IATimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField).toString())
                                    //TimeSpentPWO1=(Double)linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField);
                                }
                                else{
                                    IATimeSpentCurrent = 0
                                }
                                if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)) {
                                    IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)
                                }

                                if (linkedIssueOfPWO0.getCustomFieldValue(DomainField).toString() == "Parts Data") {
                                    PartsDataTimeSpent = PartsDataTimeSpent + IATimeSpentCurrent
                                    PartsDataEffectiveCosts = PartsDataEffectiveCosts + IAEffectiveCost
                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainField).toString() == "Maintenance Planning") {
                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + IATimeSpentCurrent
                                    MaintPlanningEffectiveCosts = MaintPlanningEffectiveCosts + IAEffectiveCost
                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainField).toString() == "Maintenance") {
                                    MaintTimeSpent = MaintTimeSpent + IATimeSpentCurrent
                                    MaintEffectiveCosts = MaintEffectiveCosts + IAEffectiveCost
                                }

                                //Get PWO-2 from PWO-1
                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                        //To remove if update not needed
                                        if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2 && linkedIssueOfPWO1.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                            Date RequestedDatePWO2 = (Timestamp) (RequestedDateTrigger - 28)

                                            linkedIssueOfPWO1.setCustomFieldValue(RequestedDateField, RequestedDatePWO2)
                                            issueManager.updateIssue(user, linkedIssueOfPWO1, EventDispatchOption.DO_NOT_DISPATCH, false);
                                            issueIndexingService.reIndex(linkedIssueOfPWO1);
                                        }
                                        
                                        if (linkedIssueOfPWO1.getCustomFieldValue(BlockedField).toString() == "Yes") {
                                            BlockedResult = "Yes"
                                        }
                                        if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                            EscalatedResult = "Yes"
                                        }
                                        DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainField).toString()
                                        log.debug "Domain of PWO-2 : " + DomainOfPWO2
                                        //Get PWO-2's subtask
                                        def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                        for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                            log.debug "linked issue of " + linkedIssueOfPWO1.getKey() + " is : " + linkedIssueOfPWO2.getKey() + " of type " + linkedIssueOfPWO2.getIssueTypeId()
                                            if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                //To remove if update not needed
                                                if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21 && linkedIssueOfPWO1.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                                    Auth_RequestedDate = (Timestamp) (RequestedDateTrigger - 84)
                                                    Auth_Verification_RequestedDate = (Timestamp) (RequestedDateTrigger - 70)
                                                    Auth_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 56)
                                                    
                                                    Date Auth_FirstCommittedDate = Auth_RequestedDate
                                                    Date Auth_Verification_FirstCommittedDate = Auth_Verification_RequestedDate
                                                    Date Auth_Validation_FirstCommittedDate = Auth_Validation_RequestedDate

                                                    Date Auth_LastCommittedDate = Auth_FirstCommittedDate
                                                    Date Auth_Verification_LastCommittedDate = Auth_Verification_FirstCommittedDate
                                                    Date Auth_Validation_LastCommittedDate = Auth_Validation_FirstCommittedDate

                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_RequestedDateField, Auth_RequestedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_Verification_RequestedDateField, Auth_Verification_RequestedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_Validation_RequestedDateField, Auth_Validation_RequestedDate)
                                                    
                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_FirstCommittedDateField, Auth_FirstCommittedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_Verification_FirstCommittedDateField, Auth_Verification_FirstCommittedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_Validation_FirstCommittedDateField, Auth_Validation_FirstCommittedDate)

                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_LastCommittedDateField, Auth_LastCommittedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_Verification_LastCommittedDateField, Auth_Verification_LastCommittedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Auth_Validation_LastCommittedDateField, Auth_Validation_LastCommittedDate)
                                                    
                                                    issueManager.updateIssue(user, linkedIssueOfPWO2, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                    issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                }
                                                
                                                log.debug "PWO-2.1"
                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField)) {
                                                    AuthTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField).toString())
                                                    //AuthTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField)
                                                }
                                                else{
                                                    AuthTimeSpentCurrent=0
                                                }
                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)) {
                                                    AuthEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)
                                                }
                                                if (DomainOfPWO2 == "Parts Data") {
                                                    PartsDataTimeSpent = PartsDataTimeSpent + AuthTimeSpentCurrent
                                                    PartsDataEffectiveCosts = PartsDataEffectiveCosts + AuthEffectiveCost
                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + AuthTimeSpentCurrent
                                                    MaintPlanningEffectiveCosts = MaintPlanningEffectiveCosts + AuthEffectiveCost
                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                    MaintTimeSpent = MaintTimeSpent + AuthTimeSpentCurrent
                                                    MaintEffectiveCosts = MaintEffectiveCosts + AuthEffectiveCost
                                                }
                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                //To remove if update not needed
                                                if (linkedIssueOfPWO2.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                                    Form_RequestedDate = (Timestamp) (RequestedDateTrigger - 49)
            										Form_Verification_RequestedDate = (Timestamp) (RequestedDateTrigger - 42)
													Form_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 35)
                                                    
                                                    Date Form_FirstCommittedDate = Form_RequestedDate
                                                    Date Form_Verification_FirstCommittedDate = Form_Verification_RequestedDate
                                                    Date Form_Validation_FirstCommittedDate = Form_Validation_RequestedDate

                                                    Date Form_LastCommittedDate = Form_FirstCommittedDate
                                                    Date Form_Verification_LastCommittedDate = Form_Verification_FirstCommittedDate
                                                    Date Form_Validation_LastCommittedDate = Form_Validation_FirstCommittedDate

                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_RequestedDateField, Form_RequestedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_Verification_RequestedDateField, Form_Verification_RequestedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_Validation_RequestedDateField, Form_Validation_RequestedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_1stCommittedDateField, Form_FirstCommittedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_Verification_1stCommittedDateField, Form_Verification_FirstCommittedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_Validation_1stCommittedDateField, Form_Validation_FirstCommittedDate)

                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_LastCommittedDateField, Form_LastCommittedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_Verification_LastCommittedDateField, Form_Verification_LastCommittedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Form_Validation_LastCommittedDateField, Form_Validation_LastCommittedDate)
                                                    
                                                    issueManager.updateIssue(user, linkedIssueOfPWO2, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                    issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                }
                                                
                                                
                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField)) {
                                                    FormTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField).toString())
                                                    //FormTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField)
                                                }
                                                else{
                                                    FormTimeSpentCurrent=0
                                                }
                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)) {
                                                    FormEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)
                                                }
                                                if (DomainOfPWO2 == "Parts Data") {
                                                    PartsDataTimeSpent = PartsDataTimeSpent + FormTimeSpentCurrent
                                                    PartsDataEffectiveCosts = PartsDataEffectiveCosts + FormEffectiveCost
                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + FormTimeSpentCurrent
                                                    MaintPlanningEffectiveCosts = MaintPlanningEffectiveCosts + FormEffectiveCost
                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                    MaintTimeSpent = MaintTimeSpent + FormTimeSpentCurrent
                                                    MaintEffectiveCosts = MaintEffectiveCosts + FormEffectiveCost
                                                }
                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                //To remove if update not needed
                                                if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23 && linkedIssueOfPWO1.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                                    Int_RequestedDate = (Timestamp) (RequestedDateTrigger - 28)
                                                    
                                                    Date Int_FirstCommittedDate = Int_RequestedDate
            										Date Int_LastCommittedDate = Int_FirstCommittedDate

                                                    linkedIssueOfPWO2.setCustomFieldValue(Int_RequestedDateField, Int_RequestedDate)
                                                    linkedIssueOfPWO2.setCustomFieldValue(Int_1stCommittedDateField, Int_FirstCommittedDate)
            										linkedIssueOfPWO2.setCustomFieldValue(Int_LastCommittedDateField, Int_LastCommittedDate)
                                                    issueManager.updateIssue(user, linkedIssueOfPWO2, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                    issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                }
                                                
                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField)) {
                                                    IntTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField).toString())
                                                    //IntTimeSpent=(double)linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField)
                                                }
                                                else{
                                                    IntTimeSpentCurrent=0
                                                }
                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)) {
                                                    IntEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)
                                                }
                                                if (DomainOfPWO2 == "Parts Data") {
                                                    PartsDataTimeSpent = PartsDataTimeSpent + IntTimeSpentCurrent
                                                    PartsDataEffectiveCosts = PartsDataEffectiveCosts + IntEffectiveCost
                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + IntTimeSpentCurrent
                                                    MaintPlanningEffectiveCosts = MaintPlanningEffectiveCosts + IntEffectiveCost
                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                    MaintTimeSpent = MaintTimeSpent + IntTimeSpentCurrent
                                                    MaintEffectiveCosts = MaintEffectiveCosts + IntEffectiveCost
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //If Cluster have Trigger linked, calculate data for Cluster
                else if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                    if (linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                        RestrictedDataResult = "Yes"
                    } else if (RestrictedDataResult != "Yes" && linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                        RestrictedDataResult = "Unknown"
                    }
                }
            }
            //Set data on Cluster
            ClusterIssue = issueManager.getIssueObject(linkedIssue.id)

            def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(ClusterIssue)
            def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
                it.toString() == RestrictedDataResult
            }
            ClusterIssue.setCustomFieldValue(RestrictedDataField, valueRestrictedData)
            issueManager.updateIssue(user, ClusterIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            issueIndexingService.reIndex(ClusterIssue);
        }
    }
    TriggerEffectiveCost = PartsDataEffectiveCosts + MaintPlanningEffectiveCosts + MaintEffectiveCosts

    //Set data on Trigger
    if (PlannedQG2){
        issue.setCustomFieldValue(RequestedDateField, PlannedQG2)
    }
    issue.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts)
    issue.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts)
    issue.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts)
    issue.setCustomFieldValue(PartsData_EffectiveCostsField, PartsDataEffectiveCosts)
    issue.setCustomFieldValue(MaintPlanning_EffectiveCostsField, MaintPlanningEffectiveCosts)
    issue.setCustomFieldValue(Maint_EffectiveCostsField, MaintEffectiveCosts)
    issue.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDuration)
    issue.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)

    if (ImpactOnTDResult) {
        def fieldConfigImpactOnTD = ImpactOnTDField.getRelevantConfig(issue)
        def valueImpactOnTD = ComponentAccessor.optionsManager.getOptions(fieldConfigImpactOnTD)?.find {
            it.toString() == ImpactOnTDResult
        }
        issue.setCustomFieldValue(ImpactOnTDField, valueImpactOnTD)
    }

    def fieldConfigBlocked = BlockedField.getRelevantConfig(issue)
    def valueBlocked = ComponentAccessor.optionsManager.getOptions(fieldConfigBlocked)?.find {
        it.toString() == BlockedResult
    }
    issue.setCustomFieldValue(BlockedField, valueBlocked)
    
    def fieldConfigEscalated = EscalatedField.getRelevantConfig(issue)
    def valueEscalated = ComponentAccessor.optionsManager.getOptions(fieldConfigEscalated)?.find {
        it.toString() == EscalatedResult
    }
    issue.setCustomFieldValue(EscalatedField, valueEscalated)

    PartsDataTimeSpentString = formatSecondesValueInHoursMinutes(PartsDataTimeSpent)
    issue.setCustomFieldValue(PartsData_TimeSpentField, PartsDataTimeSpentString)

    MaintPlanningTimeSpentString = formatSecondesValueInHoursMinutes(MaintPlanningTimeSpent)
    issue.setCustomFieldValue(MaintPlanning_TimeSpentField, MaintPlanningTimeSpentString)

    MaintTimeSpenString = formatSecondesValueInHoursMinutes(MaintTimeSpent)
    issue.setCustomFieldValue(Maint_TimeSpentField, MaintTimeSpenString)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
    issueIndexingService.reIndex(issue);
}
