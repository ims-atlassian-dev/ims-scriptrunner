package com.valiantys.ims.listeners
/**
 * Projects : PWO
 * Description : Addition of link to PWO-0 and PWO-1
 * Events : IssueLinkCreatedEvent
 * isDisabled : false
 */

// import

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.joda.time.LocalDate

import java.sql.Timestamp

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */


def getIssueFromNFeedFieldNewFieldsSingle(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound;
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ")
        issueFound = issueManager.getIssueObject(Long.parseLong(formattedValues))
    }

    if (issueFound) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + issueFound
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }
    return issueFound
}

def getIssueFromNFeedFieldNewFieldsMulitple(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    List<Issue> multipleIssue = new ArrayList<Issue>()
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String[] formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ").split(" ")
        for (String formatedValue in formattedValues) {
            issueFound = issueManager.getIssueObject(Long.parseLong(formatedValue))
            multipleIssue.add(issueFound)
        }
    }

    if (multipleIssue) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + multipleIssue
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result";
    }

    return multipleIssue
}

String formatSecondesValueInHoursMinutes(Long secondes) {
    log.debug "Start formatSecondesValueInHoursMinutes - secondes : " + secondes;
    if (secondes && secondes > 0) {
        String result
        int hoursExtracted = (int) (secondes / 3600)
        int totalMinutes = (int) (secondes / 60)
        int minutesExtracted = (int) totalMinutes.mod(60)
        result = hoursExtracted.toString() + "h " + minutesExtracted.toString() + "m"
        log.debug "End formatSecondesValueInHoursMinutes - result  : " + result
        return result
    } else {
        log.debug "End formatSecondesValueInHoursMinutes - minutes is null"
    }
}

Long formatHoursMinutesFormatInSecondes(String hoursMinutes) {
    log.debug "Start formatHoursMinutesFormatInSecondes - hoursMinutes : " + hoursMinutes;
    if (!hoursMinutes.equals("null")) {
        Long result = 0L
        String[] hoursMinutesTable
        Long hourConvertedInMinutes = 0
        String hourNumberString;
        Long minutesNumber = 0

        hoursMinutesTable = hoursMinutes.split("h")
        hourConvertedInMinutes = hoursMinutesTable[0].toString().toLong() * 60
        minutesNumber = hoursMinutesTable[1].toString().substring(0, hoursMinutesTable[1].toString().length() - 1).toLong()
        result = (hourConvertedInMinutes + minutesNumber) * 60

        log.debug "End formatHoursMinutesFormatInSecondes - result : " + result
        return result
    } else {
        log.debug "End formatHoursMinutesFormatInSecondes - hoursMinutes is null"
    }
}

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();
//org.ofbiz.core.entity.GenericValue changeLog = event.changeLog;

MutableIssue issue = (MutableIssue) event.getIssueLink().getDestinationObject()
MutableIssue issueSource = (MutableIssue) event.getIssueLink().getSourceObject()
log.debug "current issue : " + issue.getKey()
log.debug "linked issue : " + issueSource.getKey()

//Constante
//Issue type
String ID_IT_PWO0 = "10004";
String ID_IT_PWO1 = "10005";
String ID_IT_PWO2 = "10006";
String ID_IT_PWO21 = "10007";
String ID_IT_PWO22 = "10008";
String ID_IT_PWO23 = "10009";
String ID_IT_Cluster = "10001";
String ID_IT_Trigger = "10000";

List<String> PWOIT = new ArrayList<String>();
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_RestrictedData = 10800;
long ID_CF_RequestedDate = 10010;
long ID_CF_PartsData_EffectiveCost = 10113;
long ID_CF_MaintPlanning_EffectiveCost = 10114;
long ID_CF_Maint_EffectiveCost = 10115;
long ID_CF_PartsData_PreIA_RequestedDate = 10058;
long ID_CF_MaintPlanning_PreIA_RequestedDate = 10077;
long ID_CF_Maint_PreIA_RequestedDate = 10097;
long ID_CF_PartsData_PreIA_1stCommitedDate = 10060;
long ID_CF_MaintPlanning_PreIA_1stCommitedDate = 10079;
long ID_CF_Maint_PreIA_1stCommitedDate = 10099;
long ID_CF_PartsData_PreIA_LastCommitedDate = 10062;
long ID_CF_MaintPlanning_PreIA_LastCommitedDate = 10088;
long ID_CF_Maint_PreIA_LastCommitedDate = 10101;
long ID_CF_PartsData_Validation_PreIA_RequestedDate = 10066;
long ID_CF_MaintPlanning_Validation_PreIA_RequestedDate = 10085;
long ID_CF_Maint_PreIA_Validation_RequestedDate = 10105;
long ID_CF_PartsData_PreIA_Validation_1stCommitedDate = 10067;
long ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate = 10086;
long ID_CF_Maint_PreIA_Validation_1stCommitedDate = 10106;
long ID_CF_PartsData_PreIA_Validation_LastCommitedDate = 10090;
long ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate = 10081;
long ID_CF_Maint_PreIA_Validation_LastCommitedDate = 10108;
long IF_CF_PartsData_WU = 10123;
long IF_CF_PartsData_NbWU = 10124;
long IF_CF_MaintPlanning_WU = 10126;
long IF_CF_MaintPlanning_NbWU = 10127;
long IF_CF_Maint_WU = 10129;
long IF_CF_Maint_NbWU = 10130;
long ID_CF_WUValue = 10625;
long ID_CF_FirstSNAppli = 10011;
long ID_CF_PartsData_Validated = 10065;
long ID_CF_MaintPlanning_Validated = 10084;
long ID_CF_Maint_Validated = 10104;
long ID_CF_TechnicalClosure = 10040
long ID_CF_PartsData_DN = 10120;
long ID_CF_MaintPlanning_DN = 10121;
long ID_CF_Maint_DN = 10122;
long ID_CF_PartsData_1stDelivery = 10061;
long ID_CF_PartsData_LastDelivery = 10063;
long ID_CF_MaintPlanning_1stDelivery = 10080;
long ID_CF_MaintPlanning_LastDelivery = 10082;
long ID_CF_Maint_1stDelivery = 10100;
long ID_CF_Maint_LastDelivery = 10102;
long ID_CF_PartsData_Validation_1stDelivery = 10068;
long ID_CF_PartsData_Validation_LastDelivery = 10070;
long ID_CF_MaintPlanning_Validation_1stDelivery = 10087;
long ID_CF_MaintPlanning_Validation_LastDelivery = 10089;
long ID_CF_Maint_Validation_1stDelivery = 10107;
long ID_CF_Maint_Validation_LastDelivery = 10109;
long ID_CF_TechnicallyClosed = 10054;
long ID_CF_PartsDataStatus = 10049;
long ID_CF_MaintPlanningStatus = 10071;
long ID_CF_MaintStatus = 10091;
long ID_CF_Blocked = 10041;
long ID_CF_BlockStartDate = 10043;
long ID_CF_BlockEndDate = 10044;
long ID_CF_Escalated = 10055;
long ID_CF_EscalateStartDate = 10047;
long ID_CF_EscalateEndDate = 10048;
long ID_CF_IA_RequestedDate = 10144;
long ID_CF_IA_Validation_RequestedDate = 10154;
long ID_CF_IA_1stCommittedDate = 10145;
long ID_CF_IA_LastCommittedDate = 10146;
long ID_CF_IA_Validation_1stCommittedDate = 10155;
long ID_CF_IA_Validation_LastCommittedDate = 10156;
long ID_CF_IA_WU = 10163;
long ID_CF_IA_NbWU = 10164;
long ID_CF_IA_EffectiveCost = 10161;
long ID_CF_IA_Validated = 10151;
long ID_CF_IA_Validation_1stDeliveryDate = 10157;
long ID_CF_IA_Validation_LastDeliveryDate = 10158;
long ID_CF_IA_Blocked = 10138;
long ID_CF_TotalBlockingDuration = 11011;

long ID_CF_PartsDataEstimatedCost = 10024;
long ID_CF_MaintPlanningEstimatedCost = 10027;
long ID_CF_MaintEstimatedCost = 10030;
long ID_CF_DomainPWO1 = 10139;
long ID_CF_TriggerTotalEffectiveCost = 10116;
long ID_CF_AuthEffectiveCost = 10240;
long ID_CF_FormEffectiveCost = 10305
long ID_CF_IntEffectiveCost = 10332
long ID_CF_PreIA_PartsData_AddtionalCost = 11115;
long ID_CF_PreIA_MaintPlanning_AddtionalCost = 11117;
long ID_CF_PreIA_Maint_AddtionalCost = 11118;
long ID_CF_IA_AdditionalCost = 11116;


//CustomField
CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ID_CF_RestrictedData)
CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_RequestedDate)
CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EffectiveCost)
CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EffectiveCost)
CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EffectiveCost)
CustomField PartsData_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_RequestedDate)
CustomField MaintPlanning_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_RequestedDate)
CustomField Maint_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_RequestedDate)
CustomField PartsData_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_1stCommitedDate)
CustomField MaintPlanning_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_1stCommitedDate)
CustomField Maint_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_1stCommitedDate)
CustomField PartsData_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_LastCommitedDate)
CustomField MaintPlanning_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_LastCommitedDate)
CustomField Maint_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_LastCommitedDate)
CustomField PartsData_Validation_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_PreIA_RequestedDate)
CustomField MaintPlanning_Validation_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_RequestedDate)
CustomField Maint_PreIA_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_RequestedDate)
CustomField PartsData_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_1stCommitedDate)
CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate)
CustomField Maint_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_1stCommitedDate)
CustomField PartsData_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_LastCommitedDate)
CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate)
CustomField Maint_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_LastCommitedDate)
CustomField PartsData_WUField = customFieldManager.getCustomFieldObject(IF_CF_PartsData_WU)
CustomField PartsData_NbWUField = customFieldManager.getCustomFieldObject(IF_CF_PartsData_NbWU)
CustomField MaintPlanning_WUField = customFieldManager.getCustomFieldObject(IF_CF_MaintPlanning_WU)
CustomField MaintPlanning_NbWUField = customFieldManager.getCustomFieldObject(IF_CF_MaintPlanning_NbWU)
CustomField Maint_WUField = customFieldManager.getCustomFieldObject(IF_CF_Maint_WU)
CustomField Maint_NbWUField = customFieldManager.getCustomFieldObject(IF_CF_Maint_NbWU)
CustomField WUValueField = customFieldManager.getCustomFieldObject(ID_CF_WUValue)
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)
CustomField PartsDataValidatedField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validated)
CustomField MaintPlanningValidatedField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validated)
CustomField MaintValidatedField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Validated)
CustomField TechnicalClosureDateField = customFieldManager.getCustomFieldObject(ID_CF_TechnicalClosure)
CustomField PartsData_DN_Field = customFieldManager.getCustomFieldObject(ID_CF_PartsData_DN)
CustomField MaintPlanning_DN_Field = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_DN)
CustomField Maint_DN_Field = customFieldManager.getCustomFieldObject(ID_CF_Maint_DN)
CustomField PartsData_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_1stDelivery)
CustomField PartsData_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_LastDelivery)
CustomField MaintPlanning_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_1stDelivery)
CustomField MaintPlanning_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_LastDelivery)
CustomField Maint_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Maint_1stDelivery)
CustomField Maint_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Maint_LastDelivery)
CustomField PartsData_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_1stDelivery)
CustomField PartsData_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_LastDelivery)
CustomField MaintPlanning_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_1stDelivery)
CustomField MaintPlanning_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_LastDelivery)
CustomField Maint_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Validation_1stDelivery)
CustomField Maint_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Validation_LastDelivery)
CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ID_CF_TechnicallyClosed)
CustomField PartsDataStatusField = customFieldManager.getCustomFieldObject(ID_CF_PartsDataStatus)
CustomField MaintPlanningStatusField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningStatus)
CustomField MaintStatusField = customFieldManager.getCustomFieldObject(ID_CF_MaintStatus)
CustomField BlockedField = customFieldManager.getCustomFieldObject(ID_CF_Blocked)
CustomField BlockStartDateField = customFieldManager.getCustomFieldObject(ID_CF_BlockStartDate)
CustomField BlockEndDateField = customFieldManager.getCustomFieldObject(ID_CF_BlockEndDate)
CustomField EscalatedField = customFieldManager.getCustomFieldObject(ID_CF_Escalated)
CustomField EscalateStartDateField = customFieldManager.getCustomFieldObject(ID_CF_EscalateStartDate)
CustomField EscalateEndDateField = customFieldManager.getCustomFieldObject(ID_CF_EscalateEndDate)
CustomField IA_RequestdDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_RequestedDate)
CustomField IA_Validation_RequestdDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_RequestedDate)
CustomField IA_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_1stCommittedDate)
CustomField IA_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_LastCommittedDate)
CustomField IA_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stCommittedDate)
CustomField IA_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastCommittedDate)
CustomField IA_WUField = customFieldManager.getCustomFieldObject(ID_CF_IA_WU)
CustomField IA_NbWUField = customFieldManager.getCustomFieldObject(ID_CF_IA_NbWU)
CustomField IA_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_EffectiveCost)
CustomField IA_ValidatedField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validated)
CustomField IA_Validation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stDeliveryDate)
CustomField IA_Validation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastDeliveryDate)
CustomField IA_BlockedField = customFieldManager.getCustomFieldObject(ID_CF_IA_Blocked)
CustomField TotalBlockingDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalBlockingDuration)
CustomField PartsData_EstimatedCostField = customFieldManager.getCustomFieldObject(ID_CF_PartsDataEstimatedCost)
CustomField MaintPlanning_EstimatedCostField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningEstimatedCost)
CustomField Maint_EstimatedCostField = customFieldManager.getCustomFieldObject(ID_CF_MaintEstimatedCost)
CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ID_CF_DomainPWO1)
CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_TriggerTotalEffectiveCost)
CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AuthEffectiveCost)
CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_FormEffectiveCost)
CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_IntEffectiveCost)
CustomField PreIA_PartsData_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_AddtionalCost)
CustomField PreIA_MaintPlanning_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_AddtionalCost)
CustomField PreIA_Maint_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_AddtionalCost)
CustomField IA_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_AdditionalCost)

//Variables
String RestrictedDataResult
String TechnicallyClosed = "No"
String PartsDataStatus
String MaintPlanningStatus
String MaintStatus
Date RequestedDateTrigger
Date RequestedDate
Date PartsData_PreIA_RequestedDate
Date PartsData_Validation_PreIA_RequestedDate
Date PartsData_PreIA_1stCommitedDate
Date PartsData_PreIA_LastCommitedDate
Date PartsData_PreIA_Validation_1stCommitedDate
Date PartsData_PreIA_Validation_LastCommitedDate
Date MaintPlanning_PreIA_RequestedDate
Date MaintPlanning_Validation_PreIA_RequestedDate
Date MaintPlanning_PreIA_1stCommitedDate
Date MaintPlanning_PreIA_LastCommitedDate
Date MaintPlanning_PreIA_Validation_1stCommitedDate
Date MaintPlanning_PreIA_Validation_LastCommitedDate
Date Maint_PreIA_RequestedDate
Date Maint_PreIA_Validation_RequestedDate
Date Maint_PreIA_1stCommitedDate
Date Maint_PreIA_LastCommitedDate
Date Maint_PreIA_Validation_1stCommitedDate
Date Maint_PreIA_Validation_LastCommitedDate
Date TechnicalClosure
Date PartsData_1stDelivery
Date PartsData_LastDelivery
Date MaintPlanning_1stDelivery
Date MaintPlanning_LastDelivery
Date Maint_1stDelivery
Date Maint_LastDelivery
//def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate = new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
Date BlockStartDate
Date BlockEndDate
Date EscalateStartDate
Date EscalateEndDate
Date IA_RequestdDate
Date IA_Validation_RequestdDate
Date IA_1stCommittedDate
Date IA_LastCommittedDate
Date IA_Validation_1stCommittedDate
Date IA_Validation_LastCommittedDate
Date IA_Validation_1stDelivery
Date IA_Validation_LastDelivery
Issue issuePartsDataWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(PartsData_WUField, issue, issueManager);
Issue issueMaintPlanningWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(MaintPlanning_WUField, issue, issueManager);
Issue issueMaintWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(Maint_WUField, issue, issueManager);
Issue issueIAWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(IA_WUField, issue, issueManager);
double PartsDataEffectiveCost
double MaintPlanningEffectiveCost
double MaintEffectiveCost
double PartsDataWUValue
double MaintPlanningWUValue
double MaintWUValue
double PartsDataNbWU
double MaintPlanningNbWU
double MaintNbWU
double IANbWu
double IAEffectiveCost
double IAWuValue
double TotalBlockingDuration
double PartsDataEffectiveCostsOfTrigger
double MaintPlanningEffectiveCostsOfTrigger
double MaintEffectiveCostsOfTrigger
double PartsDataEstimatedCosts
double MaintPlanningEstimatedCosts
double MaintEstimatedCosts
double TriggerEffectiveCost


//Business
log.debug "issue.getIssueTypeId() : " + issue.getIssueTypeId()
if (issueSource.getIssueTypeId() == ID_IT_Cluster && issue.getIssueTypeId() == ID_IT_PWO0) {
    //if (issue.getIssueTypeId() == ID_IT_PWO0){
    log.debug "PWO-0"
    //Get linked issues
    def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssue in LinkedIssues) {
        //If linked issue is Cluster
        if (linkedIssue.getIssueTypeId() == ID_IT_Cluster) {
            log.debug "Cluster " + linkedIssue.getKey()
            RestrictedDataResult = linkedIssue.getCustomFieldValue(RestrictedDataField).toString()
            def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
            for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                    log.debug "Trigger " + LinkedIssueOfCluster.getKey()
                    RequestedDateTrigger = LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)
                    log.debug "RequestedDate : " + RequestedDateTrigger
                }
            }
        }
    }

    if (RequestedDateTrigger) {//Calculate date
        RequestedDate = (Timestamp) (RequestedDateTrigger - 119)

        PartsData_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
        PartsData_Validation_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
        PartsData_PreIA_1stCommitedDate = PartsData_PreIA_RequestedDate
        PartsData_PreIA_LastCommitedDate = PartsData_PreIA_1stCommitedDate
        PartsData_PreIA_Validation_1stCommitedDate = PartsData_Validation_PreIA_RequestedDate
        PartsData_PreIA_Validation_LastCommitedDate = PartsData_PreIA_Validation_1stCommitedDate

        MaintPlanning_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
        MaintPlanning_Validation_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
        MaintPlanning_PreIA_1stCommitedDate = MaintPlanning_PreIA_RequestedDate
        MaintPlanning_PreIA_LastCommitedDate = MaintPlanning_PreIA_1stCommitedDate
        MaintPlanning_PreIA_Validation_1stCommitedDate = MaintPlanning_Validation_PreIA_RequestedDate
        MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_PreIA_Validation_1stCommitedDate

        Maint_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
        Maint_PreIA_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
        Maint_PreIA_1stCommitedDate = Maint_PreIA_RequestedDate
        Maint_PreIA_LastCommitedDate = Maint_PreIA_1stCommitedDate
        Maint_PreIA_Validation_1stCommitedDate = Maint_PreIA_Validation_RequestedDate
        Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_1stCommitedDate
    }

    //Calculate effective costs
    if (issuePartsDataWU && issue.getCustomFieldValue(PartsData_NbWUField)) {
        PartsDataWUValue = issuePartsDataWU.getCustomFieldValue(WUValueField)
        PartsDataNbWU = issue.getCustomFieldValue(PartsData_NbWUField)
        PartsDataEffectiveCost = PartsDataWUValue * PartsDataNbWU
    }
    if (issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField)) {
        PartsDataEffectiveCost = PartsDataEffectiveCost + (double) issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField)
    }
    if (issueMaintPlanningWU && issue.getCustomFieldValue(MaintPlanning_NbWUField)) {
        MaintPlanningWUValue = issueMaintPlanningWU.getCustomFieldValue(WUValueField)
        MaintPlanningNbWU = issue.getCustomFieldValue(MaintPlanning_NbWUField)
        MaintPlanningEffectiveCost = MaintPlanningWUValue * MaintPlanningNbWU
    }
    if (issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField)) {
        MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (double) issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField)
    }
    if (issueMaintWU && issue.getCustomFieldValue(Maint_NbWUField)) {
        MaintWUValue = issueMaintWU.getCustomFieldValue(WUValueField)
        MaintNbWU = issue.getCustomFieldValue(Maint_NbWUField)
        MaintEffectiveCost = MaintWUValue * MaintNbWU
    }
    if (issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField)) {
        MaintEffectiveCost = MaintEffectiveCost + (double) issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField)
    }

    //Set Data on PWO-0
    issue.setCustomFieldValue(RequestedDateField, RequestedDate)

    issue.setCustomFieldValue(PartsData_PreIA_RequestedDateField, PartsData_PreIA_RequestedDate)
    issue.setCustomFieldValue(PartsData_Validation_PreIA_RequestedDateField, PartsData_Validation_PreIA_RequestedDate)
    issue.setCustomFieldValue(PartsData_PreIA_1stCommitedDateField, PartsData_PreIA_1stCommitedDate)
    issue.setCustomFieldValue(PartsData_PreIA_LastCommitedDateField, PartsData_PreIA_LastCommitedDate)
    issue.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate)
    issue.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate)

    issue.setCustomFieldValue(MaintPlanning_PreIA_RequestedDateField, MaintPlanning_PreIA_RequestedDate)
    issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_RequestedDateField, MaintPlanning_Validation_PreIA_RequestedDate)
    issue.setCustomFieldValue(MaintPlanning_PreIA_1stCommitedDateField, MaintPlanning_PreIA_1stCommitedDate)
    issue.setCustomFieldValue(MaintPlanning_PreIA_LastCommitedDateField, MaintPlanning_PreIA_LastCommitedDate)
    issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate)
    issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate)

    issue.setCustomFieldValue(Maint_PreIA_RequestedDateField, Maint_PreIA_RequestedDate)
    issue.setCustomFieldValue(Maint_PreIA_Validation_RequestedDateField, Maint_PreIA_Validation_RequestedDate)
    issue.setCustomFieldValue(Maint_PreIA_1stCommitedDateField, Maint_PreIA_1stCommitedDate)
    issue.setCustomFieldValue(Maint_PreIA_LastCommitedDateField, Maint_PreIA_LastCommitedDate)
    issue.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate)
    issue.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate)


    issue.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost)
    issue.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost)
    issue.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost)

    def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue)
    def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
        it.toString() == RestrictedDataResult
    }
    issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
    issueIndexingService.reIndex(issue);

    //Set costs data on Trigger
    def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
            def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
            for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                    //Trigger
                    def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                    for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                        if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                            def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                            for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {

                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostField)) {
                                        PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostField)
                                    }
                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostField)) {
                                        MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostField)
                                    }
                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostField)) {
                                        MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostField)
                                    }

                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostField)) {
                                        PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostField)
                                    }
                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostField)) {
                                        MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostField)
                                    }
                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostField)) {
                                        MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostField)
                                    }

                                    def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                    for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                        if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                            if (linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField)) {
                                                IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField)
                                            } else {
                                                IAEffectiveCost = 0
                                            }

                                            if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost
                                            } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost
                                            } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost
                                            }

                                            def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                            for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                    //Get PWO-2's subtask
                                                    DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field)
                                                    def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                                    for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                                        if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                            if (linkedIssueOfPWO2.getCustomFieldValue(Auth_EffectiveCostField)) {
                                                                AuthEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(Auth_EffectiveCostField)
                                                            }
                                                            if (DomainOfPWO2 == "Parts Data") {
                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance") {
                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost
                                                            }
                                                        } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                            if (linkedIssueOfPWO2.getCustomFieldValue(Form_EffectiveCostField)) {
                                                                FormEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(Form_EffectiveCostField)
                                                            }
                                                            if (DomainOfPWO2 == "Parts Data") {
                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance") {
                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost
                                                            }
                                                        } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                            if (linkedIssueOfPWO2.getCustomFieldValue(Int_EffectiveCostField)) {
                                                                IntEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(Int_EffectiveCostField)
                                                            }
                                                            if (DomainOfPWO2 == "Parts Data") {
                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance") {
                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger
                    //Set data on trigger
                    LinkedIssueOfCluster.setCustomFieldValue(PartsData_EstimatedCostField, PartsDataEstimatedCosts)
                    LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EstimatedCostField, MaintPlanningEstimatedCosts)
                    LinkedIssueOfCluster.setCustomFieldValue(Maint_EstimatedCostField, MaintEstimatedCosts)
                    LinkedIssueOfCluster.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCostsOfTrigger)
                    LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCostsOfTrigger)
                    LinkedIssueOfCluster.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCostsOfTrigger)
                    LinkedIssueOfCluster.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)
                    issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false);
                    issueIndexingService.reIndex(LinkedIssueOfCluster);
                }
            }
        }
    }
} else if (issueSource.getIssueTypeId() == ID_IT_PWO0 && issue.getIssueTypeId() == ID_IT_PWO1) {
    log.debug "Edit of PWO-1 : " + issue.getKey()
    //Get linked issues
    def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssue in LinkedIssues) {
        //If linked issue is PWO0
        if (linkedIssue.getIssueTypeId() == ID_IT_PWO0) {
            RestrictedDataResult = linkedIssue.getCustomFieldValue(RestrictedDataField).toString()
            def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
            for (LinkedIssueOfPWO in LinkedIssuesOfPWO0) {
                if (LinkedIssueOfPWO.getIssueTypeId() == ID_IT_Cluster) {
                    def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(LinkedIssueOfPWO, user).getAllIssues()
                    for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                        if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                            RequestedDateTrigger = LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)
                        }
                    }
                }
            }
        }
    }
    if (RequestedDateTrigger) {//Calculate date
        RequestedDate = (Timestamp) (RequestedDateTrigger - 98)
        IA_RequestdDate = (Timestamp) (RequestedDateTrigger - 105)
        IA_Validation_RequestdDate = (Timestamp) (RequestedDateTrigger - 98)
        IA_1stCommittedDate = IA_RequestdDate
        IA_LastCommittedDate = IA_1stCommittedDate
        IA_Validation_1stCommittedDate = IA_Validation_RequestdDate
        IA_Validation_LastCommittedDate = IA_Validation_1stCommittedDate
    }

    //Calculate effective costs
    if (issueIAWU && issue.getCustomFieldValue(IA_NbWUField)) {
        IAWuValue = issueIAWU.getCustomFieldValue(WUValueField)
        IANbWu = issue.getCustomFieldValue(IA_NbWUField)
        IAEffectiveCost = IAWuValue * IANbWu
    }
    if (issue.getCustomFieldValue(IA_AdditionalCostField)) {
        IAEffectiveCost = IAEffectiveCost + (double) issue.getCustomFieldValue(IA_AdditionalCostField)
    }

    //Set Data on PWO-1
    issue.setCustomFieldValue(RequestedDateField, RequestedDate)

    issue.setCustomFieldValue(IA_RequestdDateField, IA_RequestdDate)
    issue.setCustomFieldValue(IA_Validation_RequestdDateField, IA_Validation_RequestdDate)
    issue.setCustomFieldValue(IA_1stCommittedDateField, IA_1stCommittedDate)
    issue.setCustomFieldValue(IA_LastCommittedDateField, IA_LastCommittedDate)
    issue.setCustomFieldValue(IA_Validation_1stCommittedDateField, IA_Validation_1stCommittedDate)
    issue.setCustomFieldValue(IA_Validation_LastCommittedDateField, IA_Validation_LastCommittedDate)

    issue.setCustomFieldValue(IA_EffectiveCostField, IAEffectiveCost)

    def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue)
    def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
        it.toString() == RestrictedDataResult
    }
    issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
    issueIndexingService.reIndex(issue);

    //Set costs data on Trigger
    def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
            def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
            for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                    //Trigger
                    def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                    for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                        if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                            def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                            for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {

                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostField)) {
                                        PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostField)
                                    }
                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostField)) {
                                        MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostField)
                                    }
                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostField)) {
                                        MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostField)
                                    }

                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostField)) {
                                        PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostField)
                                    }
                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostField)) {
                                        MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostField)
                                    }
                                    if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostField)) {
                                        MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostField)
                                    }

                                    def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                    for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                        if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                            if (linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField)) {
                                                IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IA_EffectiveCostField)
                                            } else {
                                                IAEffectiveCost = 0
                                            }

                                            if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost
                                            } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost
                                            } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost
                                            }

                                            def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                            for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                    //Get PWO-2's subtask
                                                    def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                                    for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                                        if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                            if (linkedIssueOfPWO2.getCustomFieldValue(Auth_EffectiveCostField)) {
                                                                AuthEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(Auth_EffectiveCostField)
                                                            }
                                                            if (DomainOfPWO2 == "Parts Data") {
                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance") {
                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost
                                                            }
                                                        } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                            if (linkedIssueOfPWO2.getCustomFieldValue(Form_EffectiveCostField)) {
                                                                FormEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(Form_EffectiveCostField)
                                                            }
                                                            if (DomainOfPWO2 == "Parts Data") {
                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance") {
                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost
                                                            }
                                                        } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                            if (linkedIssueOfPWO2.getCustomFieldValue(Int_EffectiveCostField)) {
                                                                IntEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(Int_EffectiveCostField)
                                                            }
                                                            if (DomainOfPWO2 == "Parts Data") {
                                                                PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost
                                                            } else if (DomainOfPWO2 == "Maintenance") {
                                                                MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger
                    //Set data on trigger
                    LinkedIssueOfCluster.setCustomFieldValue(PartsData_EstimatedCostField, PartsDataEstimatedCosts)
                    LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EstimatedCostField, MaintPlanningEstimatedCosts)
                    LinkedIssueOfCluster.setCustomFieldValue(Maint_EstimatedCostField, MaintEstimatedCosts)
                    LinkedIssueOfCluster.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCostsOfTrigger)
                    LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCostsOfTrigger)
                    LinkedIssueOfCluster.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCostsOfTrigger)
                    LinkedIssueOfCluster.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)
                    issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false);
                    issueIndexingService.reIndex(LinkedIssueOfCluster);
                }
            }
        }
    }
}
