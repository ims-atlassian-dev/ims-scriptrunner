package com.valiantys.ims.listeners
/**
 * Projects : PWO
 * Description : Calculation of the field "<manual> nb reworks"
 * Events : PWO-0 updated, PWO-1 updated, PWO-2.1 updated, PWO-2.2 updated, PWO-2.3 updated
 * isDisabled : false
 */

// import


import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.changehistory.ChangeHistoryItem
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Category

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
}*/


boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

MutableIssue issue = (MutableIssue) event.getIssue()



Category log = log
log.setLevel(org.apache.log4j.Level.DEBUG)

log.debug "Start"

//Manager
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
def changeItems = ComponentAccessor.changeHistoryManager.getAllChangeItems(issue)
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog

log.debug "Issue : " + issue.getKey()

//Constantes
long ID_CF_IPC_NbRework = 10059
long ID_CF_IPC_Status = 10049
long ID_CF_IPC_ManuallyEdited = 10132
long ID_CF_MSM_NbRework = 10078
long ID_CF_MSM_Status = 10071
long ID_CF_MSM_ManuallyEdited = 10133
long ID_CF_Maint_NbRework = 10098
long ID_CF_Maint_Status = 10091
long ID_CF_Maint_ManuallyEdited = 10134
long ID_CF_IA_Validation_ManuallyEdited = 10171
long ID_CF_IA_Validated = 10151
long ID_CF_IA_Validation_NbRework = 10159
long ID_CF_Auth_Verification_ManuallyEdited = 10244
long ID_CF_Auth_Verified = 10183
long ID_CF_Auth_Verification_Validation_NbRework = 10188
long ID_CF_Form_Verification_ManuallyEdited = 10323
long ID_CF_Form_Verified = 10291
long ID_CF_Form_Verification_NbRework = 10297

String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String Delivered = "Delivered"

//CustomField
CustomField IPCManuallyEditedField = customFieldManager.getCustomFieldObject(ID_CF_IPC_ManuallyEdited)
CustomField IPCNbReworkField = customFieldManager.getCustomFieldObject(ID_CF_IPC_NbRework)
CustomField IPCStatusField = customFieldManager.getCustomFieldObject(ID_CF_IPC_Status)
CustomField MSMManuallyEditedField = customFieldManager.getCustomFieldObject(ID_CF_MSM_ManuallyEdited)
CustomField MSMNbReworkField = customFieldManager.getCustomFieldObject(ID_CF_MSM_NbRework)
CustomField MSMStatusField = customFieldManager.getCustomFieldObject(ID_CF_MSM_Status)
CustomField MaintManuallyEditedField = customFieldManager.getCustomFieldObject(ID_CF_Maint_ManuallyEdited)
CustomField MaintNbReworkField = customFieldManager.getCustomFieldObject(ID_CF_Maint_NbRework)
CustomField MaintStatusField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Status)
CustomField IA_Validation_ManuallyEditedField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_ManuallyEdited)
CustomField IA_ValidatedField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validated)
CustomField IA_Validation_NbReworkField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_NbRework)
CustomField Auth_Verification_ManuallyEditedField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_ManuallyEdited)
CustomField Auth_VerifiedField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verified)
CustomField Auth_Verification_NbReworkField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_Validation_NbRework)
CustomField Form_Verification_ManuallyEditedField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_ManuallyEdited)
CustomField Form_VerifiedField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verified)
CustomField Form_Verification_NbReworkField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_NbRework)


if (issue.getIssueTypeId() == ID_IT_PWO0) {
    //PWO-0
    //Valeur
    String IPCManuallyEdited = issue.getCustomFieldValue(IPCManuallyEditedField)
    double IPCNbRework = 0
    if (issue.getCustomFieldValue(IPCNbReworkField)) {
        IPCNbRework = issue.getCustomFieldValue(IPCNbReworkField).toString().toDouble()
    }
    String MSMManuallyEdited = issue.getCustomFieldValue(MSMManuallyEditedField)
    double MSMNbRework = 0
    if (issue.getCustomFieldValue(MSMNbReworkField)) {
        MSMNbRework = issue.getCustomFieldValue(MSMNbReworkField).toString().toDouble()
    }
    String MaintManuallyEdited = issue.getCustomFieldValue(MaintManuallyEditedField)
    double MaintNbRework = 0
    if (issue.getCustomFieldValue(MaintNbReworkField)) {
        MaintNbRework = issue.getCustomFieldValue(MaintNbReworkField).toString().toDouble()
    }

    //Business
    double ValeurToReturn
    double Compteur

    List<String> itemsChangedPartsDataStatus = new ArrayList<String>()
    itemsChangedPartsDataStatus.add(IPCStatusField.getFieldName())

    boolean isPartsDataStatus = (boolean) workloadIsUpdated(changeLog, itemsChangedPartsDataStatus)
    if (isPartsDataStatus) {
        if (issue.getCustomFieldValue(IPCStatusField).toString() == Delivered) {
            if (IPCManuallyEdited == "Yes") {
                ValeurToReturn = IPCNbRework + 1
            } else {
                for (ChangeHistoryItem item : changeItems) {
                    if (item.field == 'Parts Data status' && item.getTos().values().contains('Delivered')) {
                        Compteur = Compteur + 1
                    }
                }
                ValeurToReturn = Compteur - 1
                Compteur = 0
            }
            issue.setCustomFieldValue(IPCNbReworkField, ValeurToReturn)

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    }

    List<String> itemsChangedMaintPlanningStatus = new ArrayList<String>()
    itemsChangedMaintPlanningStatus.add(MSMStatusField.getFieldName())

    boolean isMaintPlanningStatus = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintPlanningStatus)
    if (isMaintPlanningStatus) {
        if (issue.getCustomFieldValue(MSMStatusField).toString() == Delivered) {
            if (MSMManuallyEdited == "Yes") {
                ValeurToReturn = MSMNbRework + 1
            } else {
                for (ChangeHistoryItem item : changeItems) {
                    if (item.field == 'Maint Planning status' && item.getTos().values().contains('Delivered')) {
                        Compteur = Compteur + 1
                    }
                }
                ValeurToReturn = Compteur - 1
                Compteur = 0
            }
            issue.setCustomFieldValue(MSMNbReworkField, ValeurToReturn)

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    }

    List<String> itemsChangedMaintStatus = new ArrayList<String>()
    itemsChangedMaintStatus.add(MaintStatusField.getFieldName())

    boolean isMaintStatus = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintStatus)
    if (isMaintStatus) {
        if (issue.getCustomFieldValue(MaintStatusField).toString() == "Delivered") {
            if (MaintManuallyEdited == "Yes") {
                ValeurToReturn = MaintNbRework + 1
            } else {
                for (ChangeHistoryItem item : changeItems) {
                    if (item.field == 'Maintenance status' && item.getTos().values().contains('Delivered')) {
                        Compteur = Compteur + 1
                    }
                }
                ValeurToReturn = Compteur - 1
                Compteur = 0
            }
            issue.setCustomFieldValue(MaintNbReworkField, ValeurToReturn)

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    }
} else if (issue.getIssueTypeId() == ID_IT_PWO1) {
    ///PWO-1
    //Valeur
    String IA_Validation_ManuallyEdited = issue.getCustomFieldValue(IA_Validation_ManuallyEditedField)
    double NbRework = 0
    if (issue.getCustomFieldValue(IA_Validation_NbReworkField)) {
        NbRework = issue.getCustomFieldValue(IA_Validation_NbReworkField).toString().toDouble()
    }

    //Business
    double ValeurToReturn
    double Compteur

    List<String> itemsChangedIAValidatedStatus = new ArrayList<String>()
    itemsChangedIAValidatedStatus.add(IA_ValidatedField.getFieldName())

    boolean isIaValidated = (boolean) workloadIsUpdated(changeLog, itemsChangedIAValidatedStatus)
    if (isIaValidated) {
        if (issue.getCustomFieldValue(IA_ValidatedField).toString() == "Validated" || issue.getCustomFieldValue(IA_ValidatedField).toString() == "Rejected") {
            if (IA_Validation_ManuallyEdited == "Yes") {
                ValeurToReturn = NbRework + 1
            } else {
                for (ChangeHistoryItem item : changeItems) {
                    if (item.field == 'IA_Validated' && item.getTos().values().contains('Validated') || item.field == 'IA_Validated' && item.getTos().values().contains('Rejected')) {
                        Compteur = Compteur + 1
                    }
                }
                ValeurToReturn = Compteur - 1
                Compteur = 0
            }
            issue.setCustomFieldValue(IA_Validation_NbReworkField, ValeurToReturn)

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    }
} else if (issue.getIssueTypeId() == ID_IT_PWO21) {
    log.debug "PWO-2.1"
    //Valeur
    String Auth_Verification_ManuallyEdited = issue.getCustomFieldValue(Auth_Verification_ManuallyEditedField)
    double NbRework = 0
    if (issue.getCustomFieldValue(Auth_Verification_NbReworkField)) {
        NbRework = issue.getCustomFieldValue(Auth_Verification_NbReworkField).toString().toDouble()
    }
    //Business
    double ValeurToReturn
    double Compteur
    List<String> itemsChangedAuthVerifiedStatus = new ArrayList<String>()
    itemsChangedAuthVerifiedStatus.add(Auth_VerifiedField.getFieldName())

    boolean isAuthVerified = (boolean) workloadIsUpdated(changeLog, itemsChangedAuthVerifiedStatus)
    if (isAuthVerified) {
        if (issue.getCustomFieldValue(Auth_VerifiedField).toString() == "Verified") {
            if (Auth_Verification_ManuallyEdited == "Yes") {
                ValeurToReturn = NbRework + 1
            } else {
                for (ChangeHistoryItem item : changeItems) {
                    if (item.field == 'Auth_Verified' && item.getTos().values().contains('Verified')) {
                        Compteur = Compteur + 1
                    }
                }
                ValeurToReturn = Compteur - 1
                Compteur = 0
            }
            issue.setCustomFieldValue(Auth_Verification_NbReworkField, ValeurToReturn)

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    }
} else if (issue.getIssueTypeId() == ID_IT_PWO22) {
    log.debug "PWO-2.2"
    //Valeur
    String Form_Verification_ManuallyEdited = issue.getCustomFieldValue(Form_Verification_ManuallyEditedField)
    double NbRework = 0
    if (issue.getCustomFieldValue(Form_Verification_NbReworkField)) {
        NbRework = issue.getCustomFieldValue(Form_Verification_NbReworkField).toString().toDouble()
    }

    //Business
    double ValeurToReturn
    double Compteur
    List<String> itemsChangedFormVerifiedStatus = new ArrayList<String>()
    itemsChangedFormVerifiedStatus.add(Form_VerifiedField.getFieldName())

    boolean isFormVerified = (boolean) workloadIsUpdated(changeLog, itemsChangedFormVerifiedStatus)
    if (isFormVerified) {
        if (issue.getCustomFieldValue(Form_VerifiedField).toString() == "Verified") {
            if (Form_Verification_ManuallyEdited == "Yes") {
                ValeurToReturn = NbRework + 1
            } else {
                for (ChangeHistoryItem item : changeItems) {
                    if (item.field == 'Form_Verified' && item.getTos().values().contains('Verified')) {
                        Compteur = Compteur + 1
                    }
                }
                ValeurToReturn = Compteur - 1
                Compteur = 0
            }
            issue.setCustomFieldValue(Form_Verification_NbReworkField, ValeurToReturn)

            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }
    }
}