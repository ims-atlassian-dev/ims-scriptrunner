package com.valiantys.ims.listeners
/**
 * Projects : 
 * Description : Copy the group selected in field "Program" to the Security Level
 * Events : Issue Created, Issue Moved, PWO-0 updated, PWO-1 updated, PWO-2 updated, PWO-2.1 updated, PWO-2.2 updated, PWO-2.3 updated, Trigger updated, AWO updated, DU updated, PO updated, WU updated, FRZ updated
 * isDisabled : false
 */

// import


import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.security.IssueSecurityLevel
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level

log.setLevel(Level.DEBUG)

//Manager
IssueManager issueManager = ComponentAccessor.getIssueManager()
MutableIssue issue = (MutableIssue) event.getIssue()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
def issueSecurityLevelManager = ComponentAccessor.issueSecurityLevelManager

//Constantes
long ID_CF_Program = 10347

//CustomField
CustomField ProgramField = customFieldManager.getCustomFieldObject(ID_CF_Program)

//Business
log.debug "issue : " + issue.getKey()


String ProgramnFeed = issue.getCustomFieldValue(ProgramField).toString()
Collection<IssueSecurityLevel> SecurityLevel
long SecurityId

String[] MOAnFeedFormatted = ProgramnFeed.substring(19).replaceAll("</value>/\n/</content>", "").replaceAll("</value>", ",").replaceAll("<value>", "").replaceAll(/\n/, "").replace("  ", " ").split(",")

SecurityLevel = issueSecurityLevelManager.getIssueSecurityLevelsByName(MOAnFeedFormatted[0])

SecurityId = SecurityLevel[0].getId()

if (issue.getSecurityLevelId() != SecurityId) {
    issue.setSecurityLevelId(SecurityId)
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
    issueIndexingService.reIndex(issue)//JIRA7
}
