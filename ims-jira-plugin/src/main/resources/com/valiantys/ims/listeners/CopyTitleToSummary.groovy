package com.valiantys.ims.listeners
/**
 * Projects : AWO
 * Description : Copy "Title" to "Summary"
 * Events : Issue Created, Issue Updated, WU updated, FRZ updated, AWO updated
 * isDisabled : false
 */

// import

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level

log.setLevel(Level.DEBUG)
log.debug "Creation/Edit of a WU/Obj. Freeze/AWO, copy reference to summary"


//Manager
IssueManager issueManager = ComponentAccessor.getIssueManager()
MutableIssue issue = (MutableIssue) event.getIssue()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()


//Constantes
long ID_CF_Reference = 10001


//CustomField
CustomField ReferenceField = customFieldManager.getCustomFieldObject(ID_CF_Reference)

//Business
log.debug "issue : " + issue.getKey()

String Reference
Reference = issue.getCustomFieldValue(ReferenceField)

issue.setSummary(Reference)
issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
issueIndexingService.reIndex(issue)
