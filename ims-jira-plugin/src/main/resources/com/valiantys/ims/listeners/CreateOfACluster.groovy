package com.valiantys.ims.listeners
/**
 * Projects : TRG
 * Description : Create of a Cluster
 * Events : Issue Created
 * isDisabled : false
 */

// import

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger


//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager)
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()

MutableIssue issue = (MutableIssue) event.getIssue()

//Constante
//Issue type
String ID_IT_Cluster = "10001"
String ID_IT_Trigger = "10000"


//Field of Trigger
long ID_CF_RestrictedDate = 10800


//CustomField
CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ID_CF_RestrictedDate)

//Variables
String RestrictedDataResult = "No"


//Business
if (issue.getIssueTypeId() == ID_IT_Cluster) {
    log.debug "issue : " + issue.getKey()
    def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssue in LinkedIssues) {
        if (linkedIssue.getIssueTypeId() == ID_IT_Trigger) {
            if (linkedIssue.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                RestrictedDataResult = "Yes"
            } else if (RestrictedDataResult != "Yes" && linkedIssue.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                RestrictedDataResult = "Unknown"
            }
        }
    }

    def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue)
    def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
        it.toString() == RestrictedDataResult
    }
    issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData)
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
    issueIndexingService.reIndex(issue)
}
