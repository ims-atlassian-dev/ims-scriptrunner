package com.valiantys.ims.listeners
/**
 * Projects : PWO
 * Description : Creation / Edit of PWO-0
 * Events : Admin re-open, PWO-0 updated, Issue Created
 * isDisabled : null
 */

// import

import com.atlassian.crowd.embedded.api.Group
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.joda.time.LocalDate

import java.sql.Timestamp

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
} */



boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

def getIssueFromNFeedFieldNewFieldsSingle(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ")
        issueFound = issueManager.getIssueObject(Long.parseLong(formattedValues))
    }

    if (issueFound) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + issueFound
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }
    return issueFound
}

def getIssueFromNFeedFieldNewFieldsMulitple(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    List<Issue> multipleIssue = new ArrayList<Issue>()
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String[] formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ").split(" ")
        for (String formatedValue in formattedValues) {
            issueFound = issueManager.getIssueObject(Long.parseLong(formatedValue))
            multipleIssue.add(issueFound)
        }
    }

    if (multipleIssue) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + multipleIssue
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }

    return multipleIssue
}

String formatSecondesValueInHoursMinutes(Long secondes) {
    log.debug "Start formatSecondesValueInHoursMinutes - secondes : " + secondes
    if (secondes && secondes > 0) {
        String result
        int hoursExtracted = (int) (secondes / 3600)
        int totalMinutes = (int) (secondes / 60)
        int minutesExtracted = (int) totalMinutes.mod(60)
        result = hoursExtracted.toString() + "h " + minutesExtracted.toString() + "m"
        log.debug "End formatSecondesValueInHoursMinutes - result  : " + result
        return result
    } else {
        log.debug "End formatSecondesValueInHoursMinutes - minutes is null"
    }
}

Long formatHoursMinutesFormatInSecondes(String hoursMinutes) {
    log.debug "Start formatHoursMinutesFormatInSecondes - hoursMinutes : " + hoursMinutes
    if (!hoursMinutes.equals("null")) {
        Long result = 0L
        String[] hoursMinutesTable
        Long hourConvertedInMinutes = 0
        String hourNumberString
        Long minutesNumber = 0

        hoursMinutesTable = hoursMinutes.split("h")
        hourConvertedInMinutes = hoursMinutesTable[0].toString().toLong() * 60
        minutesNumber = hoursMinutesTable[1].toString().substring(0, hoursMinutesTable[1].toString().length() - 1).toLong()
        result = (hourConvertedInMinutes + minutesNumber) * 60

        log.debug "End formatHoursMinutesFormatInSecondes - result : " + result
        return result
    } else {
        log.debug "End formatHoursMinutesFormatInSecondes - hoursMinutes is null"
    }
}


//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
ApplicationUser IMSUser = ComponentAccessor.getUserManager().getUserByName("IMS")
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog
GroupManager groupManager = ComponentAccessor.getGroupManager()

MutableIssue issue = (MutableIssue) event.getIssue()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"
String ID_IT_Cluster = "10001"
String ID_IT_Trigger = "10000"

List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

long ID_PR_AWO = 10003
long ID_Event_AdminReOpen = 10400

//Fields
long ID_CF_IA_Company = 10142
long ID_CF_CompanyAllowed = 10702
long ID_CF_Auth_Company = 10173
long ID_CF_Auth_VerifCompany = 10181
long ID_CF_Form_Company = 10281
long ID_CF_Form_VerifCompany = 10289
long ID_CF_Int_Company = 10324
long ID_CF_IntCompany = 11200
long ID_CF_AuthVerifCompany = 11130
long ID_CF_FormVerifCompany = 10703

long ID_CF_PartsData_EffectiveCost = 10113
long ID_CF_PartsData_PreIA_RequestedDate = 10058
long ID_CF_PartsData_PreIA_1stCommitedDate = 10060
long ID_CF_PartsData_PreIA_LastCommitedDate = 10062
long ID_CF_PartsData_Validation_PreIA_RequestedDate = 10066
long ID_CF_PartsData_PreIA_Validation_1stCommitedDate = 10067
long ID_CF_PartsData_PreIA_Validation_LastCommitedDate = 10090
long IF_CF_PartsData_WU = 10123
long IF_CF_PartsData_NbWU = 10124
long ID_CF_PartsData_Validated = 10065
long ID_CF_PartsData_DN = 10120
long ID_CF_PartsData_1stDelivery = 10061
long ID_CF_PartsData_LastDelivery = 10063
long ID_CF_PartsData_Validation_1stDelivery = 10068
long ID_CF_PartsData_Validation_LastDelivery = 10070
long ID_CF_PartsDataStatus = 10049
long ID_CF_PartsData_EstimatedCosts = 10024
long ID_CF_PartsData_TimeSpent = 11012
long ID_CF_PartsData_EffectiveCosts = 10113
long ID_CF_PartsData_Valdidation_1stDeliveryDate = 10068
long ID_CF_PartsData_Valdidation_LastDeliveryDate = 10070
long ID_CF_PartsData_ReworkDate = 11119

long ID_CF_MaintPlanning_EffectiveCost = 10114
long ID_CF_MaintPlanning_PreIA_RequestedDate = 10077
long ID_CF_MaintPlanning_PreIA_1stCommitedDate = 10079
long ID_CF_MaintPlanning_PreIA_LastCommitedDate = 10088
long ID_CF_MaintPlanning_Validation_PreIA_RequestedDate = 10085
long ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate = 10086
long ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate = 10081
long IF_CF_MaintPlanning_WU = 10126
long IF_CF_MaintPlanning_NbWU = 10127
long ID_CF_MaintPlanning_Validated = 10084
long ID_CF_MaintPlanning_DN = 10121
long ID_CF_MaintPlanning_1stDelivery = 10080
long ID_CF_MaintPlanning_LastDelivery = 10082
long ID_CF_MaintPlanning_Validation_1stDelivery = 10087
long ID_CF_MaintPlanning_Validation_LastDelivery = 10089
long ID_CF_MaintPlanningStatus = 10071
long ID_CF_MaintPlanning_EstimatedCosts = 10027
long ID_CF_MaintPlanning_TimeSpent = 11013
long ID_CF_MaintPlanning_EffectiveCosts = 10114
long ID_CF_MaintPlanning_Valdidation_1stDeliveryDate = 10087
long ID_CF_MaintPlanning_Valdidation_LastDeliveryDate = 10089
long ID_CF_MaintPlanning_ReworkDate = 11120

long ID_CF_Maint_EffectiveCost = 10115
long ID_CF_Maint_PreIA_RequestedDate = 10097
long ID_CF_Maint_PreIA_1stCommitedDate = 10099
long ID_CF_Maint_PreIA_LastCommitedDate = 10101
long ID_CF_Maint_PreIA_Validation_RequestedDate = 10105
long ID_CF_Maint_PreIA_Validation_1stCommitedDate = 10106
long ID_CF_Maint_PreIA_Validation_LastCommitedDate = 10108
long IF_CF_Maint_WU = 10129
long IF_CF_Maint_NbWU = 10130
long ID_CF_Maint_Validated = 10104
long ID_CF_Maint_DN = 10122
long ID_CF_Maint_1stDelivery = 10100
long ID_CF_Maint_LastDelivery = 10102
long ID_CF_Maint_Validation_1stDelivery = 10107
long ID_CF_Maint_Validation_LastDelivery = 10109
long ID_CF_MaintStatus = 10091
long ID_CF_Maint_EstimatedCosts = 10030
long ID_CF_Maint_TimeSpent = 11014
long ID_CF_Maint_EffectiveCosts = 10115
long ID_CF_Maint_Valdidation_1stDeliveryDate = 10107
long ID_CF_Maint_Valdidation_LastDeliveryDate = 10109
long ID_CF_Maint_ReworkDate = 11121

long ID_CF_Domain = 10034
long ID_CF_DomainPWO1 = 10139

long ID_CF_IA_RequestedDate = 10144
long ID_CF_IA_Validation_RequestedDate = 10154
long ID_CF_IA_1stCommittedDate = 10145
long ID_CF_IA_LastCommittedDate = 10146
long ID_CF_IA_Validation_1stCommittedDate = 10155
long ID_CF_IA_Validation_LastCommittedDate = 10156
long ID_CF_IA_WU = 10163
long ID_CF_IA_NbWU = 10164
long ID_CF_IA_EffectiveCost = 10161
long ID_CF_IA_Validated = 10151
long ID_CF_IA_Validation_1stDeliveryDate = 10157
long ID_CF_IA_Validation_LastDeliveryDate = 10158
long ID_CF_IA_Blocked = 10138
long ID_CF_IA_TimeSpent = 11017
long ID_CF_IA_ReworkDate = 11122


long ID_CF_RestrictedData = 10800
long ID_CF_RequestedDate = 10010
long ID_CF_WUValue = 10625
long ID_CF_FirstSNAppli = 10011
long ID_CF_TechnicalClosure = 10040
long ID_CF_TechnicallyClosed = 10054
long ID_CF_Blocked = 10041
long ID_CF_BlockStartDate = 10043
long ID_CF_OldBlockStartDate = 11800
long ID_CF_BlockEndDate = 10044
long ID_CF_OldBlockEndDate = 12000
long ID_CF_Escalated = 10055
long ID_CF_EscalateStartDate = 10047
long ID_CF_OldEscalateStartDate = 12100
long ID_CF_EscalateEndDate = 10048
long ID_CF_OldEscalateEndDate = 12101
long ID_CF_TotalBlockingDuration = 11011
long ID_CF_TotalEscalatedDuration = 11019
long ID_CF_Auth_TimeSpent = 11015
long ID_CF_Auth_EffectiveCost = 10240
long ID_CF_Form_TimeSpent = 11016
long ID_CF_Form_EffectiveCost = 10305
long ID_CF_Int_TimeSpent = 11018
long ID_CF_Int_EffectiveCost = 10332
long ID_CF_TriggerTotalEffectiveCosts = 10116
long ID_CF_ImpactConfirmed = 10141
long ID_CF_ImpactOnTD = 10008
long ID_CF_IA_AdditionalCost = 11116
long ID_CF_PreIA_PartsData_AddtionalCost = 11115
long ID_CF_PreIA_MaintPlanning_AddtionalCost = 11117
long ID_CF_PreIA_Maint_AddtionalCost = 11118

//CustomField
CustomField IACompanyField = customFieldManager.getCustomFieldObject(ID_CF_IA_Company)
CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_CompanyAllowed)
CustomField AuthCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Company)
CustomField AuthVerifCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Auth_VerifCompany)
CustomField FormCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Form_Company)
CustomField FormVerifCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Form_VerifCompany)
CustomField IntCompanyField = customFieldManager.getCustomFieldObject(ID_CF_Int_Company)
CustomField IntCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_IntCompany)
CustomField AuthVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_AuthVerifCompany)
CustomField FormVerifCompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_FormVerifCompany)

CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EffectiveCost)
CustomField PartsData_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_RequestedDate)
CustomField PartsData_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_1stCommitedDate)
CustomField PartsData_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_LastCommitedDate)
CustomField PartsData_Validation_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_PreIA_RequestedDate)
CustomField PartsData_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_1stCommitedDate)
CustomField PartsData_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_LastCommitedDate)
CustomField PartsData_WUField = customFieldManager.getCustomFieldObject(IF_CF_PartsData_WU)
CustomField PartsData_NbWUField = customFieldManager.getCustomFieldObject(IF_CF_PartsData_NbWU)
CustomField PartsDataValidatedField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validated)
CustomField PartsData_DN_Field = customFieldManager.getCustomFieldObject(ID_CF_PartsData_DN)
CustomField PartsData_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_1stDelivery)
CustomField PartsData_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_LastDelivery)
CustomField PartsData_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_1stDelivery)
CustomField PartsData_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_LastDelivery)
CustomField PartsDataStatusField = customFieldManager.getCustomFieldObject(ID_CF_PartsDataStatus)
CustomField PartsData_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EstimatedCosts)
CustomField PartsData_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_TimeSpent)
CustomField PartsData_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EffectiveCosts)
CustomField PartsData_Valdidation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Valdidation_1stDeliveryDate)
CustomField PartsData_Valdidation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_Valdidation_LastDeliveryDate)
CustomField PartsData_ReworkDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_ReworkDate)

CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EffectiveCost)
CustomField MaintPlanning_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_RequestedDate)
CustomField MaintPlanning_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_1stCommitedDate)
CustomField MaintPlanning_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_LastCommitedDate)
CustomField MaintPlanning_Validation_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_RequestedDate)
CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate)
CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate)
CustomField MaintPlanning_WUField = customFieldManager.getCustomFieldObject(IF_CF_MaintPlanning_WU)
CustomField MaintPlanning_NbWUField = customFieldManager.getCustomFieldObject(IF_CF_MaintPlanning_NbWU)
CustomField MaintPlanningValidatedField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validated)
CustomField MaintPlanning_DN_Field = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_DN)
CustomField MaintPlanning_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_1stDelivery)
CustomField MaintPlanning_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_LastDelivery)
CustomField MaintPlanning_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_1stDelivery)
CustomField MaintPlanning_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_LastDelivery)
CustomField MaintPlanningStatusField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningStatus)
CustomField MaintPlanning_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EstimatedCosts)
CustomField MaintPlanning_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_TimeSpent)
CustomField MaintPlanning_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EffectiveCosts)
CustomField MaintPlanning_Valdidation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Valdidation_1stDeliveryDate)
CustomField MaintPlanning_Valdidation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Valdidation_LastDeliveryDate)
CustomField MaintPlanning_ReworkDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_ReworkDate)

CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EffectiveCost)
CustomField Maint_PreIA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_RequestedDate)
CustomField Maint_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_1stCommitedDate)
CustomField Maint_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_LastCommitedDate)
CustomField Maint_PreIA_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_RequestedDate)
CustomField Maint_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_1stCommitedDate)
CustomField Maint_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_LastCommitedDate)
CustomField Maint_WUField = customFieldManager.getCustomFieldObject(IF_CF_Maint_WU)
CustomField Maint_NbWUField = customFieldManager.getCustomFieldObject(IF_CF_Maint_NbWU)
CustomField MaintValidatedField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Validated)
CustomField Maint_DN_Field = customFieldManager.getCustomFieldObject(ID_CF_Maint_DN)
CustomField Maint_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Maint_1stDelivery)
CustomField Maint_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Maint_LastDelivery)
CustomField Maint_Validation_1stDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Validation_1stDelivery)
CustomField Maint_Validation_LastDeliveryField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Validation_LastDelivery)
CustomField MaintStatusField = customFieldManager.getCustomFieldObject(ID_CF_MaintStatus)
CustomField Maint_EstimatedCostsField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EstimatedCosts)
CustomField Maint_TimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Maint_TimeSpent)
CustomField Maint_EffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EffectiveCosts)
CustomField Maint_Valdidation_1stDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Valdidation_1stDeliveryDate)
CustomField Maint_Valdidation_LastDeliveryDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_Valdidation_LastDeliveryDate)
CustomField Maint_ReworkDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_ReworkDate)

CustomField DomainField = customFieldManager.getCustomFieldObject(ID_CF_Domain)
CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ID_CF_DomainPWO1)

CustomField IA_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_EffectiveCost)
CustomField IATimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_IA_TimeSpent)
CustomField IAEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_EffectiveCost)
CustomField IA_BlockedField = customFieldManager.getCustomFieldObject(ID_CF_IA_Blocked)

/*CustomField IA_RequestdDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_RequestedDate)
CustomField IA_Validation_RequestdDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_RequestedDate)
CustomField IA_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_1stCommittedDate)
CustomField IA_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_LastCommittedDate)
CustomField IA_Validation_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stCommittedDate)
CustomField IA_Validation_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastCommittedDate)
CustomField IA_WUField=customFieldManager.getCustomFieldObject(ID_CF_IA_WU)
CustomField IA_NbWUField=customFieldManager.getCustomFieldObject(ID_CF_IA_NbWU)
CustomField IA_ValidatedField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validated)
CustomField IA_Validation_1stDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stDeliveryDate)
CustomField IA_Validation_LastDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastDeliveryDate)
CustomField IAReworkDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_ReworkDate)*/


CustomField RestrictedDataField = customFieldManager.getCustomFieldObject(ID_CF_RestrictedData)
CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_RequestedDate)
CustomField WUValueField = customFieldManager.getCustomFieldObject(ID_CF_WUValue)
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)
CustomField TechnicalClosureDateField = customFieldManager.getCustomFieldObject(ID_CF_TechnicalClosure)
CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ID_CF_TechnicallyClosed)
CustomField BlockedField = customFieldManager.getCustomFieldObject(ID_CF_Blocked)
CustomField OldBlockStartDateField = customFieldManager.getCustomFieldObject(ID_CF_OldBlockStartDate)
CustomField BlockStartDateField = customFieldManager.getCustomFieldObject(ID_CF_BlockStartDate)
CustomField OldBlockEndDateField = customFieldManager.getCustomFieldObject(ID_CF_OldBlockEndDate)
CustomField BlockEndDateField = customFieldManager.getCustomFieldObject(ID_CF_BlockEndDate)
CustomField EscalatedField = customFieldManager.getCustomFieldObject(ID_CF_Escalated)
CustomField EscalateStartDateField = customFieldManager.getCustomFieldObject(ID_CF_EscalateStartDate)
CustomField OldEscalateStartDateField = customFieldManager.getCustomFieldObject(ID_CF_OldEscalateStartDate)
CustomField EscalateEndDateField = customFieldManager.getCustomFieldObject(ID_CF_EscalateEndDate)
CustomField OldEscalateEndDateField = customFieldManager.getCustomFieldObject(ID_CF_OldEscalateEndDate)
CustomField TotalBlockingDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalBlockingDuration)
CustomField TotalEscalatedDurationField = customFieldManager.getCustomFieldObject(ID_CF_TotalEscalatedDuration)
CustomField ImpactConfirmedField = customFieldManager.getCustomFieldObject(ID_CF_ImpactConfirmed)
CustomField ImpactOnTDField = customFieldManager.getCustomFieldObject(ID_CF_ImpactOnTD)
CustomField AuthTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Auth_TimeSpent)
CustomField AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Auth_EffectiveCost)
CustomField FormTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Form_TimeSpent)
CustomField FormEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Form_EffectiveCost)
CustomField IntTimeSpentField = customFieldManager.getCustomFieldObject(ID_CF_Int_TimeSpent)
CustomField IntEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Int_EffectiveCost)
CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_TriggerTotalEffectiveCosts)
CustomField IA_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_AdditionalCost)
CustomField PreIA_PartsData_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_AddtionalCost)
CustomField PreIA_MaintPlanning_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_AddtionalCost)
CustomField PreIA_Maint_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_AddtionalCost)


//Variables
String RestrictedDataResult
String TechnicallyClosed = "Yes"
String PartsDataStatus
String MaintPlanningStatus
String MaintStatus
String TotalBlockingDurationString
String TotalEscalatedDurationString
String DomainOfPWO2
Date RequestedDateTrigger
Date RequestedDate
Date PartsData_PreIA_RequestedDate
Date PartsData_Validation_PreIA_RequestedDate
Date PartsData_PreIA_1stCommitedDate
Date PartsData_PreIA_LastCommitedDate
Date PartsData_PreIA_Validation_1stCommitedDate
Date PartsData_PreIA_Validation_LastCommitedDate
Date PartsData_ReworkDate
Date MaintPlanning_PreIA_RequestedDate
Date MaintPlanning_Validation_PreIA_RequestedDate
Date MaintPlanning_PreIA_1stCommitedDate
Date MaintPlanning_PreIA_LastCommitedDate
Date MaintPlanning_PreIA_Validation_1stCommitedDate
Date MaintPlanning_PreIA_Validation_LastCommitedDate
Date MaintPlanning_ReworkDate
Date Maint_PreIA_RequestedDate
Date Maint_PreIA_Validation_RequestedDate
Date Maint_PreIA_1stCommitedDate
Date Maint_PreIA_LastCommitedDate
Date Maint_PreIA_Validation_1stCommitedDate
Date Maint_PreIA_Validation_LastCommitedDate
Date Maint_ReworkDate
Date TechnicalClosure
Date PartsData_1stDelivery
Date PartsData_LastDelivery
Date MaintPlanning_1stDelivery
Date MaintPlanning_LastDelivery
Date Maint_1stDelivery
Date Maint_LastDelivery
def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate = new LocalDate(new Date()).toDate()
Timestamp TodaysDateCompare = new Timestamp(TodayDate.getTime())

Date BlockStartDate
Date BlockEndDate
Date EscalateStartDate
Date EscalateEndDate
Date IA_RequestdDate
Date IA_Validation_RequestdDate
Date IA_1stCommittedDate
Date IA_LastCommittedDate
Date IA_Validation_1stCommittedDate
Date IA_Validation_LastCommittedDate
Date IA_Validation_1stDelivery
Date IA_Validation_LastDelivery
Date IA_ReworkDate
Issue issuePartsDataWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(PartsData_WUField, issue, issueManager)
Issue issueMaintPlanningWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(MaintPlanning_WUField, issue, issueManager)
Issue issueMaintWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(Maint_WUField, issue, issueManager)
//Issue issueIAWU = (Issue)getIssueFromNFeedFieldSingle(IA_WUField,issue,issueManager)
double PartsDataEffectiveCost
double MaintPlanningEffectiveCost
double MaintEffectiveCost
double PartsDataWUValue
double MaintPlanningWUValue
double MaintWUValue
double PartsDataNbWU
double MaintPlanningNbWU
double MaintNbWU
double IANbWu
double IAEffectiveCost
double IAWuValue
double TotalBlockingDuration
double TotalEscalatedDuration

double PartsDataEstimatedCosts = 0
double MaintPlanningEstimatedCosts = 0
double MaintEstimatedCosts = 0
long PartsDataTimeSpent = 0
long MaintPlanningTimeSpent = 0
long MaintTimeSpent = 0
double PartsDataEffectiveCostsOfTrigger = 0
double MaintPlanningEffectiveCostsOfTrigger = 0
double MaintEffectiveCostsOfTrigger = 0
String PartsDataTimeSpentString
String MaintPlanningTimeSpentString
String MaintTimeSpenString
String MaintValidated
String MaintPlanningValidated
String PartsDataValidated

long IATimeSpentCurrent
long PartsDataTimeSpentCurrent
long MaintPlanningTimeSpentCurrent
long MaintTimeSpentCurrent
long AuthTimeSpentCurrent
long FormTimeSpentCurrent
long IntTimeSpentCurrent
double AuthEffectiveCost
double FormEffectiveCost
double IntEffectiveCost
double TriggerEffectiveCost

List<Group> CompanyAllowedList = new ArrayList<Group>()
List<Group> IntCompanyAllowedList = new ArrayList<Group>()
List<Group> AuthVerifCompanyAllowedList = new ArrayList<Group>()
List<Group> FormVerifCompanyAllowedList = new ArrayList<Group>()

//double IAEffectiveCost

String RestrictedDataTrigger = "No"
String RestrictedDataPWO0 = "No"

Date PartsData_Valdidation_1stDeliveryDate
Date MaintPlanning_Valdidation_1stDeliveryDate
Date Maint_Valdidation_1stDeliveryDate
Date PartsData_Valdidation_LastDeliveryDate
Date MaintPlanning_Valdidation_LastDeliveryDate
Date Maint_Valdidation_LastDeliveryDate

String ImpactOnTDResult

String TotalBlockingDurationTriggerString
double TotalBlockingDurationTrigger
double TotalBlockingDurationOld
String TotalEscalatedDurationTriggerString
double TotalEscalatedDurationTrigger
double TotalEscalatedDurationOld

String BlockedTrigger
String EscalatedTrigger

long eventId = event.getEventTypeId()

//Business
List<String> itemsChangedAtCreation = new ArrayList<String>()
itemsChangedAtCreation.add(FirstSNAppliField.getFieldName())
log.debug "issue = " + issue.getKey()

boolean isCreation = (boolean) workloadIsUpdated(changeLog, itemsChangedAtCreation)
log.debug "is creation : " + isCreation
if (isCreation || eventId == ID_Event_AdminReOpen) {//Creation
    if (issue.getIssueTypeId() == ID_IT_PWO0) {
        log.debug "is PWO-0"
        //Get linked issues
        def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssue in LinkedIssues) {
            //If linked issue is Cluster
            if (linkedIssue.getIssueTypeId() == ID_IT_Cluster) {
                log.debug "linked Cluster : " + linkedIssue.getKey()
                RestrictedDataResult = linkedIssue.getCustomFieldValue(RestrictedDataField).toString()
                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssue, user).getAllIssues()
                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                    log.debug "linked issue of cluster : " + LinkedIssueOfCluster.getKey()
                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                        log.debug "Linked Trigger : " + LinkedIssueOfCluster.getKey()
                        if(!RequestedDateTrigger || RequestedDateTrigger > (Date)LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)){
                            RequestedDateTrigger = (Date) LinkedIssueOfCluster.getCustomFieldValue(RequestedDateField)
                        	log.debug "Requested date trigger : " + RequestedDateTrigger
                        }
                    }
                }
            }
        }

        if (RequestedDateTrigger) {//Calculate date
            log.debug "Calculate date"
            RequestedDate = (Timestamp) (RequestedDateTrigger - 119)

            PartsData_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
            PartsData_Validation_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
            PartsData_PreIA_1stCommitedDate = PartsData_PreIA_RequestedDate
            PartsData_PreIA_LastCommitedDate = PartsData_PreIA_1stCommitedDate
            PartsData_PreIA_Validation_1stCommitedDate = PartsData_Validation_PreIA_RequestedDate
            PartsData_PreIA_Validation_LastCommitedDate = PartsData_PreIA_Validation_1stCommitedDate

            MaintPlanning_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
            MaintPlanning_Validation_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
            MaintPlanning_PreIA_1stCommitedDate = MaintPlanning_PreIA_RequestedDate
            MaintPlanning_PreIA_LastCommitedDate = MaintPlanning_PreIA_1stCommitedDate
            MaintPlanning_PreIA_Validation_1stCommitedDate = MaintPlanning_Validation_PreIA_RequestedDate
            MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_PreIA_Validation_1stCommitedDate

            Maint_PreIA_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
            Maint_PreIA_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
            Maint_PreIA_1stCommitedDate = Maint_PreIA_RequestedDate
            Maint_PreIA_LastCommitedDate = Maint_PreIA_1stCommitedDate
            Maint_PreIA_Validation_1stCommitedDate = Maint_PreIA_Validation_RequestedDate
            Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_1stCommitedDate
        }

        //Calculate effective costs
        if (issuePartsDataWU && issue.getCustomFieldValue(PartsData_NbWUField)) {
            PartsDataWUValue = (Double) issuePartsDataWU.getCustomFieldValue(WUValueField)
            PartsDataNbWU = (Double) issue.getCustomFieldValue(PartsData_NbWUField)
            PartsDataEffectiveCost = PartsDataWUValue * PartsDataNbWU
        }
        if (issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField)) {
            PartsDataEffectiveCost = PartsDataEffectiveCost + (double) issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField)
        }
        if (issueMaintPlanningWU && issue.getCustomFieldValue(MaintPlanning_NbWUField)) {
            MaintPlanningWUValue = (Double) issueMaintPlanningWU.getCustomFieldValue(WUValueField)
            MaintPlanningNbWU = (Double) issue.getCustomFieldValue(MaintPlanning_NbWUField)
            MaintPlanningEffectiveCost = MaintPlanningWUValue * MaintPlanningNbWU
        }
        if (issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField)) {
            MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (double) issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField)
        }
        if (issueMaintWU && issue.getCustomFieldValue(Maint_NbWUField)) {
            MaintWUValue = (Double) issueMaintWU.getCustomFieldValue(WUValueField)
            MaintNbWU = (Double) issue.getCustomFieldValue(Maint_NbWUField)
            MaintEffectiveCost = MaintWUValue * MaintNbWU
        }
        if (issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField)) {
            MaintEffectiveCost = MaintEffectiveCost + (double) issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField)
        }


        //Set Data on PWO-0
        issue.setCustomFieldValue(RequestedDateField, RequestedDate)
        log.debug "setting date on PWO-0 at creation"

        issue.setCustomFieldValue(PartsData_PreIA_RequestedDateField, PartsData_PreIA_RequestedDate)
        issue.setCustomFieldValue(PartsData_Validation_PreIA_RequestedDateField, PartsData_Validation_PreIA_RequestedDate)
        issue.setCustomFieldValue(PartsData_PreIA_1stCommitedDateField, PartsData_PreIA_1stCommitedDate)
        issue.setCustomFieldValue(PartsData_PreIA_LastCommitedDateField, PartsData_PreIA_LastCommitedDate)
        issue.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate)
        issue.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate)

        issue.setCustomFieldValue(MaintPlanning_PreIA_RequestedDateField, MaintPlanning_PreIA_RequestedDate)
        issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_RequestedDateField, MaintPlanning_Validation_PreIA_RequestedDate)
        issue.setCustomFieldValue(MaintPlanning_PreIA_1stCommitedDateField, MaintPlanning_PreIA_1stCommitedDate)
        issue.setCustomFieldValue(MaintPlanning_PreIA_LastCommitedDateField, MaintPlanning_PreIA_LastCommitedDate)
        issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate)
        issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate)

        issue.setCustomFieldValue(Maint_PreIA_RequestedDateField, Maint_PreIA_RequestedDate)
        issue.setCustomFieldValue(Maint_PreIA_Validation_RequestedDateField, Maint_PreIA_Validation_RequestedDate)
        issue.setCustomFieldValue(Maint_PreIA_1stCommitedDateField, Maint_PreIA_1stCommitedDate)
        issue.setCustomFieldValue(Maint_PreIA_LastCommitedDateField, Maint_PreIA_LastCommitedDate)
        issue.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate)
        issue.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate)


        issue.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost)
        issue.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost)
        issue.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost)

        def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(issue)
        def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
            it.toString() == RestrictedDataResult
        }
        issue.setCustomFieldValue(RestrictedDataField, valueRestrictedData)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)

        //Set costs data on Trigger
        def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                        //Trigger
                        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField)) {
                                            PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField)
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField)) {
                                            MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField)
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)) {
                                            MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)
                                        }

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField)) {
                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField)
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField)) {
                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField)
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField)) {
                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField)
                                        }

                                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)) {
                                                    IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)
                                                } else {
                                                    IAEffectiveCost = 0
                                                }

                                                if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost
                                                }

                                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                        //Get PWO-2's subtask
                                                        def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                                        for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)) {
                                                                    AuthEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)) {
                                                                    FormEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)) {
                                                                    IntEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data") {
                                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost
                                                                } else if (DomainOfPWO2 == "Maintenance Planning") {
                                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost
                                                                } else if (DomainOfPWO2 == "Maintenance") {
                                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger
                        //Set data on trigger
                        LinkedIssueOfCluster.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts)
                        LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts)
                        LinkedIssueOfCluster.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts)
                        LinkedIssueOfCluster.setCustomFieldValue(PartsData_EffectiveCostsField, PartsDataEffectiveCostsOfTrigger)
                        LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EffectiveCostsField, MaintPlanningEffectiveCostsOfTrigger)
                        LinkedIssueOfCluster.setCustomFieldValue(Maint_EffectiveCostsField, MaintEffectiveCostsOfTrigger)
                        LinkedIssueOfCluster.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)
                        issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                        issueIndexingService.reIndex(LinkedIssueOfCluster)
                    }
                }
            }
        }

    }
} else {
    log.debug "edit"
    //Edit
    if (issue.getIssueTypeId() == ID_IT_PWO0) {
        //If Restricted data is changed, propagate to Cluster and Trigger
        List<String> itemsChangedRestrictedData = new ArrayList<String>()
        itemsChangedRestrictedData.add(RestrictedDataField.getFieldName())

        boolean isRestrictedDataChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedRestrictedData)
        if (isRestrictedDataChanged) {
            def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
            for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                    def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                    for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                        if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_PWO0) {
                            if (linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                RestrictedDataTrigger = "Yes"
                            } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfCluster.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                RestrictedDataTrigger = "Unknown"
                            }
                            def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                            for (linkedIssueOfPWO in LinkedIssuesOfPWO0) {
                                if (linkedIssueOfPWO.getIssueTypeId() == ID_IT_PWO1) {
                                    if (linkedIssueOfPWO.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                        RestrictedDataTrigger = "Yes"
                                    } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfPWO.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                        RestrictedDataTrigger = "Unknown"
                                    }
                                }
                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO, user).getAllIssues()
                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                        if (linkedIssueOfPWO1.getCustomFieldValue(RestrictedDataField).toString() == "Yes") {
                                            RestrictedDataTrigger = "Yes"
                                        } else if (RestrictedDataTrigger != "Yes" && linkedIssueOfPWO1.getCustomFieldValue(RestrictedDataField).toString() == "Unknown") {
                                            RestrictedDataTrigger = "Unknown"
                                        }
                                    }
                                }
                            }
                        }
                    }
                    def fieldConfigRestrictedData = RestrictedDataField.getRelevantConfig(linkedIssueOfCurrentPWO0)
                    def valueRestrictedData = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedData)?.find {
                        it.toString() == RestrictedDataTrigger
                    }

                    //Set Restricted data on Cluster
                    linkedIssueOfCurrentPWO0.setCustomFieldValue(RestrictedDataField, valueRestrictedData)
                    issueManager.updateIssue(user, linkedIssueOfCurrentPWO0, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(linkedIssueOfCurrentPWO0)

                    //Set Restricted data on Triggers
                    def LinkedTriggersOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                    for (linkedTriggerOfCluster in LinkedTriggersOfCluster) {
                        if (linkedTriggerOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                            def fieldConfigRestrictedDataTrigger = RestrictedDataField.getRelevantConfig(linkedTriggerOfCluster)
                            def valueRestrictedDataTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigRestrictedDataTrigger)?.find {
                                it.toString() == RestrictedDataTrigger
                            }

                            linkedTriggerOfCluster.setCustomFieldValue(RestrictedDataField, valueRestrictedDataTrigger)
                            issueManager.updateIssue(user, linkedTriggerOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                            issueIndexingService.reIndex(linkedTriggerOfCluster)
                        }
                    }
                }
            }
        }


        //Calculate Technically Closed
        def DomainValues = issue.getCustomFieldValue(DomainField)
        for (domainValue in DomainValues) {
            if (domainValue.toString() == "Parts Data") {
                if (issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Validated" && TechnicallyClosed == "Yes") {
                    TechnicallyClosed = "Yes"
                } else {
                    TechnicallyClosed = "No"
                }
            }
            if (domainValue.toString() == "Maint Planning") {
                if (issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Validated" && TechnicallyClosed == "Yes") {
                    TechnicallyClosed = "Yes"
                } else {
                    TechnicallyClosed = "No"
                }
            }
            if (domainValue.toString() == "Maintenance") {
                if (issue.getCustomFieldValue(MaintValidatedField).toString() == "Validated" && TechnicallyClosed == "Yes") {
                    TechnicallyClosed = "Yes"
                } else {
                    TechnicallyClosed = "No"
                }
            }
        }

        if (TechnicallyClosed == "Yes" && !issue.getCustomFieldValue(TechnicalClosureDateField)) {
            TechnicalClosure = TodaysDateCompare
            issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosure)

        } else if (TechnicallyClosed == "No") {
            TechnicalClosure = null
            issue.setCustomFieldValue(TechnicalClosureDateField, TechnicalClosure)
        }


        List<String> itemsChangedPartsDataValidated = new ArrayList<String>()
        itemsChangedPartsDataValidated.add(PartsDataValidatedField.getFieldName())

        boolean isPartsDataValidatedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedPartsDataValidated)
        if (isPartsDataValidatedChanged) {
            if (!issue.getCustomFieldValue(PartsData_Validation_1stDeliveryField)) {
                PartsData_Valdidation_1stDeliveryDate = TodaysDateCompare
                issue.setCustomFieldValue(PartsData_Validation_1stDeliveryField, PartsData_Valdidation_1stDeliveryDate)
            }
            PartsData_Valdidation_LastDeliveryDate = TodaysDateCompare
            issue.setCustomFieldValue(PartsData_Validation_LastDeliveryField, PartsData_Valdidation_LastDeliveryDate)
            if (issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Rejected") {
                PartsDataStatus = "In progress"

                def fieldConfigPartsDataStatus = PartsDataStatusField.getRelevantConfig(issue)
                def valuePartsDataStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigPartsDataStatus)?.find {
                    it.toString() == PartsDataStatus
                }

                PartsData_ReworkDate = TodaysDateCompare + 7

                issue.setCustomFieldValue(PartsDataStatusField, valuePartsDataStatus)
                issue.setCustomFieldValue(PartsData_ReworkDateField, PartsData_ReworkDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
            }
            if (issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Validated" || issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Rejected") {
                PartsData_Valdidation_1stDeliveryDate = TodaysDateCompare
                PartsData_Valdidation_LastDeliveryDate = TodaysDateCompare

                if (!issue.getCustomFieldValue(PartsData_Valdidation_1stDeliveryDateField)) {
                    issue.setCustomFieldValue(PartsData_Valdidation_1stDeliveryDateField, PartsData_Valdidation_1stDeliveryDate)
                }
                issue.setCustomFieldValue(PartsData_Valdidation_LastDeliveryDateField, PartsData_Valdidation_LastDeliveryDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
            }
        }

        List<String> itemsChangedMaintPlanningValidated = new ArrayList<String>()
        itemsChangedMaintPlanningValidated.add(MaintPlanningValidatedField.getFieldName())

        boolean isMaintPlanningValidatedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintPlanningValidated)
        if (isMaintPlanningValidatedChanged) {
            log.debug "maint planning validated changed"
            if (!issue.getCustomFieldValue(MaintPlanning_Validation_1stDeliveryField)) {
                log.debug "setting maint planning validation 1st delivery"
                MaintPlanning_Valdidation_1stDeliveryDate = TodaysDateCompare
                issue.setCustomFieldValue(MaintPlanning_Validation_1stDeliveryField, MaintPlanning_Valdidation_1stDeliveryDate)
            }
            MaintPlanning_Valdidation_LastDeliveryDate = TodaysDateCompare
            issue.setCustomFieldValue(MaintPlanning_Validation_LastDeliveryField, MaintPlanning_Valdidation_LastDeliveryDate)
            if (issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Rejected") {
                MaintPlanningStatus = "In progress"

                def fieldConfigMaintPlanningStatus = MaintPlanningStatusField.getRelevantConfig(issue)
                def valueMaintPlanningStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintPlanningStatus)?.find {
                    it.toString() == MaintPlanningStatus
                }

                MaintPlanning_ReworkDate = TodaysDateCompare + 7

                issue.setCustomFieldValue(MaintPlanningStatusField, valueMaintPlanningStatus)
                issue.setCustomFieldValue(MaintPlanning_ReworkDateField, MaintPlanning_ReworkDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
            }
            if (issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Validated" || issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Rejected") {
                log.debug "validated = validated"
                log.debug "setting validation 1st and last delivery date"
                MaintPlanning_Valdidation_1stDeliveryDate = TodaysDateCompare
                MaintPlanning_Valdidation_LastDeliveryDate = TodaysDateCompare

                if (!issue.getCustomFieldValue(MaintPlanning_Valdidation_1stDeliveryDateField)) {
                    issue.setCustomFieldValue(MaintPlanning_Valdidation_1stDeliveryDateField, MaintPlanning_Valdidation_1stDeliveryDate)
                }
                issue.setCustomFieldValue(MaintPlanning_Valdidation_LastDeliveryDateField, MaintPlanning_Valdidation_LastDeliveryDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
            }
            log.debug "end of maint planning validated change"
        }

        List<String> itemsChangedMaintValidated = new ArrayList<String>()
        itemsChangedMaintValidated.add(MaintValidatedField.getFieldName())

        boolean isMaintValidatedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintValidated)
        if (isMaintValidatedChanged) {
            if (!issue.getCustomFieldValue(Maint_Validation_1stDeliveryField)) {
                Maint_Valdidation_1stDeliveryDate = TodaysDateCompare
                issue.setCustomFieldValue(Maint_Validation_1stDeliveryField, Maint_Valdidation_1stDeliveryDate)
            }
            Maint_Valdidation_LastDeliveryDate = TodaysDateCompare
            issue.setCustomFieldValue(Maint_Validation_LastDeliveryField, Maint_Valdidation_LastDeliveryDate)
            if (issue.getCustomFieldValue(MaintValidatedField).toString() == "Rejected") {
                MaintStatus = "In progress"

                def fieldConfigMaintStatus = MaintStatusField.getRelevantConfig(issue)
                def valueMaintStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintStatus)?.find {
                    it.toString() == MaintStatus
                }

                Maint_ReworkDate = TodaysDateCompare + 7

                issue.setCustomFieldValue(MaintStatusField, valueMaintStatus)
                issue.setCustomFieldValue(Maint_ReworkDateField, Maint_ReworkDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
            }
            if (issue.getCustomFieldValue(MaintValidatedField).toString() == "Validated" || issue.getCustomFieldValue(MaintValidatedField).toString() == "Rejected") {
                Maint_Valdidation_1stDeliveryDate = TodaysDateCompare
                Maint_Valdidation_LastDeliveryDate = TodaysDateCompare

                if (!issue.getCustomFieldValue(Maint_Valdidation_1stDeliveryDateField)) {
                    issue.setCustomFieldValue(Maint_Valdidation_1stDeliveryDateField, Maint_Valdidation_1stDeliveryDate)
                }
                issue.setCustomFieldValue(Maint_Valdidation_LastDeliveryDateField, Maint_Valdidation_LastDeliveryDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
            }
        }

        //Calculate effective costs
        if (issuePartsDataWU && issue.getCustomFieldValue(PartsData_NbWUField)) {
            PartsDataWUValue = (Double) issuePartsDataWU.getCustomFieldValue(WUValueField)
            PartsDataNbWU = (Double) issue.getCustomFieldValue(PartsData_NbWUField)
            PartsDataEffectiveCost = PartsDataWUValue * PartsDataNbWU
        }
        if (issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField)) {
            PartsDataEffectiveCost = PartsDataEffectiveCost + (double) issue.getCustomFieldValue(PreIA_PartsData_AdditionalCostField)
        }
        if (issueMaintPlanningWU && issue.getCustomFieldValue(MaintPlanning_NbWUField)) {
            MaintPlanningWUValue = (Double) issueMaintPlanningWU.getCustomFieldValue(WUValueField)
            MaintPlanningNbWU = (Double) issue.getCustomFieldValue(MaintPlanning_NbWUField)
            MaintPlanningEffectiveCost = MaintPlanningWUValue * MaintPlanningNbWU
        }
        if (issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField)) {
            MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (double) issue.getCustomFieldValue(PreIA_MaintPlanning_AdditionalCostField)
        }
        if (issueMaintWU && issue.getCustomFieldValue(Maint_NbWUField)) {
            MaintWUValue = (Double) issueMaintWU.getCustomFieldValue(WUValueField)
            MaintNbWU = (Double) issue.getCustomFieldValue(Maint_NbWUField)
            MaintEffectiveCost = MaintWUValue * MaintNbWU
        }
        if (issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField)) {
            MaintEffectiveCost = MaintEffectiveCost + (double) issue.getCustomFieldValue(PreIA_Maint_AdditionalCostField)
        }

        //Calculate status if DN is filled
        List<String> itemsChangedPartsDataDN = new ArrayList<String>()
        itemsChangedPartsDataDN.add(PartsData_DN_Field.getFieldName())

        boolean isPartsDataDNChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedPartsDataDN)
        if (isPartsDataDNChanged || isPartsDataValidatedChanged) {
            if (issue.getCustomFieldValue(PartsData_DN_Field) && issue.getCustomFieldValue(PartsDataValidatedField).toString() == "Validated") {
                PartsDataStatus = "Completed"

                def fieldConfigPartsDataStatus = PartsDataStatusField.getRelevantConfig(issue)
                def valuePartsDataStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigPartsDataStatus)?.find {
                    it.toString() == PartsDataStatus
                }
                issue.setCustomFieldValue(PartsDataStatusField, valuePartsDataStatus)
            }
        }

        List<String> itemsChangedMaintPlanningDN = new ArrayList<String>()
        itemsChangedMaintPlanningDN.add(MaintPlanning_DN_Field.getFieldName())

        boolean isMaintPlanningDNChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintPlanningDN)
        if (isMaintPlanningDNChanged || isMaintPlanningValidatedChanged) {
            if (issue.getCustomFieldValue(MaintPlanning_DN_Field) && issue.getCustomFieldValue(MaintPlanningValidatedField).toString() == "Validated") {
                MaintPlanningStatus = "Completed"

                def fieldConfigMaintPlanningStatus = MaintPlanningStatusField.getRelevantConfig(issue)
                def valueMaintPlanningStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintPlanningStatus)?.find {
                    it.toString() == MaintPlanningStatus
                }
                issue.setCustomFieldValue(MaintPlanningStatusField, valueMaintPlanningStatus)
            }
        }

        List<String> itemsChangedMaintDN = new ArrayList<String>()
        itemsChangedMaintDN.add(Maint_DN_Field.getFieldName())

        boolean isMaintDNChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintDN)
        if (isMaintDNChanged || isMaintValidatedChanged) {
            if (issue.getCustomFieldValue(Maint_DN_Field) && issue.getCustomFieldValue(MaintValidatedField).toString() == "Validated") {
                MaintStatus = "Completed"

                def fieldConfigMaintStatus = MaintStatusField.getRelevantConfig(issue)
                def valueMaintStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintStatus)?.find {
                    it.toString() == MaintStatus
                }
                issue.setCustomFieldValue(MaintStatusField, valueMaintStatus)
            }
        }
        if (isPartsDataDNChanged || isMaintPlanningDNChanged || isMaintDNChanged) {
            issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(issue)
        }

        //Calculate date if status is changed
        List<String> itemsChangedPartsDataStatus = new ArrayList<String>()
        itemsChangedPartsDataStatus.add(PartsDataStatusField.getFieldName())

        boolean isPartsDataStatusChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedPartsDataStatus)
        if (isPartsDataStatusChanged) {
            if (issue.getCustomFieldValue(PartsDataStatusField).toString() == "Delivered") {
                if (!issue.getCustomFieldValue(PartsData_1stDeliveryField)) {
                    PartsData_1stDelivery = TodaysDateCompare
                    issue.setCustomFieldValue(PartsData_1stDeliveryField, PartsData_1stDelivery)

                    PartsData_PreIA_Validation_1stCommitedDate = (Timestamp) (TodaysDateCompare + 7)
                    issue.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate)
                }
                
                List<String> itemsChangedPartsValidation = new ArrayList<String>()
        		itemsChangedPartsValidation.add(PartsDataValidatedField.getFieldName())
                boolean isPartsDataValidationChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedPartsValidation)
                
                if(!isPartsDataValidationChanged){
                    PartsDataValidated = "Validation pending"

                    def fieldConfigPartsDataValidated = PartsDataValidatedField.getRelevantConfig(issue)
                    def valuePartsDataValidated = ComponentAccessor.optionsManager.getOptions(fieldConfigPartsDataValidated)?.find {
                        it.toString() == PartsDataValidated
                    }
                    issue.setCustomFieldValue(PartsDataValidatedField, valuePartsDataValidated)
                }
                
                

                PartsData_LastDelivery = TodaysDateCompare
                PartsData_Validation_PreIA_RequestedDate = (Timestamp) (TodaysDateCompare + 7)
                PartsData_PreIA_Validation_LastCommitedDate = PartsData_Validation_PreIA_RequestedDate

                issue.setCustomFieldValue(PartsData_LastDeliveryField, PartsData_LastDelivery)
                issue.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate)
                issue.setCustomFieldValue(PartsData_Validation_PreIA_RequestedDateField, PartsData_Validation_PreIA_RequestedDate)
            }
        }

        List<String> itemsChangedMaintPlanningStatus = new ArrayList<String>()
        itemsChangedMaintPlanningStatus.add(MaintPlanningStatusField.getFieldName())

        boolean isMaintPlanningStatusChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintPlanningStatus)
        if (isMaintPlanningStatusChanged) {
            if (issue.getCustomFieldValue(MaintPlanningStatusField).toString() == "Delivered") {
                if (!issue.getCustomFieldValue(MaintPlanning_1stDeliveryField)) {
                    MaintPlanning_1stDelivery = TodaysDateCompare
                    issue.setCustomFieldValue(MaintPlanning_1stDeliveryField, MaintPlanning_1stDelivery)

                    MaintPlanning_PreIA_Validation_1stCommitedDate = (Timestamp) (TodaysDateCompare + 7)
                    issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate)
                }

                List<String> itemsChangedMaintPlanningValidation = new ArrayList<String>()
        		itemsChangedMaintPlanningValidation.add(MaintPlanningValidatedField.getFieldName())
                boolean isMaintPlanningValidationChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintPlanningValidation)
                
                if(!isMaintPlanningValidationChanged){
                    MaintPlanningValidated = "Validation pending"

                    def fieldConfigMaintPlanningValidated = MaintPlanningValidatedField.getRelevantConfig(issue)
                    def valueMaintPlanningValidated = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintPlanningValidated)?.find {
                        it.toString() == MaintPlanningValidated
                    }
                    issue.setCustomFieldValue(MaintPlanningValidatedField, valueMaintPlanningValidated)

                }
                
                MaintPlanning_LastDelivery = TodaysDateCompare
                MaintPlanning_Validation_PreIA_RequestedDate = (Timestamp) (TodaysDateCompare + 7)
                MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_Validation_PreIA_RequestedDate

                issue.setCustomFieldValue(MaintPlanning_LastDeliveryField, MaintPlanning_LastDelivery)
                issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate)
                issue.setCustomFieldValue(MaintPlanning_Validation_PreIA_RequestedDateField, MaintPlanning_Validation_PreIA_RequestedDate)
            }
        }

        List<String> itemsChangedMaintStatus = new ArrayList<String>()
        itemsChangedMaintStatus.add(MaintStatusField.getFieldName())

        boolean isMaintStatusChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintStatus)
        if (isMaintStatusChanged) {
            if (issue.getCustomFieldValue(MaintStatusField).toString() == "Delivered") {
                if (!issue.getCustomFieldValue(Maint_1stDeliveryField)) {
                    Maint_1stDelivery = TodaysDateCompare
                    issue.setCustomFieldValue(Maint_1stDeliveryField, Maint_1stDelivery)

                    Maint_PreIA_Validation_1stCommitedDate = (Timestamp) (TodaysDateCompare + 7)
                    issue.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate)
                }

                List<String> itemsChangedMaintValidation = new ArrayList<String>()
        		itemsChangedMaintValidation.add(MaintValidatedField.getFieldName())
                boolean isMaintValidationChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedMaintValidation)
                
                if(!isMaintValidationChanged){
                    MaintValidated = "Validation pending"

                    def fieldConfigMaintValidated = MaintValidatedField.getRelevantConfig(issue)
                    def valueMaintValidated = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintValidated)?.find {
                        it.toString() == MaintValidated
                    }
                	issue.setCustomFieldValue(MaintValidatedField, valueMaintValidated)
                }

                Maint_LastDelivery = TodaysDateCompare
                Maint_PreIA_Validation_RequestedDate = (Timestamp) (TodaysDateCompare + 7)
                Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_RequestedDate

                issue.setCustomFieldValue(Maint_LastDeliveryField, Maint_LastDelivery)
                issue.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate)
                issue.setCustomFieldValue(Maint_PreIA_Validation_RequestedDateField, Maint_PreIA_Validation_RequestedDate)
            }
        }


        //Calculate value when Blocked is edited
        List<String> itemsChangedBlocked = new ArrayList<String>()
        itemsChangedBlocked.add(BlockedField.getFieldName())

        boolean isBlockedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedBlocked)
        if (isBlockedChanged) {
            log.debug "PWO-0 set to Blocked"
            log.debug "value of blocked : " + issue.getCustomFieldValue(BlockedField)
            if (issue.getCustomFieldValue(BlockedField)) {
                BlockEndDate = null
                issue.setCustomFieldValue(BlockEndDateField, BlockEndDate)

                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
                if (!issue.getCustomFieldValue(OldBlockStartDateField) || issue.getCustomFieldValue(OldBlockStartDateField) != TodaysDateCompare) {
                    log.debug "Start date to update"
                    BlockStartDate = TodaysDateCompare
                    issue.setCustomFieldValue(BlockStartDateField, BlockStartDate)
                    issue.setCustomFieldValue(OldBlockStartDateField, BlockStartDate)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }
                BlockedTrigger = "No"
                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                //Trigger
                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                log.debug "PWO-0 : " + linkedIssueOfClusterTrigger.getKey()
                                                //Get total blocking duration of PWO0
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockedField)) {
                                                    log.debug "PWO-0 " + linkedIssueOfClusterTrigger.getKey() + " is blocked"
                                                    BlockedTrigger = "Yes"
                                                }
                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                        //Get total blocking duration of PWO1
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                            BlockedTrigger = "Yes"
                                                        }
                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                if (linkedIssueOfPWO1.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                    BlockedTrigger = "Yes"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                log.debug "value to set : " + BlockedTrigger
                                //Set data on trigger
                                def fieldConfigBlockedTrigger = IA_BlockedField.getRelevantConfig(LinkedIssueOfCluster)
                                def valueBlockedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigBlockedTrigger)?.find {
                                    it.toString() == BlockedTrigger
                                }
                                LinkedIssueOfCluster.setCustomFieldValue(IA_BlockedField, valueBlockedTrigger)
                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                            }
                        }
                    }
                }
            } else {
                log.debug "unblocked"
                BlockEndDate = TodaysDateCompare
                issue.setCustomFieldValue(BlockEndDateField, BlockEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)
                BlockStartDate = (Date) issue.getCustomFieldValue(BlockStartDateField)

                log.debug "old end date : " + issue.getCustomFieldValue(OldBlockEndDateField)
                Date OldBlockDate = (Date) issue.getCustomFieldValue(OldBlockEndDateField)


                if (!issue.getCustomFieldValue(OldBlockEndDateField) || issue.getCustomFieldValue(OldBlockEndDateField) != TodaysDateCompare) {
                    log.debug "set old date"
                    issue.setCustomFieldValue(OldBlockEndDateField, BlockEndDate)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                    if (issue.getCustomFieldValue(TotalBlockingDurationField)) {
                        log.debug "not empty"
                        TotalBlockingDurationOld = issue.getCustomFieldValue(TotalBlockingDurationField).replace(" d", "").toDouble()
                        if (issue.getCustomFieldValue(OldBlockStartDateField) == TodaysDateCompare || issue.getCustomFieldValue(OldBlockStartDateField) > OldBlockDate) {
                            log.debug "+1"
                            TotalBlockingDuration = BlockEndDate - BlockStartDate + 1
                        } else {
                            log.debug "without +1"
                            TotalBlockingDuration = BlockEndDate - BlockStartDate
                        }
                        log.debug "new value to add : " + TotalBlockingDuration
                        TotalBlockingDuration = TotalBlockingDurationOld + TotalBlockingDuration
                    } else {
                        TotalBlockingDuration = BlockEndDate - BlockStartDate + 1
                    }
                    TotalBlockingDuration = TotalBlockingDuration.trunc()
                    TotalBlockingDurationString = TotalBlockingDuration.trunc() + " d"
                    log.debug "final value : " + TotalBlockingDurationString

                    issue.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationString)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                }
                log.debug "end of if"

                //BlockStartDate=null
                //issue.setCustomFieldValue(BlockStartDateField,BlockStartDate)

                //Calculate Blocking duration for trigger
                TotalBlockingDurationTrigger = 0
                BlockedTrigger = "No"
                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                //Trigger
                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(BlockedField)) {
                                                    BlockedTrigger = "Yes"
                                                }
                                                //Get total blocking duration of PWO0
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField)) {
                                                    TotalBlockingDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalBlockingDurationField)
                                                    TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                    TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                }
                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                        //Get total blocking duration of PWO1
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                            BlockedTrigger = "Yes"
                                                        }
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField)) {
                                                            TotalBlockingDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalBlockingDurationField)
                                                            TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                            TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                        }
                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                if (linkedIssueOfPWO1.getCustomFieldValue(IA_BlockedField).toString() == "Yes") {
                                                                    BlockedTrigger = "Yes"
                                                                }
                                                                if (linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField)) {
                                                                    TotalBlockingDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalBlockingDurationField)
                                                                    TotalBlockingDurationTriggerString = TotalBlockingDurationTriggerString.replaceAll(" d", "")
                                                                    TotalBlockingDurationTrigger = TotalBlockingDurationTrigger + TotalBlockingDurationTriggerString.toDouble()
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //Set data on trigger
                                def fieldConfigBlockedTrigger = IA_BlockedField.getRelevantConfig(LinkedIssueOfCluster)
                                def valueBlockedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigBlockedTrigger)?.find {
                                    it.toString() == BlockedTrigger
                                }
                                LinkedIssueOfCluster.setCustomFieldValue(IA_BlockedField, valueBlockedTrigger)
                                TotalBlockingDurationTrigger = TotalBlockingDurationTrigger.trunc()
                                TotalBlockingDurationTriggerString = TotalBlockingDurationTrigger.trunc() + " d"
                                LinkedIssueOfCluster.setCustomFieldValue(TotalBlockingDurationField, TotalBlockingDurationTriggerString)
                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                            }
                        }
                    }
                }
            }
        }


        //Calculate value when Esclated is edited
        List<String> itemsChangedEscalated = new ArrayList<String>()
        itemsChangedEscalated.add(EscalatedField.getFieldName())

        boolean isEscalatedChanged = (boolean) workloadIsUpdated(changeLog, itemsChangedEscalated)
        if (isEscalatedChanged) {
            if (issue.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                EscalateEndDate = null
                issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                if (!issue.getCustomFieldValue(OldEscalateStartDateField) || issue.getCustomFieldValue(OldEscalateStartDateField) != TodaysDateCompare) {
                    log.debug "Start date to update"
                    EscalateStartDate = TodaysDateCompare
                    issue.setCustomFieldValue(EscalateStartDateField, EscalateStartDate)
                    issue.setCustomFieldValue(OldEscalateStartDateField, EscalateStartDate)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }


                EscalatedTrigger = "No"
                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                //Trigger
                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                log.debug "PWO-0 : " + linkedIssueOfClusterTrigger.getKey()
                                                //Get total blocking duration of PWO0
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                    EscalatedTrigger = "Yes"
                                                }
                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                        //Get total blocking duration of PWO1
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                            EscalatedTrigger = "Yes"
                                                        }
                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                    EscalatedTrigger = "Yes"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //Set data on trigger
                                def fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(LinkedIssueOfCluster)
                                def valueEscalatedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigEscalatedTrigger)?.find {
                                    it.toString() == EscalatedTrigger
                                }
                                LinkedIssueOfCluster.setCustomFieldValue(EscalatedField, valueEscalatedTrigger)
                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                            }
                        }
                    }
                }
            } else {
                EscalateEndDate = TodaysDateCompare
                issue.setCustomFieldValue(EscalateEndDateField, EscalateEndDate)
                issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                issueIndexingService.reIndex(issue)

                EscalateStartDate = (Date) issue.getCustomFieldValue(EscalateStartDateField)
                Date OldEscalatedDate = (Date) issue.getCustomFieldValue(OldEscalateEndDateField)
                log.debug "old escalated date : " + OldEscalatedDate
                log.debug "todaysdate : " + TodaysDateCompare

                if (!issue.getCustomFieldValue(OldEscalateEndDateField) || issue.getCustomFieldValue(OldEscalateEndDateField) != TodaysDateCompare) {
                    log.debug "set old date"
                    issue.setCustomFieldValue(OldEscalateEndDateField, EscalateEndDate)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                    if (issue.getCustomFieldValue(TotalEscalatedDurationField)) {
                        TotalEscalatedDurationOld = issue.getCustomFieldValue(TotalEscalatedDurationField).replace(" d", "").toDouble()
                        if (issue.getCustomFieldValue(OldEscalateStartDateField) == TodaysDateCompare || issue.getCustomFieldValue(OldEscalateStartDateField) > OldEscalatedDate) {
                            TotalEscalatedDuration = EscalateEndDate - EscalateStartDate + 1
                        } else {
                            TotalEscalatedDuration = EscalateEndDate - EscalateStartDate
                        }
                        TotalEscalatedDuration = TotalEscalatedDurationOld + TotalEscalatedDuration
                    } else {
                        TotalEscalatedDuration = EscalateEndDate - EscalateStartDate + 1
                    }
                    TotalEscalatedDuration = TotalEscalatedDuration.trunc()
                    TotalEscalatedDurationString = TotalEscalatedDuration.trunc() + " d"

                    issue.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationString)

                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)

                }


                //Calculate Blocking duration for trigger
                TotalEscalatedDurationTrigger = 0
                EscalatedTrigger = "No"
                def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
                for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
                    if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                        for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                            if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                                //Trigger
                                def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, user).getAllIssues()
                                for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                        def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                        for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                            if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                                //Get total blocking duration of PWO0
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                    EscalatedTrigger = "Yes"
                                                }
                                                if (linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                    TotalEscalatedDurationTriggerString = linkedIssueOfClusterTrigger.getCustomFieldValue(TotalEscalatedDurationField)
                                                    TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                    TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                }
                                                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, user).getAllIssues()
                                                for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                        //Get total blocking duration of PWO1
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                            EscalatedTrigger = "Yes"
                                                        }
                                                        if (linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                            TotalEscalatedDurationTriggerString = linkedIssueOfPWO0.getCustomFieldValue(TotalEscalatedDurationField)
                                                            TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                            TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                        }
                                                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                if (linkedIssueOfPWO1.getCustomFieldValue(EscalatedField).toString() == "Yes") {
                                                                    EscalatedTrigger = "Yes"
                                                                }
                                                                if (linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField)) {
                                                                    TotalEscalatedDurationTriggerString = linkedIssueOfPWO1.getCustomFieldValue(TotalEscalatedDurationField)
                                                                    TotalEscalatedDurationTriggerString = TotalEscalatedDurationTriggerString.replaceAll(" d", "")
                                                                    TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger + TotalEscalatedDurationTriggerString.toDouble()
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //Set data on trigger
                                def fieldConfigEscalatedTrigger = EscalatedField.getRelevantConfig(LinkedIssueOfCluster)
                                def valueEscalatedTrigger = ComponentAccessor.optionsManager.getOptions(fieldConfigEscalatedTrigger)?.find {
                                    it.toString() == EscalatedTrigger
                                }
                                LinkedIssueOfCluster.setCustomFieldValue(EscalatedField, valueEscalatedTrigger)
                                TotalEscalatedDurationTrigger = TotalEscalatedDurationTrigger.trunc()
                                TotalEscalatedDurationTriggerString = TotalEscalatedDurationTrigger.trunc() + " d"
                                LinkedIssueOfCluster.setCustomFieldValue(TotalEscalatedDurationField, TotalEscalatedDurationTriggerString)
                                issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                                issueIndexingService.reIndex(LinkedIssueOfCluster)
                            }
                        }
                    }
                }
            }
        }


        //Set data
        def fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue)
        def valueTechnicallyClosed = ComponentAccessor.optionsManager.getOptions(fieldConfigTechnicallyClosed)?.find {
            it.toString() == TechnicallyClosed
        }
        issue.setCustomFieldValue(TechnicallyClosedField, valueTechnicallyClosed)

        issue.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost)
        issue.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost)
        issue.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)


        //Set costs data on Trigger
        def LinkedIssuesOfCurrentPWO = issueLinkManager.getLinkCollection(issue, IMSUser).getAllIssues()
        for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO) {
            if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, IMSUser).getAllIssues()
                for (LinkedIssueOfCluster in LinkedIssuesOfCluster) {
                    if (LinkedIssueOfCluster.getIssueTypeId() == ID_IT_Trigger) {
                        log.debug "Trigger"
                        //Reset variables time spent
                        PartsDataTimeSpent=0
                        MaintPlanningTimeSpent=0
                        MaintTimeSpent=0
                        PartsDataEstimatedCosts=0
                        MaintPlanningEstimatedCosts=0
                        MaintEstimatedCosts=0
                        //Trigger
                        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(LinkedIssueOfCluster, IMSUser).getAllIssues()
                        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                def LinkedIssuesOfClusterTrigger = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, IMSUser).getAllIssues()
                                for (linkedIssueOfClusterTrigger in LinkedIssuesOfClusterTrigger) {
                                    if (linkedIssueOfClusterTrigger.getIssueTypeId() == ID_IT_PWO0) {
                                        log.debug "PWO-0 : "+linkedIssueOfClusterTrigger.getKey()

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField)) {
                                            PartsDataEstimatedCosts = PartsDataEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EstimatedCostsField)
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField)) {
                                            MaintPlanningEstimatedCosts = MaintPlanningEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EstimatedCostsField)
                                        }
                                        log.debug "maint estimated : "+linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)) {
                                            MaintEstimatedCosts = MaintEstimatedCosts + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EstimatedCostsField)
                                        }
                                        log.debug "value : "+MaintEstimatedCosts

                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField)) {
                                            PartsDataTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_TimeSpentField).toString())
                                            PartsDataTimeSpent = PartsDataTimeSpent + PartsDataTimeSpentCurrent
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField)) {
                                            MaintPlanningTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_TimeSpentField).toString())
                                            MaintPlanningTimeSpent = MaintPlanningTimeSpent + MaintPlanningTimeSpentCurrent
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField)) {
                                            MaintTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_TimeSpentField).toString())
                                            MaintTimeSpent = MaintTimeSpent + MaintTimeSpentCurrent
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField)) {
                                            PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(PartsData_EffectiveCostsField)
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField)) {
                                            MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(MaintPlanning_EffectiveCostsField)
                                        }
                                        if (linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField)) {
                                            MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + (Double) linkedIssueOfClusterTrigger.getCustomFieldValue(Maint_EffectiveCostsField)
                                        }

                                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfClusterTrigger, IMSUser).getAllIssues()
                                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                    IATimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField).toString())
                                                } else {
                                                    IATimeSpentCurrent = 0
                                                }
                                                if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)) {
                                                    IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)
                                                } else {
                                                    IAEffectiveCost = 0
                                                }

                                                if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Parts Data") {
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                        PartsDataTimeSpent = PartsDataTimeSpent + IATimeSpentCurrent
                                                    }
                                                    PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IAEffectiveCost
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance Planning") {
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                        MaintPlanningTimeSpent = MaintPlanningTimeSpent + IATimeSpentCurrent
                                                    }
                                                    MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IAEffectiveCost
                                                } else if (linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field).toString() == "Maintenance") {
                                                    if (linkedIssueOfPWO0.getCustomFieldValue(IATimeSpentField)) {
                                                        MaintTimeSpent = MaintTimeSpent + IATimeSpentCurrent
                                                    }
                                                    MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IAEffectiveCost
                                                }

                                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, IMSUser).getAllIssues()
                                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                        DomainOfPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field).toString()
                                                        //Get PWO-2's subtask
                                                        def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                                        for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                                            if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField)) {
                                                                    AuthTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(AuthTimeSpentField).toString())
                                                                } else {
                                                                    AuthTimeSpentCurrent = 0
                                                                }
                                                                log.debug "Auth time spent current : "+AuthTimeSpentCurrent
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)) {
                                                                    AuthEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(AuthEffectiveCostField)
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data"){
                                                                    if(AuthEffectiveCost != 0) {
                                                                        PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                    }
                                                                    PartsDataTimeSpent = PartsDataTimeSpent + AuthTimeSpentCurrent
                                                                } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                    if (AuthEffectiveCost != 0){
                                                                        MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                    }
                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + AuthTimeSpentCurrent
                                                                } else if (DomainOfPWO2 == "Maintenance"){
                                                                    if(AuthEffectiveCost != 0) {
                                                                        MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + AuthEffectiveCost
                                                                    }
                                                                    MaintTimeSpent = MaintTimeSpent + AuthTimeSpentCurrent
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField)) {
                                                                    FormTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(FormTimeSpentField).toString())
                                                                } else {
                                                                    FormTimeSpentCurrent = 0
                                                                }
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)) {
                                                                    FormEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(FormEffectiveCostField)
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data"){
                                                                    if(FormEffectiveCost != 0) {
                                                                        PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + FormEffectiveCost
                                                                    }
                                                                    PartsDataTimeSpent = PartsDataTimeSpent + FormTimeSpentCurrent
                                                                } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                    if(FormEffectiveCost != 0) {
                                                                        MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + FormEffectiveCost
                                                                    }
                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + FormTimeSpentCurrent
                                                                } else if (DomainOfPWO2 == "Maintenance"){
                                                                    if(FormEffectiveCost != 0) {
                                                                        MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + FormEffectiveCost
                                                                    }
                                                                    MaintTimeSpent = MaintTimeSpent + FormTimeSpentCurrent
                                                                }
                                                            } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField)) {
                                                                    IntTimeSpentCurrent = formatHoursMinutesFormatInSecondes(linkedIssueOfPWO2.getCustomFieldValue(IntTimeSpentField).toString())
                                                                } else {
                                                                    IntTimeSpentCurrent = 0
                                                                }
                                                                if (linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)) {
                                                                    IntEffectiveCost = (double) linkedIssueOfPWO2.getCustomFieldValue(IntEffectiveCostField)
                                                                }
                                                                if (DomainOfPWO2 == "Parts Data"){
                                                                    if(IntEffectiveCost != 0) {
                                                                        PartsDataEffectiveCostsOfTrigger = PartsDataEffectiveCostsOfTrigger + IntEffectiveCost
                                                                    }
                                                                    PartsDataTimeSpent = PartsDataTimeSpent + IntTimeSpentCurrent
                                                                } else if (DomainOfPWO2 == "Maintenance Planning"){
                                                                    if(IntEffectiveCost != 0) {
                                                                        MaintPlanningEffectiveCostsOfTrigger = MaintPlanningEffectiveCostsOfTrigger + IntEffectiveCost
                                                                    }
                                                                    MaintPlanningTimeSpent = MaintPlanningTimeSpent + IntTimeSpentCurrent
                                                                } else if (DomainOfPWO2 == "Maintenance"){
                                                                    if(IntEffectiveCost != 0) {
                                                                        MaintEffectiveCostsOfTrigger = MaintEffectiveCostsOfTrigger + IntEffectiveCost
                                                                    }
                                                                    MaintTimeSpent = MaintTimeSpent + IntTimeSpentCurrent
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //Set data on trigger
                        log.debug "setting estimated cost on trigger : "+LinkedIssueOfCluster.getKey()
                        LinkedIssueOfCluster.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts)
                        LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts)
                        LinkedIssueOfCluster.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts)
                        log.debug "updating with user IMS : "+IMSUser
                        issueManager.updateIssue(IMSUser, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                        issueIndexingService.reIndex(LinkedIssueOfCluster)
                        
                        TriggerEffectiveCost = PartsDataEffectiveCostsOfTrigger + MaintPlanningEffectiveCostsOfTrigger + MaintEffectiveCostsOfTrigger
                        LinkedIssueOfCluster.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)
                        LinkedIssueOfCluster.setCustomFieldValue(PartsData_EffectiveCostsField, PartsDataEffectiveCostsOfTrigger)
                        LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EffectiveCostsField, MaintPlanningEffectiveCostsOfTrigger)
                        LinkedIssueOfCluster.setCustomFieldValue(Maint_EffectiveCostsField, MaintEffectiveCostsOfTrigger)
                        //LinkedIssueOfCluster.setCustomFieldValue(PartsData_EstimatedCostsField, PartsDataEstimatedCosts)
                        //LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_EstimatedCostsField, MaintPlanningEstimatedCosts)
                        //LinkedIssueOfCluster.setCustomFieldValue(Maint_EstimatedCostsField, MaintEstimatedCosts)
                        PartsDataTimeSpentString = formatSecondesValueInHoursMinutes(PartsDataTimeSpent)
                        LinkedIssueOfCluster.setCustomFieldValue(PartsData_TimeSpentField, PartsDataTimeSpentString)
                        MaintPlanningTimeSpentString = formatSecondesValueInHoursMinutes(MaintPlanningTimeSpent)
                        LinkedIssueOfCluster.setCustomFieldValue(MaintPlanning_TimeSpentField, MaintPlanningTimeSpentString)
                        MaintTimeSpenString = formatSecondesValueInHoursMinutes(MaintTimeSpent)
                        LinkedIssueOfCluster.setCustomFieldValue(Maint_TimeSpentField, MaintTimeSpenString)
                        issueManager.updateIssue(user, LinkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false)
                        issueIndexingService.reIndex(LinkedIssueOfCluster)
                    }
                }
            }
        }
    }
    
    String FirstSNAppliValue="Update"
    issue.setCustomFieldValue(FirstSNAppliField, FirstSNAppliValue)
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
	issueIndexingService.reIndex(issue)
    
}
