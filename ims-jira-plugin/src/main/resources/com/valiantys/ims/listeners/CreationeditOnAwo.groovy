package com.valiantys.ims.listeners
/**
 * Projects : AWO
 * Description : Creation/Edit on AWO
 * Events : Issue Updated, AWO updated
 * isDisabled : false
 */

import com.atlassian.crowd.embedded.api.Group
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.user.ApplicationUser


/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
}*/


boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

def getIssueFromNFeedFieldNewFieldsSingle(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ")
        issueFound = issueManager.getIssueObject(Long.parseLong(formattedValues))
    }

    if (issueFound) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + issueFound
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }
    return issueFound
}

def getIssueFromNFeedFieldNewFieldsMulitple(CustomField nFeedfield, MutableIssue issue, IssueManager issueManager) {
    Issue issueFound
    List<Issue> multipleIssue = new ArrayList<Issue>()
    String fieldValue = issue.getCustomFieldValue(nFeedfield).toString()

    if (!fieldValue.equals("null")) {
        String[] formattedValues = fieldValue.substring(19).replaceAll("</value>", "").replaceAll("<value>", "").replace("</content>", "").replaceAll(/\n/, "").replace("  ", " ").split(" ")
        for (String formatedValue in formattedValues) {
            issueFound = issueManager.getIssueObject(Long.parseLong(formatedValue))
            multipleIssue.add(issueFound)
        }
    }

    if (multipleIssue) {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + ": issues found : " + multipleIssue
    } else {
        log.debug "End Function getIssueFromNFeedField for issue : " + issue.getKey() + " for field : " + nFeedfield.getFieldName() + " no result"
    }

    return multipleIssue
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog
GroupManager groupManager = ComponentAccessor.getGroupManager()


MutableIssue issue = (MutableIssue) event.getIssue()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"
String ID_IT_Cluster = "10001"
String ID_IT_Trigger = "10000"

List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

long ID_Project_AWO = 10003


//Fields
long ID_CF_AWO_AuthWU = 10359
long ID_CF_AWO_AuthNbWu = 10360
long ID_CF_AWO_AuthEffectiveCost = 11103
long ID_CF_AWO_AuthVerifWU = 10362
long ID_CF_AWO_AuthVerifNbWU = 10363
long ID_CF_AWO_AuthVerifEffectiveCost = 11105
long ID_CF_AWO_FormWU = 10365
long ID_CF_AWO_FormNbWu = 10366
long ID_CF_AWO_FormEffectiveCost = 11104
long ID_CF_AWO_FormVerifWU = 10368
long ID_CF_AWO_FormVerifNbWu = 10369
long ID_CF_AWO_FormVerifEffectiveCost = 11106
long ID_CF_AWO_TransWU = 10371
long ID_CF_AWO_TransNbWU = 10372
long ID_CF_AWO_EffectiveCost = 11107
long ID_CF_AWO_IntWU = 10374
long ID_CF_AWO_IntNbWU = 10375
long ID_CF_AWO_IntEffectiveCost = 11108
long ID_CF_WUValue = 10625
long ID_CF_FirstSNAppli = 10011

long ID_CF_PartsData_EffectiveCost = 10113
long ID_CF_MaintPlanning_EffectiveCost = 10114
long ID_CF_Maint_EffectiveCost = 10115
long ID_CF_IA_EffectiveCost = 10161
long ID_CF_DomainPWO1 = 10139

long ID_CF_Auth_EffectiveCost = 10240
long ID_CF_Auth_AdditionalCost = 11109
long ID_CF_Auth_Verif_AdditionalCost = 11110
long ID_CF_Form_EffectiveCost = 10305
long ID_CF_Form_AdditionalCost = 11111
long ID_CF_Form_Verif_AdditionalCost = 11112
long ID_CF_Trans_AdditionalCost = 11113
long ID_CF_Int_EffectiveCost = 10332
long ID_CF_Int_AdditionalCost = 11114
long ID_CF_TriggerTotalEffectiveCosts = 10116
long ID_CF_Scenario = 10152
long ID_CF_TypeOfVerif = 10354

long ID_CF_IA_Company = 10142
long ID_CF_CompanyAllowed = 10702

//CustomField
CustomField AWO_AuthWUField = customFieldManager.getCustomFieldObject(ID_CF_AWO_AuthWU)
CustomField AWOAuthNbWuField = customFieldManager.getCustomFieldObject(ID_CF_AWO_AuthNbWu)
CustomField AWO_AuthEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_AuthEffectiveCost)
CustomField AWO_AuthVerifWUField = customFieldManager.getCustomFieldObject(ID_CF_AWO_AuthVerifWU)
CustomField AWO_AuthVerifNbWuField = customFieldManager.getCustomFieldObject(ID_CF_AWO_AuthVerifNbWU)
CustomField AWO_AuthVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_AuthVerifEffectiveCost)
CustomField AWO_FormWUField = customFieldManager.getCustomFieldObject(ID_CF_AWO_FormWU)
CustomField AWO_FormNbWuField = customFieldManager.getCustomFieldObject(ID_CF_AWO_FormNbWu)
CustomField AWO_FormEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_FormEffectiveCost)
CustomField AWO_FormVerifWUField = customFieldManager.getCustomFieldObject(ID_CF_AWO_FormVerifWU)
CustomField AWO_FormVerifNbWuField = customFieldManager.getCustomFieldObject(ID_CF_AWO_FormVerifNbWu)
CustomField AWO_FormVerifEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_FormVerifEffectiveCost)
CustomField AWO_TransWUField = customFieldManager.getCustomFieldObject(ID_CF_AWO_TransWU)
CustomField AWO_TransNbWuField = customFieldManager.getCustomFieldObject(ID_CF_AWO_TransNbWU)
CustomField AWO_TransEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_EffectiveCost)
CustomField AWO_IntWUField = customFieldManager.getCustomFieldObject(ID_CF_AWO_IntWU)
CustomField AWO_IntNbWuField = customFieldManager.getCustomFieldObject(ID_CF_AWO_IntNbWU)
CustomField AWO_IntEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_AWO_IntEffectiveCost)
CustomField WUValueField = customFieldManager.getCustomFieldObject(ID_CF_WUValue)
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)

CustomField PartsData_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_EffectiveCost)
CustomField MaintPlanning_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_EffectiveCost)
CustomField Maint_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Maint_EffectiveCost)
CustomField DomainPWO1Field = customFieldManager.getCustomFieldObject(ID_CF_DomainPWO1)
CustomField IAEffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_IA_EffectiveCost)

CustomField Auth_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Auth_EffectiveCost)
CustomField Auth_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_Auth_AdditionalCost)
CustomField Auth_Verif_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verif_AdditionalCost)
CustomField Form_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Form_EffectiveCost)
CustomField Form_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_Form_AdditionalCost)
CustomField Form_Verif_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verif_AdditionalCost)
CustomField Trans_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_Trans_AdditionalCost)
CustomField Int_EffectiveCostField = customFieldManager.getCustomFieldObject(ID_CF_Int_EffectiveCost)
CustomField Int_AdditionalCostField = customFieldManager.getCustomFieldObject(ID_CF_Int_AdditionalCost)
CustomField TriggerTotalEffectiveCostsField = customFieldManager.getCustomFieldObject(ID_CF_TriggerTotalEffectiveCosts)

CustomField IACompanyField = customFieldManager.getCustomFieldObject(ID_CF_IA_Company)
CustomField CompanyAllowedField = customFieldManager.getCustomFieldObject(ID_CF_CompanyAllowed)

Issue issueAWOAuthWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(AWO_AuthWUField, issue, issueManager)
Issue issueAWOAuthVerifhWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(AWO_AuthVerifWUField, issue, issueManager)
Issue issueAWOFormWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(AWO_FormWUField, issue, issueManager)
Issue issueAWOFormVerifhWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(AWO_FormVerifWUField, issue, issueManager)
Issue issueAWOTransWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(AWO_TransWUField, issue, issueManager)
Issue issueAWOInthWU = (Issue) getIssueFromNFeedFieldNewFieldsSingle(AWO_IntWUField, issue, issueManager)

double AWOAuthEffectiveCost
double AWOAuthVerifEffectiveCost
double AWOFormEffectiveCost
double AWOFormVerifEffectiveCost
double AWOTransEffectiveCost
double AWOIntEffectiveCost

double AWOAuthWUValue
double AWOAuthVerifWUValue
double AWOFormWUValue
double AWOFormVerifWUValue
double AWOTransWUValue
double AWOIntWUValue

double AWOAuthNbWu
double AWOAuthVerifNbWu
double AWOFormNbWu
double AWOFormVerifNbWu
double AWOTransNbWu
double AWOIntNbWu

double PartsDataEffectiveCost
double MaintPlanningEffectiveCost
double MaintEffectiveCost
double IAEffectiveCost

double Auth_EffectiveCost
double Form_EffectiveCost
double Int_EffectiveCost

double AuthEffectiveCostOfTrigger
double FormEffectiveCostOfTrigger
double IntEffectiveCostOfTrigger
double TriggerEffectiveCost

String DomainPWO1
String DomainPWO2

Issue issueTrigger
List<Issue> issuesPWO0 = new ArrayList<Issue>()
List<Issue> issuesPWO1 = new ArrayList<Issue>()
List<Issue> issuesAWO = new ArrayList<Issue>()

List<Group> CompanyAllowedList = new ArrayList<Group>()

//Business

List<String> itemsChangedAtCreation = new ArrayList<String>()
itemsChangedAtCreation.add(FirstSNAppliField.getFieldName())
log.debug "issue = " + issue.getKey()

boolean isCreation = (boolean) workloadIsUpdated(changeLog, itemsChangedAtCreation)
log.debug "is creation : " + isCreation
if (isCreation) {//Creation
//Calculate effective costs
    if (issueAWOAuthWU && issue.getCustomFieldValue(AWOAuthNbWuField)) {
        AWOAuthWUValue = issueAWOAuthWU.getCustomFieldValue(WUValueField)
        AWOAuthNbWu = issue.getCustomFieldValue(AWOAuthNbWuField)
        AWOAuthEffectiveCost = AWOAuthWUValue * AWOAuthNbWu
    }
    if (issue.getCustomFieldValue(Auth_AdditionalCostField)) {
        AWOAuthEffectiveCost = AWOAuthEffectiveCost + (double) issue.getCustomFieldValue(Auth_AdditionalCostField)
    }

    if (issueAWOAuthVerifhWU && issue.getCustomFieldValue(AWO_AuthVerifNbWuField)) {
        AWOAuthVerifWUValue = issueAWOAuthVerifhWU.getCustomFieldValue(WUValueField)
        AWOAuthVerifNbWu = issue.getCustomFieldValue(AWO_AuthVerifNbWuField)
        AWOAuthVerifEffectiveCost = AWOAuthVerifWUValue * AWOAuthVerifNbWu
    }
    if (issue.getCustomFieldValue(Auth_Verif_AdditionalCostField)) {
        AWOAuthVerifEffectiveCost = AWOAuthVerifEffectiveCost + (double) issue.getCustomFieldValue(Auth_Verif_AdditionalCostField)
    }

    if (issueAWOFormWU && issue.getCustomFieldValue(AWO_FormNbWuField)) {
        AWOFormWUValue = issueAWOFormWU.getCustomFieldValue(WUValueField)
        AWOFormNbWu = issue.getCustomFieldValue(AWO_FormNbWuField)
        AWOFormEffectiveCost = AWOFormWUValue * AWOFormNbWu
    }
    if (issue.getCustomFieldValue(Auth_AdditionalCostField)) {
        AWOFormEffectiveCost = AWOFormEffectiveCost + (double) issue.getCustomFieldValue(Form_AdditionalCostField)
    }

    if (issueAWOFormVerifhWU && issue.getCustomFieldValue(AWO_FormVerifNbWuField)) {
        AWOFormVerifWUValue = issueAWOFormVerifhWU.getCustomFieldValue(WUValueField)
        AWOFormVerifNbWu = issue.getCustomFieldValue(AWO_FormVerifNbWuField)
        AWOFormVerifEffectiveCost = AWOFormVerifWUValue * AWOFormVerifNbWu
    }
    if (issue.getCustomFieldValue(Form_Verif_AdditionalCostField)) {
        AWOFormVerifEffectiveCost = AWOFormVerifEffectiveCost + (double) issue.getCustomFieldValue(Form_Verif_AdditionalCostField)
    }

    if (issueAWOTransWU && issue.getCustomFieldValue(AWO_TransNbWuField)) {
        AWOTransWUValue = issueAWOTransWU.getCustomFieldValue(WUValueField)
        AWOTransNbWu = issue.getCustomFieldValue(AWO_TransNbWuField)
        AWOTransEffectiveCost = AWOTransWUValue * AWOTransNbWu
    }
    if (issue.getCustomFieldValue(Trans_AdditionalCostField)) {
        AWOTransEffectiveCost = AWOTransEffectiveCost + (double) issue.getCustomFieldValue(Trans_AdditionalCostField)
    }

    if (issueAWOInthWU && issue.getCustomFieldValue(AWO_IntNbWuField)) {
        AWOIntWUValue = issueAWOInthWU.getCustomFieldValue(WUValueField)
        AWOIntNbWu = issue.getCustomFieldValue(AWO_IntNbWuField)
        AWOIntEffectiveCost = AWOIntWUValue * AWOIntNbWu
    }
    if (issue.getCustomFieldValue(Int_AdditionalCostField)) {
        AWOIntEffectiveCost = AWOIntEffectiveCost + (double) issue.getCustomFieldValue(Int_AdditionalCostField)
    }

    issue.setCustomFieldValue(AWO_AuthEffectiveCostField, AWOAuthEffectiveCost)
    issue.setCustomFieldValue(AWO_AuthVerifEffectiveCostField, AWOAuthVerifEffectiveCost)
    issue.setCustomFieldValue(AWO_FormEffectiveCostField, AWOFormEffectiveCost)
    issue.setCustomFieldValue(AWO_FormVerifEffectiveCostField, AWOFormVerifEffectiveCost)
    issue.setCustomFieldValue(AWO_TransEffectiveCostField, AWOTransEffectiveCost)
    issue.setCustomFieldValue(AWO_IntEffectiveCostField, AWOIntEffectiveCost)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
    issueIndexingService.reIndex(issue)

    //Set cost data on PWO-2
    def LinkedIssuesOfCurrentAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfCurrentAWO in LinkedIssuesOfCurrentAWO) {
        if (linkedIssueOfCurrentAWO.getIssueTypeId() == ID_IT_PWO21) {
            def LinkesIssuesOfCurrentPWO21 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues()
            for (linkedIssueOfCurrentPWO21 in LinkesIssuesOfCurrentPWO21) {
                if (linkedIssueOfCurrentPWO21.getProjectId() == ID_Project_AWO) {
                    if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField)) {
                        Auth_EffectiveCost = Auth_EffectiveCost + (double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField)
                    }
                    if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField)) {
                        Auth_EffectiveCost = Auth_EffectiveCost + (double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField)
                    }
                }
            }
            linkedIssueOfCurrentAWO.setCustomFieldValue(Auth_EffectiveCostField, Auth_EffectiveCost)
            issueManager.updateIssue(user, linkedIssueOfCurrentAWO, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(linkedIssueOfCurrentAWO)
        } else if (linkedIssueOfCurrentAWO.getIssueTypeId() == ID_IT_PWO22) {
            def LinkesIssuesOfCurrentPWO22 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues()
            for (linkedIssueOfCurrentPWO22 in LinkesIssuesOfCurrentPWO22) {
                if (linkedIssueOfCurrentPWO22.getProjectId() == ID_Project_AWO) {
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField)) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField)
                    }
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField)) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField)
                    }
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField)) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField)
                    }
                }
            }
            linkedIssueOfCurrentAWO.setCustomFieldValue(Form_EffectiveCostField, Form_EffectiveCost)
            issueManager.updateIssue(user, linkedIssueOfCurrentAWO, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(linkedIssueOfCurrentAWO)
        } else if (linkedIssueOfCurrentAWO.getIssueTypeId() == ID_IT_PWO23) {
            def LinkesIssuesOfCurrentPWO23 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues()
            for (linkedIssueOfCurrentPWO23 in LinkesIssuesOfCurrentPWO23) {
                if (linkedIssueOfCurrentPWO23.getProjectId() == ID_Project_AWO) {
                    if (linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField)) {
                        Int_EffectiveCost = Int_EffectiveCost + (double) linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField)
                    }
                }
            }
            linkedIssueOfCurrentAWO.setCustomFieldValue(Int_EffectiveCostField, Int_EffectiveCost)
            issueManager.updateIssue(user, linkedIssueOfCurrentAWO, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(linkedIssueOfCurrentAWO)
        }
    }

    //Set costs data on Trigger
    def LinkedIssuesOfAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfAWO in LinkedIssuesOfAWO) {
        if (linkedIssueOfAWO.getIssueTypeId() == ID_IT_PWO1) {
            def LinkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfAWO, user).getAllIssues()
            for (LinkedIssueOfCurrentPWO1 in LinkedIssuesOfCurrentPWO1) {
                if (LinkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                    def LinkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfCurrentPWO1, user).getAllIssues()
                    for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO0) {
                        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                            def LinkedIssuesOfCurrentCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                            for (linkedIssueOfCurrentCluster in LinkedIssuesOfCurrentCluster) {
                                if (linkedIssueOfCurrentCluster.getIssueTypeId() == ID_IT_Trigger) {
                                    issueTrigger = linkedIssueOfCurrentCluster
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if (issueTrigger) {
        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(issueTrigger, user).getAllIssues()
        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
            if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                log.debug "Cluster is : " + linkedIssueOfTrigger.getKey()
                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                    if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_PWO0) {
                        if (linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostField)) {
                            PartsDataEffectiveCost = PartsDataEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostField)
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostField)) {
                            MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostField)
                        }
                        if (linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostField)) {
                            MaintEffectiveCost = MaintEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostField)
                        }

                        def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                        for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                            if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                DomainPWO1 = linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field)
                                if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)) {
                                    IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)
                                } else {
                                    IAEffectiveCost = 0
                                }

                                if (DomainPWO1 == "Parts Data") {
                                    PartsDataEffectiveCost = PartsDataEffectiveCost + IAEffectiveCost
                                }
                                if (DomainPWO1 == "Maintenance Planning") {
                                    MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + IAEffectiveCost
                                }
                                if (DomainPWO1 == "Maintenance") {
                                    MaintEffectiveCost = MaintEffectiveCost + IAEffectiveCost
                                }

                                def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                    if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                        DomainPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field)
                                        def SubTasksOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                        for (subTaskOfPWO2 in SubTasksOfPWO2) {
                                            if (subTaskOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                if (subTaskOfPWO2.getCustomFieldValue(Auth_EffectiveCostField)) {
                                                    AuthEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Auth_EffectiveCostField)
                                                } else {
                                                    AuthEffectiveCostOfTrigger = 0
                                                }

                                                if (DomainPWO2 == "Parts Data") {
                                                    PartsDataEffectiveCost = PartsDataEffectiveCost + AuthEffectiveCostOfTrigger
                                                }
                                                if (DomainPWO2 == "Maintenance Planning") {
                                                    MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + AuthEffectiveCostOfTrigger
                                                }
                                                if (DomainPWO2 == "Maintenance") {
                                                    MaintEffectiveCost = MaintEffectiveCost + AuthEffectiveCostOfTrigger
                                                }
                                            } else if (subTaskOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                if (subTaskOfPWO2.getCustomFieldValue(Form_EffectiveCostField)) {
                                                    FormEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Form_EffectiveCostField)
                                                } else {
                                                    FormEffectiveCostOfTrigger = 0
                                                }

                                                if (DomainPWO2 == "Parts Data") {
                                                    PartsDataEffectiveCost = PartsDataEffectiveCost + FormEffectiveCostOfTrigger
                                                }
                                                if (DomainPWO2 == "Maintenance Planning") {
                                                    MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + FormEffectiveCostOfTrigger
                                                }
                                                if (DomainPWO2 == "Maintenance") {
                                                    MaintEffectiveCost = MaintEffectiveCost + FormEffectiveCostOfTrigger
                                                }
                                            } else if (subTaskOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                if (subTaskOfPWO2.getCustomFieldValue(Int_EffectiveCostField)) {
                                                    IntEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Int_EffectiveCostField)
                                                } else {
                                                    IntEffectiveCostOfTrigger = 0
                                                }

                                                if (DomainPWO2 == "Parts Data") {
                                                    PartsDataEffectiveCost = PartsDataEffectiveCost + IntEffectiveCostOfTrigger
                                                }
                                                if (DomainPWO2 == "Maintenance Planning") {
                                                    MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + IntEffectiveCostOfTrigger
                                                }
                                                if (DomainPWO2 == "Maintenance") {
                                                    MaintEffectiveCost = MaintEffectiveCost + IntEffectiveCostOfTrigger
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        TriggerEffectiveCost = PartsDataEffectiveCost + MaintPlanningEffectiveCost + MaintEffectiveCost
        issueTrigger.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)
        issueTrigger.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost)
        issueTrigger.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost)
        issueTrigger.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost)

        issueManager.updateIssue(user, issueTrigger, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issueTrigger)
    }

    def LinkedIssuesOfAWOForCompany = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfAWOForCompany in LinkedIssuesOfAWOForCompany) {
        if (linkedIssueOfAWOForCompany.getIssueTypeId() == ID_IT_PWO1) {
            if (linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) && linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField) != "AIRBUS") {
                String IA_Company_Value = linkedIssueOfAWOForCompany.getCustomFieldValue(IACompanyField)
                String IA_Company_Formatted = "Partner_" + IA_Company_Value

                Group IA_Company_Management_Formatted_Group = groupManager.getGroup(IA_Company_Formatted)

                if (!CompanyAllowedList.contains(IA_Company_Management_Formatted_Group)) {
                    CompanyAllowedList.add(IA_Company_Management_Formatted_Group)
                }
            }
        }
    }
    if (CompanyAllowedList != null) {
        issue.setCustomFieldValue(CompanyAllowedField, CompanyAllowedList)
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)//JIRA7
        issueIndexingService.reIndex(issue)//JIRA7
    }
} else {
    //Calculate effective costs
    if (issueAWOAuthWU && issue.getCustomFieldValue(AWOAuthNbWuField)) {
        AWOAuthWUValue = issueAWOAuthWU.getCustomFieldValue(WUValueField)
        AWOAuthNbWu = issue.getCustomFieldValue(AWOAuthNbWuField)
        AWOAuthEffectiveCost = AWOAuthWUValue * AWOAuthNbWu
    }
    if (issue.getCustomFieldValue(Auth_AdditionalCostField)) {
        AWOAuthEffectiveCost = AWOAuthEffectiveCost + (double) issue.getCustomFieldValue(Auth_AdditionalCostField)
    }

    if (issueAWOAuthVerifhWU && issue.getCustomFieldValue(AWO_AuthVerifNbWuField)) {
        AWOAuthVerifWUValue = issueAWOAuthVerifhWU.getCustomFieldValue(WUValueField)
        AWOAuthVerifNbWu = issue.getCustomFieldValue(AWO_AuthVerifNbWuField)
        AWOAuthVerifEffectiveCost = AWOAuthVerifWUValue * AWOAuthVerifNbWu
    }
    if (issue.getCustomFieldValue(Auth_Verif_AdditionalCostField)) {
        AWOAuthVerifEffectiveCost = AWOAuthVerifEffectiveCost + (double) issue.getCustomFieldValue(Auth_Verif_AdditionalCostField)
    }

    if (issueAWOFormWU && issue.getCustomFieldValue(AWO_FormNbWuField)) {
        AWOFormWUValue = issueAWOFormWU.getCustomFieldValue(WUValueField)
        AWOFormNbWu = issue.getCustomFieldValue(AWO_FormNbWuField)
        AWOFormEffectiveCost = AWOFormWUValue * AWOFormNbWu
    }
    if (issue.getCustomFieldValue(Form_AdditionalCostField)) {
        AWOFormEffectiveCost = AWOFormEffectiveCost + (double) issue.getCustomFieldValue(Form_AdditionalCostField)
    }

    if (issueAWOFormVerifhWU && issue.getCustomFieldValue(AWO_FormVerifNbWuField)) {
        AWOFormVerifWUValue = issueAWOFormVerifhWU.getCustomFieldValue(WUValueField)
        AWOFormVerifNbWu = issue.getCustomFieldValue(AWO_FormVerifNbWuField)
        AWOFormVerifEffectiveCost = AWOFormVerifWUValue * AWOFormVerifNbWu
    }
    if (issue.getCustomFieldValue(Form_Verif_AdditionalCostField)) {
        AWOFormVerifEffectiveCost = AWOFormVerifEffectiveCost + (double) issue.getCustomFieldValue(Form_Verif_AdditionalCostField)
    }

    if (issueAWOTransWU && issue.getCustomFieldValue(AWO_TransNbWuField)) {
        AWOTransWUValue = issueAWOTransWU.getCustomFieldValue(WUValueField)
        AWOTransNbWu = issue.getCustomFieldValue(AWO_TransNbWuField)
        AWOTransEffectiveCost = AWOTransWUValue * AWOTransNbWu
    }
    if (issue.getCustomFieldValue(Trans_AdditionalCostField)) {
        AWOTransEffectiveCost = AWOTransEffectiveCost + (double) issue.getCustomFieldValue(Trans_AdditionalCostField)
    }

    if (issueAWOInthWU && issue.getCustomFieldValue(AWO_IntNbWuField)) {
        AWOIntWUValue = issueAWOInthWU.getCustomFieldValue(WUValueField)
        AWOIntNbWu = issue.getCustomFieldValue(AWO_IntNbWuField)
        AWOIntEffectiveCost = AWOIntWUValue * AWOIntNbWu
    }
    if (issue.getCustomFieldValue(Int_AdditionalCostField)) {
        AWOIntEffectiveCost = AWOIntEffectiveCost + (double) issue.getCustomFieldValue(Int_AdditionalCostField)
    }

    issue.setCustomFieldValue(AWO_AuthEffectiveCostField, AWOAuthEffectiveCost)
    issue.setCustomFieldValue(AWO_AuthVerifEffectiveCostField, AWOAuthVerifEffectiveCost)
    issue.setCustomFieldValue(AWO_FormEffectiveCostField, AWOFormEffectiveCost)
    issue.setCustomFieldValue(AWO_FormVerifEffectiveCostField, AWOFormVerifEffectiveCost)
    issue.setCustomFieldValue(AWO_TransEffectiveCostField, AWOTransEffectiveCost)
    issue.setCustomFieldValue(AWO_IntEffectiveCostField, AWOIntEffectiveCost)

    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
    issueIndexingService.reIndex(issue)

    //Set cost data on PWO-2
    def LinkedIssuesOfCurrentAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfCurrentAWO in LinkedIssuesOfCurrentAWO) {
        if (linkedIssueOfCurrentAWO.getIssueTypeId() == ID_IT_PWO21) {
            def LinkesIssuesOfCurrentPWO21 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues()
            for (linkedIssueOfCurrentPWO21 in LinkesIssuesOfCurrentPWO21) {
                if (linkedIssueOfCurrentPWO21.getProjectId() == ID_Project_AWO) {
                    if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField)) {
                        Auth_EffectiveCost = Auth_EffectiveCost + (double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthEffectiveCostField)
                    }
                    if (linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField)) {
                        Auth_EffectiveCost = Auth_EffectiveCost + (double) linkedIssueOfCurrentPWO21.getCustomFieldValue(AWO_AuthVerifEffectiveCostField)
                    }
                }
            }
            linkedIssueOfCurrentAWO.setCustomFieldValue(Auth_EffectiveCostField, Auth_EffectiveCost)
            issueManager.updateIssue(user, linkedIssueOfCurrentAWO, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(linkedIssueOfCurrentAWO)
        } else if (linkedIssueOfCurrentAWO.getIssueTypeId() == ID_IT_PWO22) {
            def LinkesIssuesOfCurrentPWO22 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues()
            for (linkedIssueOfCurrentPWO22 in LinkesIssuesOfCurrentPWO22) {
                if (linkedIssueOfCurrentPWO22.getProjectId() == ID_Project_AWO) {
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField)) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormEffectiveCostField)
                    }
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField)) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_FormVerifEffectiveCostField)
                    }
                    if (linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField)) {
                        Form_EffectiveCost = Form_EffectiveCost + (double) linkedIssueOfCurrentPWO22.getCustomFieldValue(AWO_TransEffectiveCostField)
                    }
                }
            }
            linkedIssueOfCurrentAWO.setCustomFieldValue(Form_EffectiveCostField, Form_EffectiveCost)
            issueManager.updateIssue(user, linkedIssueOfCurrentAWO, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(linkedIssueOfCurrentAWO)
        } else if (linkedIssueOfCurrentAWO.getIssueTypeId() == ID_IT_PWO23) {
            def LinkesIssuesOfCurrentPWO23 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO, user).getAllIssues()
            for (linkedIssueOfCurrentPWO23 in LinkesIssuesOfCurrentPWO23) {
                if (linkedIssueOfCurrentPWO23.getProjectId() == ID_Project_AWO) {
                    if (linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField)) {
                        Int_EffectiveCost = Int_EffectiveCost + (double) linkedIssueOfCurrentPWO23.getCustomFieldValue(AWO_IntEffectiveCostField)
                    }
                }
            }
            linkedIssueOfCurrentAWO.setCustomFieldValue(Int_EffectiveCostField, Int_EffectiveCost)
            issueManager.updateIssue(user, linkedIssueOfCurrentAWO, EventDispatchOption.DO_NOT_DISPATCH, false)
            issueIndexingService.reIndex(linkedIssueOfCurrentAWO)
        }
    }

    //Set costs data on Trigger
    def LinkedIssuesOfAWO = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
    for (linkedIssueOfAWO in LinkedIssuesOfAWO) {
        if (linkedIssueOfAWO.getIssueTypeId() == ID_IT_PWO1) {
            def LinkedIssuesOfCurrentPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfAWO, user).getAllIssues()
            for (LinkedIssueOfCurrentPWO1 in LinkedIssuesOfCurrentPWO1) {
                if (LinkedIssueOfCurrentPWO1.getIssueTypeId() == ID_IT_PWO0) {
                    def LinkedIssuesOfCurrentPWO0 = issueLinkManager.getLinkCollection(LinkedIssueOfCurrentPWO1, user).getAllIssues()
                    for (linkedIssueOfCurrentPWO0 in LinkedIssuesOfCurrentPWO0) {
                        if (linkedIssueOfCurrentPWO0.getIssueTypeId() == ID_IT_Cluster) {
                            def LinkedIssuesOfCurrentCluster = issueLinkManager.getLinkCollection(linkedIssueOfCurrentPWO0, user).getAllIssues()
                            for (linkedIssueOfCurrentCluster in LinkedIssuesOfCurrentCluster) {
                                if (linkedIssueOfCurrentCluster.getIssueTypeId() == ID_IT_Trigger) {
                                    issueTrigger = linkedIssueOfCurrentCluster
                                    log.debug "trigger is : " + issueTrigger.getKey()
                                    def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(issueTrigger, user).getAllIssues()
                                    for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
                                        if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                                            log.debug "Cluster is : " + linkedIssueOfTrigger.getKey()
                                            def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                                            for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                                                if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_PWO0) {
                                                    if (linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostField)) {
                                                        PartsDataEffectiveCost = PartsDataEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(PartsData_EffectiveCostField)
                                                    }
                                                    if (linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostField)) {
                                                        MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(MaintPlanning_EffectiveCostField)
                                                    }
                                                    if (linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostField)) {
                                                        MaintEffectiveCost = MaintEffectiveCost + (Double) linkedIssueOfCluster.getCustomFieldValue(Maint_EffectiveCostField)
                                                    }

                                                    def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                                                    for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                                        if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                                            DomainPWO1 = linkedIssueOfPWO0.getCustomFieldValue(DomainPWO1Field)
                                                            if (linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)) {
                                                                IAEffectiveCost = (Double) linkedIssueOfPWO0.getCustomFieldValue(IAEffectiveCostField)
                                                            } else {
                                                                IAEffectiveCost = 0
                                                            }

                                                            if (DomainPWO1 == "Parts Data") {
                                                                PartsDataEffectiveCost = PartsDataEffectiveCost + IAEffectiveCost
                                                            }
                                                            if (DomainPWO1 == "Maintenance Planning") {
                                                                MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + IAEffectiveCost
                                                            }
                                                            if (DomainPWO1 == "Maintenance") {
                                                                MaintEffectiveCost = MaintEffectiveCost + IAEffectiveCost
                                                            }

                                                            def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                                            for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                                                if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                                                    DomainPWO2 = linkedIssueOfPWO1.getCustomFieldValue(DomainPWO1Field)
                                                                    def SubTasksOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                                                    for (subTaskOfPWO2 in SubTasksOfPWO2) {
                                                                        if (subTaskOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                                            if (subTaskOfPWO2.getCustomFieldValue(Auth_EffectiveCostField)) {
                                                                                AuthEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Auth_EffectiveCostField)
                                                                            } else {
                                                                                AuthEffectiveCostOfTrigger = 0
                                                                            }

                                                                            if (DomainPWO2 == "Parts Data") {
                                                                                PartsDataEffectiveCost = PartsDataEffectiveCost + AuthEffectiveCostOfTrigger
                                                                            }
                                                                            if (DomainPWO2 == "Maintenance Planning") {
                                                                                MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + AuthEffectiveCostOfTrigger
                                                                            }
                                                                            if (DomainPWO2 == "Maintenance") {
                                                                                MaintEffectiveCost = MaintEffectiveCost + AuthEffectiveCostOfTrigger
                                                                            }
                                                                        } else if (subTaskOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                                            if (subTaskOfPWO2.getCustomFieldValue(Form_EffectiveCostField)) {
                                                                                FormEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Form_EffectiveCostField)
                                                                            } else {
                                                                                FormEffectiveCostOfTrigger = 0
                                                                            }

                                                                            if (DomainPWO2 == "Parts Data") {
                                                                                PartsDataEffectiveCost = PartsDataEffectiveCost + FormEffectiveCostOfTrigger
                                                                            }
                                                                            if (DomainPWO2 == "Maintenance Planning") {
                                                                                MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + FormEffectiveCostOfTrigger
                                                                            }
                                                                            if (DomainPWO2 == "Maintenance") {
                                                                                MaintEffectiveCost = MaintEffectiveCost + FormEffectiveCostOfTrigger
                                                                            }
                                                                        } else if (subTaskOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                                            if (subTaskOfPWO2.getCustomFieldValue(Int_EffectiveCostField)) {
                                                                                IntEffectiveCostOfTrigger = (Double) subTaskOfPWO2.getCustomFieldValue(Int_EffectiveCostField)
                                                                            } else {
                                                                                IntEffectiveCostOfTrigger = 0
                                                                            }

                                                                            if (DomainPWO2 == "Parts Data") {
                                                                                PartsDataEffectiveCost = PartsDataEffectiveCost + IntEffectiveCostOfTrigger
                                                                            }
                                                                            if (DomainPWO2 == "Maintenance Planning") {
                                                                                MaintPlanningEffectiveCost = MaintPlanningEffectiveCost + IntEffectiveCostOfTrigger
                                                                            }
                                                                            if (DomainPWO2 == "Maintenance") {
                                                                                MaintEffectiveCost = MaintEffectiveCost + IntEffectiveCostOfTrigger
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    TriggerEffectiveCost = PartsDataEffectiveCost + MaintPlanningEffectiveCost + MaintEffectiveCost
                                    issueTrigger.setCustomFieldValue(TriggerTotalEffectiveCostsField, TriggerEffectiveCost)
                                    issueTrigger.setCustomFieldValue(PartsData_EffectiveCostField, PartsDataEffectiveCost)
                                    issueTrigger.setCustomFieldValue(MaintPlanning_EffectiveCostField, MaintPlanningEffectiveCost)
                                    issueTrigger.setCustomFieldValue(Maint_EffectiveCostField, MaintEffectiveCost)

                                    issueManager.updateIssue(user, issueTrigger, EventDispatchOption.DO_NOT_DISPATCH, false)
                                    issueIndexingService.reIndex(issueTrigger)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
