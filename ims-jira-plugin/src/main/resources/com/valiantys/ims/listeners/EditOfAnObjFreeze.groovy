package com.valiantys.ims.listeners
/**
 * Projects : FRZ
 * Description : Edit of an Obj. Freeze
 * Events : FRZ updated
 * isDisabled : false
 */

// import


import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger

import java.sql.Timestamp

//Manager

IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager)
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()


MutableIssue issue = (MutableIssue) event.getIssue()

//Constante
//Issue type
String ID_IT_Trigger = "10000"
String ID_IT_Cluster = "10001"
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"

//Field of obj. freeze
long ID_CF_PlannedQG2 = 10631

//Field of other object
long ID_CF_RequestedDate = 10010
long ID_CF_PreIA_PartsData_RequestedDate = 10058
long ID_CF_PreIA_PartsData_Validation_RequestedDate = 10066
long ID_CF_PartsData_PreIA_1stCommitedDate = 10060
long ID_CF_PartsData_PreIA_LastCommitedDate = 10062
long ID_CF_PartsData_PreIA_Validation_1stCommitedDate = 10067
long ID_CF_PartsData_PreIA_Validation_LastCommitedDate = 10090


long ID_CF_PreIA_MaintPlanning_RequestedDate = 10077
long ID_CF_PreIA_MaintPlanning_Validation_RequestedDate = 10085
long ID_CF_MaintPlanning_PreIA_1stCommitedDate = 10079
long ID_CF_MaintPlanning_PreIA_LastCommitedDate = 10088
long ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate = 10086
long ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate = 10081

long ID_CF_PreIA_Maint_RequestedDate = 10097
long ID_CF_PreIA_Maint_Validation_RequestedDate = 10105
long ID_CF_Maint_PreIA_1stCommitedDate = 10099
long ID_CF_Maint_PreIA_LastCommitedDate = 10101
long ID_CF_Maint_PreIA_Validation_1stCommitedDate = 10106
long ID_CF_Maint_PreIA_Validation_LastCommitedDate = 10108

long ID_CF_IA_RequestedDate = 10144
long ID_CF_IA_Validation_RequestedDate = 10154
long ID_CF_IA_1stCommittedDate = 10145
long ID_CF_IA_LastCommittedDate = 10146
long ID_CF_IA_Validation_1stCommittedDate = 10155
long ID_CF_IA_Validation_LastCommittedDate = 10156

long ID_CF_Auth_RequestedDate = 10175
long ID_CF_Auth_Verification_RequestedDate = 10192
long ID_CF_Auth_Validation_RequestedDate = 10190

long ID_CF_Auth_LastCommittedDate = 10177
long ID_CF_Auth_Verification_LastCommittedDate = 10185
long ID_CF_Auth_Validation_LastCommittedDate = 10193

long ID_CF_Auth_FirstCommittedDate = 10176
long ID_CF_Auth_Verification_FirstCommittedDate = 10184
long ID_CF_Auth_Validation_FirstCommittedDate = 10191

long ID_CF_Form_RequestedDate = 10283
long ID_CF_Form_Verification_RequestedDate = 10292
long ID_CF_Form_Validation_RequestedDate = 10299

long ID_CF_Form_1stCommittedDate = 10284
long ID_CF_Form_Verification_1stCommittedDate = 10293
long ID_CF_Form_Validation_1stCommittedDate = 10300

long ID_CF_Form_LastCommittedDate = 10285
long ID_CF_Form_Verification_LastCommittedDate = 10294
long ID_CF_Form_Validation_LastCommittedDate = 10301

long ID_CF_Int_RequestedDate = 10326
long ID_CF_Int_1stCommittedDate = 10327
long ID_CF_Int_LastCommittedDate = 10328


//CustomField
CustomField PlannedQG2Field = customFieldManager.getCustomFieldObject(ID_CF_PlannedQG2)
CustomField RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_RequestedDate)

CustomField PreIA_PartsData_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_RequestedDate)
CustomField PreIA_PartsData_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_Validation_RequestedDate)
CustomField PartsData_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_1stCommitedDate)
CustomField PartsData_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_LastCommitedDate)
CustomField PartsData_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_1stCommitedDate)
CustomField PartsData_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_PartsData_PreIA_Validation_LastCommitedDate)

CustomField PreIA_MaintPlanning_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_RequestedDate)
CustomField PreIA_MaintPlanning_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_Validation_RequestedDate)
CustomField MaintPlanning_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_1stCommitedDate)
CustomField MaintPlanning_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_PreIA_LastCommitedDate)
CustomField MaintPlanning_Validation_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_1stCommitedDate)
CustomField MaintPlanning_Validation_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Validation_PreIA_LastCommitedDate)

CustomField PreIA_Maint_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_RequestedDate)
CustomField PreIA_Maint_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_Validation_RequestedDate)
CustomField Maint_PreIA_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_1stCommitedDate)
CustomField Maint_PreIA_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_LastCommitedDate)
CustomField Maint_PreIA_Validation_1stCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_1stCommitedDate)
CustomField Maint_PreIA_Validation_LastCommitedDateField = customFieldManager.getCustomFieldObject(ID_CF_Maint_PreIA_Validation_LastCommitedDate)

CustomField IA_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_RequestedDate)
CustomField IA_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_RequestedDate)
CustomField IA_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_1stCommittedDate)
CustomField IA_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_LastCommittedDate)
CustomField IA_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stCommittedDate)
CustomField IA_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastCommittedDate)

CustomField Auth_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_RequestedDate)
CustomField Auth_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_RequestedDate)
CustomField Auth_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_RequestedDate)

CustomField Auth_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_FirstCommittedDate)
CustomField Auth_Verification_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_FirstCommittedDate)
CustomField Auth_Validation_FirstCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_FirstCommittedDate)

CustomField Auth_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_LastCommittedDate)
CustomField Auth_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verification_LastCommittedDate)
CustomField Auth_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validation_LastCommittedDate)

CustomField Form_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_RequestedDate)
CustomField Form_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_RequestedDate)
CustomField Form_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_RequestedDate)

CustomField Form_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_1stCommittedDate)
CustomField Form_Verification_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_1stCommittedDate)
CustomField Form_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_1stCommittedDate)

CustomField Form_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_LastCommittedDate)
CustomField Form_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_LastCommittedDate)
CustomField Form_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_LastCommittedDate)

CustomField Int_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_RequestedDate)
CustomField Int_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_1stCommittedDate)
CustomField Int_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Int_LastCommittedDate)


//Variables
MutableIssue TriggerIssue
Timestamp PlannedQG2
Date RequestedDateTrigger

Date RequestedDatePWO0
Date RequestedDatePWO1
Date PreIA_PartsData_RequestedDate
Date PreIA_PartsData_Validation_RequestedDate
Date PreIA_MaintPlanning_RequestedDate
Date PreIA_MaintPlanning_Validation_RequestedDate
Date PreIA_Maint_RequestedDate
Date PreIA_Maint_Validation_RequestedDate
Date IA_RequestedDate
Date IA_Validation_RequestedDate

Date Auth_RequestedDate
Date Auth_Verification_RequestedDate
Date Auth_Validation_RequestedDate
Date Form_RequestedDate
Date Form_Verification_RequestedDate
Date Form_Validation_RequestedDate
Date Int_RequestedDate

//Business
log.debug "issue : " + issue.getKey()

//Get value to propagate
PlannedQG2 = (Timestamp) issue.getCustomFieldValue(PlannedQG2Field)

//Get all link
def LinkedIssues = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
for (linkedIssue in LinkedIssues) {
    if (linkedIssue.getIssueTypeId() == ID_IT_Trigger) {
        TriggerIssue = issueManager.getIssueObject(linkedIssue.id)
        TriggerIssue.setCustomFieldValue(RequestedDateField, PlannedQG2)

        //issueManager.updateIssue(user, TriggerIssue, EventDispatchOption.ISSUE_UPDATED, false)
        issueManager.updateIssue(user, TriggerIssue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
        
        //Get linked issue of Trigger
        def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(TriggerIssue, user).getAllIssues()
        for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
             if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {
                 //Get linked issue of Cluster
                def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger, user).getAllIssues()
                for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                    if (linkedIssueOfCluster.getIssueTypeId()==ID_IT_Trigger){
                    	if (linkedIssueOfCluster.getCustomFieldValue(RequestedDateField)){
                        	if(!RequestedDateTrigger || RequestedDateTrigger > (Date)linkedIssueOfCluster.getCustomFieldValue(RequestedDateField)){
                            	RequestedDateTrigger = (Date) linkedIssueOfCluster.getCustomFieldValue(RequestedDateField)
                                log.debug "Requested date trigger : " + RequestedDateTrigger
                            }
                        }
                    }
                }
             }
        }
        if (RequestedDateTrigger) {
            def LinkedIssuesOfTrigger2 = issueLinkManager.getLinkCollection(TriggerIssue, user).getAllIssues()
            for (linkedIssueOfTrigger2 in LinkedIssuesOfTrigger2) {
                if (linkedIssueOfTrigger2.getIssueTypeId() == ID_IT_Cluster) {
                    //Get linked issue of Cluster
                    def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(linkedIssueOfTrigger2, user).getAllIssues()
                    for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
                        if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_PWO0){
                            if (linkedIssueOfCluster.getStatusObject().getName() == "Not started") {
                                RequestedDatePWO0 = (Timestamp) (RequestedDateTrigger - 119)

                                PreIA_PartsData_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
                                PreIA_PartsData_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
                                Date PartsData_PreIA_1stCommitedDate = PreIA_PartsData_RequestedDate
                                Date PartsData_PreIA_LastCommitedDate = PartsData_PreIA_1stCommitedDate
                                Date PartsData_PreIA_Validation_1stCommitedDate = PreIA_PartsData_Validation_RequestedDate
                                Date PartsData_PreIA_Validation_LastCommitedDate = PartsData_PreIA_Validation_1stCommitedDate

                                PreIA_MaintPlanning_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
                                PreIA_MaintPlanning_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
                                Date MaintPlanning_PreIA_1stCommitedDate = PreIA_MaintPlanning_RequestedDate
                                Date MaintPlanning_PreIA_LastCommitedDate = MaintPlanning_PreIA_1stCommitedDate
                                Date MaintPlanning_PreIA_Validation_1stCommitedDate = PreIA_MaintPlanning_Validation_RequestedDate
                                Date MaintPlanning_PreIA_Validation_LastCommitedDate = MaintPlanning_PreIA_Validation_1stCommitedDate

                                PreIA_Maint_RequestedDate = (Timestamp) (RequestedDateTrigger - 126)
                                PreIA_Maint_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 119)
                                Date Maint_PreIA_1stCommitedDate = PreIA_Maint_RequestedDate
                                Date Maint_PreIA_LastCommitedDate = Maint_PreIA_1stCommitedDate
                                Date Maint_PreIA_Validation_1stCommitedDate = PreIA_Maint_Validation_RequestedDate
                                Date Maint_PreIA_Validation_LastCommitedDate = Maint_PreIA_Validation_1stCommitedDate

                                linkedIssueOfCluster.setCustomFieldValue(RequestedDateField, RequestedDatePWO0)

                                linkedIssueOfCluster.setCustomFieldValue(PreIA_PartsData_RequestedDateField, PreIA_PartsData_RequestedDate)
                                linkedIssueOfCluster.setCustomFieldValue(PreIA_PartsData_Validation_RequestedDateField, PreIA_PartsData_Validation_RequestedDate)
                                linkedIssueOfCluster.setCustomFieldValue(PartsData_PreIA_1stCommitedDateField, PartsData_PreIA_1stCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(PartsData_PreIA_LastCommitedDateField, PartsData_PreIA_LastCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(PartsData_PreIA_Validation_1stCommitedDateField, PartsData_PreIA_Validation_1stCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(PartsData_PreIA_Validation_LastCommitedDateField, PartsData_PreIA_Validation_LastCommitedDate)

                                linkedIssueOfCluster.setCustomFieldValue(PreIA_MaintPlanning_RequestedDateField, PreIA_MaintPlanning_RequestedDate)
                                linkedIssueOfCluster.setCustomFieldValue(PreIA_MaintPlanning_Validation_RequestedDateField, PreIA_MaintPlanning_Validation_RequestedDate)
                                linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_PreIA_1stCommitedDateField, MaintPlanning_PreIA_1stCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_PreIA_LastCommitedDateField, MaintPlanning_PreIA_LastCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_Validation_PreIA_1stCommitedDateField, MaintPlanning_PreIA_Validation_1stCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(MaintPlanning_Validation_PreIA_LastCommitedDateField, MaintPlanning_PreIA_Validation_LastCommitedDate)

                                linkedIssueOfCluster.setCustomFieldValue(PreIA_Maint_RequestedDateField, PreIA_Maint_RequestedDate)
                                linkedIssueOfCluster.setCustomFieldValue(PreIA_Maint_Validation_RequestedDateField, PreIA_Maint_Validation_RequestedDate)
                                linkedIssueOfCluster.setCustomFieldValue(Maint_PreIA_1stCommitedDateField, Maint_PreIA_1stCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(Maint_PreIA_LastCommitedDateField, Maint_PreIA_LastCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(Maint_PreIA_Validation_1stCommitedDateField, Maint_PreIA_Validation_1stCommitedDate)
                                linkedIssueOfCluster.setCustomFieldValue(Maint_PreIA_Validation_LastCommitedDateField, Maint_PreIA_Validation_LastCommitedDate)

                                issueManager.updateIssue(user, linkedIssueOfCluster, EventDispatchOption.DO_NOT_DISPATCH, false);
                                issueIndexingService.reIndex(linkedIssueOfCluster);
                            }
                            def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
                        	for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                                if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                                    if (linkedIssueOfPWO0.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                        RequestedDatePWO1 = (Timestamp) (RequestedDateTrigger - 98)
                                        IA_RequestedDate = (Timestamp) (RequestedDateTrigger - 105)
                                        IA_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 98)
                                        Date IA_1stCommittedDate = IA_RequestedDate
                                        Date IA_LastCommittedDate = IA_1stCommittedDate
                                        Date IA_Validation_1stCommittedDate = IA_Validation_RequestedDate
                                        Date IA_Validation_LastCommittedDate = IA_Validation_1stCommittedDate

                                        linkedIssueOfPWO0.setCustomFieldValue(RequestedDateField, RequestedDatePWO1)
                                        linkedIssueOfPWO0.setCustomFieldValue(IA_RequestedDateField, IA_RequestedDate)
                                        linkedIssueOfPWO0.setCustomFieldValue(IA_Validation_RequestedDateField, IA_Validation_RequestedDate)
                                        linkedIssueOfPWO0.setCustomFieldValue(IA_1stCommittedDateField, IA_1stCommittedDate)
                                        linkedIssueOfPWO0.setCustomFieldValue(IA_LastCommittedDateField, IA_LastCommittedDate)
                                        linkedIssueOfPWO0.setCustomFieldValue(IA_Validation_1stCommittedDateField, IA_Validation_1stCommittedDate)
                                        linkedIssueOfPWO0.setCustomFieldValue(IA_Validation_LastCommittedDateField, IA_Validation_LastCommittedDate)
                                        issueManager.updateIssue(user, linkedIssueOfPWO0, EventDispatchOption.DO_NOT_DISPATCH, false);
                                        issueIndexingService.reIndex(linkedIssueOfPWO0);
                                    }

                                    def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                                    for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                                        if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                            //To remove if update not needed
                                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2 && linkedIssueOfPWO1.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                                Date RequestedDatePWO2 = (Timestamp) (RequestedDateTrigger - 28)

                                                linkedIssueOfPWO1.setCustomFieldValue(RequestedDateField, RequestedDatePWO2)
                                                issueManager.updateIssue(user, linkedIssueOfPWO1, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                issueIndexingService.reIndex(linkedIssueOfPWO1);
                                            }

                                            def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                            for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                                if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                                    //To remove if update not needed
                                                    if (linkedIssueOfPWO2.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                                        Auth_RequestedDate = (Timestamp) (RequestedDateTrigger - 84)
                                                        Auth_Verification_RequestedDate = (Timestamp) (RequestedDateTrigger - 70)
                                                        Auth_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 56)

                                                        Date Auth_FirstCommittedDate = Auth_RequestedDate
                                                        Date Auth_Verification_FirstCommittedDate = Auth_Verification_RequestedDate
                                                        Date Auth_Validation_FirstCommittedDate = Auth_Validation_RequestedDate

                                                        Date Auth_LastCommittedDate = Auth_FirstCommittedDate
                                                        Date Auth_Verification_LastCommittedDate = Auth_Verification_FirstCommittedDate
                                                        Date Auth_Validation_LastCommittedDate = Auth_Validation_FirstCommittedDate

                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_RequestedDateField, Auth_RequestedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_Verification_RequestedDateField, Auth_Verification_RequestedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_Validation_RequestedDateField, Auth_Validation_RequestedDate)

                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_FirstCommittedDateField, Auth_FirstCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_Verification_FirstCommittedDateField, Auth_Verification_FirstCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_Validation_FirstCommittedDateField, Auth_Validation_FirstCommittedDate)

                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_LastCommittedDateField, Auth_LastCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_Verification_LastCommittedDateField, Auth_Verification_LastCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Auth_Validation_LastCommittedDateField, Auth_Validation_LastCommittedDate)

                                                        issueManager.updateIssue(user, linkedIssueOfPWO2, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                        issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                    }
                                                } else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                                    //To remove if update not needed
                                                    if (linkedIssueOfPWO2.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                                        Form_RequestedDate = (Timestamp) (RequestedDateTrigger - 49)
                                                        Form_Verification_RequestedDate = (Timestamp) (RequestedDateTrigger - 42)
                                                        Form_Validation_RequestedDate = (Timestamp) (RequestedDateTrigger - 35)

                                                        Date Form_FirstCommittedDate = Form_RequestedDate
                                                        Date Form_Verification_FirstCommittedDate = Form_Verification_RequestedDate
                                                        Date Form_Validation_FirstCommittedDate = Form_Validation_RequestedDate

                                                        Date Form_LastCommittedDate = Form_FirstCommittedDate
                                                        Date Form_Verification_LastCommittedDate = Form_Verification_FirstCommittedDate
                                                        Date Form_Validation_LastCommittedDate = Form_Validation_FirstCommittedDate

                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_RequestedDateField, Form_RequestedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_Verification_RequestedDateField, Form_Verification_RequestedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_Validation_RequestedDateField, Form_Validation_RequestedDate)

                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_1stCommittedDateField, Form_FirstCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_Verification_1stCommittedDateField, Form_Verification_FirstCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_Validation_1stCommittedDateField, Form_Validation_FirstCommittedDate)

                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_LastCommittedDateField, Form_LastCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_Verification_LastCommittedDateField, Form_Verification_LastCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Form_Validation_LastCommittedDateField, Form_Validation_LastCommittedDate)

                                                        issueManager.updateIssue(user, linkedIssueOfPWO2, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                        issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                    }
                                                }else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                                    //To remove if update not needed
                                                    if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23 && linkedIssueOfPWO2.getStatusObject().getName() == "Not started" && RequestedDateTrigger) {
                                                        Int_RequestedDate = (Timestamp) (RequestedDateTrigger - 28)
                                                        Date Int_FirstCommittedDate = Int_RequestedDate
                                                        Date Int_LastCommittedDate = Int_FirstCommittedDate

                                                        linkedIssueOfPWO2.setCustomFieldValue(Int_RequestedDateField, Int_RequestedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Int_1stCommittedDateField, Int_FirstCommittedDate)
                                                        linkedIssueOfPWO2.setCustomFieldValue(Int_LastCommittedDateField, Int_LastCommittedDate)
                                                        issueManager.updateIssue(user, linkedIssueOfPWO2, EventDispatchOption.DO_NOT_DISPATCH, false);
                                                        issueIndexingService.reIndex(linkedIssueOfPWO2);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

