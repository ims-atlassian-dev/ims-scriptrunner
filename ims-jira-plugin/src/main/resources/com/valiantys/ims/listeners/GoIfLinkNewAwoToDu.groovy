package com.valiantys.ims.listeners
/**
 * Projects : AWO
 * Description : "Go if" link new AWO to DU
 * Events : Issue Updated
 * isDisabled : false
 */

// import

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
}*/

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans){
            if (itemsChanged.contains(changeIteamBean.getField())){
                isUpdated = true
            }
        }
    }

    return isUpdated
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog

MutableIssue issue = (MutableIssue)event.getIssue()

String ID_IT_AWO_DUDM_ATB = "10014"
String ID_IT_AWO_DUDM_ASD = "10015"
String ID_IT_PWO1 = "10005"

long ID_Project_AWO=10003
long ID_Project_DU=10002


long ID_CF_FirstSNAppli=10011
CustomField FirstSNAppliField=customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)

Issue DUissue
String FirstSnAppli="Calculation of status"


List<String> itemsChangedAtCreation = new ArrayList<String>()
itemsChangedAtCreation.add(FirstSNAppliField.getFieldName())
log.debug "issue = "+issue.getKey()

boolean isCreation = (boolean)workloadIsUpdated(changeLog, itemsChangedAtCreation)
log.debug "is creation : "+isCreation
if(isCreation && issue.getCustomFieldValue(FirstSNAppliField).toString()=="Copied from Go if"){//Creation
    if (issue.getIssueTypeId()==ID_IT_AWO_DUDM_ATB || issue.getIssueTypeId()==ID_IT_AWO_DUDM_ASD ){
        def LinkedIssuesOfCurrentAWO = issueLinkManager.getLinkCollection(issue,user).getAllIssues()
        for (linkedIssueOfCurrentAWO in LinkedIssuesOfCurrentAWO){
            //If linked issue is Cluster
            if (linkedIssueOfCurrentAWO.getProjectId() == ID_Project_AWO){
                log.debug "AWO linked = "+linkedIssueOfCurrentAWO.getKey()
                def LinkedIssuesOfLinkedAWO=issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO,user).getAllIssues()
                for (linkedIssueOfLinkedAWO in LinkedIssuesOfLinkedAWO){
                    if (linkedIssueOfLinkedAWO.getProjectId()== ID_Project_DU){
                        log.debug "DU linked : "+linkedIssueOfLinkedAWO.getKey()
                        def LinkedIssuesOfDU = issueLinkManager.getLinkCollection(linkedIssueOfLinkedAWO,user).getInwardIssues("Copies")
                        for (linkedIssueOfDU in LinkedIssuesOfDU){
                            log.debug "Outward link of DU = "+linkedIssueOfDU.getKey()
                            if (linkedIssueOfDU.getProjectId()== ID_Project_DU){
                                log.debug "DU copied and �to link : "+linkedIssueOfDU.getKey()
                                DUissue=linkedIssueOfDU
                            }
                        }
                    }
                }
            }
        }
    }
    else{
        def LinkedIssuesOfCurrentAWO = issueLinkManager.getLinkCollection(issue,user).getAllIssues()
        for (linkedIssueOfCurrentAWO in LinkedIssuesOfCurrentAWO){
            //If linked issue is Cluster
            if (linkedIssueOfCurrentAWO.getProjectId() == ID_Project_AWO){
                log.debug "AWO linked = "+linkedIssueOfCurrentAWO.getKey()
                def LinkedIssuesOfLinkedAWO=issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO,user).getAllIssues()
                for (linkedIssueOfLinkedAWO in LinkedIssuesOfLinkedAWO){
                    if (linkedIssueOfLinkedAWO.getProjectId()== ID_Project_DU){
                        log.debug "DU copied and to link : "+linkedIssueOfLinkedAWO.getKey()
                        DUissue=linkedIssueOfLinkedAWO
                    }
                }
            }
        }
    }
    if (DUissue){
        issueLinkManager.createIssueLink(issue.getId(),DUissue.getId(),10003, 1, user)
        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)

        MutableIssue DUIssueMutable = DUissue as MutableIssue
        DUIssueMutable.setCustomFieldValue(FirstSNAppliField,FirstSnAppli)
        issueManager.updateIssue(user, DUIssueMutable, EventDispatchOption.ISSUE_UPDATED, false)
        issueIndexingService.reIndex(DUIssueMutable)
    }


    log.debug "issue : "+issue.getKey()
    def LinkedIssuesOfCurrentAWO2 = issueLinkManager.getLinkCollection(issue,user).getAllIssues()
    for (linkedIssueOfCurrentAWO2 in LinkedIssuesOfCurrentAWO2){
        if (linkedIssueOfCurrentAWO2.getProjectId() == ID_Project_AWO){
            def LinkedIssuesOfLinkedAWO2 = issueLinkManager.getLinkCollection(linkedIssueOfCurrentAWO2,user).getAllIssues()
            for (linkedIssueOfLinkedAWO2 in LinkedIssuesOfLinkedAWO2){
                if (linkedIssueOfLinkedAWO2.getIssueTypeId()==ID_IT_PWO1){
                    issueLinkManager.createIssueLink(linkedIssueOfLinkedAWO2.getId(),issue.getId(),10301, 1, user)
                    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
                    issueIndexingService.reIndex(issue)
                }
            }
        }
    }

}
