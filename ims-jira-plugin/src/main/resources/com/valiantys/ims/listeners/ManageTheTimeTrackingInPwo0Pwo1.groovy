package com.valiantys.ims.listeners
/**
 * Projects : PWO
 * Description : Manage the time tracking in PWO-0 / PWO-1
 * Events : Work Logged On Issue
 * isDisabled : false
 */

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.worklog.Worklog
import com.atlassian.jira.issue.worklog.WorklogManager
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger
import com.atlassian.jira.issue.index.IssueIndexingService

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

String formatSecondesValueInHoursMinutes(Long secondes) {
    log.debug "Start formatSecondesValueInHoursMinutes - secondes : " + secondes
    if (secondes && secondes > 0) {
        String result

        int hoursExtracted = (int) (secondes / 3600)
        int totalMinutes = (int) (secondes / 60)
        int minutesExtracted = (int) totalMinutes.mod(60)

        result = hoursExtracted.toString() + "h " + minutesExtracted.toString() + "m"
        log.debug "End formatSecondesValueInHoursMinutes - result  : " + result
        return result
    } else {
        log.debug "End formatSecondesValueInHoursMinutes - minutes is null"
    }
}

Long formatHoursMinutesFormatInSecondes(String hoursMinutes) {
    log.debug "Start formatHoursMinutesFormatInSecondes - hoursMinutes : " + hoursMinutes
    if (!hoursMinutes.equals("null")) {
        Long result = 0L

        String[] hoursMinutesTable
        Long hourConvertedInMinutes = 0
        String hourNumberString
        Long minutesNumber = 0

        hoursMinutesTable = hoursMinutes.split("h")
        hourConvertedInMinutes = hoursMinutesTable[0].toString().toLong() * 60
        minutesNumber = hoursMinutesTable[1].toString().substring(0, hoursMinutesTable[1].toString().length() - 1).toLong()

        result = (hourConvertedInMinutes + minutesNumber) * 60

        log.debug "End formatHoursMinutesFormatInSecondes - result : " + result
        return result
    } else {
        log.debug "End formatHoursMinutesFormatInSecondes - hoursMinutes is null"
    }
}

String sumTimeSpentForDomain(MutableIssue issue, CustomField timeSpentForSelectedDomainCF, Long lastTimeSpent) {
    //get current time logged for domain
    String timeLogged = issue.getCustomFieldValue(timeSpentForSelectedDomainCF)

    Long totalTime = 0L
    if (timeLogged) {
        //Convert time in second
        Long currentTime = formatHoursMinutesFormatInSecondes(timeLogged)

        //Sum
        totalTime = currentTime + lastTimeSpent
    } else {
        totalTime = lastTimeSpent
    }
    return formatSecondesValueInHoursMinutes(totalTime)
}

//Manager
CustomFieldManager customFieldManager = ComponentAccessor.customFieldManager
WorklogManager worklogManager = ComponentAccessor.getWorklogManager()
IssueManager issueManager = ComponentAccessor.issueManager
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)

String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"

Long CF_DOMAIN = 10139
Long CF_TS_PARTS_DATA = 11012
Long CF_TS_MAINT_PLANNING = 11013
Long CF_TS_MAINT = 11014
Long CF_TS_IA = 11017
Long CF_TS_Auth = 11015
Long CF_TS_Form = 11016
Long CF_TS_Int = 11018

//CustomFields
CustomField domainCF = customFieldManager.getCustomFieldObject(CF_DOMAIN)
CustomField partsDataCF = customFieldManager.getCustomFieldObject(CF_TS_PARTS_DATA)
CustomField maintPlanningCF = customFieldManager.getCustomFieldObject(CF_TS_MAINT_PLANNING)
CustomField maintCF = customFieldManager.getCustomFieldObject(CF_TS_MAINT)
CustomField IACF = customFieldManager.getCustomFieldObject(CF_TS_IA)
CustomField AuthCF = customFieldManager.getCustomFieldObject(CF_TS_Auth)
CustomField FormCF = customFieldManager.getCustomFieldObject(CF_TS_Form)
CustomField IntCF = customFieldManager.getCustomFieldObject(CF_TS_Int)


String PARTS_DATA = "Parts Data"
String MAINT_PLANNING = "Maintenance Planning"
String MAINT = "Maintenance"

//Issue
MutableIssue issue = (MutableIssue) event.getIssue()

//User
ApplicationUser user = (ApplicationUser) event.user

//Change log
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog

//get domain selected during transition
String domainSelected = issue.getCustomFieldValue(domainCF).toString()

//Get Time Spent logged during transition
List<Worklog> worklogs = worklogManager.getByIssue(issue)
Long lastTimeSpent = worklogs.get(worklogs.size() - 1).getTimeSpent()

log.debug "lastTimeSpent " + lastTimeSpent

if (issue.getIssueTypeId() == ID_IT_PWO0) {
    if (PARTS_DATA.equals(domainSelected)) {
        String newTimeSpent = sumTimeSpentForDomain(issue, partsDataCF, lastTimeSpent)
        issue.setCustomFieldValue(partsDataCF, newTimeSpent)
    } else if (MAINT_PLANNING.equals(domainSelected)) {
        String newTimeSpent = sumTimeSpentForDomain(issue, maintPlanningCF, lastTimeSpent)
        issue.setCustomFieldValue(maintPlanningCF, newTimeSpent)
    } else if (MAINT.equals(domainSelected)) {
        String newTimeSpent = sumTimeSpentForDomain(issue, maintCF, lastTimeSpent)
        issue.setCustomFieldValue(maintCF, newTimeSpent)
    }

    //Clear domain
    issue.setCustomFieldValue(domainCF, null)

    //Update Issue
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
    issueIndexingService.reIndex(issue)

} else if (issue.getIssueTypeId() == ID_IT_PWO1) {
    log.debug "PWO-1"
    String newTimeSpent = sumTimeSpentForDomain(issue, IACF, lastTimeSpent)
    log.debug "newTimeSpent : " + newTimeSpent
    issue.setCustomFieldValue(IACF, newTimeSpent)
    //Update Issue
    issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
} else if (issue.getIssueTypeId() == ID_IT_PWO21) {
    log.debug "PWO-2.1"
    String newTimeSpent = sumTimeSpentForDomain(issue, AuthCF, lastTimeSpent)
    log.debug "newTimeSpent : " + newTimeSpent
    issue.setCustomFieldValue(AuthCF, newTimeSpent)
    //Update Issue
    issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
} else if (issue.getIssueTypeId() == ID_IT_PWO22) {
    log.debug "PWO-2.2"
    String newTimeSpent = sumTimeSpentForDomain(issue, FormCF, lastTimeSpent)
    log.debug "newTimeSpent : " + newTimeSpent
    issue.setCustomFieldValue(FormCF, newTimeSpent)
    //Update Issue
    issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
} else if (issue.getIssueTypeId() == ID_IT_PWO23) {
    log.debug "PWO-2.3"
    String newTimeSpent = sumTimeSpentForDomain(issue, IntCF, lastTimeSpent)
    log.debug "newTimeSpent : " + newTimeSpent
    issue.setCustomFieldValue(IntCF, newTimeSpent)
    //Update Issue
    issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
}
