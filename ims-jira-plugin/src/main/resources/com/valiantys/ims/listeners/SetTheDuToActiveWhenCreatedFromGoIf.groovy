package com.valiantys.ims.listeners
/**
 * Projects : DU
 * Description : Set the DU to Active when created from Go if
 * Events : Issue Updated
 * isDisabled : null
 */

// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
        boolean isUpdated = false;
            if (changeLog != null) {
                ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
                List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
                for (changeIteamBean in changeItemBeans){
                    if (itemsChanged.contains(changeIteamBean.getField())){
                        isUpdated = true;
                    }
                }
            }

        return isUpdated;
	}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog;

MutableIssue issue = (MutableIssue)event.getIssue();

String ID_IT_AWO_DUDM_ATB = "10014";
String ID_IT_AWO_DUDM_ASD = "10015";

long ID_Project_AWO=10003
long ID_Project_DU=10002


long ID_CF_FirstSNAppli=10011;
long ID_CF_TypeOfChange=10348
long ID_CF_GoIf=10358
CustomField FirstSNAppliField=customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)
CustomField TypeOfChangeField = customFieldManager.getCustomFieldObject(ID_CF_TypeOfChange)
CustomField GoIfField = customFieldManager.getCustomFieldObject(ID_CF_GoIf)

Issue DUissue

log.debug "issue : "+issue


List<String> itemsChangedAtCreation = new ArrayList<String>();
itemsChangedAtCreation.add(FirstSNAppliField.getFieldName());

boolean isCreation = (boolean)workloadIsUpdated(changeLog, itemsChangedAtCreation);
log.debug "is creation : "+isCreation
log.debug "First SN applied : "+issue.getCustomFieldValue(FirstSNAppliField)
if(isCreation && issue.getCustomFieldValue(FirstSNAppliField).toString()=="Calculation of status"){//Creation
    log.debug "creation of DU"
    def LinkedIssuesOfCurrentDU = issueLinkManager.getLinkCollection(issue,user).getAllIssues()
    for (linkedIssueOfCurrentDU in LinkedIssuesOfCurrentDU){
        if (linkedIssueOfCurrentDU.getProjectId() == ID_Project_AWO){
            log.debug "AWO found"
            if (linkedIssueOfCurrentDU.getCustomFieldValue(TypeOfChangeField).toString() != "Deleted" || linkedIssueOfCurrentDU.getCustomFieldValue(GoIfField).toString() == "Yes"){
                return true
            }
            else{
                return false
            }
        }
    }
}
else{
    return false
}