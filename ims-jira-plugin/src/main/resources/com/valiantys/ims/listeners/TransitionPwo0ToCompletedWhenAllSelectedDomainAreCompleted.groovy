package com.valiantys.ims.listeners
/**
 * Projects : PWO
 * Description : Transition PWO-0 to "Completed" when all selected domain are completed
 * Events : PWO-0 updated
 * isDisabled : null
 */

// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean

log.setLevel(Level.DEBUG)
/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
        boolean isUpdated = false;
            if (changeLog != null) {
                ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
                List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
                for (changeIteamBean in changeItemBeans){
                    if (itemsChanged.contains(changeIteamBean.getField())){
                        isUpdated = true;
                    }
                }
            }

        return isUpdated;
	}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog;

MutableIssue issue = (MutableIssue)event.getIssue();

//Constante
//Fields
long ID_CF_Domain=10034;
//long ID_CF_PartsDataStatus=10049;
long ID_CF_PartsDataValidated=10065;
//long ID_CF_MaintPlanningStatus=10071;
long ID_CF_MaintPlanningValidated=10084;
//long ID_CF_MaintStatus=10091;
long ID_CF_MaintValidated=10104;
long ID_CF_PartDataDN=10120;
long ID_CF_MaintPlanningDN=10121;
long ID_CF_MaintDN=10122;
long ID_CF_FirstSNAppli = 10011	

//CustomField
CustomField DomainField=customFieldManager.getCustomFieldObject(ID_CF_Domain)
CustomField PartsDataValidatedField=customFieldManager.getCustomFieldObject(ID_CF_PartsDataValidated)
CustomField MaintPlanningValidatedField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningValidated)
CustomField MaintValidatedField=customFieldManager.getCustomFieldObject(ID_CF_MaintValidated)
CustomField PartsDataDNField=customFieldManager.getCustomFieldObject(ID_CF_PartDataDN)
CustomField MaintPlanningDNField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningDN)
CustomField MaintDNField=customFieldManager.getCustomFieldObject(ID_CF_MaintDN)
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)

//Variables
List<String> DomainValues
String Result="OK"

//Business
if(issue.getCustomFieldValue(FirstSNAppliField)=="Update"){
  DomainValues = (List <String>) issue.getCustomFieldValue(DomainField)
    if (!DomainValues){
        Result="KO"
    }
    for (domainValue in DomainValues){
        if(domainValue.toString()=="Parts Data"){
            if (issue.getCustomFieldValue(PartsDataValidatedField).toString()=="Validated" && issue.getCustomFieldValue(PartsDataDNField) && Result=="OK"){
                Result = "OK"
            }else{
                Result = "KO"
            }
        }
        if(domainValue.toString()=="Maint Planning"){
            if (issue.getCustomFieldValue(MaintPlanningValidatedField).toString()=="Validated" && issue.getCustomFieldValue(MaintPlanningDNField) && Result=="OK"){
                Result = "OK"
            }else{
                Result = "KO"
            }
        }
        if(domainValue.toString()=="Maintenance"){
            if (issue.getCustomFieldValue(MaintValidatedField).toString()=="Validated" && issue.getCustomFieldValue(MaintDNField) && Result=="OK"){
                Result = "OK"
            }else{
                Result = "KO"
            }
        }
    }

    if (Result=="OK"){
        true
    }else{
        false
    }  
}
else{
    false
}
