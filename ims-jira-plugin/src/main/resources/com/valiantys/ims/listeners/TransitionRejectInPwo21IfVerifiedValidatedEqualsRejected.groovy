package com.valiantys.ims.listeners
/**
 * Projects : PWO
 * Description : Transition "Reject" in PWO-2.1 if Verified / Validated = Rejected
 * Events : PWO-2.1 updated
 * isDisabled : false
 */

// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged){
        boolean isUpdated = false;
            if (changeLog != null) {
                ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
                List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
                for (changeIteamBean in changeItemBeans){
                    if (itemsChanged.contains(changeIteamBean.getField())){
                        isUpdated = true;
                    }
                }
            }

        return isUpdated;
	}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog;

MutableIssue issue = (MutableIssue)event.getIssue();


//Constante
//Issue type
String ID_IT_PWO0 = "10004";
String ID_IT_PWO1 = "10005";
String ID_IT_PWO2 = "10006";
String ID_IT_PWO21 = "10007";
String ID_IT_PWO22 = "10008";
String ID_IT_PWO23 = "10009";
String ID_IT_Cluster="10001";
String ID_IT_Trigger="10000";

List<String> PWOIT = new ArrayList<String>();
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_Auth_Verified=10183;
long ID_CF_Auth_Validated=10189;


//CustomField
CustomField Auth_VerifiedField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verified)
CustomField Auth_ValidatedField = customFieldManager.getCustomFieldObject(ID_CF_Auth_Validated)

//Variables


//Business
if(issue.getIssueTypeId()==ID_IT_PWO21){
	List<String> itemsChangedAuthVerifiedValidated = new ArrayList<String>();
    itemsChangedAuthVerifiedValidated.add(Auth_ValidatedField.getFieldName());
        
	boolean isAuthVerifiedValidatedChanged =  (boolean)workloadIsUpdated(changeLog, itemsChangedAuthVerifiedValidated);
    if (isAuthVerifiedValidatedChanged){
        if (issue.getCustomFieldValue(Auth_ValidatedField).toString()=="Rejected"){
            return true
        }
        else{
            return false
        }
    }
}