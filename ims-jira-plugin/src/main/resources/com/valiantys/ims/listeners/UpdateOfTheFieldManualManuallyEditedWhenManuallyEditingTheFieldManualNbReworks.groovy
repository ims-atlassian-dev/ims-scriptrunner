package com.valiantys.ims.listeners
/**
 * Projects : PWO
 * Description : Update of the field "<manual> manually edited" when manually editing the field "<manual> nb reworks"
 * Events : PWO-0 updated, PWO-1 updated, PWO-2.1 updated, PWO-2.2 updated, PWO-2.3 updated
 * isDisabled : false
 */

// import


import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level

log.setLevel(Level.DEBUG)
log.debug "Edit of issue, update of the field Manually Edited"

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy"

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path)
}*/

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}


//Manager
IssueManager issueManager = ComponentAccessor.getIssueManager()
MutableIssue issue = (MutableIssue) event.getIssue()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
org.ofbiz.core.entity.GenericValue changeLog = event.changeLog


//Constantes
long ID_CF_IPC_ManuallyEdited = 10132
long ID_CF_MSM_ManuallyEdited = 10133
long ID_CF_Maint_ManuallyEdited = 10134
long ID_CF_IA_Validation_ManuallyEdited = 10171
long ID_CF_IA_ManuallyEdited = 10172
long ID_CF_Auth_Verified_ManuallyEdited = 10244
long ID_CF_Auth_ManuallyEdited = 10243
long ID_CF_Form_Verification_ManuallyEdited = 10323
long ID_CF_Form_ManuallyEdited = 10322
String ID_CF_PartsData_NbRework_String = 'Parts Data_Pre-IA Nb reworks'
String ID_CF_MaintPlanning_NbRework_String = 'Maint Planning_Pre-IA Nb reworks'
String ID_CF_Maint_NbRework_String = 'Maint_Pre-IA Nb reworks'
String ID_CF_IA_Validation_NbRework = 'IA_Validation_Nb reworks'
String ID_CF_IA_NbRework = 'IA_Nb reworks'
String ID_CF_Auth_Verified_NbRework = 'Auth_Verification_Nb reworks'
String ID_CF_Auth_NbRework = 'Auth_Nb reworks'
String ID_CF_Form_Verification_NbRework = 'Form_Verification_Nb reworks'
String ID_CF_Form_NbRework = 'Form_Nb reworks'
String Yes = "Yes"
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"

//CustomField
CustomField IPCManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_IPC_ManuallyEdited)
CustomField MSMManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_MSM_ManuallyEdited)
CustomField MaintManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_Maint_ManuallyEdited)
CustomField IA_Validation_ManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_ManuallyEdited)
CustomField IA_ManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_IA_ManuallyEdited)
CustomField Auth_Verified_ManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_Auth_Verified_ManuallyEdited)
CustomField Auth_ManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_Auth_ManuallyEdited)
CustomField Form_Verification_ManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_ManuallyEdited)
CustomField Form_ManuallyEdited = customFieldManager.getCustomFieldObject(ID_CF_Form_ManuallyEdited)

List<String> PartsDataNbReworkChanged = new ArrayList<String>()
PartsDataNbReworkChanged.add(ID_CF_PartsData_NbRework_String)

List<String> MaintPlanningNbReworkChanged = new ArrayList<String>()
MaintPlanningNbReworkChanged.add(ID_CF_MaintPlanning_NbRework_String)

List<String> MaintNbReworkChanged = new ArrayList<String>()
MaintNbReworkChanged.add(ID_CF_Maint_NbRework_String)

List<String> IA_Validated_NbRework_Changed = new ArrayList<String>()
IA_Validated_NbRework_Changed.add(ID_CF_IA_Validation_NbRework)

List<String> IA_NbRework_Changed = new ArrayList<String>()
IA_NbRework_Changed.add(ID_CF_IA_NbRework)

List<String> Auth_Verified_NbRework_Changed = new ArrayList<String>()
Auth_Verified_NbRework_Changed.add(ID_CF_Auth_Verified_NbRework)

List<String> Auth_NbRework_Changed = new ArrayList<String>()
Auth_NbRework_Changed.add(ID_CF_Auth_NbRework)

List<String> Form_Verification_NbRework_Changed = new ArrayList<String>()
Form_Verification_NbRework_Changed.add(ID_CF_Form_Verification_NbRework)

List<String> Form_NbRework_Changed = new ArrayList<String>()
Form_NbRework_Changed.add(ID_CF_Form_NbRework)

//Business
log.debug "issue : " + issue.getKey()

if (issue.getIssueTypeId() == ID_IT_PWO0) {
    //PWO-0
    boolean PartsDataUpdated = (boolean) workloadIsUpdated(changeLog, PartsDataNbReworkChanged)
    boolean MaintPlanningUpdated = (boolean) workloadIsUpdated(changeLog, MaintPlanningNbReworkChanged)
    boolean MaintUpdated = (boolean) workloadIsUpdated(changeLog, MaintNbReworkChanged)
    log.debug "Parts data manual " + PartsDataUpdated

    if (PartsDataUpdated) {
        issue.setCustomFieldValue(IPCManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
    if (MaintPlanningUpdated) {
        issue.setCustomFieldValue(MSMManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
    if (MaintUpdated) {
        issue.setCustomFieldValue(MaintManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
} else if (issue.getIssueTypeId() == ID_IT_PWO1) {
    //PWO-1
    boolean IAValidatedUpdated = (boolean) workloadIsUpdated(changeLog, IA_Validated_NbRework_Changed)
    boolean IAUpdated = (boolean) workloadIsUpdated(changeLog, IA_NbRework_Changed)

    if (IAValidatedUpdated) {
        issue.setCustomFieldValue(IA_Validation_ManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
    if (IAUpdated) {
        issue.setCustomFieldValue(IA_ManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
} else if (issue.getIssueTypeId() == ID_IT_PWO21) {
    //PWO-2.1
    boolean Auth_VerifiedUpdated = (boolean) workloadIsUpdated(changeLog, Auth_Verified_NbRework_Changed)
    boolean AuthUpdated = (boolean) workloadIsUpdated(changeLog, Auth_NbRework_Changed)

    if (Auth_VerifiedUpdated) {
        issue.setCustomFieldValue(Auth_Verified_ManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
    if (AuthUpdated) {
        issue.setCustomFieldValue(Auth_ManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
} else if (issue.getIssueTypeId() == ID_IT_PWO22) {
    //PWO-2.2
    boolean Form_VerificationUpdated = (boolean) workloadIsUpdated(changeLog, Form_Verification_NbRework_Changed)
    boolean FormUpdated = (boolean) workloadIsUpdated(changeLog, Form_NbRework_Changed)

    if (Form_VerificationUpdated) {
        issue.setCustomFieldValue(Form_Verification_ManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
    if (FormUpdated) {
        issue.setCustomFieldValue(Form_ManuallyEdited, Yes)

        issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
        issueIndexingService.reIndex(issue)
    }
}

