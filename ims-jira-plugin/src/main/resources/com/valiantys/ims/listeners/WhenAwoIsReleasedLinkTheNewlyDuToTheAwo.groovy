package com.valiantys.ims.listeners
/**
 * Projects : DU
 * Description : When AWO is released, link the newly DU to the AWO
 * Events : Issue Updated
 * isDisabled : null
 */

// import


import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Level
import org.apache.log4j.Logger

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */


boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"))
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans()
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true
            }
        }
    }

    return isUpdated
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()

org.ofbiz.core.entity.GenericValue changeLog = event.changeLog

MutableIssue issue = (MutableIssue) event.getIssue()

//Constante
//Issue type
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"


List<String> PWOIT = new ArrayList<String>()
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_AWO_Released = 11201
long ID_CF_FirstSNAppli = 10011


//CustomField
CustomField AWOReleasedField = customFieldManager.getCustomFieldObject(ID_CF_AWO_Released)
CustomField FirstSNAppliField = customFieldManager.getCustomFieldObject(ID_CF_FirstSNAppli)

//Variables
Issue issuesAWOtoLink
String issueAWOId
String FirstSnAppli = "Calculation of status"

//Business
log.debug "issue : " + issue.getKey()
List<String> itemsChangedAtCreation = new ArrayList<String>()
itemsChangedAtCreation.add(FirstSNAppliField.getFieldName())

boolean isCreation = (boolean) workloadIsUpdated(changeLog, itemsChangedAtCreation)
log.debug "is creation : " + isCreation
if (isCreation && issue.getCustomFieldValue(FirstSNAppliField).toString() == "New issue") {//Creation
    issueAWOId = issue.getCustomFieldValue(AWOReleasedField)
    if (issueAWOId) {
        log.debug "content of field AWO release : " + issueAWOId
        issuesAWOtoLink = issueManager.getIssueObject(Long.parseLong(issueAWOId))
        log.debug "issue AWO to link : " + issuesAWOtoLink
        //if (!issuesAWOtoLink.getCustomFieldValue(GoIfField)){
        issueLinkManager.createIssueLink(issuesAWOtoLink.getId(), issue.getId(), 10003, 1, user)
        issue.setCustomFieldValue(FirstSNAppliField, FirstSnAppli)
        issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
        issueIndexingService.reIndex(issue)
    }
}
