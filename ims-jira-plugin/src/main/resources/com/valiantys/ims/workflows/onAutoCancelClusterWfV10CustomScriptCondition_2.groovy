package com.valiantys.ims.workflows
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl

IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager()
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()

String ID_IT_Cluster = "10001"
String ID_IT_Trigger = "10000"
String ID_IT_PWO0 = "10004"
String ID_IT_PWO1 = "10005"
String ID_IT_PWO2 = "10006"
String ID_IT_PWO21 = "10007"
String ID_IT_PWO22 = "10008"
String ID_IT_PWO23 = "10009"

String Cancelled="Cancelled"
String Result="OK"


/*def LinkedIssuesOfTrigger = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
for (linkedIssueOfTrigger in LinkedIssuesOfTrigger) {
    //If cluster
    if (linkedIssueOfTrigger.getIssueTypeId() == ID_IT_Cluster) {*/
        //Get linked issues of Cluster
        def LinkedIssuesOfCluster = issueLinkManager.getLinkCollection(issue, user).getAllIssues()
        for (linkedIssueOfCluster in LinkedIssuesOfCluster) {
            //If PWO-0
            if (linkedIssueOfCluster.getIssueTypeId() == ID_IT_PWO0) {
                log.debug "PWO-0 : "+linkedIssueOfCluster.getKey()
                log.debug "PWO-0 status : "+linkedIssueOfCluster.getStatus().getSimpleStatus().getName()
                //Get status
                if (linkedIssueOfCluster.getStatus().getSimpleStatus().getName() == Cancelled && Result == "OK"){
                    Result = "OK"
                }
                else{
                    Result = "KO"
                }
                //Get linked issues of PWO-0
                def LinkedIssuesOfPWO0 = issueLinkManager.getLinkCollection(linkedIssueOfCluster, user).getAllIssues()
        		for (linkedIssueOfPWO0 in LinkedIssuesOfPWO0) {
                    //if PWO-1
                    if (linkedIssueOfPWO0.getIssueTypeId() == ID_IT_PWO1) {
                        log.debug "PWO-1 : "+linkedIssueOfPWO0.getKey()
                		log.debug "PWO-1 status : "+linkedIssueOfPWO0.getStatus().getSimpleStatus().getName()
                        //Get status
                        if (linkedIssueOfPWO0.getStatus().getSimpleStatus().getName() == Cancelled && Result == "OK"){
                            Result = "OK"
                        }
                        else{
                            Result = "KO"
                        }
                        //Get linked issues of PWO-1
                        def LinkedIssuesOfPWO1 = issueLinkManager.getLinkCollection(linkedIssueOfPWO0, user).getAllIssues()
                        for (linkedIssueOfPWO1 in LinkedIssuesOfPWO1) {
                            //If PWO-2
                            if (linkedIssueOfPWO1.getIssueTypeId() == ID_IT_PWO2) {
                                //Get sub-task
                                def LinkedIssuesOfPWO2 = linkedIssueOfPWO1.getSubTaskObjects()
                                for (linkedIssueOfPWO2 in LinkedIssuesOfPWO2) {
                                    if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO21) {
                                        log.debug "PWO-2.1 : "+linkedIssueOfPWO2.getKey()
                						log.debug "PWO-2.1 status : "+linkedIssueOfPWO2.getStatus().getSimpleStatus().getName()
                                        //Get status
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == Cancelled && Result == "OK"){
                                            Result = "OK"
                                        }
                                        else{
                                            Result = "KO"
                                        }
                                    }
                                    else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO22) {
                                        log.debug "PWO-2.2 : "+linkedIssueOfPWO2.getKey()
                						log.debug "PWO-2.2 status : "+linkedIssueOfPWO2.getStatus().getSimpleStatus().getName()
                                        //Get status
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == Cancelled && Result == "OK"){
                                            Result = "OK"
                                        }
                                        else{
                                            Result = "KO"
                                        }
                                    }
                                    else if (linkedIssueOfPWO2.getIssueTypeId() == ID_IT_PWO23) {
                                        log.debug "PWO-2.3 : "+linkedIssueOfPWO2.getKey()
                						log.debug "PWO-2.3 status : "+linkedIssueOfPWO2.getStatus().getSimpleStatus().getName()
                                        //Get status
                                        if (linkedIssueOfPWO2.getStatus().getSimpleStatus().getName() == Cancelled && Result == "OK"){
                                            Result = "OK"
                                        }
                                        else{
                                            Result = "KO"
                                        }
                                    }
                                }
                            }
                        }
                    }                 
                }
            }
        }
    //}
//}

if (Result=="OK"){
	passesCondition = true
}else{
	passesCondition = false
} 