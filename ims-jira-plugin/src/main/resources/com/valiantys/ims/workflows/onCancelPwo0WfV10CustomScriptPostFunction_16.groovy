package com.airbus.ims.workflow.runner
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.link.IssueLinkManager
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp

//String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

//def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
//    loadScriptByName(path);
//} 

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager);
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();

MutableIssue issue = issue;

//Constantes
//Field of PWO-0
long ID_CF_PartsData_Status=10049
long ID_CF_MaintPlanning_Status=10071
long ID_CF_Maint_Status=10091

//CustomField
CustomField PartsData_StatusField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Status)
CustomField MaintPlanning_StatusField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Status)
CustomField Maint_StatusField=customFieldManager.getCustomFieldObject(ID_CF_Maint_Status)

//Variables

//Business


def fieldConfigPartsDataStatus = PartsData_StatusField.getRelevantConfig(issue)
def valuePartsDataStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigPartsDataStatus)?.find { it.toString() == "Cancelled" }
def fieldConfigMaintPlanningStatus = MaintPlanning_StatusField.getRelevantConfig(issue)
def valueMaintPlanningStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintPlanningStatus)?.find { it.toString() == "Cancelled" }
def fieldConfigMaintStatus = Maint_StatusField.getRelevantConfig(issue)
def valueMaintStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintStatus)?.find { it.toString() == "Cancelled" }

issue.setCustomFieldValue(PartsData_StatusField,valuePartsDataStatus)
issue.setCustomFieldValue(MaintPlanning_StatusField,valueMaintPlanningStatus)
issue.setCustomFieldValue(Maint_StatusField,valueMaintStatus)

issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);