package com.valiantys.ims.workflows
// import

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.query.Query
import org.apache.log4j.Level
import org.apache.log4j.Logger

Logger log = Logger.getLogger("com.valiantys.script.listener")
log.setLevel(Level.DEBUG)
log.warn("PWO created UpdateTitleCopyToSummary")

log.debug "Creation/Edit of a PWO, update of the reference"


//Manager
IssueManager issueManager = ComponentAccessor.getIssueManager()
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext()
ApplicationUser user = authenticationContext.getLoggedInUser()
JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class)
SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class)


//Constantes
//old long ID_CF_MOA = 10135
long ID_CF_Program = 10347
long ID_CF_Reference = 10001
long ID_CF_IncrementalID = 10136


//CustomField
CustomField ProgramField = customFieldManager.getCustomFieldObject(ID_CF_Program)
CustomField ReferenceField = customFieldManager.getCustomFieldObject(ID_CF_Reference)
CustomField IncrementalIDField = customFieldManager.getCustomFieldObject(ID_CF_IncrementalID)

//Business

log.debug "issue : " + issue.getKey()

String ValueToReturn
String ProgramValueBrut
//String[] ProgramValueList
String ProgramValue
String YearValue
String PWO0 = "PWO0"
String PWO1 = "PWO1"
String PWO2 = "PWO2"
String PWO21 = "PWO21"
String PWO22 = "PWO22"
String PWO23 = "PWO23"
String AWO = "AWO"
String JQL_QUERY = "project = \"%s\" and issuetype in  (\"%s\") AND created > startOfYear() and \"Program\"=\"%s [Program_%s]\" and \"Incremental ID\" is not empty ORDER BY \"Incremental ID\" DESC"
String IT
String CompleteCompteur
String query
double compteur

List<Issue> issuesFound = []
Issue firstIssue


ProgramValueBrut = issue.getCustomFieldValue(ProgramField)
ProgramValue = ProgramValueBrut.replaceAll("Program_", "").replaceAll("<content>", "").replaceAll("<value>", "").replaceAll("</value>", ",").replaceAll("</content>", "").replaceAll(",", "").replaceAll("\n", "").replaceAll("  ", "")


String DateCreated = issue.getCreated()
YearValue = DateCreated.replaceAll(/-.*/, '')

log.debug "issue type : " + issue.getIssueTypeId()

IT = PWO21

query = String.format(JQL_QUERY, issue.getProjectObject().getName(), issue.getIssueType().getName(), ProgramValue, ProgramValue, issue.id)
log.debug "Query : " + query
Query JQLquery = jqlQueryParser.parseQuery(query)
SearchResults results = searchProvider.search(JQLquery, user, PagerFilter.getUnlimitedFilter())
issuesFound = results.getIssues()
log.debug "issues found : " + issuesFound
log.debug "issues found size : " + issuesFound.size()
if (issuesFound.size() > 0) {
    if (issuesFound[0].getKey() != issue.getKey()) {
        firstIssue = issuesFound[0]
    } else if (issuesFound[1]) {
        firstIssue = issuesFound[1]
    } else {
        compteur = 1
    }
    log.debug "issue : " + firstIssue
    if (!compteur && firstIssue.getCustomFieldValue(IncrementalIDField)) {
        compteur = firstIssue.getCustomFieldValue(IncrementalIDField).toString().toDouble().round() + 1
    }
    log.debug "compteur : " + compteur
} else {
    compteur = 1
}

if (compteur < 10) {
    CompleteCompteur = "0000"
} else if (compteur < 100) {
    CompleteCompteur = "000"
} else if (compteur < 1000) {
    CompleteCompteur = "00"
} else if (compteur < 10000) {
    CompleteCompteur = "0"
} else {
    CompleteCompteur = ""
}

ValueToReturn = ProgramValue + "_" + YearValue + "_" + IT + "_" + CompleteCompteur + compteur.round()
log.debug "Value : " + ValueToReturn


issue.setCustomFieldValue(ReferenceField, ValueToReturn)
issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
issueIndexingService.reIndex(issue)

issue.setCustomFieldValue(IncrementalIDField, compteur)
issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
issueIndexingService.reIndex(issue)

issue.setSummary(ValueToReturn)
issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
issueIndexingService.reIndex(issue)

