package com.valiantys.ims.workflows
// import
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.query.Query
import com.atlassian.jira.issue.search.SearchResults
import java.util.List
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter

//Manager
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class)
SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class)

String JQL_QUERY = "issuetype = \"Trigger\" and summary ~ \"%s\" and Program = \"%s [Program_%s]\" and \"Type of H/C\" = \"%s\" ";
String query = "";

//Constantes
//old long ID_CF_MOA = 10135;
long ID_CF_Program = 10347
long ID_CF_TypeOfHC = 10701


//CustomField
CustomField ProgramField = customFieldManager.getCustomFieldObject(ID_CF_Program);
CustomField TypeOfHCField = customFieldManager.getCustomFieldObject(ID_CF_TypeOfHC);

String CurrentSummary = issue.getSummary()
String CurrentProgramBrut = issue.getCustomFieldValue(ProgramField)
String CurrentProgram = CurrentProgramBrut.replaceAll("Program_","").replaceAll("<content>","").replaceAll("<value>","").replaceAll("</value>",",").replaceAll("</content>","").replaceAll(",","").replaceAll("\n","").replaceAll("  ","")
String CurrentTypeOfHC = issue.getCustomFieldValue(TypeOfHCField)

List<Issue> issuesFound = [];

query = String.format(JQL_QUERY,CurrentSummary,CurrentProgram, CurrentProgram,CurrentTypeOfHC, issue.id);
log.debug "Query : " + query;
Query JQLquery = jqlQueryParser.parseQuery(query)
SearchResults results = searchProvider.search(JQLquery, user, PagerFilter.getUnlimitedFilter())
issuesFound = results.getIssues();
log.debug "issues found : "+issuesFound
if (issuesFound){
    return false
}
else{
    return true
}