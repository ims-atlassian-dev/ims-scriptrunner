package com.valiantys.ims.workflows
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean
import org.joda.time.LocalDate

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List itemsChanged){
    boolean isUpdated = false;
        if (changeLog != null) {
            ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
            List changeItemBeans = change.getChangeItemBeans();
            for (changeIteamBean in changeItemBeans){
                if (itemsChanged.contains(changeIteamBean.getField())){
                    isUpdated = true;
                }
            }
        }

    return isUpdated;
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();


//Constante
//Issue type
String ID_IT_PWO0 = "10004";
String ID_IT_PWO1 = "10005";
String ID_IT_PWO2 = "10006";
String ID_IT_PWO21 = "10007";
String ID_IT_PWO22 = "10008";
String ID_IT_PWO23 = "10009";
String ID_IT_Cluster="10001";
String ID_IT_Trigger="10000";

List PWOIT = new ArrayList();
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_Int_1stDeliveryDate=10329;
long ID_CF_Int_LastDeliveryDate=10330;
long ID_CF_TechnicallyClosed=10054;
long ID_CF_TechnicalClosureDate=10040;

//CustomField
CustomField Int_1stDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_Int_1stDeliveryDate)
CustomField Int_LastDeliveryDateField=customFieldManager.getCustomFieldObject(ID_CF_Int_LastDeliveryDate)
CustomField TechnicallyClosedField = customFieldManager.getCustomFieldObject(ID_CF_TechnicallyClosed)
CustomField TechnicalClosureDateField = customFieldManager.getCustomFieldObject(ID_CF_TechnicalClosureDate)

//Variables
//def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate=new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
Date Int_1stDeliveryDate
Date Int_LastDeliveryDate
Date TechnicalClosureDate
String TechnicallyClosed="Yes"


//Business
//Calculate date on PWO-2.1
if (!issue.getCustomFieldValue(Int_1stDeliveryDateField)){
    Int_1stDeliveryDate=TodaysDate
   
    issue.setCustomFieldValue(Int_1stDeliveryDateField,Int_1stDeliveryDate)
}

Int_LastDeliveryDate=TodaysDate
TechnicalClosureDate=TodaysDate

issue.setCustomFieldValue(Int_LastDeliveryDateField,Int_LastDeliveryDate)

def fieldConfigTechnicallyClosed = TechnicallyClosedField.getRelevantConfig(issue)
def valueTechnicallyClosed = ComponentAccessor.optionsManager.getOptions(fieldConfigTechnicallyClosed)?.find { it.toString() == TechnicallyClosed }
issue.setCustomFieldValue(TechnicallyClosedField,valueTechnicallyClosed)
issue.setCustomFieldValue(TechnicalClosureDateField,TechnicalClosureDate)

issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);