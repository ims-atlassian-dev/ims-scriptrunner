package com.valiantys.ims.workflows
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;

//Manager
def issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
passesCondition=true

if (issueLinkManager.getInwardLinks(issue.id)){
    issueLinkManager.getInwardLinks(issue.id).each { issueLink ->
        Issue sourceIssue=issueLink.getSourceObject()
        String issueType = sourceIssue.getIssueTypeId().toString()
        if (issueType == "10001") { 
            passesCondition = false
        }
    }
}