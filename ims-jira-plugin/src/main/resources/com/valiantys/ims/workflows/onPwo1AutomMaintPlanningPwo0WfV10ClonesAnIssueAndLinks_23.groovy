package com.airbus.ims.workflow.runner
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.customfields.manager.OptionsManager
 
def pluginAccessor = ComponentAccessor.getPluginAccessor();
def plugin = pluginAccessor.getPlugin("com.valiantys.jira.plugins.SQLFeed");
def serviceClass = plugin.getClassLoader().loadClass("com.valiantys.nfeed.api.IFieldValueService");
def fieldValueService = ComponentAccessor.getOSGiComponentInstanceOfType(serviceClass);

def optionsManager = ComponentAccessor.getComponent(OptionsManager)
def customFieldManager = ComponentAccessor.getCustomFieldManager()

def domainCF = customFieldManager.getCustomFieldObjects(sourceIssue).find {it.name == 'Domain'}
def firstSNApplicabilityCF = customFieldManager.getCustomFieldObjects(sourceIssue).find {it.name == 'First S/N applicability'}

def fieldConfig = domainCF.getRelevantConfig(sourceIssue)
def option = optionsManager.getOptions(fieldConfig).getOptionForValue("Maintenance Planning", null)

issue.setCustomFieldValue(domainCF, option) 
issue.setReporter(currentUser)
issue.setCustomFieldValue(firstSNApplicabilityCF, "To be configured")

//Collection<String> sourceProgramCFValue = (Collection<String>) fieldValueService.getFieldValues(sourceIssue.getKey(), "customfield_10347");
//fieldValueService.setFieldValue(issue.getKey(), "customfield_10347", sourceProgramCFValue);