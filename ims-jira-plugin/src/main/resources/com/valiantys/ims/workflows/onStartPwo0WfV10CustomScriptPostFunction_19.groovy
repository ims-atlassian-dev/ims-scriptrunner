package com.airbus.ims.workflow.runner
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import org.joda.time.LocalDate
import java.sql.Timestamp

//Manager
def issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
def changeItems = ComponentAccessor.changeHistoryManager.getAllChangeItems(issue)
def changeHolder = new DefaultIssueChangeHolder();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();


//Constante
long ID_CF_StartDate = 10009

//CustomField
CustomField StartDate = customFieldManager.getCustomFieldObject(ID_CF_StartDate);

Issue ClusterIssue;
Issue TriggerIssue;
//def DateDuJour = new java.sql.Timestamp(new Date().getTime())
Date TodayDate=new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
//log.debug "Date du jour : "+DateDuJour

log.debug "issue en cours : "+issue.getKey()

if (issueLinkManager.getOutwardLinks(issue.id)){
    issueLinkManager.getOutwardLinks(issue.id).each { issueLink ->
        Issue sourceIssue=issueLink.getDestinationObject()
        log.debug "Linked issue of PWO-0 : "+sourceIssue.getKey()
        String issueType = sourceIssue.getIssueTypeId().toString()
        if (issueType == "10001") { 
            ClusterIssue = sourceIssue
            if (issueLinkManager.getOutwardLinks(ClusterIssue.id)){
            issueLinkManager.getOutwardLinks(ClusterIssue.id).each { issueLink2 ->
                Issue sourceIssue2=issueLink2.getDestinationObject()
                log.debug "Linked issue of Cluster : "+sourceIssue2.getKey()
                String issueType2 = sourceIssue2.getIssueTypeId().toString()
                if (issueType2 == "10000") { 
                    log.debug "Trigger found : "+sourceIssue2.getKey()
                    TriggerIssue = issueManager.getIssueObject(sourceIssue2.id)
                    if (TriggerIssue.getStatusId()!="10004"){
                        TriggerIssue.setCustomFieldValue(StartDate,TodaysDate)

                        issueManager.updateIssue(user, TriggerIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(TriggerIssue);
                    }
                }
            }
        }
        }
    }
}
else if (issueLinkManager.getInwardLinks(issue.id)){
    issueLinkManager.getInwardLinks(issue.id).each { issueLink ->
        Issue sourceIssue=issueLink.getSourceObject()
        log.debug "Linked issue of PWO-0 : "+sourceIssue.getKey()
        String issueType = sourceIssue.getIssueTypeId().toString()
        if (issueType == "10001") { 
            ClusterIssue = sourceIssue
            if (issueLinkManager.getOutwardLinks(ClusterIssue.id)){
            issueLinkManager.getOutwardLinks(ClusterIssue.id).each { issueLink2 ->
                Issue sourceIssue2=issueLink2.getDestinationObject()
                log.debug "Linked issue of Cluster : "+sourceIssue2.getKey()
                String issueType2 = sourceIssue2.getIssueTypeId().toString()
                if (issueType2 == "10000") { 
                    log.debug "Trigger found : "+sourceIssue2.getKey()
                    TriggerIssue = issueManager.getIssueObject(sourceIssue2.id)
                    if (TriggerIssue.getStatusId()!="10004"){
                        TriggerIssue.setCustomFieldValue(StartDate,TodaysDate)

                        issueManager.updateIssue(user, TriggerIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
                        issueIndexingService.reIndex(TriggerIssue);
                    }
                }
            }
        }
        }
    }
}