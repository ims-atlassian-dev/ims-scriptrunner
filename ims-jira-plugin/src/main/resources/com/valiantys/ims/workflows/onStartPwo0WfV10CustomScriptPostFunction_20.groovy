package com.airbus.ims.workflow.runner
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.link.IssueLinkManager
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import org.joda.time.LocalDate

/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager);
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();

MutableIssue issue = issue;

//Constantes
//Field of PWO-0
long ID_CF_RequestedDate=10010;
long ID_CF_PreIA_PartsData_RequestedDate=10058
long ID_CF_PreIA_PartsData_Validation_RequestedDate=10066
long ID_CF_PreIA_MaintPlanning_RequestedDate=10077
long ID_CF_PreIA_MaintPlanning_Validation_RequestedDate=10085
long ID_CF_PreIA_Maint_RequestedDate=10097
long ID_CF_PreIA_Maint_Validation_RequestedDate=10105
long ID_CF_PartsData_Status=10049
long ID_CF_MaintPlanning_Status=10071
long ID_CF_Maint_Status=10091
long ID_CF_PartsData_1stCommittedDate=10060
long ID_CF_MaintPlanning_1stCommittedDate=10079
long IF_CF_Maint_1stCommittedDate=10099
long ID_CF_PartsData_LastCommittedDate=10062
long ID_CF_MaintPlanning_LastCommittedDate=10088
long IF_CF_Maint_LastCommittedDate=10101
long ID_CF_PartsData_Validation_1stCommittedDate=10067
long ID_CF_MaintPlanningValidation_1stCommittedDate=10086
long IF_CF_MaintValidation_1stCommittedDate=10106
long ID_CF_PartsDataValidation_LastCommittedDate=10090
long ID_CF_MaintPlanningValidation_LastCommittedDate=10081
long IF_CF_MaintValidation_LastCommittedDate=10108;
long ID_CF_Domain=10034

//CustomField
CustomField PreIA_PartsData_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_RequestedDate)
CustomField PreIA_PartsData_Validation_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_PreIA_PartsData_Validation_RequestedDate)
CustomField PreIA_MaintPlanning_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_RequestedDate)
CustomField PreIA_MaintPlanning_Validation_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_PreIA_MaintPlanning_Validation_RequestedDate)
CustomField PreIA_Maint_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_RequestedDate)
CustomField PreIA_Maint_Validation_RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_PreIA_Maint_Validation_RequestedDate)
CustomField PartsData_StatusField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Status)
CustomField MaintPlanning_StatusField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_Status)
CustomField Maint_StatusField=customFieldManager.getCustomFieldObject(ID_CF_Maint_Status)
CustomField PartsData_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_1stCommittedDate)
CustomField MaintPlanning_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_1stCommittedDate)
CustomField Maint_1stCommittedDateField=customFieldManager.getCustomFieldObject(IF_CF_Maint_1stCommittedDate)
CustomField PartsData_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_LastCommittedDate)
CustomField MaintPlanning_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanning_LastCommittedDate)
CustomField Maint_LastCommittedDateField=customFieldManager.getCustomFieldObject(IF_CF_Maint_LastCommittedDate)
CustomField PartsData_Validation_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsData_Validation_1stCommittedDate)
CustomField MaintPlanning_Validation_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningValidation_1stCommittedDate)
CustomField Maint_Validation_1stCommittedDateField=customFieldManager.getCustomFieldObject(IF_CF_MaintValidation_1stCommittedDate)
CustomField PartsData_Validation_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_PartsDataValidation_LastCommittedDate)
CustomField MaintPlanning_Validation_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_MaintPlanningValidation_LastCommittedDate)
CustomField Maint_Validation_LastCommittedDateField=customFieldManager.getCustomFieldObject(IF_CF_MaintValidation_LastCommittedDate)
CustomField RequestedDateField=customFieldManager.getCustomFieldObject(ID_CF_RequestedDate)
CustomField DomainField=customFieldManager.getCustomFieldObject(ID_CF_Domain)

//Variables
//def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate=new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
log.debug "Todays Date : "+TodaysDate
Date PreIA_PartsData_RequestedDate
Date PreIA_MaintPlanning_RequestedDate
Date PreIA_Maint_RequestedDate
Date PreIA_PartsData_Validation_RequestedDate
Date PreIA_MaintPlanning_Validation_RequestedDate
Date PreIA_Maint_Validation_RequestedDate
Date PartsData_1stCommittedDate
Date Maintplanning_1stCommittedDate
Date Maint_1stCommittedDate
Date PartsData_LastCommittedDate
Date Maintplanning_LastCommittedDate
Date Maint_LastCommittedDate
Date PartsData_Validation_1stCommittedDate
Date Maintplanning_Validation_1stCommittedDate
Date Maint_Validation_1stCommittedDate
Date PartsData_Validation_LastCommittedDate
Date Maintplanning_Validation_LastCommittedDate
Date Maint_Validation_LastCommittedDate

//Business

PreIA_PartsData_RequestedDate=(Timestamp) (TodaysDate+7)
PreIA_MaintPlanning_RequestedDate=(Timestamp) (TodaysDate+7)
PreIA_Maint_RequestedDate=(Timestamp) (TodaysDate+7)
log.debug "PreIA_Maint_RequestedDate : "+PreIA_Maint_RequestedDate

PreIA_PartsData_Validation_RequestedDate=(Timestamp) (TodaysDate+14)
PreIA_MaintPlanning_Validation_RequestedDate=(Timestamp) (TodaysDate+14)
PreIA_Maint_Validation_RequestedDate=(Timestamp) (TodaysDate+14)

def fieldConfigPartsDataStatus = PartsData_StatusField.getRelevantConfig(issue)
def fieldConfigMaintPlanningStatus = MaintPlanning_StatusField.getRelevantConfig(issue)
def fieldConfigMaintStatus = Maint_StatusField.getRelevantConfig(issue)

String ResultsPartsData 
String ResultsMaintPlanning 
String ResultsMaint


DomainValues = issue.getCustomFieldValue(DomainField)
log.debug "Domains values = "+DomainValues
for (domainValue in DomainValues){
    if(domainValue.toString()=="Parts Data"){
		ResultsPartsData="In progress"
    }
    else if (ResultsPartsData!="In progress"){
		ResultsPartsData = "Cancelled"
    }
    
    if(domainValue.toString()=="Maint Planning"){
		ResultsMaintPlanning = "In progress"
    }
    else if (ResultsMaintPlanning != "In progress"){
		ResultsMaintPlanning = "Cancelled" 
    }
    
    if(domainValue.toString()=="Maintenance"){
        ResultsMaint = "In progress"
    }
    else if (ResultsMaint != "In progress"){
        ResultsMaint = "Cancelled"
    }
}

valuePartsDataStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigPartsDataStatus)?.find { it.toString() == ResultsPartsData}
valueMaintPlanningStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintPlanningStatus)?.find { it.toString() == ResultsMaintPlanning }
valueMaintStatus = ComponentAccessor.optionsManager.getOptions(fieldConfigMaintStatus)?.find { it.toString() == ResultsMaint }

issue.setCustomFieldValue(PartsData_StatusField,valuePartsDataStatus)
issue.setCustomFieldValue(MaintPlanning_StatusField,valueMaintPlanningStatus)
issue.setCustomFieldValue(Maint_StatusField,valueMaintStatus)


PartsData_1stCommittedDate=(Timestamp)PreIA_PartsData_RequestedDate
Maintplanning_1stCommittedDate=(Timestamp)PreIA_MaintPlanning_RequestedDate
log.debug "maint planning 1st committed : "+Maintplanning_1stCommittedDate
Maint_1stCommittedDate=(Timestamp)PreIA_Maint_RequestedDate

PartsData_LastCommittedDate=(Timestamp)PartsData_1stCommittedDate
MaintPlanning_LastCommittedDate=(Timestamp)Maintplanning_1stCommittedDate
log.debug "maint planning last committed : "+MaintPlanning_LastCommittedDate
Maint_LastCommittedDate=(Timestamp)Maint_1stCommittedDate

PartsData_Validation_1stCommittedDate=(Timestamp)PreIA_PartsData_Validation_RequestedDate
Maintplanning_Validation_1stCommittedDate=(Timestamp)PreIA_MaintPlanning_Validation_RequestedDate
Maint_Validation_1stCommittedDate=(Timestamp)PreIA_Maint_Validation_RequestedDate

PartsData_Validation_LastCommittedDate=(Timestamp)PartsData_Validation_1stCommittedDate
MaintPlanning_Validation_LastCommittedDate=(Timestamp)Maintplanning_Validation_1stCommittedDate
Maint_Validation_LastCommittedDate=(Timestamp)Maint_Validation_1stCommittedDate

issue.setCustomFieldValue(PreIA_PartsData_RequestedDateField,PreIA_PartsData_RequestedDate)
issue.setCustomFieldValue(PreIA_MaintPlanning_RequestedDateField,PreIA_MaintPlanning_RequestedDate)
issue.setCustomFieldValue(PreIA_Maint_RequestedDateField,PreIA_Maint_RequestedDate)

issue.setCustomFieldValue(PreIA_PartsData_Validation_RequestedDateField,PreIA_PartsData_Validation_RequestedDate)
issue.setCustomFieldValue(PreIA_MaintPlanning_Validation_RequestedDateField,PreIA_MaintPlanning_Validation_RequestedDate)
issue.setCustomFieldValue(PreIA_Maint_Validation_RequestedDateField,PreIA_Maint_Validation_RequestedDate)

issue.setCustomFieldValue(PartsData_1stCommittedDateField,PartsData_1stCommittedDate)
issue.setCustomFieldValue(MaintPlanning_1stCommittedDateField,Maintplanning_1stCommittedDate)
issue.setCustomFieldValue(Maint_1stCommittedDateField,Maint_1stCommittedDate)

issue.setCustomFieldValue(PartsData_LastCommittedDateField,PartsData_LastCommittedDate)
log.debug "maint planning last committed avant setting: "+MaintPlanning_LastCommittedDate
issue.setCustomFieldValue(MaintPlanning_LastCommittedDateField,MaintPlanning_LastCommittedDate)
issue.setCustomFieldValue(Maint_LastCommittedDateField,Maint_LastCommittedDate)

issue.setCustomFieldValue(PartsData_Validation_1stCommittedDateField,PartsData_Validation_1stCommittedDate)
issue.setCustomFieldValue(MaintPlanning_Validation_1stCommittedDateField,Maintplanning_Validation_1stCommittedDate)
issue.setCustomFieldValue(Maint_Validation_1stCommittedDateField,Maint_Validation_1stCommittedDate)

issue.setCustomFieldValue(PartsData_Validation_LastCommittedDateField,PartsData_Validation_LastCommittedDate)
issue.setCustomFieldValue(MaintPlanning_Validation_LastCommittedDateField,Maintplanning_Validation_1stCommittedDate)
issue.setCustomFieldValue(Maint_Validation_LastCommittedDateField,Maint_Validation_LastCommittedDate)

issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);