package com.airbus.ims.workflow.runner
// import
import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.link.IssueLinkManager
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.history.ChangeItemBean
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import java.sql.Timestamp
import org.joda.time.LocalDate


/*String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getComponent(IssueLinkManager);
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();

MutableIssue issue = issue;

//Constantes
//Field of PWO-0
long ID_CF_IA_RequestedDate=10144;
long ID_CF_IA_Validation_RequestedDate=10154;
long ID_CF_IA_1stCommittedDate =10145;
long ID_CF_IA_LastCommittedDate = 10146;
long ID_CF_IA_Validation_1stCommittedDate=10155;
long ID_CF_IA_Validation_LastCommittedDate=10156;

//CustomField
CustomField IA_RequestdDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_RequestedDate)
CustomField IA_Validation_RequestdDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_RequestedDate)
CustomField IA_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_1stCommittedDate)
CustomField IA_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_LastCommittedDate)
CustomField IA_Validation_1stCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_1stCommittedDate)
CustomField IA_Validation_LastCommittedDateField=customFieldManager.getCustomFieldObject(ID_CF_IA_Validation_LastCommittedDate)


//Variables
//def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate=new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
log.debug "Todays Date : "+TodaysDate
Date IA_RequestdDate
Date IA_Validation_RequestdDate
Date IA_1stCommittedDate
Date IA_LastCommittedDate
Date IA_Validation_1stCommittedDate
Date IA_Validation_LastCommittedDate


//Business

IA_RequestdDate=(Timestamp) (TodaysDate+14)
IA_1stCommittedDate=IA_RequestdDate
IA_LastCommittedDate=IA_1stCommittedDate
IA_Validation_RequestdDate=(Timestamp) (TodaysDate+21)
IA_Validation_1stCommittedDate=IA_Validation_RequestdDate
IA_Validation_LastCommittedDate=IA_Validation_1stCommittedDate

issue.setCustomFieldValue(IA_RequestdDateField,IA_RequestdDate)
issue.setCustomFieldValue(IA_Validation_RequestdDateField,IA_Validation_RequestdDate)
issue.setCustomFieldValue(IA_1stCommittedDateField,IA_1stCommittedDate)
issue.setCustomFieldValue(IA_LastCommittedDateField,IA_LastCommittedDate)
issue.setCustomFieldValue(IA_Validation_1stCommittedDateField,IA_Validation_1stCommittedDate)
issue.setCustomFieldValue(IA_Validation_LastCommittedDateField,IA_Validation_LastCommittedDate)

issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);