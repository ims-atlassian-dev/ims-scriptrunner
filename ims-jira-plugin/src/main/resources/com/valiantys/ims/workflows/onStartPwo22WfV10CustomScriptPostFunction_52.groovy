package com.airbus.ims.workflow.runner
import com.atlassian.jira.component.ComponentAccessor

// import

import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.changehistory.ChangeHistory
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import org.joda.time.LocalDate

import java.sql.Timestamp

//String path = "/atlassian/jira/data/scripts/util/Functions.groovy";

/*def script = new GroovyScriptEngine( '.',ScriptRunnerImpl.scriptRunner.groovyScriptEngine.groovyClassLoader).with {
    loadScriptByName(path);
} */

boolean workloadIsUpdated(org.ofbiz.core.entity.GenericValue changeLog, List<String> itemsChanged) {
    boolean isUpdated = false;
    if (changeLog != null) {
        ChangeHistory change = ComponentAccessor.changeHistoryManager.getChangeHistoryById(changeLog.getLong("id"));
        List<ChangeItemBean> changeItemBeans = change.getChangeItemBeans();
        for (changeIteamBean in changeItemBeans) {
            if (itemsChanged.contains(changeIteamBean.getField())) {
                isUpdated = true;
            }
        }
    }

    return isUpdated;
}

//Manager
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
IssueManager issueManager = ComponentAccessor.getIssueManager();
CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
def optionsManager = ComponentAccessor.getOptionsManager()
def changeHolder = new DefaultIssueChangeHolder();


//Constante
//Issue type
String ID_IT_PWO0 = "10004";
String ID_IT_PWO1 = "10005";
String ID_IT_PWO2 = "10006";
String ID_IT_PWO21 = "10007";
String ID_IT_PWO22 = "10008";
String ID_IT_PWO23 = "10009";
String ID_IT_Cluster = "10001";
String ID_IT_Trigger = "10000";

List<String> PWOIT = new ArrayList<String>();
PWOIT.add(ID_IT_PWO0)
PWOIT.add(ID_IT_PWO1)
PWOIT.add(ID_IT_PWO2)
PWOIT.add(ID_IT_PWO21)
PWOIT.add(ID_IT_PWO22)
PWOIT.add(ID_IT_PWO23)

//Fields
long ID_CF_Form_RequestedDate = 10283;
long ID_CF_Form_Verification_RequestedDate = 10292;
long ID_CF_Form_Validation_RequestedDate = 10299;
long ID_CF_Form_1stCommittedDate = 10284;
long ID_CF_Form_Verification_1stCommittedDate = 10293;
long ID_CF_Form_Validation_1stCommittedDate = 10300;
long ID_CF_Form_LastCommittedDate = 10285;
long ID_CF_Form_Verification_LastCommittedDate = 10294;
long ID_CF_Form_Validation_LastCommittedDate = 10301;

//CustomField
CustomField Form_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_RequestedDate)
CustomField Form_Verification_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_RequestedDate)
CustomField Form_Validation_RequestedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_RequestedDate)
CustomField Form_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_1stCommittedDate)
CustomField Form_Verification_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_1stCommittedDate)
CustomField Form_Validation_1stCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_1stCommittedDate)
CustomField Form_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_LastCommittedDate)
CustomField Form_Verification_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Verification_LastCommittedDate)
CustomField Form_Validation_LastCommittedDateField = customFieldManager.getCustomFieldObject(ID_CF_Form_Validation_LastCommittedDate)


//Variables
//def TodaysDate = new Timestamp(new Date().getTime())
Date TodayDate=new LocalDate(new Date()).toDate()
Timestamp TodaysDate = new Timestamp(TodayDate.getTime())
Date Form_RequestedDate
Date Form_Verification_RequestedDate
Date Form_Validation_RequestedDate
Date Form_1stCommittedDate
Date Form_Verification_1stCommittedDate
Date Form_Validation_1stCommittedDate
Date Form_LastCommittedDate
Date Form_Verification_LastCommittedDate
Date Form_Validation_LastCommittedDate


//Business
//Calculate date on PWO-2.1
Form_RequestedDate = (Timestamp) (TodaysDate + 7)
Form_Verification_RequestedDate = (Timestamp) (TodaysDate + 14)
Form_Validation_RequestedDate = (Timestamp) (TodaysDate + 21)

Form_1stCommittedDate = Form_RequestedDate
Form_Verification_1stCommittedDate = Form_Verification_RequestedDate
Form_Validation_1stCommittedDate = Form_Validation_RequestedDate

Form_LastCommittedDate = Form_1stCommittedDate
Form_Verification_LastCommittedDate = Form_Verification_1stCommittedDate
Form_Validation_LastCommittedDate = Form_Validation_1stCommittedDate

issue.setCustomFieldValue(Form_RequestedDateField, Form_RequestedDate)
issue.setCustomFieldValue(Form_Verification_RequestedDateField, Form_Verification_RequestedDate)
issue.setCustomFieldValue(Form_Validation_RequestedDateField, Form_Validation_RequestedDate)

issue.setCustomFieldValue(Form_1stCommittedDateField, Form_1stCommittedDate)
issue.setCustomFieldValue(Form_Verification_1stCommittedDateField, Form_Verification_1stCommittedDate)
issue.setCustomFieldValue(Form_Validation_1stCommittedDateField, Form_Validation_1stCommittedDate)

issue.setCustomFieldValue(Form_LastCommittedDateField, Form_LastCommittedDate)
issue.setCustomFieldValue(Form_Verification_LastCommittedDateField, Form_Verification_LastCommittedDate)
issue.setCustomFieldValue(Form_Validation_LastCommittedDateField, Form_Validation_LastCommittedDate)

//issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
//issueIndexingService.reIndex(issue);

