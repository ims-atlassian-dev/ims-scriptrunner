package com.airbus.ims.workflow.runner
import org.apache.log4j.Category
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser

// Please give the server name where is the script to execute
String server = "msahd01"
// Please give the path where is the script to execute
String network = "gdat632"
String folder = "IMS"
String subfolder = "IMS_Script"

log.setLevel(Level.DEBUG)
log.debug "Call a remote script from Start transition for "+issue.key

// Managers
IssueManager issueManager = ComponentAccessor.getIssueManager();
IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService);
JiraAuthenticationContext  authenticationContext= ComponentAccessor.getJiraAuthenticationContext();
ApplicationUser user = authenticationContext.getLoggedInUser();
MutableIssue issue = (MutableIssue)issue;

//Customfield
CustomFieldManager cfManager = ComponentAccessor.getCustomFieldManager()
Long ID_CF_LinkToData = 10038
CustomField LinkToData = cfManager.getCustomFieldObject(ID_CF_LinkToData)

// Windows
//String script = "\\\\"+server+"\\"+network+"\\"+folder+"\\"+subfolder+"\\IMS_Script.bat "+issue.getSummary()
String path="\\\\"+server+"\\"+network+"\\"+folder+"\\"+ issue.getSummary()
String script = "\\\\D:\\JIRA-HOME\\NAS\\"+"IMS_Script.bat "+ path
// Unix
//String script = "sh /"+server+"/"+network+"/script-IMS-Test-TLFWP1-81.sh"
log.debug "script : "+script
def proc = script.execute()

log.debug("Executed")
proc.waitForOrKill(5000)
if (proc.exitValue()) {
log.debug("Error")
log.debug(proc.err.text)
} else {
log.debug("Applied")
log.debug(proc.text)
}

issue.setCustomFieldValue(LinkToData,path)
issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
issueIndexingService.reIndex(issue);
log.debug("Field Link to data updated")